using System;

namespace SucfaceMount
{
	public static class Const
	{
		public const string Version = "V2.12";

		public const int FAxis = 0;

		public const int ZAxis = 1;

		public const int XAxis = 2;

		public const int A1Axis = 3;

		public const int YAxis = 4;

		public const int A2Axis = 5;

		public const double PrickOffsetYForWidth8 = 3.5;

		public const double PrickOffsetYForWidth12 = 5.5;

		public const double PrickOffsetYForWidth16 = 7.5;

		public const double PrickOffsetYForWidth24 = 11.0;

		public const int Exposure = 6;

		public const double PCBThickness = 1.6;

		public const double DefCompHeight = 0.5;

		public const double ZAxisPrickDownPos = 13.5;

		public const double ZAxisNozzleDownPos = -13.0;

		public static readonly int[] StackWidth = new int[]
		{
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			12,
			12,
			12,
			12,
			16,
			16,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			8,
			12,
			12,
			12,
			12,
			16,
			16,
			8,
			8,
			8,
			8,
			8,
			8
		};

		public static readonly double[] StackX = new double[60];

		public static readonly double[] StackY = new double[60];

		public static readonly int[] StackFeed = new int[]
		{
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			8,
			8,
			8,
			8,
			12,
			12,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			4,
			8,
			8,
			8,
			8,
			12,
			12,
			4,
			4,
			4,
			4,
			4,
			4
		};

		public static readonly double[] IcStackStartX = new double[30];

		public static readonly double[] IcStackStartY = new double[30];

		public static readonly double[] IcStackEndX = new double[30];

		public static readonly double[] IcStackEndY = new double[30];

		public static readonly int[] IcStackArrayX = new int[30];

		public static readonly int[] IcStackArrayY = new int[30];
	}
}
