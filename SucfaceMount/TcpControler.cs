using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Resources;
using System.Threading;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class TcpControler
	{
		public enum PosShowType
		{
			NozzlePosShow,
			VisionPosShow
		}

		[Flags]
		public enum AxisType : short
		{
			Null = 0,
			FMark = 1,
			ZMark = 2,
			XMark = 4,
			A1Mark = 8,
			YMark = 16,
			A2Mark = 32,
			AxisAll = 63
		}

		public enum AxisStateType : short
		{
			AX_READY,
			AX_PTP_STARTING,
			AX_PTP_MOTION,
			AX_PTP_HALTING,
			AX_PTP_STOPPING
		}

		public const int FAxis = 0;

		public const int ZAxis = 1;

		public const int XAxis = 2;

		public const int A1Axis = 3;

		public const int YAxis = 4;

		public const int A2Axis = 5;

		public const uint DO_Laser_BitOffset = 1u;

		public const uint DO_Vacuum1_BitOffset = 4u;

		public const uint DO_Vacuum2_BitOffset = 8192u;

		public const uint DO_Blowing1_BitOffset = 2u;

		public const uint DO_Blowing2_BitOffset = 4096u;

		public const uint DO_Pump_BitOffset = 1032u;

		public const uint DO_LED_Connect_BitOffset = 16u;

		public const uint DO_LED_Working_BitOffset = 32u;

		public const uint DO_LED_Suspend_BitOffset = 64u;

		public const uint DO_LED_Speed1_BitOffset = 128u;

		public const uint DO_LED_Speed2_BitOffset = 256u;

		public const uint DO_LED_Vision_BitOffset = 512u;

		public const uint DO_Buzzer_BitOffset = 2048u;

		public const uint DO_Prick_BitOffset = 16384u;

		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		private TcpClient tcpClient;

		private NetworkStream tcpStream;

		public uint IpAdd;

		public uint Port;

		private int[,] AccDictionary = new int[,]
		{
			{
				3,
				3,
				3,
				3,
				3,
				3,
				4,
				4,
				4,
				4,
				4,
				4
			},
			{
				3,
				3,
				3,
				3,
				3,
				3,
				4,
				6,
				8,
				10,
				12,
				15
			},
			{
				3,
				4,
				5,
				3,
				3,
				3,
				4,
				5,
				6,
				7,
				7,
				10
			},
			{
				3,
				3,
				3,
				3,
				4,
				4,
				5,
				5,
				6,
				6,
				7,
				7
			},
			{
				3,
				4,
				5,
				3,
				3,
				3,
				3,
				4,
				5,
				6,
				6,
				7
			},
			{
				3,
				3,
				3,
				3,
				4,
				4,
				5,
				5,
				6,
				6,
				7,
				7
			}
		};

		private int[,] DecDictionary = new int[,]
		{
			{
				3,
				3,
				3,
				3,
				3,
				3,
				4,
				4,
				4,
				4,
				4,
				4
			},
			{
				3,
				3,
				3,
				3,
				3,
				3,
				4,
				6,
				8,
				10,
				12,
				15
			},
			{
				3,
				4,
				5,
				3,
				3,
				3,
				4,
				5,
				6,
				7,
				7,
				10
			},
			{
				3,
				3,
				3,
				3,
				4,
				4,
				5,
				5,
				6,
				6,
				7,
				7
			},
			{
				3,
				4,
				5,
				3,
				3,
				3,
				3,
				4,
				5,
				6,
				6,
				7
			},
			{
				3,
				3,
				3,
				3,
				4,
				4,
				5,
				5,
				6,
				6,
				7,
				7
			}
		};

		private int[,] VelDictionary = new int[,]
		{
			{
				1000,
				1500,
				2000,
				3000,
				4000,
				4500,
				5000,
				5000,
				5500,
				6000,
				8000,
				8000
			},
			{
				1,
				500,
				1000,
				2000,
				3000,
				4000,
				5000,
				7000,
				9000,
				11000,
				17000,
				17000
			},
			{
				1,
				500,
				1500,
				2500,
				5000,
				9000,
				12000,
				15000,
				17000,
				18000,
				20000,
				20000
			},
			{
				500,
				1000,
				2000,
				3000,
				4000,
				5000,
				7000,
				9000,
				11000,
				14000,
				20000,
				20000
			},
			{
				1,
				500,
				1500,
				2500,
				5000,
				9000,
				12000,
				15000,
				17000,
				18000,
				20000,
				20000
			},
			{
				500,
				1000,
				2000,
				3000,
				4000,
				5000,
				7000,
				9000,
				11000,
				14000,
				20000,
				20000
			}
		};

		public int[] PosPulse = new int[6];

		public double[] PosMM = new double[6];

		public TcpControler.AxisStateType[] Status = new TcpControler.AxisStateType[6];

		public double[] EFAC = new double[]
		{
			1.0,
			1.0,
			32.8084,
			4.44444444444,
			32.8084,
			4.44444444444
		};

		public TcpControler.PosShowType PosShow;

		public int speedValue = 1;

		public int speedValueGrade = 1;

		private uint DO_ReadBack;

		public int FreeBufNum;

		public bool XLMTP;

		public bool XLMTN;

		public bool YLMTP;

		public bool YLMTN;

		public bool DI_Pressure;

		public bool DI_PrickHomeForA;

		public bool DI_NozzleHomeForA;

		public bool DI_NozzleStack1;

		public bool DI_NozzleStack2;

		public bool DI_NozzleStack3;

		public bool DI_NozzleStack4;

		public bool DI_Button_Y_Down;

		public bool DI_Button_X_Up;

		public bool DI_Button_Step;

		public bool DI_Button_Pause;

		public bool DI_Button_Start;

		public bool DI_Button_X_Down;

		public bool DI_Button_Speed_Grade;

		public bool DI_Button_Y_Up;

		public bool DI_PrickHome;

		public bool DI_Pressure2;

		public Form owner;

		public List<byte[]> SendBuffers = new List<byte[]>();

		private static bool IsConnectionSuccessful = false;

		private static ManualResetEvent TimeoutObject = new ManualResetEvent(false);

		public ControlBufType ControlState;

		public int SpeedValue
		{
			get
			{
				return this.speedValue;
			}
			set
			{
				this.speedValue = value;
			}
		}

		public int ManualSpeedGrade
		{
			get
			{
				return this.speedValueGrade;
			}
			set
			{
				this.speedValueGrade = value;
				switch (this.speedValueGrade)
				{
				case 0:
					this.SetDO_OFF_IMM(128u);
					this.SetDO_ON_IMM(256u);
					return;
				case 1:
					break;
				case 2:
					this.SetDO_ON_IMM(128u);
					this.SetDO_OFF_IMM(256u);
					break;
				default:
					return;
				}
			}
		}

		public bool DO_Laser
		{
			get
			{
				return (this.DO_ReadBack & 1u) != 0u;
			}
		}

		public bool DO_Vacuum1
		{
			get
			{
				return (this.DO_ReadBack & 4u) != 0u;
			}
		}

		public bool DO_Vacuum2
		{
			get
			{
				return (this.DO_ReadBack & 8192u) != 0u;
			}
		}

		public bool DO_Blowing1
		{
			get
			{
				return (this.DO_ReadBack & 2u) != 0u;
			}
		}

		public bool DO_Blowing2
		{
			get
			{
				return (this.DO_ReadBack & 4096u) != 0u;
			}
		}

		public bool DO_Pump
		{
			get
			{
				return (this.DO_ReadBack & 1032u) != 0u;
			}
		}

		public bool DO_LED_Connect
		{
			get
			{
				return (this.DO_ReadBack & 16u) != 0u;
			}
		}

		public bool DO_LED_Working
		{
			get
			{
				return (this.DO_ReadBack & 32u) != 0u;
			}
		}

		public bool DO_LED_Suspend
		{
			get
			{
				return (this.DO_ReadBack & 64u) != 0u;
			}
		}

		public bool DO_LED_Speed1
		{
			get
			{
				return (this.DO_ReadBack & 128u) != 0u;
			}
		}

		public bool DO_LED_Speed2
		{
			get
			{
				return (this.DO_ReadBack & 256u) != 0u;
			}
		}

		public bool DO_LED_Vision
		{
			get
			{
				return (this.DO_ReadBack & 512u) != 0u;
			}
		}

		public bool DO_Buzzer
		{
			get
			{
				return (this.DO_ReadBack & 2048u) != 0u;
			}
		}

		public bool DO_Prick
		{
			get
			{
				return (this.DO_ReadBack & 16384u) != 0u;
			}
		}

		public bool IsConnected
		{
			get
			{
				return this.tcpClient != null && this.tcpClient.Connected;
			}
		}

		public bool IsHomed
		{
			get
			{
				int num;
				return this.ReadParam(500u, out num) && num != 0;
			}
			set
			{
				if (!this.WriteParam(500u, value ? 1 : 0))
				{
					this.WriteParam(500u, value ? 1 : 0);
				}
			}
		}

		public void SetDoVacummQueue(int axisNum, bool value)
		{
			if (axisNum != 3)
			{
				if (axisNum == 5)
				{
					if (value)
					{
						this.SetDO_ON_QUEUE(8192u);
						return;
					}
					this.SetDO_OFF_QUEUE(8192u);
				}
				return;
			}
			if (value)
			{
				this.SetDO_ON_QUEUE(4u);
				return;
			}
			this.SetDO_OFF_QUEUE(4u);
		}

		public void SetDoBlowingQueue(int axisNum, bool value)
		{
			if (axisNum != 3)
			{
				if (axisNum == 5)
				{
					if (value)
					{
						this.SetDO_ON_QUEUE(4096u);
						return;
					}
					this.SetDO_OFF_QUEUE(4096u);
				}
				return;
			}
			if (value)
			{
				this.SetDO_ON_QUEUE(2u);
				return;
			}
			this.SetDO_OFF_QUEUE(2u);
		}

		public bool GetDiPressure(int axisNum)
		{
			if (axisNum == 3)
			{
				return this.DI_Pressure;
			}
			return axisNum == 5 && this.DI_Pressure2;
		}

		private bool ConnectTo(uint ipAdd, uint port, int timeoutMSec)
		{
			if (this.tcpClient != null && this.tcpClient.Connected)
			{
				return true;
			}
			this.tcpClient = new TcpClient();
			this.tcpClient.SendTimeout = 100;
			this.tcpClient.ReceiveTimeout = 100;
			this.tcpClient.NoDelay = true;
			TcpControler.TimeoutObject.Reset();
			this.tcpClient.BeginConnect(new IPAddress((long)((ulong)ipAdd)), (int)port, new AsyncCallback(TcpControler.CallBackMethod), this.tcpClient);
			if (!TcpControler.TimeoutObject.WaitOne(timeoutMSec, false) || !TcpControler.IsConnectionSuccessful)
			{
				this.tcpClient.Close();
				this.tcpClient = null;
				return false;
			}
			this.tcpStream = this.tcpClient.GetStream();
			if (!this.tcpStream.CanWrite || !this.tcpStream.CanRead)
			{
				this.tcpStream.Close();
				this.tcpClient.Close();
				this.tcpClient = null;
				return false;
			}
			return true;
		}

		private static void CallBackMethod(IAsyncResult asyncresult)
		{
			try
			{
				TcpControler.IsConnectionSuccessful = false;
				TcpClient tcpClient = (TcpClient)asyncresult.AsyncState;
				if (tcpClient.Client != null)
				{
					tcpClient.EndConnect(asyncresult);
					TcpControler.IsConnectionSuccessful = true;
				}
			}
			catch (Exception)
			{
				TcpControler.IsConnectionSuccessful = false;
			}
			finally
			{
				TcpControler.TimeoutObject.Set();
			}
		}

		public bool ConnectTo()
		{
			return this.ConnectTo(this.IpAdd, this.Port, 100);
		}

		public bool CheckConnect()
		{
			if (!this.IsConnected)
			{
				if (this.ConnectTo(this.IpAdd, this.Port, 100))
				{
					return true;
				}
				new MyMessageBox(this, this.LocStrings.GetString("labelConnectionFailed"), this.owner.Location.X + this.owner.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2).ShowDialog();
				IpConfig ipConfig = new IpConfig(this.IpAdd, this.Port);
				ipConfig.Location = new Point(this.owner.Location.X + this.owner.Size.Width / 2 - ipConfig.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2 - ipConfig.Size.Height / 2);
				while (ipConfig.ShowDialog() == DialogResult.OK)
				{
					this.IpAdd = ipConfig.IpAdd;
					this.Port = ipConfig.Port;
					if (!this.ConnectTo(this.IpAdd, this.Port, 100))
					{
						new MyMessageBox(this, this.LocStrings.GetString("labelConnectionFailed"), this.owner.Location.X + this.owner.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2).ShowDialog();
					}
					if (this.IsConnected)
					{
						return true;
					}
				}
				return false;
			}
			return true;
		}

		public void CloseConnect()
		{
			if (this.tcpStream != null)
			{
				this.tcpStream.Close();
			}
			if (this.tcpClient != null)
			{
				this.tcpClient.Close();
				this.tcpClient = null;
			}
		}

		private bool Send(byte[] SendBuffer)
		{
			if (!this.CheckConnect())
			{
				return false;
			}
			try
			{
				if (this.tcpStream.DataAvailable)
				{
					this.tcpStream.Read(new byte[this.tcpClient.Available], 0, this.tcpClient.Available);
				}
				this.tcpStream.Write(SendBuffer, 0, SendBuffer.Length);
			}
			catch (Exception)
			{
				return false;
			}
			return true;
		}

		private bool Receive(byte[] ReceiveBuffer)
		{
			return this.tcpStream.DataAvailable && this.tcpStream.Read(ReceiveBuffer, 0, ReceiveBuffer.Length) != 0;
		}

		private bool SendAndReceive(byte[] SendBuffer, out byte[] ReceiveBuffer)
		{
			int i = 0;
			while (i < 5)
			{
				if (!this.Send(SendBuffer))
				{
					ReceiveBuffer = null;
					return false;
				}
				Thread.Sleep(2);
				ReceiveBuffer = new byte[this.tcpClient.Available];
				if (this.Receive(ReceiveBuffer))
				{
					goto IL_56;
				}
				Thread.Sleep(2);
				ReceiveBuffer = new byte[this.tcpClient.Available];
				if (this.Receive(ReceiveBuffer))
				{
					goto IL_56;
				}
				IL_61:
				i++;
				continue;
				IL_56:
				if (ReceiveBuffer[0] == SendBuffer[0])
				{
					return true;
				}
				goto IL_61;
			}
			ReceiveBuffer = null;
			return false;
		}

		private double ZAxisHeightToPulse(double height)
		{
			double num = 17.0;
			double num2 = Math.Asin(height / num);
			return num2 * 1600.0 / 6.2831853071795862;
		}

		private double ZAxisPulseToHeight(double pulse)
		{
			double num = 17.0;
			double a = pulse * 2.0 * 3.1415926535897931 / 1600.0;
			return Math.Sin(a) * num;
		}

		public void SPulse(int axisNum, int gPulse)
		{
			int[] array = new int[6];
			array[axisNum] = gPulse;
			TcpControler.AxisType axisType = (TcpControler.AxisType)(1 << axisNum);
			byte[] array2 = new byte[28];
			array2[0] = 8;
			array2[1] = 0;
			array2[2] = (byte)axisType;
			array2[3] = 0;
			int num = 4;
			for (int i = 0; i < 6; i++)
			{
				int num2 = array[i] * 1000;
				array2[num++] = (byte)num2;
				array2[num++] = (byte)(num2 >> 8);
				array2[num++] = (byte)(num2 >> 16);
				array2[num++] = (byte)(num2 >> 24);
			}
			byte[] array3;
			if (!this.SendAndReceive(array2, out array3))
			{
				new MyMessageBox(this, this.LocStrings.GetString("sendCommandFailed"), this.owner.Location.X + this.owner.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2).Show();
			}
		}

		public bool IsBufRunDone(int value)
		{
			return this.FreeBufNum >= 7 - value;
		}

		public bool IsBufRunDone()
		{
			return this.IsBufRunDone(0);
		}

		public bool InPos(int Axis, double pos)
		{
			return (int)Math.Round(pos * this.EFAC[Axis]) == this.PosPulse[Axis];
		}

		public CmdResultType MPTP_QUEUE(TcpControler.AxisType axisMark, double[] goal, CmdType cmdType)
		{
			int[] array = new int[6];
			if ((short)(axisMark & TcpControler.AxisType.FMark) == 1)
			{
				array[0] = (int)Math.Round(goal[0] * this.EFAC[0]);
			}
			if ((short)(axisMark & TcpControler.AxisType.A1Mark) == 8)
			{
				array[3] = (int)Math.Round(goal[3] * this.EFAC[3]);
			}
			if ((short)(axisMark & TcpControler.AxisType.A2Mark) == 32)
			{
				array[5] = (int)Math.Round(goal[5] * this.EFAC[5]);
			}
			if ((short)(axisMark & TcpControler.AxisType.ZMark) == 2)
			{
				array[1] = (int)Math.Round(this.ZAxisHeightToPulse(goal[1]));
			}
			if ((short)(axisMark & TcpControler.AxisType.XMark) == 4)
			{
				array[2] = (int)Math.Round(goal[2] * this.EFAC[2]);
			}
			if ((short)(axisMark & TcpControler.AxisType.YMark) == 16)
			{
				array[4] = (int)Math.Round(goal[4] * this.EFAC[4]);
			}
			if (axisMark == TcpControler.AxisType.Null)
			{
				return CmdResultType.Done;
			}
			byte[] array2 = new byte[28];
			array2[0] = (byte)cmdType;
			array2[1] = 0;
			array2[2] = (byte)axisMark;
			array2[3] = 0;
			int num = 4;
			for (int i = 0; i < 6; i++)
			{
				int num2 = array[i] * 1000;
				array2[num++] = (byte)num2;
				array2[num++] = (byte)(num2 >> 8);
				array2[num++] = (byte)(num2 >> 16);
				array2[num++] = (byte)(num2 >> 24);
			}
			this.SendBuffers.Add(array2);
			return CmdResultType.Done;
		}

		public CmdResultType MPTP_IMM(TcpControler.AxisType axisMark, double[] goal)
		{
			int[] array = new int[6];
			if ((short)(axisMark & TcpControler.AxisType.FMark) == 1)
			{
				array[0] = (int)Math.Round(goal[0] * this.EFAC[0]);
				if (array[0] == this.PosPulse[0])
				{
					axisMark &= ~TcpControler.AxisType.FMark;
				}
			}
			if ((short)(axisMark & TcpControler.AxisType.A1Mark) == 8)
			{
				array[3] = (int)Math.Round(goal[3] * this.EFAC[3]);
				if (array[3] == this.PosPulse[3])
				{
					axisMark &= ~TcpControler.AxisType.A1Mark;
				}
			}
			if ((short)(axisMark & TcpControler.AxisType.A2Mark) == 32)
			{
				array[5] = (int)Math.Round(goal[5] * this.EFAC[5]);
				if (array[5] == this.PosPulse[5])
				{
					axisMark &= ~TcpControler.AxisType.A2Mark;
				}
			}
			if ((short)(axisMark & TcpControler.AxisType.ZMark) == 2)
			{
				array[1] = (int)Math.Round(this.ZAxisHeightToPulse(goal[1]));
				if (array[1] == this.PosPulse[1])
				{
					axisMark &= ~TcpControler.AxisType.ZMark;
				}
			}
			if ((short)(axisMark & TcpControler.AxisType.XMark) == 4)
			{
				array[2] = (int)Math.Round(goal[2] * this.EFAC[2]);
				if (array[2] == this.PosPulse[2])
				{
					axisMark &= ~TcpControler.AxisType.XMark;
				}
			}
			if ((short)(axisMark & TcpControler.AxisType.YMark) == 16)
			{
				array[4] = (int)Math.Round(goal[4] * this.EFAC[4]);
				if (array[4] == this.PosPulse[4])
				{
					axisMark &= ~TcpControler.AxisType.YMark;
				}
			}
			if (axisMark == TcpControler.AxisType.Null)
			{
				return CmdResultType.Done;
			}
			byte[] array2 = new byte[28];
			array2[0] = 8;
			array2[1] = 0;
			array2[2] = (byte)axisMark;
			array2[3] = 0;
			int num = 4;
			for (int i = 0; i < 6; i++)
			{
				int num2 = array[i] * 1000;
				array2[num++] = (byte)num2;
				array2[num++] = (byte)(num2 >> 8);
				array2[num++] = (byte)(num2 >> 16);
				array2[num++] = (byte)(num2 >> 24);
			}
			byte[] array3;
			if (!this.SendAndReceive(array2, out array3))
			{
				new MyMessageBox(this, this.LocStrings.GetString("sendCommandFailed"), this.owner.Location.X + this.owner.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2).Show();
				return CmdResultType.Error;
			}
			return CmdResultType.Doing;
		}

		public CmdResultType SPTP(int axisNum, double goal, CmdType cmdType)
		{
			double[] array = new double[6];
			array[axisNum] = goal;
			if (cmdType == CmdType.CMD_PTP_IMM)
			{
				return this.MPTP_IMM((TcpControler.AxisType)(1 << axisNum), array);
			}
			return this.MPTP_QUEUE((TcpControler.AxisType)(1 << axisNum), array, cmdType);
		}

		public CmdResultType DPTP(int axisNum1, int axisNum2, double goal1, double goal2, CmdType cmdType)
		{
			double[] array = new double[6];
			array[axisNum1] = goal1;
			array[axisNum2] = goal2;
			if (cmdType == CmdType.CMD_PTP_IMM)
			{
				return this.MPTP_IMM((TcpControler.AxisType)(1 << axisNum1 | 1 << axisNum2), array);
			}
			return this.MPTP_QUEUE((TcpControler.AxisType)(1 << axisNum1 | 1 << axisNum2), array, cmdType);
		}

		public CmdResultType TPTP(int axisNum1, int axisNum2, int axisNum3, double goal1, double goal2, double goal3, CmdType cmdType)
		{
			double[] array = new double[6];
			array[axisNum1] = goal1;
			array[axisNum2] = goal2;
			array[axisNum3] = goal3;
			if (cmdType == CmdType.CMD_PTP_IMM)
			{
				return this.MPTP_IMM((TcpControler.AxisType)(1 << axisNum1 | 1 << axisNum2 | 1 << axisNum3), array);
			}
			return this.MPTP_QUEUE((TcpControler.AxisType)(1 << axisNum1 | 1 << axisNum2 | 1 << axisNum3), array, cmdType);
		}

		public CmdResultType FPTP(int axisNum1, int axisNum2, int axisNum3, int axisNum4, double goal1, double goal2, double goal3, double goal4, CmdType cmdType)
		{
			double[] array = new double[6];
			array[axisNum1] = goal1;
			array[axisNum2] = goal2;
			array[axisNum3] = goal3;
			array[axisNum4] = goal4;
			if (cmdType == CmdType.CMD_PTP_IMM)
			{
				return this.MPTP_IMM((TcpControler.AxisType)(1 << axisNum1 | 1 << axisNum2 | 1 << axisNum3 | 1 << axisNum4), array);
			}
			return this.MPTP_QUEUE((TcpControler.AxisType)(1 << axisNum1 | 1 << axisNum2 | 1 << axisNum3 | 1 << axisNum4), array, cmdType);
		}

		public CmdResultType MHalt(TcpControler.AxisType axisMark)
		{
			byte[] array;
			if (!this.SendAndReceive(new byte[]
			{
				9,
				0,
				(byte)axisMark,
				0
			}, out array))
			{
				new MyMessageBox(this, this.LocStrings.GetString("sendCommandFailed"), this.owner.Location.X + this.owner.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2).Show();
				return CmdResultType.Error;
			}
			return CmdResultType.Done;
		}

		public CmdResultType SHalt(int axisNum)
		{
			return this.MHalt((TcpControler.AxisType)(1 << axisNum));
		}

		public CmdResultType Update()
		{
			byte[] array = new byte[4];
			array[0] = 0;
			array[1] = 0;
			byte[] array2;
			if (!this.SendAndReceive(array, out array2))
			{
				new MyMessageBox(this, this.LocStrings.GetString("sendCommandFailed"), this.owner.Location.X + this.owner.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2).Show();
				return CmdResultType.Error;
			}
			this.DO_ReadBack = (uint)((int)array2[4] + ((int)array2[5] << 8));
			if ((array2[2] & 128) == 0)
			{
				this.FreeBufNum = (int)(7 - array2[2]);
			}
			else
			{
				this.FreeBufNum = (int)((byte)(-1 - (int)array2[2]));
			}
			this.XLMTP = ((array2[8] & 1) == 1);
			this.XLMTN = ((array2[8] & 2) == 2);
			this.YLMTP = ((array2[8] & 4) == 4);
			this.YLMTN = ((array2[8] & 8) == 8);
			this.DI_Pressure = ((array2[8] & 16) != 16);
			this.DI_NozzleHomeForA = ((array2[8] & 32) != 32);
			this.DI_PrickHomeForA = ((array2[8] & 64) != 64);
			this.DI_NozzleStack1 = ((array2[8] & 128) != 128);
			this.DI_NozzleStack2 = ((array2[9] & 1) != 1);
			this.DI_NozzleStack3 = ((array2[9] & 2) != 2);
			this.DI_NozzleStack4 = ((array2[9] & 4) != 4);
			this.DI_Button_Y_Down = ((array2[9] & 8) != 8);
			this.DI_Button_X_Up = ((array2[9] & 16) != 16);
			this.DI_Button_Step = ((array2[9] & 32) != 32);
			this.DI_Button_Pause = ((array2[9] & 64) != 64);
			this.DI_Button_Start = ((array2[9] & 128) != 128);
			this.DI_Button_X_Down = ((array2[10] & 1) != 1);
			this.DI_Button_Speed_Grade = ((array2[10] & 2) != 2);
			this.DI_Button_Y_Up = ((array2[10] & 4) != 4);
			this.DI_PrickHome = ((array2[10] & 8) != 8);
			this.DI_Pressure2 = ((array2[10] & 16) != 16);
			for (int i = 0; i < 6; i++)
			{
				int num = (int)array2[(i << 2) + 12] + ((int)array2[(i << 2) + 13] << 8) + ((int)array2[(i << 2) + 14] << 16) + ((int)array2[(i << 2) + 15] << 24);
				this.PosPulse[i] = num / 1000;
				switch (i)
				{
				case 0:
				case 3:
				case 5:
					this.PosMM[i] = (double)this.PosPulse[i] / this.EFAC[i];
					break;
				case 1:
					this.PosMM[1] = this.ZAxisPulseToHeight((double)this.PosPulse[1]);
					break;
				case 2:
					this.PosMM[2] = (double)this.PosPulse[2] / this.EFAC[2];
					if (this.PosShow == TcpControler.PosShowType.VisionPosShow)
					{
						this.PosMM[2] += ((MainForm)this.owner).VisionOffsetX;
					}
					break;
				case 4:
					this.PosMM[4] = (double)this.PosPulse[4] / this.EFAC[4];
					if (this.PosShow == TcpControler.PosShowType.VisionPosShow)
					{
						this.PosMM[4] += ((MainForm)this.owner).VisionOffsetY;
					}
					break;
				}
			}
			for (int j = 0; j < 6; j++)
			{
				this.Status[j] = (TcpControler.AxisStateType)array2[j + 36];
			}
			return CmdResultType.Done;
		}

		public CmdResultType SetDO_IMM()
		{
			byte[] array;
			if (!this.SendAndReceive(new byte[]
			{
				4,
				0,
				0,
				0,
				(byte)((this.DO_Laser ? 1 : 0) + ((this.DO_Blowing1 ? 1 : 0) << 1) + ((this.DO_Vacuum1 ? 1 : 0) << 2) + ((this.DO_Pump ? 1 : 0) << 3) + ((this.DO_LED_Working ? 0 : 1) << 5) + ((this.DO_LED_Suspend ? 0 : 1) << 6) + ((this.DO_LED_Speed1 ? 0 : 1) << 7)),
				(byte)((this.DO_LED_Speed2 ? 0 : 1) + ((this.DO_LED_Vision ? 1 : 0) << 1) + ((this.DO_Pump ? 1 : 0) << 2) + ((this.DO_Buzzer ? 1 : 0) << 3) + ((this.DO_Blowing2 ? 1 : 0) << 4) + ((this.DO_Vacuum2 ? 1 : 0) << 5) + ((this.DO_Prick ? 1 : 0) << 6)),
				0,
				0
			}, out array))
			{
				return CmdResultType.Error;
			}
			return CmdResultType.Done;
		}

		public CmdResultType SetDO_QUEUE()
		{
			byte[] item = new byte[]
			{
				14,
				0,
				0,
				0,
				(byte)((this.DO_Laser ? 1 : 0) + ((this.DO_Blowing1 ? 1 : 0) << 1) + ((this.DO_Vacuum1 ? 1 : 0) << 2) + ((this.DO_Pump ? 1 : 0) << 3) + ((this.DO_LED_Working ? 0 : 1) << 5) + ((this.DO_LED_Suspend ? 0 : 1) << 6) + ((this.DO_LED_Speed1 ? 0 : 1) << 7)),
				(byte)((this.DO_LED_Speed2 ? 0 : 1) + ((this.DO_LED_Vision ? 1 : 0) << 1) + ((this.DO_Pump ? 1 : 0) << 2) + ((this.DO_Buzzer ? 1 : 0) << 3) + ((this.DO_Blowing2 ? 1 : 0) << 4) + ((this.DO_Vacuum2 ? 1 : 0) << 5) + ((this.DO_Prick ? 1 : 0) << 6)),
				0,
				0
			};
			this.SendBuffers.Add(item);
			return CmdResultType.Done;
		}

		public CmdResultType SetDO_ON_IMM(uint OnBitOffset)
		{
			byte[] array;
			if (!this.SendAndReceive(new byte[]
			{
				20,
				0,
				0,
				0,
				(byte)OnBitOffset,
				(byte)(OnBitOffset >> 8),
				0,
				0
			}, out array))
			{
				return CmdResultType.Error;
			}
			return CmdResultType.Done;
		}

		public CmdResultType SetDO_OFF_IMM(uint OffBitOffset)
		{
			byte[] array;
			if (!this.SendAndReceive(new byte[]
			{
				21,
				0,
				0,
				0,
				(byte)OffBitOffset,
				(byte)(OffBitOffset >> 8),
				0,
				0
			}, out array))
			{
				return CmdResultType.Error;
			}
			return CmdResultType.Done;
		}

		public CmdResultType SetDO_ON_QUEUE(uint OnBitOffset)
		{
			byte[] item = new byte[]
			{
				22,
				0,
				0,
				0,
				(byte)OnBitOffset,
				(byte)(OnBitOffset >> 8),
				0,
				0
			};
			this.SendBuffers.Add(item);
			return CmdResultType.Done;
		}

		public CmdResultType SetDO_OFF_QUEUE(uint OffBitOffset)
		{
			byte[] item = new byte[]
			{
				23,
				0,
				0,
				0,
				(byte)OffBitOffset,
				(byte)(OffBitOffset >> 8),
				0,
				0
			};
			this.SendBuffers.Add(item);
			return CmdResultType.Done;
		}

		public CmdResultType WaitPrickHome_QUEUE(bool value)
		{
			byte[] array = new byte[12];
			array[0] = 19;
			array[1] = 0;
			array[2] = 0;
			array[3] = 0;
			array[4] = 0;
			array[5] = 0;
			array[6] = 8;
			array[7] = 0;
			if (value)
			{
				array[8] = 0;
				array[9] = 0;
				array[10] = 0;
				array[11] = 0;
			}
			else
			{
				array[8] = 0;
				array[9] = 0;
				array[10] = 8;
				array[11] = 0;
			}
			this.SendBuffers.Add(array);
			return CmdResultType.Done;
		}

		public bool ReadSpeed()
		{
			byte[] array = new byte[8];
			array[0] = 5;
			array[1] = 0;
			byte[] array2;
			return this.SendAndReceive(array, out array2);
		}

		public CmdResultType WriteSpeed_IMM(int value, int scale)
		{
			byte[] array = new byte[76];
			array[0] = 6;
			array[1] = 0;
			array[2] = (byte)value;
			int num = 4;
			for (int i = 0; i < 6; i++)
			{
				int num2 = this.AccDictionary[i, value];
				array[num++] = (byte)num2;
				array[num++] = (byte)(num2 >> 8);
				array[num++] = (byte)(num2 >> 16);
				array[num++] = (byte)(num2 >> 24);
			}
			for (int j = 0; j < 6; j++)
			{
				int num2 = this.DecDictionary[j, value];
				array[num++] = (byte)num2;
				array[num++] = (byte)(num2 >> 8);
				array[num++] = (byte)(num2 >> 16);
				array[num++] = (byte)(num2 >> 24);
			}
			for (int k = 0; k < 6; k++)
			{
				int num2 = this.VelDictionary[k, value] * scale / 100;
				array[num++] = (byte)num2;
				array[num++] = (byte)(num2 >> 8);
				array[num++] = (byte)(num2 >> 16);
				array[num++] = (byte)(num2 >> 24);
			}
			byte[] array2;
			if (!this.SendAndReceive(array, out array2))
			{
				return CmdResultType.Error;
			}
			return CmdResultType.Done;
		}

		public CmdResultType WriteSpeed_QUEUE(int value, int scale)
		{
			byte[] array = new byte[76];
			array[0] = 15;
			array[1] = 0;
			array[2] = (byte)value;
			int num = 4;
			for (int i = 0; i < 6; i++)
			{
				int num2 = this.AccDictionary[i, value];
				array[num++] = (byte)num2;
				array[num++] = (byte)(num2 >> 8);
				array[num++] = (byte)(num2 >> 16);
				array[num++] = (byte)(num2 >> 24);
			}
			for (int j = 0; j < 6; j++)
			{
				int num2 = this.DecDictionary[j, value];
				array[num++] = (byte)num2;
				array[num++] = (byte)(num2 >> 8);
				array[num++] = (byte)(num2 >> 16);
				array[num++] = (byte)(num2 >> 24);
			}
			for (int k = 0; k < 6; k++)
			{
				int num2 = this.VelDictionary[k, value] * scale / 100;
				array[num++] = (byte)num2;
				array[num++] = (byte)(num2 >> 8);
				array[num++] = (byte)(num2 >> 16);
				array[num++] = (byte)(num2 >> 24);
			}
			this.SendBuffers.Add(array);
			return CmdResultType.Done;
		}

		public bool ReadParam(uint address, out int date)
		{
			byte[] array = new byte[12];
			array[0] = 1;
			array[1] = 0;
			array[4] = (byte)address;
			array[5] = (byte)(address >> 8);
			array[6] = (byte)(address >> 16);
			array[7] = (byte)(address >> 24);
			byte[] array2;
			if (!this.SendAndReceive(array, out array2))
			{
				date = 0;
				return false;
			}
			date = (int)array2[8] + ((int)array2[9] << 8) + ((int)array2[10] << 16) + ((int)array2[11] << 24);
			return true;
		}

		public bool WriteParam(uint address, int date)
		{
			byte[] array;
			return this.SendAndReceive(new byte[]
			{
				2,
				0,
				0,
				0,
				(byte)address,
				(byte)(address >> 8),
				(byte)(address >> 16),
				(byte)(address >> 24),
				(byte)date,
				(byte)(date >> 8),
				(byte)(date >> 16),
				(byte)(date >> 24)
			}, out array);
		}

		public bool SaveParam()
		{
			byte[] array = new byte[4];
			array[0] = 3;
			array[1] = 0;
			byte[] array2;
			return this.SendAndReceive(array, out array2);
		}

		public bool WritePos(TcpControler.AxisType axisNum, double[] pos)
		{
			int[] array = new int[6];
			for (int i = 0; i < 6; i++)
			{
				array[i] = (int)Math.Round(pos[i] * this.EFAC[i]) * 1000;
			}
			byte[] array2 = new byte[28];
			array2[0] = 7;
			array2[1] = 0;
			array2[2] = (byte)axisNum;
			array2[3] = 0;
			int num = 4;
			for (int j = 0; j < 6; j++)
			{
				array2[num++] = (byte)array[j];
				array2[num++] = (byte)(array[j] >> 8);
				array2[num++] = (byte)(array[j] >> 16);
				array2[num++] = (byte)(array[j] >> 24);
			}
			byte[] array3;
			if (!this.SendAndReceive(array2, out array3))
			{
				new MyMessageBox(this, this.LocStrings.GetString("sendCommandFailed"), this.owner.Location.X + this.owner.Size.Width / 2, this.owner.Location.Y + this.owner.Size.Height / 2).Show();
				return false;
			}
			return true;
		}

		public bool WritePos_QUEUE(TcpControler.AxisType axisNum, double[] pos)
		{
			int[] array = new int[6];
			for (int i = 0; i < 6; i++)
			{
				array[i] = (int)Math.Round(pos[i] * this.EFAC[i]) * 1000;
			}
			byte[] array2 = new byte[28];
			array2[0] = 18;
			array2[1] = 0;
			array2[2] = (byte)axisNum;
			array2[3] = 0;
			int num = 4;
			for (int j = 0; j < 6; j++)
			{
				array2[num++] = (byte)array[j];
				array2[num++] = (byte)(array[j] >> 8);
				array2[num++] = (byte)(array[j] >> 16);
				array2[num++] = (byte)(array[j] >> 24);
			}
			this.SendBuffers.Add(array2);
			return true;
		}

		public CmdResultType SetDelayMs_QUEUE(ushort delayMs)
		{
			byte[] item = new byte[]
			{
				17,
				0,
				(byte)(delayMs & 255),
				(byte)(delayMs >> 8)
			};
			if (delayMs == 0)
			{
				delayMs = 1;
			}
			this.SendBuffers.Add(item);
			return CmdResultType.Done;
		}

		public CmdResultType ControlCmdBuf(ControlBufType cb)
		{
			byte[] array;
			if (!this.SendAndReceive(new byte[]
			{
				11,
				0,
				(byte)cb,
				0
			}, out array))
			{
				return CmdResultType.Error;
			}
			this.ControlState = cb;
			return CmdResultType.Done;
		}

		public CmdResultType SendQueueCmd()
		{
			while (this.FreeBufNum > 0 && this.SendBuffers.Count > 0)
			{
				this.FreeBufNum--;
				byte[] array;
				if (!this.SendAndReceive(this.SendBuffers[0], out array))
				{
					return CmdResultType.Error;
				}
				this.SendBuffers.RemoveAt(0);
			}
			return CmdResultType.Done;
		}
	}
}
