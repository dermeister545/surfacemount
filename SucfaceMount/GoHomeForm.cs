using System;
using System.ComponentModel;
using System.Resources;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class GoHomeForm : Form
	{
		private enum HomeStateType
		{
			None,
			Stage1,
			Stage2,
			Stage3,
			OK,
			Done
		}

		private IContainer components;

		private Button buttonCancel;

		private Button buttonStart;

		private Button buttonStop;

		private Label labelYLimitN;

		private Label labelYLimitP;

		private Label labelXLimitP;

		private Label labelXLimitN;

		private Label labelXState;

		private Label labelYState;

		private Label label1;

		private Label label2;

		private Label label3;

		private Timer timerUI;

		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		private TcpControler controler;

		private GoHomeForm.HomeStateType XState;

		private GoHomeForm.HomeStateType YState;

		private bool XLMTP;

		private bool XLMTN;

		private bool YLMTP;

		private bool YLMTN;

		private int SpeedBackUp = 3;

		public double HomePosX;

		public double HomePosY;

		private bool IsEnter;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager resources = new ComponentResourceManager(typeof(GoHomeForm));
			this.labelXState = new Label();
			this.labelYState = new Label();
			this.label1 = new Label();
			this.label2 = new Label();
			this.label3 = new Label();
			this.timerUI = new Timer(this.components);
			this.labelYLimitN = new Label();
			this.labelYLimitP = new Label();
			this.labelXLimitP = new Label();
			this.labelXLimitN = new Label();
			this.buttonStop = new Button();
			this.buttonCancel = new Button();
			this.buttonStart = new Button();
			base.SuspendLayout();
			resources.ApplyResources(this.labelXState, "labelXState");
			this.labelXState.Name = "labelXState";
			resources.ApplyResources(this.labelYState, "labelYState");
			this.labelYState.Name = "labelYState";
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			this.timerUI.Tick += new EventHandler(this.timerUI_Tick);
			resources.ApplyResources(this.labelYLimitN, "labelYLimitN");
			this.labelYLimitN.Name = "labelYLimitN";
			resources.ApplyResources(this.labelYLimitP, "labelYLimitP");
			this.labelYLimitP.Name = "labelYLimitP";
			resources.ApplyResources(this.labelXLimitP, "labelXLimitP");
			this.labelXLimitP.Name = "labelXLimitP";
			resources.ApplyResources(this.labelXLimitN, "labelXLimitN");
			this.labelXLimitN.Image = Resource.TransparentLED;
			this.labelXLimitN.Name = "labelXLimitN";
			resources.ApplyResources(this.buttonStop, "buttonStop");
			this.buttonStop.BackgroundImage = Resource.ButtonUp;
			this.buttonStop.Name = "buttonStop";
			this.buttonStop.UseVisualStyleBackColor = true;
			this.buttonStop.Click += new EventHandler(this.buttonStop_Click);
			resources.ApplyResources(this.buttonCancel, "buttonCancel");
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			resources.ApplyResources(this.buttonStart, "buttonStart");
			this.buttonStart.BackgroundImage = Resource.ButtonUp;
			this.buttonStart.Name = "buttonStart";
			this.buttonStart.UseVisualStyleBackColor = true;
			this.buttonStart.Click += new EventHandler(this.buttonStart_Click);
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.label3);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.label1);
			base.Controls.Add(this.labelYState);
			base.Controls.Add(this.labelXState);
			base.Controls.Add(this.labelYLimitN);
			base.Controls.Add(this.labelYLimitP);
			base.Controls.Add(this.labelXLimitP);
			base.Controls.Add(this.labelXLimitN);
			base.Controls.Add(this.buttonStop);
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonStart);
			base.Name = "GoHomeForm";
			base.ShowIcon = false;
			base.FormClosing += new FormClosingEventHandler(this.GoHomeForm_FormClosing);
			base.Load += new EventHandler(this.GoHomeForm_Load);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public GoHomeForm(TcpControler c, double x, double y)
		{
			this.InitializeComponent();
			this.controler = c;
			this.HomePosX = x;
			this.HomePosY = y;
		}

		private void GoHomeForm_Load(object sender, EventArgs e)
		{
			if (this.controler.XLMTP)
			{
				this.labelXLimitP.Image = Resource.RedLED;
			}
			else
			{
				this.labelXLimitP.Image = Resource.TransparentLED;
			}
			this.XLMTP = this.controler.XLMTP;
			if (this.controler.XLMTN)
			{
				this.labelXLimitN.Image = Resource.RedLED;
			}
			else
			{
				this.labelXLimitN.Image = Resource.TransparentLED;
			}
			this.XLMTN = this.controler.XLMTN;
			if (this.controler.YLMTP)
			{
				this.labelYLimitP.Image = Resource.RedLED;
			}
			else
			{
				this.labelYLimitP.Image = Resource.TransparentLED;
			}
			this.YLMTP = this.controler.YLMTP;
			if (this.controler.YLMTN)
			{
				this.labelYLimitN.Image = Resource.RedLED;
			}
			else
			{
				this.labelYLimitN.Image = Resource.TransparentLED;
			}
			this.YLMTN = this.controler.YLMTN;
			this.controler.WriteSpeed_IMM(4, 100);
			this.timerUI.Enabled = true;
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		private void timerUI_Tick(object sender, EventArgs e)
		{
			if (this.controler.XLMTP != this.XLMTP)
			{
				if (this.controler.XLMTP)
				{
					this.labelXLimitP.Image = Resource.RedLED;
				}
				else
				{
					this.labelXLimitP.Image = Resource.TransparentLED;
				}
				this.XLMTP = this.controler.XLMTP;
			}
			if (this.controler.XLMTN != this.XLMTN)
			{
				if (this.controler.XLMTN)
				{
					this.labelXLimitN.Image = Resource.RedLED;
				}
				else
				{
					this.labelXLimitN.Image = Resource.TransparentLED;
				}
				this.XLMTN = this.controler.XLMTN;
			}
			if (this.controler.YLMTP != this.YLMTP)
			{
				if (this.controler.YLMTP)
				{
					this.labelYLimitP.Image = Resource.RedLED;
				}
				else
				{
					this.labelYLimitP.Image = Resource.TransparentLED;
				}
				this.YLMTP = this.controler.YLMTP;
			}
			if (this.controler.YLMTN != this.YLMTN)
			{
				if (this.controler.YLMTN)
				{
					this.labelYLimitN.Image = Resource.RedLED;
				}
				else
				{
					this.labelYLimitN.Image = Resource.TransparentLED;
				}
				this.YLMTN = this.controler.YLMTN;
			}
			switch (this.XState)
			{
			case GoHomeForm.HomeStateType.None:
				this.labelXState.Text = "...";
				break;
			case GoHomeForm.HomeStateType.Stage1:
				this.labelXState.Text = this.LocStrings.GetString("GoHoming") + ".";
				break;
			case GoHomeForm.HomeStateType.Stage2:
				this.labelXState.Text = this.LocStrings.GetString("GoHoming") + "..";
				break;
			case GoHomeForm.HomeStateType.Stage3:
				this.labelXState.Text = this.LocStrings.GetString("GoHoming") + "...";
				break;
			case GoHomeForm.HomeStateType.OK:
				this.labelXState.Text = this.LocStrings.GetString("GoHoming") + "....";
				break;
			case GoHomeForm.HomeStateType.Done:
				this.labelXState.Text = this.LocStrings.GetString("Done");
				break;
			}
			switch (this.YState)
			{
			case GoHomeForm.HomeStateType.None:
				this.labelYState.Text = "...";
				break;
			case GoHomeForm.HomeStateType.Stage1:
				this.labelYState.Text = this.LocStrings.GetString("GoHoming") + ".";
				break;
			case GoHomeForm.HomeStateType.Stage2:
				this.labelYState.Text = this.LocStrings.GetString("GoHoming") + "..";
				break;
			case GoHomeForm.HomeStateType.Stage3:
				this.labelYState.Text = this.LocStrings.GetString("GoHoming") + "...";
				break;
			case GoHomeForm.HomeStateType.OK:
				this.labelYState.Text = this.LocStrings.GetString("GoHoming") + "....";
				break;
			case GoHomeForm.HomeStateType.Done:
				this.labelYState.Text = this.LocStrings.GetString("Done");
				break;
			}
			if (!this.IsEnter)
			{
				this.IsEnter = true;
				switch (this.XState)
				{
				case GoHomeForm.HomeStateType.None:
					if (this.controler.Status[2] != TcpControler.AxisStateType.AX_READY)
					{
						this.controler.SHalt(2);
					}
					break;
				case GoHomeForm.HomeStateType.Stage1:
					if (this.XLMTP)
					{
						this.XState = GoHomeForm.HomeStateType.Stage2;
					}
					else if (this.controler.Status[2] == TcpControler.AxisStateType.AX_READY && this.controler.SPTP(2, 1000.0, CmdType.CMD_PTP_IMM) == CmdResultType.Error)
					{
						this.XState = GoHomeForm.HomeStateType.None;
					}
					break;
				case GoHomeForm.HomeStateType.Stage2:
					if (!this.XLMTP)
					{
						this.XState = GoHomeForm.HomeStateType.Stage3;
					}
					else if (this.controler.Status[2] == TcpControler.AxisStateType.AX_READY && this.controler.SPTP(2, this.controler.PosMM[2] - 5.0, CmdType.CMD_PTP_IMM) == CmdResultType.Error)
					{
						this.XState = GoHomeForm.HomeStateType.None;
					}
					break;
				case GoHomeForm.HomeStateType.Stage3:
					if (this.XLMTP)
					{
						this.XState = GoHomeForm.HomeStateType.OK;
					}
					else if (this.controler.Status[2] == TcpControler.AxisStateType.AX_READY && this.controler.SPTP(2, this.controler.PosMM[2] + 100.0, CmdType.CMD_PTP_IMM) == CmdResultType.Error)
					{
						this.XState = GoHomeForm.HomeStateType.None;
					}
					break;
				case GoHomeForm.HomeStateType.OK:
					if (this.controler.Status[2] != TcpControler.AxisStateType.AX_READY)
					{
						if (this.controler.SHalt(2) == CmdResultType.Error)
						{
							this.XState = GoHomeForm.HomeStateType.None;
						}
					}
					else
					{
						double[] array = new double[6];
						array[2] = this.HomePosX;
						if (this.controler.WritePos(TcpControler.AxisType.XMark, array))
						{
							this.controler.IsHomed = true;
							this.XState = GoHomeForm.HomeStateType.Done;
						}
					}
					break;
				}
				switch (this.YState)
				{
				case GoHomeForm.HomeStateType.None:
					if (this.controler.Status[4] != TcpControler.AxisStateType.AX_READY)
					{
						this.controler.SHalt(4);
					}
					break;
				case GoHomeForm.HomeStateType.Stage1:
					if (this.YLMTP)
					{
						this.YState = GoHomeForm.HomeStateType.Stage2;
					}
					else if (this.controler.Status[4] == TcpControler.AxisStateType.AX_READY && this.controler.SPTP(4, 1000.0, CmdType.CMD_PTP_IMM) == CmdResultType.Error)
					{
						this.YState = GoHomeForm.HomeStateType.None;
					}
					break;
				case GoHomeForm.HomeStateType.Stage2:
					if (!this.YLMTP)
					{
						this.YState = GoHomeForm.HomeStateType.Stage3;
					}
					else if (this.controler.Status[4] == TcpControler.AxisStateType.AX_READY && this.controler.SPTP(4, this.controler.PosMM[4] - 5.0, CmdType.CMD_PTP_IMM) == CmdResultType.Error)
					{
						this.YState = GoHomeForm.HomeStateType.None;
					}
					break;
				case GoHomeForm.HomeStateType.Stage3:
					if (this.YLMTP)
					{
						this.YState = GoHomeForm.HomeStateType.OK;
					}
					else if (this.controler.Status[4] == TcpControler.AxisStateType.AX_READY && this.controler.SPTP(4, this.controler.PosMM[4] + 100.0, CmdType.CMD_PTP_IMM) == CmdResultType.Error)
					{
						this.YState = GoHomeForm.HomeStateType.None;
					}
					break;
				case GoHomeForm.HomeStateType.OK:
					if (this.controler.Status[4] != TcpControler.AxisStateType.AX_READY)
					{
						if (this.controler.SHalt(4) == CmdResultType.Error)
						{
							this.YState = GoHomeForm.HomeStateType.None;
						}
					}
					else
					{
						double[] array2 = new double[6];
						array2[4] = this.HomePosY;
						if (this.controler.WritePos(TcpControler.AxisType.YMark, array2))
						{
							if (this.XState == GoHomeForm.HomeStateType.None || this.XState == GoHomeForm.HomeStateType.OK)
							{
								this.XState = GoHomeForm.HomeStateType.Stage1;
							}
							this.YState = GoHomeForm.HomeStateType.Done;
						}
					}
					break;
				}
				this.IsEnter = false;
			}
		}

		private void buttonStart_Click(object sender, EventArgs e)
		{
			if (this.YState == GoHomeForm.HomeStateType.None || this.YState == GoHomeForm.HomeStateType.OK)
			{
				this.controler.WriteSpeed_IMM(4, 100);
				this.YState = GoHomeForm.HomeStateType.Stage1;
			}
		}

		private void buttonStop_Click(object sender, EventArgs e)
		{
			if (this.XState != GoHomeForm.HomeStateType.None && this.XState != GoHomeForm.HomeStateType.OK)
			{
				this.XState = GoHomeForm.HomeStateType.None;
			}
			if (this.YState != GoHomeForm.HomeStateType.None && this.YState != GoHomeForm.HomeStateType.OK)
			{
				this.YState = GoHomeForm.HomeStateType.None;
			}
		}

		private void GoHomeForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			this.timerUI.Enabled = false;
			this.controler.MHalt(TcpControler.AxisType.AxisAll);
		}
	}
}
