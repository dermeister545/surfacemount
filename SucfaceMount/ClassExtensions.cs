using System;
using System.Reflection;
using System.Windows.Forms;

namespace SucfaceMount
{
	public static class ClassExtensions
	{
		public static void SetDoubleBuffered(this DataGridView datagrid, bool opened)
		{
			Type type = datagrid.GetType();
			PropertyInfo property = type.GetProperty("DoubleBuffered", BindingFlags.Instance | BindingFlags.NonPublic);
			property.SetValue(datagrid, opened, null);
		}
	}
}
