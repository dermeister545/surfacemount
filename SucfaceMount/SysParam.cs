using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace SucfaceMount
{
	[Serializable]
	public class SysParam
	{
		public static volatile SysParam Instance;

		public SysParam()
		{
			SysParam.Instance = this;
		}

		public static void Load()
		{
			Stream stream = new FileStream("SystemParam.dat", FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read);
			try
			{
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				SysParam.Instance = (SysParam)binaryFormatter.Deserialize(stream);
			}
			catch (SerializationException)
			{
				SysParam.Instance = new SysParam();
			}
			finally
			{
				stream.Close();
			}
		}

		public static void Save()
		{
			FileStream fileStream = new FileStream("SystemParam.dat", FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			try
			{
				binaryFormatter.Serialize(fileStream, SysParam.Instance);
			}
			catch (SerializationException)
			{
			}
			finally
			{
				fileStream.Close();
			}
		}
	}
}
