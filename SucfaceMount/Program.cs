using System;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Windows.Forms;

namespace SucfaceMount
{
	internal static class Program
	{
		private static Mutex mutex;

		[STAThread]
		private static void Main()
		{
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Program.mutex = new Mutex(true, "OnlyRun");
			if (Program.mutex.WaitOne(0, false))
			{
				Application.Run(new MainForm());
				return;
			}
			ResourceManager resourceManager = new ResourceManager(typeof(MyStrings));
			MessageBox.Show(resourceManager.GetString("labelAlreadyRunning"), resourceManager.GetString("labelWarning"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			Application.Exit();
		}
	}
}
