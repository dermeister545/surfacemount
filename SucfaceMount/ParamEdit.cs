using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Net;
using System.Resources;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class ParamEdit : Form
	{
		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		private MainForm owner;

		private bool ResetCamera;

		private double[] LeftAndBackStackX;

		private double[] LeftAndBackStackY;

		private int[] LeftAndBackStackWidth;

		private int[] LeftAndBackStackFeed;

		private double[] IcStackStartX;

		private double[] IcStackStartY;

		private double[] IcStackEndX;

		private double[] IcStackEndY;

		private int[] IcStackArrayX;

		private int[] IcStackArrayY;

		private int leftPageNum;

		private int backPageNum;

		private int icStackPageNum;

		private IContainer components;

		private Button buttonCancel;

		private Button buttonSave;

		private Label label13;

		private TextBox textBoxIP1;

		private Label label12;

		private TextBox textBoxIP2;

		private Label label11;

		private TextBox textBoxIP3;

		private TextBox textBoxIP4;

		private Label label10;

		private Label label9;

		private TextBox textBoxNozzleDownDiscardPos;

		private Label label17;

		private TextBox textBoxNozzleDownFrontStackPos;

		private Label label16;

		private TextBox textBoxNozzleDownStackPos;

		private Label label15;

		private TextBox textBoxNozzleDownPcbPos;

		private Label label14;

		private TabControl tabControl;

		private TabPage tabPage1;

		private Label labelArrow1;

		private ComboBox comboBoxLeftFeed10;

		private ComboBox comboBoxLeftFeed9;

		private ComboBox comboBoxLeftFeed8;

		private ComboBox comboBoxLeftFeed7;

		private ComboBox comboBoxLeftFeed6;

		private ComboBox comboBoxLeftFeed5;

		private ComboBox comboBoxLeftFeed4;

		private ComboBox comboBoxLeftFeed3;

		private ComboBox comboBoxLeftFeed2;

		private ComboBox comboBoxLeftFeed1;

		private Label label19;

		private Label label20;

		private Label labelLeftNo10;

		private Label label21;

		private Label label22;

		private TextBox textBoxLeftY10;

		private TextBox textBoxLeftX1;

		private TextBox textBoxLeftX10;

		private TextBox textBoxLeftY1;

		private Label labelLeftNo9;

		private Label labelLeftNo1;

		private TextBox textBoxLeftY9;

		private TextBox textBoxLeftX9;

		private TextBox textBoxLeftX2;

		private TextBox textBoxLeftY2;

		private Label labelLeftNo8;

		private Label labelLeftNo2;

		private TextBox textBoxLeftY8;

		private TextBox textBoxLeftX8;

		private TextBox textBoxLeftX3;

		private TextBox textBoxLeftY3;

		private Label labelLeftNo3;

		private Label labelLeftNo7;

		private TextBox textBoxLeftX4;

		private TextBox textBoxLeftY7;

		private TextBox textBoxLeftY4;

		private TextBox textBoxLeftX7;

		private Label labelLeftNo4;

		private Label labelLeftNo6;

		private TextBox textBoxLeftX5;

		private TextBox textBoxLeftY6;

		private TextBox textBoxLeftY5;

		private TextBox textBoxLeftX6;

		private Label labelLeftNo5;

		private TabPage tabPage3;

		private Label labelArrow3;

		private Label label25;

		private Label label26;

		private TextBox textBoxIcStackStartX1;

		private TextBox textBoxIcStackStartY1;

		private Label labelIcStackStart1;

		private TextBox textBoxIcStackEndX1;

		private TextBox textBoxIcStackEndY1;

		private Label labelIcStackEnd1;

		private TextBox textBoxIcStackArrayX1;

		private TextBox textBoxIcStackArrayY1;

		private Label labelIcStackArray1;

		private TabPage tabPage4;

		private TabPage tabPage6;

		private Button buttonGetCur1;

		private Button buttonMoveTo1;

		private Button buttonUp1;

		private Button buttonDown1;

		private TabPage tabPage5;

		private TextBox textBoxPixelScaleX1;

		private TextBox textBoxPixelScaleY1;

		private Label label27;

		private CheckBox checkBoxPressureCheck;

		private ComboBox comboBoxLeftWidth10;

		private ComboBox comboBoxLeftWidth9;

		private ComboBox comboBoxLeftWidth8;

		private ComboBox comboBoxLeftWidth7;

		private ComboBox comboBoxLeftWidth6;

		private ComboBox comboBoxLeftWidth5;

		private ComboBox comboBoxLeftWidth4;

		private ComboBox comboBoxLeftWidth3;

		private ComboBox comboBoxLeftWidth2;

		private Label label23;

		private ComboBox comboBoxLeftWidth1;

		private TextBox textBoxLeftPrickOffsetY;

		private TextBox textBoxLeftPrickOffsetX;

		private Label label28;

		private TextBox textBoxVisionOffsetY;

		private TextBox textBoxVisionOffsetX;

		private Label label29;

		private TextBox textBoxIpPort;

		private TextBox textBoxNozzleOffsetY;

		private TextBox textBoxNozzleOffsetX;

		private Label labelNozzleOffset;

		private GroupBox groupBox1;

		private Label label32;

		private Label label31;

		private TextBox textBoxPcbDealy2;

		private TextBox textBoxPcbDealy1;

		private TextBox textBoxStackDealy;

		private Label label30;

		private TextBox textBoxPixelScaleX2;

		private TextBox textBoxPixelScaleY2;

		private Label label1;

		private Button buttonNozzle2Test;

		private Label label2;

		private TextBox textBoxCameraAngle1;

		private Label label4;

		private Label label3;

		private TextBox textBoxNozzle2VisionY;

		private TextBox textBoxNozzle2VisionX;

		private TextBox textBoxNozzle1VisionY;

		private TextBox textBoxNozzle1VisionX;

		private Button buttonVisionOffsetTest;

		private GroupBox groupBox2;

		private ComboBox comboBoxBlowingDealy2;

		private ComboBox comboBoxBlowingDealy1;

		private Label label5;

		private Label label6;

		private GroupBox groupBox4;

		private Label label18;

		private Label label8;

		private ComboBox comboBoxFeedCamera1;

		private TextBox textBoxVisionValueComp;

		private GroupBox groupBox3;

		private Label label7;

		private ComboBox comboBoxFeedCamera2;

		private TextBox textBoxVisionValueNozzle;

		private Label labelVisionValue;

		private TabPage tabPage2;

		private Button buttonGetCur2;

		private ComboBox comboBoxBackWidth10;

		private Button buttonMoveTo2;

		private ComboBox comboBoxBackWidth9;

		private Button buttonDown2;

		private Button buttonUp2;

		private ComboBox comboBoxBackWidth8;

		private ComboBox comboBoxBackWidth7;

		private ComboBox comboBoxBackWidth6;

		private ComboBox comboBoxBackWidth5;

		private ComboBox comboBoxBackWidth4;

		private ComboBox comboBoxBackWidth3;

		private ComboBox comboBoxBackWidth2;

		private Label label33;

		private ComboBox comboBoxBackWidth1;

		private Label labelArrow2;

		private ComboBox comboBoxBackFeed10;

		private ComboBox comboBoxBackFeed9;

		private ComboBox comboBoxBackFeed8;

		private ComboBox comboBoxBackFeed7;

		private ComboBox comboBoxBackFeed6;

		private ComboBox comboBoxBackFeed5;

		private ComboBox comboBoxBackFeed4;

		private ComboBox comboBoxBackFeed3;

		private ComboBox comboBoxBackFeed2;

		private ComboBox comboBoxBackFeed1;

		private Label label35;

		private Label label36;

		private Label labelBackNo10;

		private Label label38;

		private Label label39;

		private TextBox textBoxBackY10;

		private TextBox textBoxBackX1;

		private TextBox textBoxBackX10;

		private TextBox textBoxBackY1;

		private Label labelBackNo9;

		private Label labelBackNo1;

		private TextBox textBoxBackY9;

		private TextBox textBoxBackX9;

		private TextBox textBoxBackX2;

		private TextBox textBoxBackY2;

		private Label labelBackNo8;

		private Label labelBackNo2;

		private TextBox textBoxBackY8;

		private TextBox textBoxBackX8;

		private TextBox textBoxBackX3;

		private TextBox textBoxBackY3;

		private Label labelBackNo3;

		private Label labelBackNo7;

		private TextBox textBoxBackX4;

		private TextBox textBoxBackY7;

		private TextBox textBoxBackY4;

		private TextBox textBoxBackX7;

		private Label labelBackNo4;

		private Label labelBackNo6;

		private TextBox textBoxBackX5;

		private TextBox textBoxBackY6;

		private TextBox textBoxBackY5;

		private TextBox textBoxBackX6;

		private Label labelBackNo5;

		private Button buttonGetCur3;

		private Button buttonMoveTo3;

		private Button buttonDown3;

		private Button buttonUp3;

		private GroupBox groupBoxIcStack2;

		private TextBox textBoxIcStackArrayY2;

		private Label labelIcStackArray2;

		private TextBox textBoxIcStackArrayX2;

		private Label labelIcStackEnd2;

		private TextBox textBoxIcStackEndY2;

		private TextBox textBoxIcStackEndX2;

		private Label label40;

		private Label labelIcStackStart2;

		private Label label42;

		private TextBox textBoxIcStackStartY2;

		private TextBox textBoxIcStackStartX2;

		private GroupBox groupBoxIcStack1;

		private Label labelArrow4;

		private TextBox textBoxVisionZ;

		private TextBox textBoxVisionY;

		private TextBox textBoxVisionX;

		private Label label24;

		private TextBox textBoxBackPrickOffsetY;

		private TextBox textBoxBackPrickOffsetX;

		private Label label34;

		public int LeftPageNum
		{
			get
			{
				return this.leftPageNum;
			}
			set
			{
				if (value >= 0 && value <= 2)
				{
					this.leftPageNum = value;
					this.UpdateLeftStackUI();
				}
			}
		}

		public int BackPageNum
		{
			get
			{
				return this.backPageNum;
			}
			set
			{
				if (value >= 0 && value <= 2)
				{
					this.backPageNum = value;
					this.UpdateBackStackUI();
				}
			}
		}

		public int IcStackPageNum
		{
			get
			{
				return this.icStackPageNum;
			}
			set
			{
				if (value >= 0 && value <= 14)
				{
					this.icStackPageNum = value;
					this.UpdateFrontStackUI();
				}
			}
		}

		public ParamEdit(MainForm ow)
		{
			this.InitializeComponent();
			this.owner = ow;
		}

		private void ParamEdit_Load(object sender, EventArgs e)
		{
			string[] array = new IPAddress((long)((ulong)this.owner.ipAdd)).ToString().Split(new char[]
			{
				'.'
			});
			this.textBoxIP1.Text = array[3];
			this.textBoxIP2.Text = array[2];
			this.textBoxIP3.Text = array[1];
			this.textBoxIP4.Text = array[0];
			this.textBoxIpPort.Text = this.owner.ipPort.ToString();
			this.textBoxNozzleOffsetX.Text = this.owner.NozzleOffsetX.ToString("F2");
			this.textBoxNozzleOffsetY.Text = this.owner.NozzleOffsetY.ToString("F2");
			this.textBoxVisionOffsetX.Text = this.owner.VisionOffsetX.ToString("F2");
			this.textBoxVisionOffsetY.Text = this.owner.VisionOffsetY.ToString("F2");
			this.textBoxLeftPrickOffsetX.Text = this.owner.LeftPrickOffsetX.ToString("F2");
			this.textBoxLeftPrickOffsetY.Text = this.owner.LeftPrickOffsetY.ToString("F2");
			this.textBoxBackPrickOffsetX.Text = this.owner.BackPrickOffsetX.ToString("F2");
			this.textBoxBackPrickOffsetY.Text = this.owner.BackPrickOffsetY.ToString("F2");
			this.textBoxNozzle1VisionX.Text = this.owner.Nozzle1VisionX.ToString("F2");
			this.textBoxNozzle1VisionY.Text = this.owner.Nozzle1VisionY.ToString("F2");
			this.textBoxNozzle2VisionX.Text = this.owner.Nozzle2VisionX.ToString("F2");
			this.textBoxNozzle2VisionY.Text = this.owner.Nozzle2VisionY.ToString("F2");
			this.textBoxCameraAngle1.Text = this.owner.CameraAngle1.ToString("F2");
			this.checkBoxPressureCheck.Checked = this.owner.PressureCheckTotal;
			this.textBoxNozzleDownPcbPos.Text = this.owner.ZAxisNozzleDownPCBPos.ToString("F2");
			this.textBoxNozzleDownStackPos.Text = this.owner.ZAxisNozzleDownStackPos.ToString("F2");
			this.textBoxNozzleDownFrontStackPos.Text = this.owner.ZAxisNozzleDownFrontStackPos.ToString("F2");
			this.textBoxNozzleDownDiscardPos.Text = this.owner.ZAxisNozzleDownDiscardPos.ToString("F2");
			this.comboBoxBlowingDealy1.SelectedIndex = this.owner.BlowingDealy1;
			this.comboBoxBlowingDealy2.SelectedIndex = this.owner.BlowingDealy2;
			this.textBoxStackDealy.Text = this.owner.ZAxisStackDealy.ToString();
			this.textBoxPcbDealy1.Text = this.owner.ZAxisPcbDealy1.ToString();
			this.textBoxPcbDealy2.Text = this.owner.ZAxisPcbDealy2.ToString();
			this.textBoxPixelScaleX1.Text = this.owner.PixelScaleX1.ToString("F4");
			this.textBoxPixelScaleY1.Text = this.owner.PixelScaleY1.ToString("F4");
			this.textBoxPixelScaleX2.Text = this.owner.PixelScaleX2.ToString("F4");
			this.textBoxPixelScaleY2.Text = this.owner.PixelScaleY2.ToString("F4");
			this.textBoxVisionX.Text = this.owner.VisionX.ToString("F2");
			this.textBoxVisionY.Text = this.owner.VisionY.ToString("F2");
			this.textBoxVisionZ.Text = this.owner.VisionZ.ToString("F2");
			this.LeftAndBackStackX = (double[])this.owner.LeftAndBackStack.X.Clone();
			this.LeftAndBackStackY = (double[])this.owner.LeftAndBackStack.Y.Clone();
			this.LeftAndBackStackWidth = (int[])this.owner.LeftAndBackStack.Width.Clone();
			this.LeftAndBackStackFeed = (int[])this.owner.LeftAndBackStack.Feed.Clone();
			this.IcStackStartX = (double[])this.owner.IcStack.StartX.Clone();
			this.IcStackStartY = (double[])this.owner.IcStack.StartY.Clone();
			this.IcStackEndX = (double[])this.owner.IcStack.EndX.Clone();
			this.IcStackEndY = (double[])this.owner.IcStack.EndY.Clone();
			this.IcStackArrayX = (int[])this.owner.IcStack.ArrayX.Clone();
			this.IcStackArrayY = (int[])this.owner.IcStack.ArrayY.Clone();
			this.UpdateLeftStackUI();
			this.UpdateBackStackUI();
			this.UpdateFrontStackUI();
			this.textBoxVisionValueNozzle.Text = this.owner.VisionValueNozzle.ToString("F2");
			this.textBoxVisionValueComp.Text = this.owner.VisionValueComp.ToString("F2");
			this.LoadCameraKey();
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			IPAddress iPAddress;
			if (IPAddress.TryParse(string.Concat(new string[]
			{
				this.textBoxIP4.Text,
				".",
				this.textBoxIP3.Text,
				".",
				this.textBoxIP2.Text,
				".",
				this.textBoxIP1.Text
			}), out iPAddress))
			{
				this.owner.ipAdd = (uint)iPAddress.Address;
			}
			uint num;
			if (uint.TryParse(this.textBoxIpPort.Text, out num))
			{
				this.owner.ipPort = num;
			}
			double num2;
			if (double.TryParse(this.textBoxNozzleOffsetX.Text, out num2))
			{
				this.owner.NozzleOffsetX = num2;
			}
			if (double.TryParse(this.textBoxNozzleOffsetY.Text, out num2))
			{
				this.owner.NozzleOffsetY = num2;
			}
			if (double.TryParse(this.textBoxVisionOffsetX.Text, out num2))
			{
				this.owner.VisionOffsetX = num2;
			}
			if (double.TryParse(this.textBoxVisionOffsetY.Text, out num2))
			{
				this.owner.VisionOffsetY = num2;
			}
			if (double.TryParse(this.textBoxLeftPrickOffsetX.Text, out num2))
			{
				this.owner.LeftPrickOffsetX = num2;
			}
			if (double.TryParse(this.textBoxLeftPrickOffsetY.Text, out num2))
			{
				this.owner.LeftPrickOffsetY = num2;
			}
			if (double.TryParse(this.textBoxBackPrickOffsetX.Text, out num2))
			{
				this.owner.BackPrickOffsetX = num2;
			}
			if (double.TryParse(this.textBoxBackPrickOffsetY.Text, out num2))
			{
				this.owner.BackPrickOffsetY = num2;
			}
			if (double.TryParse(this.textBoxNozzle1VisionX.Text, out num2))
			{
				this.owner.Nozzle1VisionX = num2;
			}
			if (double.TryParse(this.textBoxNozzle1VisionY.Text, out num2))
			{
				this.owner.Nozzle1VisionY = num2;
			}
			if (double.TryParse(this.textBoxNozzle2VisionX.Text, out num2))
			{
				this.owner.Nozzle2VisionX = num2;
			}
			if (double.TryParse(this.textBoxNozzle2VisionY.Text, out num2))
			{
				this.owner.Nozzle2VisionY = num2;
			}
			if (double.TryParse(this.textBoxCameraAngle1.Text, out num2))
			{
				this.owner.CameraAngle1 = num2;
			}
			this.owner.PressureCheckTotal = this.checkBoxPressureCheck.Checked;
			if (double.TryParse(this.textBoxNozzleDownPcbPos.Text, out num2))
			{
				this.owner.ZAxisNozzleDownPCBPos = num2;
			}
			if (double.TryParse(this.textBoxNozzleDownStackPos.Text, out num2))
			{
				this.owner.ZAxisNozzleDownStackPos = num2;
			}
			if (double.TryParse(this.textBoxNozzleDownFrontStackPos.Text, out num2))
			{
				this.owner.ZAxisNozzleDownFrontStackPos = num2;
			}
			if (double.TryParse(this.textBoxNozzleDownDiscardPos.Text, out num2))
			{
				this.owner.ZAxisNozzleDownDiscardPos = num2;
			}
			this.owner.BlowingDealy1 = this.comboBoxBlowingDealy1.SelectedIndex;
			this.owner.BlowingDealy2 = this.comboBoxBlowingDealy2.SelectedIndex;
			if (uint.TryParse(this.textBoxStackDealy.Text, out num))
			{
				this.owner.ZAxisStackDealy = num;
			}
			if (uint.TryParse(this.textBoxPcbDealy1.Text, out num))
			{
				this.owner.ZAxisPcbDealy1 = num;
			}
			if (uint.TryParse(this.textBoxPcbDealy2.Text, out num))
			{
				this.owner.ZAxisPcbDealy2 = num;
			}
			if (double.TryParse(this.textBoxPixelScaleX1.Text, out num2))
			{
				this.owner.PixelScaleX1 = num2;
			}
			if (double.TryParse(this.textBoxPixelScaleY1.Text, out num2))
			{
				this.owner.PixelScaleY1 = num2;
			}
			if (double.TryParse(this.textBoxPixelScaleX2.Text, out num2))
			{
				this.owner.PixelScaleX2 = num2;
			}
			if (double.TryParse(this.textBoxPixelScaleY2.Text, out num2))
			{
				this.owner.PixelScaleY2 = num2;
			}
			if (double.TryParse(this.textBoxVisionX.Text, out num2))
			{
				this.owner.VisionX = num2;
			}
			if (double.TryParse(this.textBoxVisionY.Text, out num2))
			{
				this.owner.VisionY = num2;
			}
			if (double.TryParse(this.textBoxVisionZ.Text, out num2))
			{
				this.owner.VisionZ = num2;
			}
			this.LeftAndBackStackX.CopyTo(this.owner.LeftAndBackStack.X, 0);
			this.LeftAndBackStackY.CopyTo(this.owner.LeftAndBackStack.Y, 0);
			this.LeftAndBackStackWidth.CopyTo(this.owner.LeftAndBackStack.Width, 0);
			for (int i = 0; i < this.LeftAndBackStackWidth.Length; i++)
			{
				int num3 = this.LeftAndBackStackWidth[i];
				if (num3 <= 12)
				{
					if (num3 != 8)
					{
						if (num3 == 12)
						{
							this.owner.LeftAndBackStack.PrickOffsetY[i] = 5.5;
						}
					}
					else
					{
						this.owner.LeftAndBackStack.PrickOffsetY[i] = 3.5;
					}
				}
				else if (num3 != 16)
				{
					if (num3 == 24)
					{
						this.owner.LeftAndBackStack.PrickOffsetY[i] = 11.0;
					}
				}
				else
				{
					this.owner.LeftAndBackStack.PrickOffsetY[i] = 7.5;
				}
			}
			this.LeftAndBackStackFeed.CopyTo(this.owner.LeftAndBackStack.Feed, 0);
			this.IcStackStartX.CopyTo(this.owner.IcStack.StartX, 0);
			this.IcStackStartY.CopyTo(this.owner.IcStack.StartY, 0);
			this.IcStackEndX.CopyTo(this.owner.IcStack.EndX, 0);
			this.IcStackEndY.CopyTo(this.owner.IcStack.EndY, 0);
			this.IcStackArrayX.CopyTo(this.owner.IcStack.ArrayX, 0);
			this.IcStackArrayY.CopyTo(this.owner.IcStack.ArrayY, 0);
			if (double.TryParse(this.textBoxVisionValueNozzle.Text, out num2))
			{
				if (num2 > 200.0)
				{
					num2 = 200.0;
				}
				if (num2 < 0.0)
				{
					num2 = 0.0;
				}
				this.owner.VisionValueNozzle = num2;
			}
			if (double.TryParse(this.textBoxVisionValueComp.Text, out num2))
			{
				if (num2 > 99.0)
				{
					num2 = 99.0;
				}
				if (num2 < 0.0)
				{
					num2 = 0.0;
				}
				this.owner.VisionValueComp = num2;
			}
			if (uint.TryParse(this.comboBoxFeedCamera1.Text, out num))
			{
				this.owner.CameraKey1 = num;
			}
			if (uint.TryParse(this.comboBoxFeedCamera2.Text, out num))
			{
				this.owner.CameraKey2 = num;
			}
			this.owner.SaveParam();
			if (this.ResetCamera)
			{
				this.owner.mVision1.Release();
				this.owner.mVision1.Initialize(this.owner.CameraKey1.ToString("D9"), this.owner.CameraKey2.ToString("D9"));
			}
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void UpdateLeftStackUI()
		{
			int num = this.leftPageNum * 10;
			this.labelLeftNo1.Text = (num + 1).ToString();
			this.comboBoxLeftWidth1.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth1.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth1.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX1.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY1.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed1.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed1.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed1.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo2.Text = (num + 1).ToString();
			this.comboBoxLeftWidth2.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth2.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth2.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX2.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY2.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed2.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed2.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed2.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo3.Text = (num + 1).ToString();
			this.comboBoxLeftWidth3.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth3.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth3.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX3.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY3.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed3.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed3.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed3.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo4.Text = (num + 1).ToString();
			this.comboBoxLeftWidth4.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth4.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth4.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX4.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY4.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed4.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed4.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed4.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo5.Text = (num + 1).ToString();
			this.comboBoxLeftWidth5.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth5.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth5.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX5.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY5.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed5.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed5.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed5.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo6.Text = (num + 1).ToString();
			this.comboBoxLeftWidth6.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth6.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth6.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX6.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY6.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed6.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed6.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed6.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo7.Text = (num + 1).ToString();
			this.comboBoxLeftWidth7.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth7.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth7.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX7.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY7.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed7.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed7.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed7.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo8.Text = (num + 1).ToString();
			this.comboBoxLeftWidth8.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth8.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth8.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX8.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY8.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed8.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed8.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed8.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo9.Text = (num + 1).ToString();
			this.comboBoxLeftWidth9.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth9.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth9.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX9.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY9.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed9.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed9.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed9.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
			num++;
			this.labelLeftNo10.Text = (num + 1).ToString();
			this.comboBoxLeftWidth10.Text = this.LeftAndBackStackWidth[num].ToString();
			if (!int.TryParse(this.comboBoxLeftWidth10.Text, out this.LeftAndBackStackWidth[num]))
			{
				this.comboBoxLeftWidth10.Text = "8";
				this.LeftAndBackStackWidth[num] = 8;
			}
			this.textBoxLeftX10.Text = this.LeftAndBackStackX[num].ToString("F2");
			this.textBoxLeftY10.Text = this.LeftAndBackStackY[num].ToString("F2");
			this.comboBoxLeftFeed10.Text = this.LeftAndBackStackFeed[num].ToString();
			if (!int.TryParse(this.comboBoxLeftFeed10.Text, out this.LeftAndBackStackFeed[num]))
			{
				this.comboBoxLeftFeed10.Text = "4";
				this.LeftAndBackStackFeed[num] = 4;
			}
		}

		private void comboBoxLeftWidth_Leave(object sender, EventArgs e)
		{
			int num;
			int num2;
			if (int.TryParse(((ComboBox)sender).Name.Remove(0, 17), out num) && int.TryParse(((ComboBox)sender).Text, out num2))
			{
				this.LeftAndBackStackWidth[this.LeftPageNum * 10 + num - 1] = num2;
			}
		}

		private void textBoxLeftX_Leave(object sender, EventArgs e)
		{
			int num;
			if (int.TryParse(((TextBox)sender).Name.Remove(0, 12), out num))
			{
				double num2;
				if (double.TryParse(((TextBox)sender).Text, out num2))
				{
					this.LeftAndBackStackX[this.LeftPageNum * 10 + num - 1] = num2;
					return;
				}
				((TextBox)sender).Text = this.LeftAndBackStackX[this.LeftPageNum * 10 + num - 1].ToString("F2");
			}
		}

		private void textBoxLeftY_Leave(object sender, EventArgs e)
		{
			int num;
			if (int.TryParse(((TextBox)sender).Name.Remove(0, 12), out num))
			{
				double num2;
				if (double.TryParse(((TextBox)sender).Text, out num2))
				{
					this.LeftAndBackStackY[this.LeftPageNum * 10 + num - 1] = num2;
					return;
				}
				((TextBox)sender).Text = this.LeftAndBackStackY[this.LeftPageNum * 10 + num - 1].ToString("F2");
			}
		}

		private void comboBoxLeftFeed_Leave(object sender, EventArgs e)
		{
			int num;
			int num2;
			if (int.TryParse(((ComboBox)sender).Name.Remove(0, 16), out num) && int.TryParse(((ComboBox)sender).Text, out num2))
			{
				this.LeftAndBackStackFeed[this.LeftPageNum * 10 + num - 1] = num2;
			}
		}

		private void LeftArrow_Click(object sender, EventArgs e)
		{
			int num = ((Control)sender).Location.Y + ((Control)sender).Size.Height / 2;
			this.labelArrow1.Location = new Point(this.labelArrow1.Location.X, num - this.labelArrow1.Size.Height / 2);
		}

		private void buttonUp1_Click(object sender, EventArgs e)
		{
			this.LeftPageNum--;
		}

		private void buttonDown1_Click(object sender, EventArgs e)
		{
			this.LeftPageNum++;
		}

		private void UpdateBackStackUI()
		{
			int num = this.backPageNum * 10;
			this.labelBackNo1.Text = (num + 1).ToString();
			this.comboBoxBackWidth1.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth1.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth1.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX1.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY1.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed1.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed1.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed1.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo2.Text = (num + 1).ToString();
			this.comboBoxBackWidth2.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth2.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth2.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX2.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY2.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed2.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed2.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed2.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo3.Text = (num + 1).ToString();
			this.comboBoxBackWidth3.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth3.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth3.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX3.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY3.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed3.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed3.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed3.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo4.Text = (num + 1).ToString();
			this.comboBoxBackWidth4.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth4.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth4.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX4.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY4.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed4.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed4.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed4.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo5.Text = (num + 1).ToString();
			this.comboBoxBackWidth5.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth5.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth5.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX5.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY5.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed5.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed5.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed5.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo6.Text = (num + 1).ToString();
			this.comboBoxBackWidth6.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth6.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth6.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX6.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY6.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed6.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed6.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed6.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo7.Text = (num + 1).ToString();
			this.comboBoxBackWidth7.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth7.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth7.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX7.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY7.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed7.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed7.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed7.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo8.Text = (num + 1).ToString();
			this.comboBoxBackWidth8.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth8.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth8.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX8.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY8.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed8.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed8.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed8.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo9.Text = (num + 1).ToString();
			this.comboBoxBackWidth9.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth9.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth9.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX9.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY9.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed9.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed9.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed9.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
			num++;
			this.labelBackNo10.Text = (num + 1).ToString();
			this.comboBoxBackWidth10.Text = this.LeftAndBackStackWidth[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackWidth10.Text, out this.LeftAndBackStackWidth[num + 30]))
			{
				this.comboBoxBackWidth10.Text = "8";
				this.LeftAndBackStackWidth[num + 30] = 8;
			}
			this.textBoxBackX10.Text = this.LeftAndBackStackX[num + 30].ToString("F2");
			this.textBoxBackY10.Text = this.LeftAndBackStackY[num + 30].ToString("F2");
			this.comboBoxBackFeed10.Text = this.LeftAndBackStackFeed[num + 30].ToString();
			if (!int.TryParse(this.comboBoxBackFeed10.Text, out this.LeftAndBackStackFeed[num + 30]))
			{
				this.comboBoxBackFeed10.Text = "4";
				this.LeftAndBackStackFeed[num + 30] = 4;
			}
		}

		private void comboBoxBackWidth_Leave(object sender, EventArgs e)
		{
			int num;
			int num2;
			if (int.TryParse(((ComboBox)sender).Name.Remove(0, 17), out num) && int.TryParse(((ComboBox)sender).Text, out num2))
			{
				this.LeftAndBackStackWidth[30 + this.BackPageNum * 10 + num - 1] = num2;
			}
		}

		private void textBoxBackX_Leave(object sender, EventArgs e)
		{
			int num;
			if (int.TryParse(((TextBox)sender).Name.Remove(0, 12), out num))
			{
				double num2;
				if (double.TryParse(((TextBox)sender).Text, out num2))
				{
					this.LeftAndBackStackX[30 + this.BackPageNum * 10 + num - 1] = num2;
					return;
				}
				((TextBox)sender).Text = this.LeftAndBackStackX[30 + this.BackPageNum * 10 + num - 1].ToString("F2");
			}
		}

		private void textBoxBackY_Leave(object sender, EventArgs e)
		{
			int num;
			if (int.TryParse(((TextBox)sender).Name.Remove(0, 12), out num))
			{
				double num2;
				if (double.TryParse(((TextBox)sender).Text, out num2))
				{
					this.LeftAndBackStackY[30 + this.BackPageNum * 10 + num - 1] = num2;
					return;
				}
				((TextBox)sender).Text = this.LeftAndBackStackY[30 + this.BackPageNum * 10 + num - 1].ToString("F2");
			}
		}

		private void comboBoxBackFeed_Leave(object sender, EventArgs e)
		{
			int num;
			int num2;
			if (int.TryParse(((ComboBox)sender).Name.Remove(0, 16), out num) && int.TryParse(((ComboBox)sender).Text, out num2))
			{
				this.LeftAndBackStackFeed[30 + this.BackPageNum * 10 + num - 1] = num2;
			}
		}

		private void BackArrow_Click(object sender, EventArgs e)
		{
			int num = ((Control)sender).Location.Y + ((Control)sender).Size.Height / 2;
			this.labelArrow2.Location = new Point(this.labelArrow2.Location.X, num - this.labelArrow2.Size.Height / 2);
		}

		private void buttonUp2_Click(object sender, EventArgs e)
		{
			this.BackPageNum--;
		}

		private void buttonDown2_Click(object sender, EventArgs e)
		{
			this.BackPageNum++;
		}

		private void UpdateFrontStackUI()
		{
			int num = this.icStackPageNum * 2;
			this.groupBoxIcStack1.Text = this.LocStrings.GetString("labelIcStack") + (num + 1).ToString();
			this.textBoxIcStackStartX1.Text = this.IcStackStartX[num].ToString("F2");
			this.textBoxIcStackStartY1.Text = this.IcStackStartY[num].ToString("F2");
			this.textBoxIcStackEndX1.Text = this.IcStackEndX[num].ToString("F2");
			this.textBoxIcStackEndY1.Text = this.IcStackEndY[num].ToString("F2");
			this.textBoxIcStackArrayX1.Text = this.IcStackArrayX[num].ToString("F0");
			this.textBoxIcStackArrayY1.Text = this.IcStackArrayY[num].ToString("F0");
			num++;
			this.groupBoxIcStack2.Text = this.LocStrings.GetString("labelIcStack") + (num + 1).ToString();
			this.textBoxIcStackStartX2.Text = this.IcStackStartX[num].ToString("F2");
			this.textBoxIcStackStartY2.Text = this.IcStackStartY[num].ToString("F2");
			this.textBoxIcStackEndX2.Text = this.IcStackEndX[num].ToString("F2");
			this.textBoxIcStackEndY2.Text = this.IcStackEndY[num].ToString("F2");
			this.textBoxIcStackArrayX2.Text = this.IcStackArrayX[num].ToString("F0");
			this.textBoxIcStackArrayY2.Text = this.IcStackArrayY[num].ToString("F0");
		}

		private void textBoxIcStackStartX_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, 20)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.IcStackStartX[this.IcStackPageNum * 2 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.IcStackStartX[this.IcStackPageNum * 2 + num].ToString("F2");
		}

		private void textBoxIcStackStartY_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, 20)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.IcStackStartY[this.IcStackPageNum * 2 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.IcStackStartY[this.IcStackPageNum * 2 + num].ToString("F2");
		}

		private void textBoxIcStackEndX_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, 18)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.IcStackEndX[this.IcStackPageNum * 2 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.IcStackEndX[this.IcStackPageNum * 2 + num].ToString("F2");
		}

		private void textBoxIcStackEndY_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, 18)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.IcStackEndY[this.IcStackPageNum * 2 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.IcStackEndY[this.IcStackPageNum * 2 + num].ToString("F2");
		}

		private void textBoxIcStackArrayX_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, 20)) - 1;
			int num2;
			if (int.TryParse(((TextBox)sender).Text, out num2))
			{
				this.IcStackArrayX[this.IcStackPageNum * 2 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.IcStackArrayX[this.IcStackPageNum * 2 + num].ToString("F2");
		}

		private void textBoxIcStackArrayY_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, 20)) - 1;
			int num2;
			if (int.TryParse(((TextBox)sender).Text, out num2))
			{
				this.IcStackArrayY[this.IcStackPageNum * 2 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.IcStackArrayY[this.IcStackPageNum * 2 + num].ToString("F2");
		}

		private void Arrow3_Click(object sender, EventArgs e)
		{
			this.labelArrow4.Visible = false;
			int num = ((Control)sender).Location.Y + ((Control)sender).Size.Height / 2;
			this.labelArrow3.Location = new Point(this.labelArrow3.Location.X, num - this.labelArrow3.Size.Height / 2);
			this.labelArrow3.Visible = true;
		}

		private void Arrow4_Click(object sender, EventArgs e)
		{
			this.labelArrow3.Visible = false;
			int num = ((Control)sender).Location.Y + ((Control)sender).Size.Height / 2;
			this.labelArrow4.Location = new Point(this.labelArrow4.Location.X, num - this.labelArrow4.Size.Height / 2);
			this.labelArrow4.Visible = true;
		}

		private void Arrow34None_Click(object sender, EventArgs e)
		{
			this.labelArrow3.Visible = false;
			this.labelArrow4.Visible = false;
		}

		private void buttonUp3_Click(object sender, EventArgs e)
		{
			this.IcStackPageNum--;
		}

		private void buttonDown3_Click(object sender, EventArgs e)
		{
			this.IcStackPageNum++;
		}

		private void buttonMoveTo_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				new MyMessageBox(null, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
			{
				new MyMessageBox(null, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			switch (this.tabControl.SelectedIndex)
			{
			case 0:
			{
				this.owner.controler.WriteSpeed_IMM(this.owner.controler.SpeedValue, 100);
				int num = this.labelArrow1.Location.Y + this.labelArrow1.Size.Height / 2;
				int num2 = (num - this.labelLeftNo1.Location.Y) / (this.labelLeftNo2.Location.Y - this.labelLeftNo1.Location.Y);
				double goal = this.LeftAndBackStackX[this.LeftPageNum * 10 + num2];
				double goal2 = this.LeftAndBackStackY[this.LeftPageNum * 10 + num2];
				this.owner.controler.DPTP(2, 4, goal, goal2, CmdType.CMD_PTP_IMM);
				return;
			}
			case 1:
			{
				this.owner.controler.WriteSpeed_IMM(this.owner.controler.SpeedValue, 100);
				int num = this.labelArrow2.Location.Y + this.labelArrow2.Size.Height / 2;
				int num2 = (num - this.labelBackNo1.Location.Y) / (this.labelBackNo2.Location.Y - this.labelBackNo1.Location.Y);
				double goal = this.LeftAndBackStackX[30 + this.BackPageNum * 10 + num2];
				double goal2 = this.LeftAndBackStackY[30 + this.BackPageNum * 10 + num2];
				this.owner.controler.DPTP(2, 4, goal, goal2, CmdType.CMD_PTP_IMM);
				return;
			}
			case 2:
				if (this.labelArrow3.Visible)
				{
					this.owner.controler.WriteSpeed_IMM(this.owner.controler.SpeedValue, 100);
					int num = this.labelArrow3.Location.Y + this.labelArrow3.Size.Height / 2;
					int num2 = (num - this.labelIcStackStart1.Location.Y) / (this.labelIcStackEnd1.Location.Y - this.labelIcStackStart1.Location.Y);
					if (num2 == 0)
					{
						double goal = this.IcStackStartX[this.IcStackPageNum * 2];
						double goal2 = this.IcStackStartY[this.IcStackPageNum * 2];
						this.owner.controler.DPTP(2, 4, goal, goal2, CmdType.CMD_PTP_IMM);
						return;
					}
					if (num2 == 1)
					{
						double goal = this.IcStackEndX[this.IcStackPageNum * 2];
						double goal2 = this.IcStackEndY[this.IcStackPageNum * 2];
						this.owner.controler.DPTP(2, 4, goal, goal2, CmdType.CMD_PTP_IMM);
						return;
					}
				}
				else if (this.labelArrow4.Visible)
				{
					this.owner.controler.WriteSpeed_IMM(this.owner.controler.SpeedValue, 100);
					int num = this.labelArrow4.Location.Y + this.labelArrow4.Size.Height / 2;
					int num2 = (num - this.labelIcStackStart2.Location.Y) / (this.labelIcStackEnd2.Location.Y - this.labelIcStackStart2.Location.Y);
					if (num2 == 0)
					{
						double goal = this.IcStackStartX[this.IcStackPageNum * 2 + 1];
						double goal2 = this.IcStackStartY[this.IcStackPageNum * 2 + 1];
						this.owner.controler.DPTP(2, 4, goal, goal2, CmdType.CMD_PTP_IMM);
						return;
					}
					if (num2 == 1)
					{
						double goal = this.IcStackEndX[this.IcStackPageNum * 2 + 1];
						double goal2 = this.IcStackEndY[this.IcStackPageNum * 2 + 1];
						this.owner.controler.DPTP(2, 4, goal, goal2, CmdType.CMD_PTP_IMM);
					}
				}
				return;
			default:
				return;
			}
		}

		private void buttonGetCur_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			double num = this.owner.controler.PosMM[2];
			double num2 = this.owner.controler.PosMM[4];
			switch (this.tabControl.SelectedIndex)
			{
			case 0:
			{
				int num3 = this.labelArrow1.Location.Y + this.labelArrow1.Size.Height / 2;
				int num4 = (num3 - this.labelLeftNo1.Location.Y) / (this.labelLeftNo2.Location.Y - this.labelLeftNo1.Location.Y);
				this.LeftAndBackStackX[this.LeftPageNum * 10 + num4] = num;
				this.LeftAndBackStackY[this.LeftPageNum * 10 + num4] = num2;
				this.UpdateLeftStackUI();
				return;
			}
			case 1:
			{
				int num3 = this.labelArrow2.Location.Y + this.labelArrow2.Size.Height / 2;
				int num4 = (num3 - this.labelBackNo1.Location.Y) / (this.labelBackNo2.Location.Y - this.labelBackNo1.Location.Y);
				this.LeftAndBackStackX[30 + this.BackPageNum * 10 + num4] = num;
				this.LeftAndBackStackY[30 + this.BackPageNum * 10 + num4] = num2;
				this.UpdateBackStackUI();
				return;
			}
			case 2:
				if (this.labelArrow3.Visible)
				{
					int num3 = this.labelArrow3.Location.Y + this.labelArrow3.Size.Height / 2;
					int num4 = (num3 - this.labelIcStackStart1.Location.Y) / (this.labelIcStackEnd1.Location.Y - this.labelIcStackStart1.Location.Y);
					if (num4 == 0)
					{
						this.IcStackStartX[this.IcStackPageNum * 2] = num;
						this.IcStackStartY[this.IcStackPageNum * 2] = num2;
					}
					else if (num4 == 1)
					{
						this.IcStackEndX[this.IcStackPageNum * 2] = num;
						this.IcStackEndY[this.IcStackPageNum * 2] = num2;
					}
					this.UpdateFrontStackUI();
					return;
				}
				if (this.labelArrow4.Visible)
				{
					int num3 = this.labelArrow4.Location.Y + this.labelArrow4.Size.Height / 2;
					int num4 = (num3 - this.labelIcStackStart2.Location.Y) / (this.labelIcStackEnd2.Location.Y - this.labelIcStackStart2.Location.Y);
					if (num4 == 0)
					{
						this.IcStackStartX[this.IcStackPageNum * 2 + 1] = num;
						this.IcStackStartY[this.IcStackPageNum * 2 + 1] = num2;
					}
					else if (num4 == 1)
					{
						this.IcStackEndX[this.IcStackPageNum * 2 + 1] = num;
						this.IcStackEndY[this.IcStackPageNum * 2 + 1] = num2;
					}
					this.UpdateFrontStackUI();
				}
				return;
			default:
				return;
			}
		}

		private void ParamEdit_FormClosed(object sender, FormClosedEventArgs e)
		{
			this.owner.IsParamEdit = false;
			this.owner.buttonParam.BackgroundImage = Resource.ButtonUp;
		}

		private void buttonNozzle2Test_Click(object sender, EventArgs e)
		{
			Nozzle2Test nozzle2Test = new Nozzle2Test(this.owner);
			nozzle2Test.Location = new Point(base.Location.X + base.Size.Width / 2 - nozzle2Test.Size.Width / 2, base.Location.Y + base.Size.Height / 2 - nozzle2Test.Size.Height / 2);
			nozzle2Test.VisionX = this.owner.VisionX;
			nozzle2Test.VisionY = this.owner.VisionY;
			nozzle2Test.Nozzle2OffsetX = this.owner.NozzleOffsetX;
			nozzle2Test.Nozzle2OffsetY = this.owner.NozzleOffsetY;
			if (nozzle2Test.ShowDialog() == DialogResult.OK)
			{
				this.textBoxNozzleOffsetX.Text = nozzle2Test.Nozzle2OffsetTestX.ToString("F2");
				this.textBoxNozzleOffsetY.Text = nozzle2Test.Nozzle2OffsetTestY.ToString("F2");
			}
		}

		private void buttonVisionOffsetTest_Click(object sender, EventArgs e)
		{
			VisionOffsetTest visionOffsetTest = new VisionOffsetTest(this.owner);
			visionOffsetTest.Location = new Point(base.Location.X + base.Size.Width / 2 - visionOffsetTest.Size.Width / 2, base.Location.Y + base.Size.Height / 2 - visionOffsetTest.Size.Height / 2);
			visionOffsetTest.VisionX = this.owner.VisionX;
			visionOffsetTest.VisionY = this.owner.VisionY;
			double num;
			if (double.TryParse(this.textBoxNozzleOffsetX.Text, out num))
			{
				visionOffsetTest.Nozzle2OffsetX = num;
			}
			else
			{
				visionOffsetTest.Nozzle2OffsetX = this.owner.NozzleOffsetX;
			}
			if (double.TryParse(this.textBoxNozzleOffsetY.Text, out num))
			{
				visionOffsetTest.Nozzle2OffsetY = num;
			}
			else
			{
				visionOffsetTest.Nozzle2OffsetY = this.owner.NozzleOffsetY;
			}
			if (visionOffsetTest.ShowDialog() == DialogResult.OK)
			{
				this.textBoxNozzle1VisionX.Text = visionOffsetTest.Nozzle1CenterOffsetX.ToString("F2");
				this.textBoxNozzle1VisionY.Text = visionOffsetTest.Nozzle1CenterOffsetY.ToString("F2");
				this.textBoxNozzle2VisionX.Text = visionOffsetTest.Nozzle2CenterOffsetX.ToString("F2");
				this.textBoxNozzle2VisionY.Text = visionOffsetTest.Nozzle2CenterOffsetY.ToString("F2");
			}
		}

		private void LoadCameraKey()
		{
			this.comboBoxFeedCamera1.Items.Add(this.owner.CameraKey1.ToString("D9"));
			this.comboBoxFeedCamera1.SelectedIndex = 0;
			this.comboBoxFeedCamera2.Items.Add(this.owner.CameraKey2.ToString("D9"));
			this.comboBoxFeedCamera2.SelectedIndex = 0;
			RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\services\\usbccgp\\Enum");
			if (registryKey != null)
			{
				string[] valueNames = registryKey.GetValueNames();
				string[] array = valueNames;
				for (int i = 0; i < array.Length; i++)
				{
					string name = array[i];
					if (registryKey.GetValueKind(name) == RegistryValueKind.String)
					{
						string text = (string)registryKey.GetValue(name);
						int num = text.Length;
						uint num2 = 0u;
						for (int j = 0; j < 8; j++)
						{
							if (--num >= 0)
							{
								num2 += (uint)((double)(text[num] - '0') * Math.Pow(10.0, (double)j));
							}
						}
						text = num2.ToString("D9");
						if (num2 != this.owner.CameraKey1)
						{
							this.comboBoxFeedCamera1.Items.Add(text);
						}
						if (num2 != this.owner.CameraKey2)
						{
							this.comboBoxFeedCamera2.Items.Add(text);
						}
					}
				}
			}
		}

		private void comboBoxFeedCamera_Click(object sender, EventArgs e)
		{
			this.ResetCamera = true;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager resources = new ComponentResourceManager(typeof(ParamEdit));
			this.textBoxNozzleDownDiscardPos = new TextBox();
			this.label17 = new Label();
			this.textBoxNozzleDownFrontStackPos = new TextBox();
			this.label16 = new Label();
			this.textBoxNozzleDownStackPos = new TextBox();
			this.label15 = new Label();
			this.textBoxNozzleDownPcbPos = new TextBox();
			this.label14 = new Label();
			this.textBoxIpPort = new TextBox();
			this.label13 = new Label();
			this.textBoxIP1 = new TextBox();
			this.label12 = new Label();
			this.textBoxIP2 = new TextBox();
			this.label11 = new Label();
			this.textBoxIP3 = new TextBox();
			this.textBoxIP4 = new TextBox();
			this.label10 = new Label();
			this.label9 = new Label();
			this.tabControl = new TabControl();
			this.tabPage1 = new TabPage();
			this.buttonGetCur1 = new Button();
			this.comboBoxLeftWidth10 = new ComboBox();
			this.buttonMoveTo1 = new Button();
			this.comboBoxLeftWidth9 = new ComboBox();
			this.buttonDown1 = new Button();
			this.buttonUp1 = new Button();
			this.comboBoxLeftWidth8 = new ComboBox();
			this.comboBoxLeftWidth7 = new ComboBox();
			this.comboBoxLeftWidth6 = new ComboBox();
			this.comboBoxLeftWidth5 = new ComboBox();
			this.comboBoxLeftWidth4 = new ComboBox();
			this.comboBoxLeftWidth3 = new ComboBox();
			this.comboBoxLeftWidth2 = new ComboBox();
			this.label23 = new Label();
			this.comboBoxLeftWidth1 = new ComboBox();
			this.labelArrow1 = new Label();
			this.comboBoxLeftFeed10 = new ComboBox();
			this.comboBoxLeftFeed9 = new ComboBox();
			this.comboBoxLeftFeed8 = new ComboBox();
			this.comboBoxLeftFeed7 = new ComboBox();
			this.comboBoxLeftFeed6 = new ComboBox();
			this.comboBoxLeftFeed5 = new ComboBox();
			this.comboBoxLeftFeed4 = new ComboBox();
			this.comboBoxLeftFeed3 = new ComboBox();
			this.comboBoxLeftFeed2 = new ComboBox();
			this.comboBoxLeftFeed1 = new ComboBox();
			this.label19 = new Label();
			this.label20 = new Label();
			this.labelLeftNo10 = new Label();
			this.label21 = new Label();
			this.label22 = new Label();
			this.textBoxLeftY10 = new TextBox();
			this.textBoxLeftX1 = new TextBox();
			this.textBoxLeftX10 = new TextBox();
			this.textBoxLeftY1 = new TextBox();
			this.labelLeftNo9 = new Label();
			this.labelLeftNo1 = new Label();
			this.textBoxLeftY9 = new TextBox();
			this.textBoxLeftX9 = new TextBox();
			this.textBoxLeftX2 = new TextBox();
			this.textBoxLeftY2 = new TextBox();
			this.labelLeftNo8 = new Label();
			this.labelLeftNo2 = new Label();
			this.textBoxLeftY8 = new TextBox();
			this.textBoxLeftX8 = new TextBox();
			this.textBoxLeftX3 = new TextBox();
			this.textBoxLeftY3 = new TextBox();
			this.labelLeftNo3 = new Label();
			this.labelLeftNo7 = new Label();
			this.textBoxLeftX4 = new TextBox();
			this.textBoxLeftY7 = new TextBox();
			this.textBoxLeftY4 = new TextBox();
			this.textBoxLeftX7 = new TextBox();
			this.labelLeftNo4 = new Label();
			this.labelLeftNo6 = new Label();
			this.textBoxLeftX5 = new TextBox();
			this.textBoxLeftY6 = new TextBox();
			this.textBoxLeftY5 = new TextBox();
			this.textBoxLeftX6 = new TextBox();
			this.labelLeftNo5 = new Label();
			this.tabPage2 = new TabPage();
			this.buttonGetCur2 = new Button();
			this.comboBoxBackWidth10 = new ComboBox();
			this.buttonMoveTo2 = new Button();
			this.comboBoxBackWidth9 = new ComboBox();
			this.buttonDown2 = new Button();
			this.buttonUp2 = new Button();
			this.comboBoxBackWidth8 = new ComboBox();
			this.comboBoxBackWidth7 = new ComboBox();
			this.comboBoxBackWidth6 = new ComboBox();
			this.comboBoxBackWidth5 = new ComboBox();
			this.comboBoxBackWidth4 = new ComboBox();
			this.comboBoxBackWidth3 = new ComboBox();
			this.comboBoxBackWidth2 = new ComboBox();
			this.label33 = new Label();
			this.comboBoxBackWidth1 = new ComboBox();
			this.labelArrow2 = new Label();
			this.comboBoxBackFeed10 = new ComboBox();
			this.comboBoxBackFeed9 = new ComboBox();
			this.comboBoxBackFeed8 = new ComboBox();
			this.comboBoxBackFeed7 = new ComboBox();
			this.comboBoxBackFeed6 = new ComboBox();
			this.comboBoxBackFeed5 = new ComboBox();
			this.comboBoxBackFeed4 = new ComboBox();
			this.comboBoxBackFeed3 = new ComboBox();
			this.comboBoxBackFeed2 = new ComboBox();
			this.comboBoxBackFeed1 = new ComboBox();
			this.label35 = new Label();
			this.label36 = new Label();
			this.labelBackNo10 = new Label();
			this.label38 = new Label();
			this.label39 = new Label();
			this.textBoxBackY10 = new TextBox();
			this.textBoxBackX1 = new TextBox();
			this.textBoxBackX10 = new TextBox();
			this.textBoxBackY1 = new TextBox();
			this.labelBackNo9 = new Label();
			this.labelBackNo1 = new Label();
			this.textBoxBackY9 = new TextBox();
			this.textBoxBackX9 = new TextBox();
			this.textBoxBackX2 = new TextBox();
			this.textBoxBackY2 = new TextBox();
			this.labelBackNo8 = new Label();
			this.labelBackNo2 = new Label();
			this.textBoxBackY8 = new TextBox();
			this.textBoxBackX8 = new TextBox();
			this.textBoxBackX3 = new TextBox();
			this.textBoxBackY3 = new TextBox();
			this.labelBackNo3 = new Label();
			this.labelBackNo7 = new Label();
			this.textBoxBackX4 = new TextBox();
			this.textBoxBackY7 = new TextBox();
			this.textBoxBackY4 = new TextBox();
			this.textBoxBackX7 = new TextBox();
			this.labelBackNo4 = new Label();
			this.labelBackNo6 = new Label();
			this.textBoxBackX5 = new TextBox();
			this.textBoxBackY6 = new TextBox();
			this.textBoxBackY5 = new TextBox();
			this.textBoxBackX6 = new TextBox();
			this.labelBackNo5 = new Label();
			this.tabPage3 = new TabPage();
			this.groupBoxIcStack2 = new GroupBox();
			this.labelArrow4 = new Label();
			this.textBoxIcStackArrayY2 = new TextBox();
			this.labelIcStackArray2 = new Label();
			this.textBoxIcStackArrayX2 = new TextBox();
			this.labelIcStackEnd2 = new Label();
			this.textBoxIcStackEndY2 = new TextBox();
			this.textBoxIcStackEndX2 = new TextBox();
			this.label40 = new Label();
			this.labelIcStackStart2 = new Label();
			this.label42 = new Label();
			this.textBoxIcStackStartY2 = new TextBox();
			this.textBoxIcStackStartX2 = new TextBox();
			this.groupBoxIcStack1 = new GroupBox();
			this.textBoxIcStackArrayY1 = new TextBox();
			this.labelIcStackArray1 = new Label();
			this.textBoxIcStackArrayX1 = new TextBox();
			this.labelIcStackEnd1 = new Label();
			this.textBoxIcStackEndY1 = new TextBox();
			this.labelArrow3 = new Label();
			this.textBoxIcStackEndX1 = new TextBox();
			this.label25 = new Label();
			this.labelIcStackStart1 = new Label();
			this.label26 = new Label();
			this.textBoxIcStackStartY1 = new TextBox();
			this.textBoxIcStackStartX1 = new TextBox();
			this.buttonGetCur3 = new Button();
			this.buttonMoveTo3 = new Button();
			this.buttonDown3 = new Button();
			this.buttonUp3 = new Button();
			this.tabPage4 = new TabPage();
			this.groupBox2 = new GroupBox();
			this.comboBoxBlowingDealy2 = new ComboBox();
			this.comboBoxBlowingDealy1 = new ComboBox();
			this.label5 = new Label();
			this.label6 = new Label();
			this.buttonVisionOffsetTest = new Button();
			this.label4 = new Label();
			this.label3 = new Label();
			this.textBoxNozzle2VisionY = new TextBox();
			this.textBoxNozzle2VisionX = new TextBox();
			this.textBoxNozzle1VisionY = new TextBox();
			this.textBoxNozzle1VisionX = new TextBox();
			this.buttonNozzle2Test = new Button();
			this.groupBox1 = new GroupBox();
			this.label32 = new Label();
			this.label31 = new Label();
			this.textBoxPcbDealy2 = new TextBox();
			this.textBoxPcbDealy1 = new TextBox();
			this.textBoxStackDealy = new TextBox();
			this.label30 = new Label();
			this.textBoxNozzleOffsetY = new TextBox();
			this.textBoxNozzleOffsetX = new TextBox();
			this.labelNozzleOffset = new Label();
			this.tabPage5 = new TabPage();
			this.groupBox4 = new GroupBox();
			this.textBoxVisionZ = new TextBox();
			this.label18 = new Label();
			this.textBoxVisionY = new TextBox();
			this.label27 = new Label();
			this.textBoxVisionX = new TextBox();
			this.label24 = new Label();
			this.label8 = new Label();
			this.textBoxPixelScaleY1 = new TextBox();
			this.textBoxPixelScaleX1 = new TextBox();
			this.comboBoxFeedCamera1 = new ComboBox();
			this.label2 = new Label();
			this.textBoxVisionValueComp = new TextBox();
			this.textBoxCameraAngle1 = new TextBox();
			this.groupBox3 = new GroupBox();
			this.label7 = new Label();
			this.label29 = new Label();
			this.label1 = new Label();
			this.textBoxVisionOffsetX = new TextBox();
			this.comboBoxFeedCamera2 = new ComboBox();
			this.textBoxVisionOffsetY = new TextBox();
			this.textBoxPixelScaleY2 = new TextBox();
			this.textBoxPixelScaleX2 = new TextBox();
			this.textBoxVisionValueNozzle = new TextBox();
			this.labelVisionValue = new Label();
			this.tabPage6 = new TabPage();
			this.textBoxBackPrickOffsetY = new TextBox();
			this.textBoxBackPrickOffsetX = new TextBox();
			this.label34 = new Label();
			this.textBoxLeftPrickOffsetY = new TextBox();
			this.textBoxLeftPrickOffsetX = new TextBox();
			this.label28 = new Label();
			this.checkBoxPressureCheck = new CheckBox();
			this.buttonCancel = new Button();
			this.buttonSave = new Button();
			this.tabControl.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.groupBoxIcStack2.SuspendLayout();
			this.groupBoxIcStack1.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tabPage5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.tabPage6.SuspendLayout();
			base.SuspendLayout();
			resources.ApplyResources(this.textBoxNozzleDownDiscardPos, "textBoxNozzleDownDiscardPos");
			this.textBoxNozzleDownDiscardPos.Name = "textBoxNozzleDownDiscardPos";
			resources.ApplyResources(this.label17, "label17");
			this.label17.Name = "label17";
			resources.ApplyResources(this.textBoxNozzleDownFrontStackPos, "textBoxNozzleDownFrontStackPos");
			this.textBoxNozzleDownFrontStackPos.Name = "textBoxNozzleDownFrontStackPos";
			resources.ApplyResources(this.label16, "label16");
			this.label16.Name = "label16";
			resources.ApplyResources(this.textBoxNozzleDownStackPos, "textBoxNozzleDownStackPos");
			this.textBoxNozzleDownStackPos.Name = "textBoxNozzleDownStackPos";
			resources.ApplyResources(this.label15, "label15");
			this.label15.Name = "label15";
			resources.ApplyResources(this.textBoxNozzleDownPcbPos, "textBoxNozzleDownPcbPos");
			this.textBoxNozzleDownPcbPos.Name = "textBoxNozzleDownPcbPos";
			resources.ApplyResources(this.label14, "label14");
			this.label14.Name = "label14";
			resources.ApplyResources(this.textBoxIpPort, "textBoxIpPort");
			this.textBoxIpPort.Name = "textBoxIpPort";
			resources.ApplyResources(this.label13, "label13");
			this.label13.Name = "label13";
			resources.ApplyResources(this.textBoxIP1, "textBoxIP1");
			this.textBoxIP1.Name = "textBoxIP1";
			resources.ApplyResources(this.label12, "label12");
			this.label12.Name = "label12";
			resources.ApplyResources(this.textBoxIP2, "textBoxIP2");
			this.textBoxIP2.Name = "textBoxIP2";
			resources.ApplyResources(this.label11, "label11");
			this.label11.Name = "label11";
			resources.ApplyResources(this.textBoxIP3, "textBoxIP3");
			this.textBoxIP3.Name = "textBoxIP3";
			resources.ApplyResources(this.textBoxIP4, "textBoxIP4");
			this.textBoxIP4.Name = "textBoxIP4";
			resources.ApplyResources(this.label10, "label10");
			this.label10.Name = "label10";
			resources.ApplyResources(this.label9, "label9");
			this.label9.Name = "label9";
			this.tabControl.Controls.Add(this.tabPage1);
			this.tabControl.Controls.Add(this.tabPage2);
			this.tabControl.Controls.Add(this.tabPage3);
			this.tabControl.Controls.Add(this.tabPage4);
			this.tabControl.Controls.Add(this.tabPage5);
			this.tabControl.Controls.Add(this.tabPage6);
			resources.ApplyResources(this.tabControl, "tabControl");
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabPage1.Controls.Add(this.buttonGetCur1);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth10);
			this.tabPage1.Controls.Add(this.buttonMoveTo1);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth9);
			this.tabPage1.Controls.Add(this.buttonDown1);
			this.tabPage1.Controls.Add(this.buttonUp1);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth8);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth7);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth6);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth5);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth4);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth3);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth2);
			this.tabPage1.Controls.Add(this.label23);
			this.tabPage1.Controls.Add(this.comboBoxLeftWidth1);
			this.tabPage1.Controls.Add(this.labelArrow1);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed10);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed9);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed8);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed7);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed6);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed5);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed4);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed3);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed2);
			this.tabPage1.Controls.Add(this.comboBoxLeftFeed1);
			this.tabPage1.Controls.Add(this.label19);
			this.tabPage1.Controls.Add(this.label20);
			this.tabPage1.Controls.Add(this.labelLeftNo10);
			this.tabPage1.Controls.Add(this.label21);
			this.tabPage1.Controls.Add(this.label22);
			this.tabPage1.Controls.Add(this.textBoxLeftY10);
			this.tabPage1.Controls.Add(this.textBoxLeftX1);
			this.tabPage1.Controls.Add(this.textBoxLeftX10);
			this.tabPage1.Controls.Add(this.textBoxLeftY1);
			this.tabPage1.Controls.Add(this.labelLeftNo9);
			this.tabPage1.Controls.Add(this.labelLeftNo1);
			this.tabPage1.Controls.Add(this.textBoxLeftY9);
			this.tabPage1.Controls.Add(this.textBoxLeftX9);
			this.tabPage1.Controls.Add(this.textBoxLeftX2);
			this.tabPage1.Controls.Add(this.textBoxLeftY2);
			this.tabPage1.Controls.Add(this.labelLeftNo8);
			this.tabPage1.Controls.Add(this.labelLeftNo2);
			this.tabPage1.Controls.Add(this.textBoxLeftY8);
			this.tabPage1.Controls.Add(this.textBoxLeftX8);
			this.tabPage1.Controls.Add(this.textBoxLeftX3);
			this.tabPage1.Controls.Add(this.textBoxLeftY3);
			this.tabPage1.Controls.Add(this.labelLeftNo3);
			this.tabPage1.Controls.Add(this.labelLeftNo7);
			this.tabPage1.Controls.Add(this.textBoxLeftX4);
			this.tabPage1.Controls.Add(this.textBoxLeftY7);
			this.tabPage1.Controls.Add(this.textBoxLeftY4);
			this.tabPage1.Controls.Add(this.textBoxLeftX7);
			this.tabPage1.Controls.Add(this.labelLeftNo4);
			this.tabPage1.Controls.Add(this.labelLeftNo6);
			this.tabPage1.Controls.Add(this.textBoxLeftX5);
			this.tabPage1.Controls.Add(this.textBoxLeftY6);
			this.tabPage1.Controls.Add(this.textBoxLeftY5);
			this.tabPage1.Controls.Add(this.textBoxLeftX6);
			this.tabPage1.Controls.Add(this.labelLeftNo5);
			resources.ApplyResources(this.tabPage1, "tabPage1");
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.buttonGetCur1, "buttonGetCur1");
			this.buttonGetCur1.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur1.Name = "buttonGetCur1";
			this.buttonGetCur1.UseVisualStyleBackColor = true;
			this.buttonGetCur1.Click += new EventHandler(this.buttonGetCur_Click);
			this.comboBoxLeftWidth10.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth10, "comboBoxLeftWidth10");
			this.comboBoxLeftWidth10.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth10.Items"),
				resources.GetString("comboBoxLeftWidth10.Items1"),
				resources.GetString("comboBoxLeftWidth10.Items2"),
				resources.GetString("comboBoxLeftWidth10.Items3")
			});
			this.comboBoxLeftWidth10.Name = "comboBoxLeftWidth10";
			this.comboBoxLeftWidth10.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth10.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			resources.ApplyResources(this.buttonMoveTo1, "buttonMoveTo1");
			this.buttonMoveTo1.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo1.Name = "buttonMoveTo1";
			this.buttonMoveTo1.UseVisualStyleBackColor = true;
			this.buttonMoveTo1.Click += new EventHandler(this.buttonMoveTo_Click);
			this.comboBoxLeftWidth9.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth9, "comboBoxLeftWidth9");
			this.comboBoxLeftWidth9.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth9.Items"),
				resources.GetString("comboBoxLeftWidth9.Items1"),
				resources.GetString("comboBoxLeftWidth9.Items2"),
				resources.GetString("comboBoxLeftWidth9.Items3")
			});
			this.comboBoxLeftWidth9.Name = "comboBoxLeftWidth9";
			this.comboBoxLeftWidth9.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth9.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			resources.ApplyResources(this.buttonDown1, "buttonDown1");
			this.buttonDown1.BackgroundImage = Resource.ButtonUp;
			this.buttonDown1.Name = "buttonDown1";
			this.buttonDown1.UseVisualStyleBackColor = true;
			this.buttonDown1.Click += new EventHandler(this.buttonDown1_Click);
			resources.ApplyResources(this.buttonUp1, "buttonUp1");
			this.buttonUp1.BackgroundImage = Resource.ButtonUp;
			this.buttonUp1.Name = "buttonUp1";
			this.buttonUp1.UseVisualStyleBackColor = true;
			this.buttonUp1.Click += new EventHandler(this.buttonUp1_Click);
			this.comboBoxLeftWidth8.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth8, "comboBoxLeftWidth8");
			this.comboBoxLeftWidth8.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth8.Items"),
				resources.GetString("comboBoxLeftWidth8.Items1"),
				resources.GetString("comboBoxLeftWidth8.Items2"),
				resources.GetString("comboBoxLeftWidth8.Items3")
			});
			this.comboBoxLeftWidth8.Name = "comboBoxLeftWidth8";
			this.comboBoxLeftWidth8.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth8.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			this.comboBoxLeftWidth7.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth7, "comboBoxLeftWidth7");
			this.comboBoxLeftWidth7.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth7.Items"),
				resources.GetString("comboBoxLeftWidth7.Items1"),
				resources.GetString("comboBoxLeftWidth7.Items2"),
				resources.GetString("comboBoxLeftWidth7.Items3")
			});
			this.comboBoxLeftWidth7.Name = "comboBoxLeftWidth7";
			this.comboBoxLeftWidth7.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth7.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			this.comboBoxLeftWidth6.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth6, "comboBoxLeftWidth6");
			this.comboBoxLeftWidth6.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth6.Items"),
				resources.GetString("comboBoxLeftWidth6.Items1"),
				resources.GetString("comboBoxLeftWidth6.Items2"),
				resources.GetString("comboBoxLeftWidth6.Items3")
			});
			this.comboBoxLeftWidth6.Name = "comboBoxLeftWidth6";
			this.comboBoxLeftWidth6.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth6.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			this.comboBoxLeftWidth5.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth5, "comboBoxLeftWidth5");
			this.comboBoxLeftWidth5.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth5.Items"),
				resources.GetString("comboBoxLeftWidth5.Items1"),
				resources.GetString("comboBoxLeftWidth5.Items2"),
				resources.GetString("comboBoxLeftWidth5.Items3")
			});
			this.comboBoxLeftWidth5.Name = "comboBoxLeftWidth5";
			this.comboBoxLeftWidth5.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth5.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			this.comboBoxLeftWidth4.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth4, "comboBoxLeftWidth4");
			this.comboBoxLeftWidth4.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth4.Items"),
				resources.GetString("comboBoxLeftWidth4.Items1"),
				resources.GetString("comboBoxLeftWidth4.Items2"),
				resources.GetString("comboBoxLeftWidth4.Items3")
			});
			this.comboBoxLeftWidth4.Name = "comboBoxLeftWidth4";
			this.comboBoxLeftWidth4.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth4.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			this.comboBoxLeftWidth3.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth3, "comboBoxLeftWidth3");
			this.comboBoxLeftWidth3.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth3.Items"),
				resources.GetString("comboBoxLeftWidth3.Items1"),
				resources.GetString("comboBoxLeftWidth3.Items2"),
				resources.GetString("comboBoxLeftWidth3.Items3")
			});
			this.comboBoxLeftWidth3.Name = "comboBoxLeftWidth3";
			this.comboBoxLeftWidth3.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth3.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			this.comboBoxLeftWidth2.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth2, "comboBoxLeftWidth2");
			this.comboBoxLeftWidth2.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth2.Items"),
				resources.GetString("comboBoxLeftWidth2.Items1"),
				resources.GetString("comboBoxLeftWidth2.Items2"),
				resources.GetString("comboBoxLeftWidth2.Items3")
			});
			this.comboBoxLeftWidth2.Name = "comboBoxLeftWidth2";
			this.comboBoxLeftWidth2.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth2.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			resources.ApplyResources(this.label23, "label23");
			this.label23.Name = "label23";
			this.comboBoxLeftWidth1.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftWidth1, "comboBoxLeftWidth1");
			this.comboBoxLeftWidth1.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftWidth1.Items"),
				resources.GetString("comboBoxLeftWidth1.Items1"),
				resources.GetString("comboBoxLeftWidth1.Items2"),
				resources.GetString("comboBoxLeftWidth1.Items3")
			});
			this.comboBoxLeftWidth1.Name = "comboBoxLeftWidth1";
			this.comboBoxLeftWidth1.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftWidth1.Leave += new EventHandler(this.comboBoxLeftWidth_Leave);
			this.labelArrow1.Image = Resource.Arrow;
			resources.ApplyResources(this.labelArrow1, "labelArrow1");
			this.labelArrow1.Name = "labelArrow1";
			this.comboBoxLeftFeed10.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed10, "comboBoxLeftFeed10");
			this.comboBoxLeftFeed10.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed10.Items"),
				resources.GetString("comboBoxLeftFeed10.Items1"),
				resources.GetString("comboBoxLeftFeed10.Items2"),
				resources.GetString("comboBoxLeftFeed10.Items3")
			});
			this.comboBoxLeftFeed10.Name = "comboBoxLeftFeed10";
			this.comboBoxLeftFeed10.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed10.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed9.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed9, "comboBoxLeftFeed9");
			this.comboBoxLeftFeed9.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed9.Items"),
				resources.GetString("comboBoxLeftFeed9.Items1"),
				resources.GetString("comboBoxLeftFeed9.Items2"),
				resources.GetString("comboBoxLeftFeed9.Items3")
			});
			this.comboBoxLeftFeed9.Name = "comboBoxLeftFeed9";
			this.comboBoxLeftFeed9.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed9.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed8.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed8, "comboBoxLeftFeed8");
			this.comboBoxLeftFeed8.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed8.Items"),
				resources.GetString("comboBoxLeftFeed8.Items1"),
				resources.GetString("comboBoxLeftFeed8.Items2"),
				resources.GetString("comboBoxLeftFeed8.Items3")
			});
			this.comboBoxLeftFeed8.Name = "comboBoxLeftFeed8";
			this.comboBoxLeftFeed8.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed8.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed7.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed7, "comboBoxLeftFeed7");
			this.comboBoxLeftFeed7.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed7.Items"),
				resources.GetString("comboBoxLeftFeed7.Items1"),
				resources.GetString("comboBoxLeftFeed7.Items2"),
				resources.GetString("comboBoxLeftFeed7.Items3")
			});
			this.comboBoxLeftFeed7.Name = "comboBoxLeftFeed7";
			this.comboBoxLeftFeed7.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed7.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed6.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed6, "comboBoxLeftFeed6");
			this.comboBoxLeftFeed6.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed6.Items"),
				resources.GetString("comboBoxLeftFeed6.Items1"),
				resources.GetString("comboBoxLeftFeed6.Items2"),
				resources.GetString("comboBoxLeftFeed6.Items3")
			});
			this.comboBoxLeftFeed6.Name = "comboBoxLeftFeed6";
			this.comboBoxLeftFeed6.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed6.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed5.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed5, "comboBoxLeftFeed5");
			this.comboBoxLeftFeed5.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed5.Items"),
				resources.GetString("comboBoxLeftFeed5.Items1"),
				resources.GetString("comboBoxLeftFeed5.Items2"),
				resources.GetString("comboBoxLeftFeed5.Items3")
			});
			this.comboBoxLeftFeed5.Name = "comboBoxLeftFeed5";
			this.comboBoxLeftFeed5.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed5.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed4.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed4, "comboBoxLeftFeed4");
			this.comboBoxLeftFeed4.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed4.Items"),
				resources.GetString("comboBoxLeftFeed4.Items1"),
				resources.GetString("comboBoxLeftFeed4.Items2"),
				resources.GetString("comboBoxLeftFeed4.Items3")
			});
			this.comboBoxLeftFeed4.Name = "comboBoxLeftFeed4";
			this.comboBoxLeftFeed4.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed4.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed3.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed3, "comboBoxLeftFeed3");
			this.comboBoxLeftFeed3.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed3.Items"),
				resources.GetString("comboBoxLeftFeed3.Items1"),
				resources.GetString("comboBoxLeftFeed3.Items2"),
				resources.GetString("comboBoxLeftFeed3.Items3")
			});
			this.comboBoxLeftFeed3.Name = "comboBoxLeftFeed3";
			this.comboBoxLeftFeed3.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed3.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed2.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed2, "comboBoxLeftFeed2");
			this.comboBoxLeftFeed2.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed2.Items"),
				resources.GetString("comboBoxLeftFeed2.Items1"),
				resources.GetString("comboBoxLeftFeed2.Items2"),
				resources.GetString("comboBoxLeftFeed2.Items3")
			});
			this.comboBoxLeftFeed2.Name = "comboBoxLeftFeed2";
			this.comboBoxLeftFeed2.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed2.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			this.comboBoxLeftFeed1.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxLeftFeed1, "comboBoxLeftFeed1");
			this.comboBoxLeftFeed1.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxLeftFeed1.Items"),
				resources.GetString("comboBoxLeftFeed1.Items1"),
				resources.GetString("comboBoxLeftFeed1.Items2"),
				resources.GetString("comboBoxLeftFeed1.Items3")
			});
			this.comboBoxLeftFeed1.Name = "comboBoxLeftFeed1";
			this.comboBoxLeftFeed1.Enter += new EventHandler(this.LeftArrow_Click);
			this.comboBoxLeftFeed1.Leave += new EventHandler(this.comboBoxLeftFeed_Leave);
			resources.ApplyResources(this.label19, "label19");
			this.label19.Name = "label19";
			resources.ApplyResources(this.label20, "label20");
			this.label20.Name = "label20";
			resources.ApplyResources(this.labelLeftNo10, "labelLeftNo10");
			this.labelLeftNo10.Name = "labelLeftNo10";
			this.labelLeftNo10.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.label21, "label21");
			this.label21.Name = "label21";
			resources.ApplyResources(this.label22, "label22");
			this.label22.Name = "label22";
			resources.ApplyResources(this.textBoxLeftY10, "textBoxLeftY10");
			this.textBoxLeftY10.Name = "textBoxLeftY10";
			this.textBoxLeftY10.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY10.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.textBoxLeftX1, "textBoxLeftX1");
			this.textBoxLeftX1.Name = "textBoxLeftX1";
			this.textBoxLeftX1.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX1.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftX10, "textBoxLeftX10");
			this.textBoxLeftX10.Name = "textBoxLeftX10";
			this.textBoxLeftX10.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX10.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftY1, "textBoxLeftY1");
			this.textBoxLeftY1.Name = "textBoxLeftY1";
			this.textBoxLeftY1.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY1.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.labelLeftNo9, "labelLeftNo9");
			this.labelLeftNo9.Name = "labelLeftNo9";
			this.labelLeftNo9.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.labelLeftNo1, "labelLeftNo1");
			this.labelLeftNo1.Name = "labelLeftNo1";
			this.labelLeftNo1.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.textBoxLeftY9, "textBoxLeftY9");
			this.textBoxLeftY9.Name = "textBoxLeftY9";
			this.textBoxLeftY9.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY9.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.textBoxLeftX9, "textBoxLeftX9");
			this.textBoxLeftX9.Name = "textBoxLeftX9";
			this.textBoxLeftX9.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX9.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftX2, "textBoxLeftX2");
			this.textBoxLeftX2.Name = "textBoxLeftX2";
			this.textBoxLeftX2.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX2.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftY2, "textBoxLeftY2");
			this.textBoxLeftY2.Name = "textBoxLeftY2";
			this.textBoxLeftY2.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY2.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.labelLeftNo8, "labelLeftNo8");
			this.labelLeftNo8.Name = "labelLeftNo8";
			this.labelLeftNo8.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.labelLeftNo2, "labelLeftNo2");
			this.labelLeftNo2.Name = "labelLeftNo2";
			this.labelLeftNo2.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.textBoxLeftY8, "textBoxLeftY8");
			this.textBoxLeftY8.Name = "textBoxLeftY8";
			this.textBoxLeftY8.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY8.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.textBoxLeftX8, "textBoxLeftX8");
			this.textBoxLeftX8.Name = "textBoxLeftX8";
			this.textBoxLeftX8.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX8.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftX3, "textBoxLeftX3");
			this.textBoxLeftX3.Name = "textBoxLeftX3";
			this.textBoxLeftX3.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX3.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftY3, "textBoxLeftY3");
			this.textBoxLeftY3.Name = "textBoxLeftY3";
			this.textBoxLeftY3.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY3.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.labelLeftNo3, "labelLeftNo3");
			this.labelLeftNo3.Name = "labelLeftNo3";
			this.labelLeftNo3.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.labelLeftNo7, "labelLeftNo7");
			this.labelLeftNo7.Name = "labelLeftNo7";
			this.labelLeftNo7.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.textBoxLeftX4, "textBoxLeftX4");
			this.textBoxLeftX4.Name = "textBoxLeftX4";
			this.textBoxLeftX4.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX4.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftY7, "textBoxLeftY7");
			this.textBoxLeftY7.Name = "textBoxLeftY7";
			this.textBoxLeftY7.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY7.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.textBoxLeftY4, "textBoxLeftY4");
			this.textBoxLeftY4.Name = "textBoxLeftY4";
			this.textBoxLeftY4.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY4.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.textBoxLeftX7, "textBoxLeftX7");
			this.textBoxLeftX7.Name = "textBoxLeftX7";
			this.textBoxLeftX7.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX7.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.labelLeftNo4, "labelLeftNo4");
			this.labelLeftNo4.Name = "labelLeftNo4";
			this.labelLeftNo4.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.labelLeftNo6, "labelLeftNo6");
			this.labelLeftNo6.Name = "labelLeftNo6";
			this.labelLeftNo6.Click += new EventHandler(this.LeftArrow_Click);
			resources.ApplyResources(this.textBoxLeftX5, "textBoxLeftX5");
			this.textBoxLeftX5.Name = "textBoxLeftX5";
			this.textBoxLeftX5.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX5.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.textBoxLeftY6, "textBoxLeftY6");
			this.textBoxLeftY6.Name = "textBoxLeftY6";
			this.textBoxLeftY6.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY6.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.textBoxLeftY5, "textBoxLeftY5");
			this.textBoxLeftY5.Name = "textBoxLeftY5";
			this.textBoxLeftY5.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftY5.Leave += new EventHandler(this.textBoxLeftY_Leave);
			resources.ApplyResources(this.textBoxLeftX6, "textBoxLeftX6");
			this.textBoxLeftX6.Name = "textBoxLeftX6";
			this.textBoxLeftX6.Enter += new EventHandler(this.LeftArrow_Click);
			this.textBoxLeftX6.Leave += new EventHandler(this.textBoxLeftX_Leave);
			resources.ApplyResources(this.labelLeftNo5, "labelLeftNo5");
			this.labelLeftNo5.Name = "labelLeftNo5";
			this.labelLeftNo5.Click += new EventHandler(this.LeftArrow_Click);
			this.tabPage2.Controls.Add(this.buttonGetCur2);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth10);
			this.tabPage2.Controls.Add(this.buttonMoveTo2);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth9);
			this.tabPage2.Controls.Add(this.buttonDown2);
			this.tabPage2.Controls.Add(this.buttonUp2);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth8);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth7);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth6);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth5);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth4);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth3);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth2);
			this.tabPage2.Controls.Add(this.label33);
			this.tabPage2.Controls.Add(this.comboBoxBackWidth1);
			this.tabPage2.Controls.Add(this.labelArrow2);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed10);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed9);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed8);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed7);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed6);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed5);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed4);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed3);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed2);
			this.tabPage2.Controls.Add(this.comboBoxBackFeed1);
			this.tabPage2.Controls.Add(this.label35);
			this.tabPage2.Controls.Add(this.label36);
			this.tabPage2.Controls.Add(this.labelBackNo10);
			this.tabPage2.Controls.Add(this.label38);
			this.tabPage2.Controls.Add(this.label39);
			this.tabPage2.Controls.Add(this.textBoxBackY10);
			this.tabPage2.Controls.Add(this.textBoxBackX1);
			this.tabPage2.Controls.Add(this.textBoxBackX10);
			this.tabPage2.Controls.Add(this.textBoxBackY1);
			this.tabPage2.Controls.Add(this.labelBackNo9);
			this.tabPage2.Controls.Add(this.labelBackNo1);
			this.tabPage2.Controls.Add(this.textBoxBackY9);
			this.tabPage2.Controls.Add(this.textBoxBackX9);
			this.tabPage2.Controls.Add(this.textBoxBackX2);
			this.tabPage2.Controls.Add(this.textBoxBackY2);
			this.tabPage2.Controls.Add(this.labelBackNo8);
			this.tabPage2.Controls.Add(this.labelBackNo2);
			this.tabPage2.Controls.Add(this.textBoxBackY8);
			this.tabPage2.Controls.Add(this.textBoxBackX8);
			this.tabPage2.Controls.Add(this.textBoxBackX3);
			this.tabPage2.Controls.Add(this.textBoxBackY3);
			this.tabPage2.Controls.Add(this.labelBackNo3);
			this.tabPage2.Controls.Add(this.labelBackNo7);
			this.tabPage2.Controls.Add(this.textBoxBackX4);
			this.tabPage2.Controls.Add(this.textBoxBackY7);
			this.tabPage2.Controls.Add(this.textBoxBackY4);
			this.tabPage2.Controls.Add(this.textBoxBackX7);
			this.tabPage2.Controls.Add(this.labelBackNo4);
			this.tabPage2.Controls.Add(this.labelBackNo6);
			this.tabPage2.Controls.Add(this.textBoxBackX5);
			this.tabPage2.Controls.Add(this.textBoxBackY6);
			this.tabPage2.Controls.Add(this.textBoxBackY5);
			this.tabPage2.Controls.Add(this.textBoxBackX6);
			this.tabPage2.Controls.Add(this.labelBackNo5);
			resources.ApplyResources(this.tabPage2, "tabPage2");
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.buttonGetCur2, "buttonGetCur2");
			this.buttonGetCur2.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur2.Name = "buttonGetCur2";
			this.buttonGetCur2.UseVisualStyleBackColor = true;
			this.buttonGetCur2.Click += new EventHandler(this.buttonGetCur_Click);
			this.comboBoxBackWidth10.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth10, "comboBoxBackWidth10");
			this.comboBoxBackWidth10.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth10.Items"),
				resources.GetString("comboBoxBackWidth10.Items1"),
				resources.GetString("comboBoxBackWidth10.Items2"),
				resources.GetString("comboBoxBackWidth10.Items3")
			});
			this.comboBoxBackWidth10.Name = "comboBoxBackWidth10";
			this.comboBoxBackWidth10.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth10.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			resources.ApplyResources(this.buttonMoveTo2, "buttonMoveTo2");
			this.buttonMoveTo2.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo2.Name = "buttonMoveTo2";
			this.buttonMoveTo2.UseVisualStyleBackColor = true;
			this.buttonMoveTo2.Click += new EventHandler(this.buttonMoveTo_Click);
			this.comboBoxBackWidth9.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth9, "comboBoxBackWidth9");
			this.comboBoxBackWidth9.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth9.Items"),
				resources.GetString("comboBoxBackWidth9.Items1"),
				resources.GetString("comboBoxBackWidth9.Items2"),
				resources.GetString("comboBoxBackWidth9.Items3")
			});
			this.comboBoxBackWidth9.Name = "comboBoxBackWidth9";
			this.comboBoxBackWidth9.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth9.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			resources.ApplyResources(this.buttonDown2, "buttonDown2");
			this.buttonDown2.BackgroundImage = Resource.ButtonUp;
			this.buttonDown2.Name = "buttonDown2";
			this.buttonDown2.UseVisualStyleBackColor = true;
			this.buttonDown2.Click += new EventHandler(this.buttonDown2_Click);
			resources.ApplyResources(this.buttonUp2, "buttonUp2");
			this.buttonUp2.BackgroundImage = Resource.ButtonUp;
			this.buttonUp2.Name = "buttonUp2";
			this.buttonUp2.UseVisualStyleBackColor = true;
			this.buttonUp2.Click += new EventHandler(this.buttonUp2_Click);
			this.comboBoxBackWidth8.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth8, "comboBoxBackWidth8");
			this.comboBoxBackWidth8.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth8.Items"),
				resources.GetString("comboBoxBackWidth8.Items1"),
				resources.GetString("comboBoxBackWidth8.Items2"),
				resources.GetString("comboBoxBackWidth8.Items3")
			});
			this.comboBoxBackWidth8.Name = "comboBoxBackWidth8";
			this.comboBoxBackWidth8.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth8.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			this.comboBoxBackWidth7.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth7, "comboBoxBackWidth7");
			this.comboBoxBackWidth7.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth7.Items"),
				resources.GetString("comboBoxBackWidth7.Items1"),
				resources.GetString("comboBoxBackWidth7.Items2"),
				resources.GetString("comboBoxBackWidth7.Items3")
			});
			this.comboBoxBackWidth7.Name = "comboBoxBackWidth7";
			this.comboBoxBackWidth7.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth7.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			this.comboBoxBackWidth6.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth6, "comboBoxBackWidth6");
			this.comboBoxBackWidth6.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth6.Items"),
				resources.GetString("comboBoxBackWidth6.Items1"),
				resources.GetString("comboBoxBackWidth6.Items2"),
				resources.GetString("comboBoxBackWidth6.Items3")
			});
			this.comboBoxBackWidth6.Name = "comboBoxBackWidth6";
			this.comboBoxBackWidth6.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth6.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			this.comboBoxBackWidth5.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth5, "comboBoxBackWidth5");
			this.comboBoxBackWidth5.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth5.Items"),
				resources.GetString("comboBoxBackWidth5.Items1"),
				resources.GetString("comboBoxBackWidth5.Items2"),
				resources.GetString("comboBoxBackWidth5.Items3")
			});
			this.comboBoxBackWidth5.Name = "comboBoxBackWidth5";
			this.comboBoxBackWidth5.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth5.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			this.comboBoxBackWidth4.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth4, "comboBoxBackWidth4");
			this.comboBoxBackWidth4.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth4.Items"),
				resources.GetString("comboBoxBackWidth4.Items1"),
				resources.GetString("comboBoxBackWidth4.Items2"),
				resources.GetString("comboBoxBackWidth4.Items3")
			});
			this.comboBoxBackWidth4.Name = "comboBoxBackWidth4";
			this.comboBoxBackWidth4.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth4.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			this.comboBoxBackWidth3.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth3, "comboBoxBackWidth3");
			this.comboBoxBackWidth3.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth3.Items"),
				resources.GetString("comboBoxBackWidth3.Items1"),
				resources.GetString("comboBoxBackWidth3.Items2"),
				resources.GetString("comboBoxBackWidth3.Items3")
			});
			this.comboBoxBackWidth3.Name = "comboBoxBackWidth3";
			this.comboBoxBackWidth3.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth3.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			this.comboBoxBackWidth2.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth2, "comboBoxBackWidth2");
			this.comboBoxBackWidth2.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth2.Items"),
				resources.GetString("comboBoxBackWidth2.Items1"),
				resources.GetString("comboBoxBackWidth2.Items2"),
				resources.GetString("comboBoxBackWidth2.Items3")
			});
			this.comboBoxBackWidth2.Name = "comboBoxBackWidth2";
			this.comboBoxBackWidth2.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth2.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			resources.ApplyResources(this.label33, "label33");
			this.label33.Name = "label33";
			this.comboBoxBackWidth1.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackWidth1, "comboBoxBackWidth1");
			this.comboBoxBackWidth1.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackWidth1.Items"),
				resources.GetString("comboBoxBackWidth1.Items1"),
				resources.GetString("comboBoxBackWidth1.Items2"),
				resources.GetString("comboBoxBackWidth1.Items3")
			});
			this.comboBoxBackWidth1.Name = "comboBoxBackWidth1";
			this.comboBoxBackWidth1.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackWidth1.Leave += new EventHandler(this.comboBoxBackWidth_Leave);
			this.labelArrow2.Image = Resource.Arrow;
			resources.ApplyResources(this.labelArrow2, "labelArrow2");
			this.labelArrow2.Name = "labelArrow2";
			this.comboBoxBackFeed10.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed10, "comboBoxBackFeed10");
			this.comboBoxBackFeed10.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed10.Items"),
				resources.GetString("comboBoxBackFeed10.Items1"),
				resources.GetString("comboBoxBackFeed10.Items2"),
				resources.GetString("comboBoxBackFeed10.Items3")
			});
			this.comboBoxBackFeed10.Name = "comboBoxBackFeed10";
			this.comboBoxBackFeed10.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed10.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed9.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed9, "comboBoxBackFeed9");
			this.comboBoxBackFeed9.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed9.Items"),
				resources.GetString("comboBoxBackFeed9.Items1"),
				resources.GetString("comboBoxBackFeed9.Items2"),
				resources.GetString("comboBoxBackFeed9.Items3")
			});
			this.comboBoxBackFeed9.Name = "comboBoxBackFeed9";
			this.comboBoxBackFeed9.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed9.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed8.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed8, "comboBoxBackFeed8");
			this.comboBoxBackFeed8.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed8.Items"),
				resources.GetString("comboBoxBackFeed8.Items1"),
				resources.GetString("comboBoxBackFeed8.Items2"),
				resources.GetString("comboBoxBackFeed8.Items3")
			});
			this.comboBoxBackFeed8.Name = "comboBoxBackFeed8";
			this.comboBoxBackFeed8.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed8.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed7.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed7, "comboBoxBackFeed7");
			this.comboBoxBackFeed7.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed7.Items"),
				resources.GetString("comboBoxBackFeed7.Items1"),
				resources.GetString("comboBoxBackFeed7.Items2"),
				resources.GetString("comboBoxBackFeed7.Items3")
			});
			this.comboBoxBackFeed7.Name = "comboBoxBackFeed7";
			this.comboBoxBackFeed7.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed7.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed6.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed6, "comboBoxBackFeed6");
			this.comboBoxBackFeed6.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed6.Items"),
				resources.GetString("comboBoxBackFeed6.Items1"),
				resources.GetString("comboBoxBackFeed6.Items2"),
				resources.GetString("comboBoxBackFeed6.Items3")
			});
			this.comboBoxBackFeed6.Name = "comboBoxBackFeed6";
			this.comboBoxBackFeed6.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed6.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed5.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed5, "comboBoxBackFeed5");
			this.comboBoxBackFeed5.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed5.Items"),
				resources.GetString("comboBoxBackFeed5.Items1"),
				resources.GetString("comboBoxBackFeed5.Items2"),
				resources.GetString("comboBoxBackFeed5.Items3")
			});
			this.comboBoxBackFeed5.Name = "comboBoxBackFeed5";
			this.comboBoxBackFeed5.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed5.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed4.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed4, "comboBoxBackFeed4");
			this.comboBoxBackFeed4.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed4.Items"),
				resources.GetString("comboBoxBackFeed4.Items1"),
				resources.GetString("comboBoxBackFeed4.Items2"),
				resources.GetString("comboBoxBackFeed4.Items3")
			});
			this.comboBoxBackFeed4.Name = "comboBoxBackFeed4";
			this.comboBoxBackFeed4.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed4.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed3.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed3, "comboBoxBackFeed3");
			this.comboBoxBackFeed3.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed3.Items"),
				resources.GetString("comboBoxBackFeed3.Items1"),
				resources.GetString("comboBoxBackFeed3.Items2"),
				resources.GetString("comboBoxBackFeed3.Items3")
			});
			this.comboBoxBackFeed3.Name = "comboBoxBackFeed3";
			this.comboBoxBackFeed3.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed3.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed2.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed2, "comboBoxBackFeed2");
			this.comboBoxBackFeed2.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed2.Items"),
				resources.GetString("comboBoxBackFeed2.Items1"),
				resources.GetString("comboBoxBackFeed2.Items2"),
				resources.GetString("comboBoxBackFeed2.Items3")
			});
			this.comboBoxBackFeed2.Name = "comboBoxBackFeed2";
			this.comboBoxBackFeed2.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed2.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			this.comboBoxBackFeed1.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBackFeed1, "comboBoxBackFeed1");
			this.comboBoxBackFeed1.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBackFeed1.Items"),
				resources.GetString("comboBoxBackFeed1.Items1"),
				resources.GetString("comboBoxBackFeed1.Items2"),
				resources.GetString("comboBoxBackFeed1.Items3")
			});
			this.comboBoxBackFeed1.Name = "comboBoxBackFeed1";
			this.comboBoxBackFeed1.Enter += new EventHandler(this.BackArrow_Click);
			this.comboBoxBackFeed1.Leave += new EventHandler(this.comboBoxBackFeed_Leave);
			resources.ApplyResources(this.label35, "label35");
			this.label35.Name = "label35";
			resources.ApplyResources(this.label36, "label36");
			this.label36.Name = "label36";
			resources.ApplyResources(this.labelBackNo10, "labelBackNo10");
			this.labelBackNo10.Name = "labelBackNo10";
			this.labelBackNo10.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.label38, "label38");
			this.label38.Name = "label38";
			resources.ApplyResources(this.label39, "label39");
			this.label39.Name = "label39";
			resources.ApplyResources(this.textBoxBackY10, "textBoxBackY10");
			this.textBoxBackY10.Name = "textBoxBackY10";
			this.textBoxBackY10.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY10.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.textBoxBackX1, "textBoxBackX1");
			this.textBoxBackX1.Name = "textBoxBackX1";
			this.textBoxBackX1.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX1.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackX10, "textBoxBackX10");
			this.textBoxBackX10.Name = "textBoxBackX10";
			this.textBoxBackX10.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX10.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackY1, "textBoxBackY1");
			this.textBoxBackY1.Name = "textBoxBackY1";
			this.textBoxBackY1.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY1.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.labelBackNo9, "labelBackNo9");
			this.labelBackNo9.Name = "labelBackNo9";
			this.labelBackNo9.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.labelBackNo1, "labelBackNo1");
			this.labelBackNo1.Name = "labelBackNo1";
			this.labelBackNo1.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.textBoxBackY9, "textBoxBackY9");
			this.textBoxBackY9.Name = "textBoxBackY9";
			this.textBoxBackY9.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY9.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.textBoxBackX9, "textBoxBackX9");
			this.textBoxBackX9.Name = "textBoxBackX9";
			this.textBoxBackX9.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX9.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackX2, "textBoxBackX2");
			this.textBoxBackX2.Name = "textBoxBackX2";
			this.textBoxBackX2.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX2.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackY2, "textBoxBackY2");
			this.textBoxBackY2.Name = "textBoxBackY2";
			this.textBoxBackY2.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY2.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.labelBackNo8, "labelBackNo8");
			this.labelBackNo8.Name = "labelBackNo8";
			this.labelBackNo8.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.labelBackNo2, "labelBackNo2");
			this.labelBackNo2.Name = "labelBackNo2";
			this.labelBackNo2.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.textBoxBackY8, "textBoxBackY8");
			this.textBoxBackY8.Name = "textBoxBackY8";
			this.textBoxBackY8.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY8.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.textBoxBackX8, "textBoxBackX8");
			this.textBoxBackX8.Name = "textBoxBackX8";
			this.textBoxBackX8.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX8.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackX3, "textBoxBackX3");
			this.textBoxBackX3.Name = "textBoxBackX3";
			this.textBoxBackX3.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX3.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackY3, "textBoxBackY3");
			this.textBoxBackY3.Name = "textBoxBackY3";
			this.textBoxBackY3.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY3.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.labelBackNo3, "labelBackNo3");
			this.labelBackNo3.Name = "labelBackNo3";
			this.labelBackNo3.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.labelBackNo7, "labelBackNo7");
			this.labelBackNo7.Name = "labelBackNo7";
			this.labelBackNo7.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.textBoxBackX4, "textBoxBackX4");
			this.textBoxBackX4.Name = "textBoxBackX4";
			this.textBoxBackX4.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX4.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackY7, "textBoxBackY7");
			this.textBoxBackY7.Name = "textBoxBackY7";
			this.textBoxBackY7.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY7.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.textBoxBackY4, "textBoxBackY4");
			this.textBoxBackY4.Name = "textBoxBackY4";
			this.textBoxBackY4.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY4.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.textBoxBackX7, "textBoxBackX7");
			this.textBoxBackX7.Name = "textBoxBackX7";
			this.textBoxBackX7.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX7.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.labelBackNo4, "labelBackNo4");
			this.labelBackNo4.Name = "labelBackNo4";
			this.labelBackNo4.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.labelBackNo6, "labelBackNo6");
			this.labelBackNo6.Name = "labelBackNo6";
			this.labelBackNo6.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.textBoxBackX5, "textBoxBackX5");
			this.textBoxBackX5.Name = "textBoxBackX5";
			this.textBoxBackX5.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX5.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.textBoxBackY6, "textBoxBackY6");
			this.textBoxBackY6.Name = "textBoxBackY6";
			this.textBoxBackY6.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY6.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.textBoxBackY5, "textBoxBackY5");
			this.textBoxBackY5.Name = "textBoxBackY5";
			this.textBoxBackY5.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackY5.Leave += new EventHandler(this.textBoxBackY_Leave);
			resources.ApplyResources(this.textBoxBackX6, "textBoxBackX6");
			this.textBoxBackX6.Name = "textBoxBackX6";
			this.textBoxBackX6.Enter += new EventHandler(this.BackArrow_Click);
			this.textBoxBackX6.Leave += new EventHandler(this.textBoxBackX_Leave);
			resources.ApplyResources(this.labelBackNo5, "labelBackNo5");
			this.labelBackNo5.Name = "labelBackNo5";
			this.labelBackNo5.Click += new EventHandler(this.BackArrow_Click);
			resources.ApplyResources(this.tabPage3, "tabPage3");
			this.tabPage3.Controls.Add(this.groupBoxIcStack2);
			this.tabPage3.Controls.Add(this.groupBoxIcStack1);
			this.tabPage3.Controls.Add(this.buttonGetCur3);
			this.tabPage3.Controls.Add(this.buttonMoveTo3);
			this.tabPage3.Controls.Add(this.buttonDown3);
			this.tabPage3.Controls.Add(this.buttonUp3);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			this.groupBoxIcStack2.Controls.Add(this.labelArrow4);
			this.groupBoxIcStack2.Controls.Add(this.textBoxIcStackArrayY2);
			this.groupBoxIcStack2.Controls.Add(this.labelIcStackArray2);
			this.groupBoxIcStack2.Controls.Add(this.textBoxIcStackArrayX2);
			this.groupBoxIcStack2.Controls.Add(this.labelIcStackEnd2);
			this.groupBoxIcStack2.Controls.Add(this.textBoxIcStackEndY2);
			this.groupBoxIcStack2.Controls.Add(this.textBoxIcStackEndX2);
			this.groupBoxIcStack2.Controls.Add(this.label40);
			this.groupBoxIcStack2.Controls.Add(this.labelIcStackStart2);
			this.groupBoxIcStack2.Controls.Add(this.label42);
			this.groupBoxIcStack2.Controls.Add(this.textBoxIcStackStartY2);
			this.groupBoxIcStack2.Controls.Add(this.textBoxIcStackStartX2);
			resources.ApplyResources(this.groupBoxIcStack2, "groupBoxIcStack2");
			this.groupBoxIcStack2.Name = "groupBoxIcStack2";
			this.groupBoxIcStack2.TabStop = false;
			this.labelArrow4.Image = Resource.Arrow;
			resources.ApplyResources(this.labelArrow4, "labelArrow4");
			this.labelArrow4.Name = "labelArrow4";
			resources.ApplyResources(this.textBoxIcStackArrayY2, "textBoxIcStackArrayY2");
			this.textBoxIcStackArrayY2.Name = "textBoxIcStackArrayY2";
			this.textBoxIcStackArrayY2.Enter += new EventHandler(this.Arrow34None_Click);
			this.textBoxIcStackArrayY2.Leave += new EventHandler(this.textBoxIcStackArrayY_Leave);
			resources.ApplyResources(this.labelIcStackArray2, "labelIcStackArray2");
			this.labelIcStackArray2.Name = "labelIcStackArray2";
			resources.ApplyResources(this.textBoxIcStackArrayX2, "textBoxIcStackArrayX2");
			this.textBoxIcStackArrayX2.Name = "textBoxIcStackArrayX2";
			this.textBoxIcStackArrayX2.Enter += new EventHandler(this.Arrow34None_Click);
			this.textBoxIcStackArrayX2.Leave += new EventHandler(this.textBoxIcStackArrayX_Leave);
			resources.ApplyResources(this.labelIcStackEnd2, "labelIcStackEnd2");
			this.labelIcStackEnd2.ForeColor = SystemColors.ControlText;
			this.labelIcStackEnd2.Name = "labelIcStackEnd2";
			resources.ApplyResources(this.textBoxIcStackEndY2, "textBoxIcStackEndY2");
			this.textBoxIcStackEndY2.Name = "textBoxIcStackEndY2";
			this.textBoxIcStackEndY2.Enter += new EventHandler(this.Arrow4_Click);
			this.textBoxIcStackEndY2.Leave += new EventHandler(this.textBoxIcStackEndY_Leave);
			resources.ApplyResources(this.textBoxIcStackEndX2, "textBoxIcStackEndX2");
			this.textBoxIcStackEndX2.Name = "textBoxIcStackEndX2";
			this.textBoxIcStackEndX2.Enter += new EventHandler(this.Arrow4_Click);
			this.textBoxIcStackEndX2.Leave += new EventHandler(this.textBoxIcStackEndX_Leave);
			resources.ApplyResources(this.label40, "label40");
			this.label40.Name = "label40";
			resources.ApplyResources(this.labelIcStackStart2, "labelIcStackStart2");
			this.labelIcStackStart2.ForeColor = SystemColors.ControlText;
			this.labelIcStackStart2.Name = "labelIcStackStart2";
			resources.ApplyResources(this.label42, "label42");
			this.label42.Name = "label42";
			resources.ApplyResources(this.textBoxIcStackStartY2, "textBoxIcStackStartY2");
			this.textBoxIcStackStartY2.Name = "textBoxIcStackStartY2";
			this.textBoxIcStackStartY2.Enter += new EventHandler(this.Arrow4_Click);
			this.textBoxIcStackStartY2.Leave += new EventHandler(this.textBoxIcStackStartY_Leave);
			resources.ApplyResources(this.textBoxIcStackStartX2, "textBoxIcStackStartX2");
			this.textBoxIcStackStartX2.Name = "textBoxIcStackStartX2";
			this.textBoxIcStackStartX2.Enter += new EventHandler(this.Arrow4_Click);
			this.textBoxIcStackStartX2.Leave += new EventHandler(this.textBoxIcStackStartX_Leave);
			this.groupBoxIcStack1.Controls.Add(this.textBoxIcStackArrayY1);
			this.groupBoxIcStack1.Controls.Add(this.labelIcStackArray1);
			this.groupBoxIcStack1.Controls.Add(this.textBoxIcStackArrayX1);
			this.groupBoxIcStack1.Controls.Add(this.labelIcStackEnd1);
			this.groupBoxIcStack1.Controls.Add(this.textBoxIcStackEndY1);
			this.groupBoxIcStack1.Controls.Add(this.labelArrow3);
			this.groupBoxIcStack1.Controls.Add(this.textBoxIcStackEndX1);
			this.groupBoxIcStack1.Controls.Add(this.label25);
			this.groupBoxIcStack1.Controls.Add(this.labelIcStackStart1);
			this.groupBoxIcStack1.Controls.Add(this.label26);
			this.groupBoxIcStack1.Controls.Add(this.textBoxIcStackStartY1);
			this.groupBoxIcStack1.Controls.Add(this.textBoxIcStackStartX1);
			resources.ApplyResources(this.groupBoxIcStack1, "groupBoxIcStack1");
			this.groupBoxIcStack1.Name = "groupBoxIcStack1";
			this.groupBoxIcStack1.TabStop = false;
			resources.ApplyResources(this.textBoxIcStackArrayY1, "textBoxIcStackArrayY1");
			this.textBoxIcStackArrayY1.Name = "textBoxIcStackArrayY1";
			this.textBoxIcStackArrayY1.Enter += new EventHandler(this.Arrow34None_Click);
			this.textBoxIcStackArrayY1.Leave += new EventHandler(this.textBoxIcStackArrayY_Leave);
			resources.ApplyResources(this.labelIcStackArray1, "labelIcStackArray1");
			this.labelIcStackArray1.Name = "labelIcStackArray1";
			this.labelIcStackArray1.Click += new EventHandler(this.Arrow3_Click);
			resources.ApplyResources(this.textBoxIcStackArrayX1, "textBoxIcStackArrayX1");
			this.textBoxIcStackArrayX1.Name = "textBoxIcStackArrayX1";
			this.textBoxIcStackArrayX1.Enter += new EventHandler(this.Arrow34None_Click);
			this.textBoxIcStackArrayX1.Leave += new EventHandler(this.textBoxIcStackArrayX_Leave);
			resources.ApplyResources(this.labelIcStackEnd1, "labelIcStackEnd1");
			this.labelIcStackEnd1.ForeColor = SystemColors.ControlText;
			this.labelIcStackEnd1.Name = "labelIcStackEnd1";
			this.labelIcStackEnd1.Click += new EventHandler(this.Arrow3_Click);
			resources.ApplyResources(this.textBoxIcStackEndY1, "textBoxIcStackEndY1");
			this.textBoxIcStackEndY1.Name = "textBoxIcStackEndY1";
			this.textBoxIcStackEndY1.Enter += new EventHandler(this.Arrow3_Click);
			this.textBoxIcStackEndY1.Leave += new EventHandler(this.textBoxIcStackEndY_Leave);
			this.labelArrow3.Image = Resource.Arrow;
			resources.ApplyResources(this.labelArrow3, "labelArrow3");
			this.labelArrow3.Name = "labelArrow3";
			resources.ApplyResources(this.textBoxIcStackEndX1, "textBoxIcStackEndX1");
			this.textBoxIcStackEndX1.Name = "textBoxIcStackEndX1";
			this.textBoxIcStackEndX1.Enter += new EventHandler(this.Arrow3_Click);
			this.textBoxIcStackEndX1.Leave += new EventHandler(this.textBoxIcStackEndX_Leave);
			resources.ApplyResources(this.label25, "label25");
			this.label25.Name = "label25";
			resources.ApplyResources(this.labelIcStackStart1, "labelIcStackStart1");
			this.labelIcStackStart1.ForeColor = SystemColors.ControlText;
			this.labelIcStackStart1.Name = "labelIcStackStart1";
			this.labelIcStackStart1.Click += new EventHandler(this.Arrow3_Click);
			resources.ApplyResources(this.label26, "label26");
			this.label26.Name = "label26";
			resources.ApplyResources(this.textBoxIcStackStartY1, "textBoxIcStackStartY1");
			this.textBoxIcStackStartY1.Name = "textBoxIcStackStartY1";
			this.textBoxIcStackStartY1.Enter += new EventHandler(this.Arrow3_Click);
			this.textBoxIcStackStartY1.Leave += new EventHandler(this.textBoxIcStackStartY_Leave);
			resources.ApplyResources(this.textBoxIcStackStartX1, "textBoxIcStackStartX1");
			this.textBoxIcStackStartX1.Name = "textBoxIcStackStartX1";
			this.textBoxIcStackStartX1.Enter += new EventHandler(this.Arrow3_Click);
			this.textBoxIcStackStartX1.Leave += new EventHandler(this.textBoxIcStackStartX_Leave);
			resources.ApplyResources(this.buttonGetCur3, "buttonGetCur3");
			this.buttonGetCur3.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur3.Name = "buttonGetCur3";
			this.buttonGetCur3.UseVisualStyleBackColor = true;
			this.buttonGetCur3.Click += new EventHandler(this.buttonGetCur_Click);
			resources.ApplyResources(this.buttonMoveTo3, "buttonMoveTo3");
			this.buttonMoveTo3.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo3.Name = "buttonMoveTo3";
			this.buttonMoveTo3.UseVisualStyleBackColor = true;
			this.buttonMoveTo3.Click += new EventHandler(this.buttonMoveTo_Click);
			resources.ApplyResources(this.buttonDown3, "buttonDown3");
			this.buttonDown3.BackgroundImage = Resource.ButtonUp;
			this.buttonDown3.Name = "buttonDown3";
			this.buttonDown3.UseVisualStyleBackColor = true;
			this.buttonDown3.Click += new EventHandler(this.buttonDown3_Click);
			resources.ApplyResources(this.buttonUp3, "buttonUp3");
			this.buttonUp3.BackgroundImage = Resource.ButtonUp;
			this.buttonUp3.Name = "buttonUp3";
			this.buttonUp3.UseVisualStyleBackColor = true;
			this.buttonUp3.Click += new EventHandler(this.buttonUp3_Click);
			this.tabPage4.Controls.Add(this.groupBox2);
			this.tabPage4.Controls.Add(this.buttonVisionOffsetTest);
			this.tabPage4.Controls.Add(this.label4);
			this.tabPage4.Controls.Add(this.label3);
			this.tabPage4.Controls.Add(this.textBoxNozzle2VisionY);
			this.tabPage4.Controls.Add(this.textBoxNozzle2VisionX);
			this.tabPage4.Controls.Add(this.textBoxNozzle1VisionY);
			this.tabPage4.Controls.Add(this.textBoxNozzle1VisionX);
			this.tabPage4.Controls.Add(this.buttonNozzle2Test);
			this.tabPage4.Controls.Add(this.groupBox1);
			this.tabPage4.Controls.Add(this.textBoxStackDealy);
			this.tabPage4.Controls.Add(this.label30);
			this.tabPage4.Controls.Add(this.textBoxNozzleOffsetY);
			this.tabPage4.Controls.Add(this.textBoxNozzleOffsetX);
			this.tabPage4.Controls.Add(this.labelNozzleOffset);
			this.tabPage4.Controls.Add(this.textBoxNozzleDownDiscardPos);
			this.tabPage4.Controls.Add(this.label17);
			this.tabPage4.Controls.Add(this.textBoxNozzleDownFrontStackPos);
			this.tabPage4.Controls.Add(this.label16);
			this.tabPage4.Controls.Add(this.textBoxNozzleDownStackPos);
			this.tabPage4.Controls.Add(this.label15);
			this.tabPage4.Controls.Add(this.textBoxNozzleDownPcbPos);
			this.tabPage4.Controls.Add(this.label14);
			resources.ApplyResources(this.tabPage4, "tabPage4");
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			this.groupBox2.Controls.Add(this.comboBoxBlowingDealy2);
			this.groupBox2.Controls.Add(this.comboBoxBlowingDealy1);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label6);
			resources.ApplyResources(this.groupBox2, "groupBox2");
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.TabStop = false;
			this.comboBoxBlowingDealy2.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBlowingDealy2, "comboBoxBlowingDealy2");
			this.comboBoxBlowingDealy2.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBlowingDealy2.Items"),
				resources.GetString("comboBoxBlowingDealy2.Items1"),
				resources.GetString("comboBoxBlowingDealy2.Items2")
			});
			this.comboBoxBlowingDealy2.Name = "comboBoxBlowingDealy2";
			this.comboBoxBlowingDealy1.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxBlowingDealy1, "comboBoxBlowingDealy1");
			this.comboBoxBlowingDealy1.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxBlowingDealy1.Items"),
				resources.GetString("comboBoxBlowingDealy1.Items1"),
				resources.GetString("comboBoxBlowingDealy1.Items2")
			});
			this.comboBoxBlowingDealy1.Name = "comboBoxBlowingDealy1";
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			this.buttonVisionOffsetTest.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonVisionOffsetTest, "buttonVisionOffsetTest");
			this.buttonVisionOffsetTest.Name = "buttonVisionOffsetTest";
			this.buttonVisionOffsetTest.UseVisualStyleBackColor = true;
			this.buttonVisionOffsetTest.Click += new EventHandler(this.buttonVisionOffsetTest_Click);
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			resources.ApplyResources(this.textBoxNozzle2VisionY, "textBoxNozzle2VisionY");
			this.textBoxNozzle2VisionY.Name = "textBoxNozzle2VisionY";
			resources.ApplyResources(this.textBoxNozzle2VisionX, "textBoxNozzle2VisionX");
			this.textBoxNozzle2VisionX.Name = "textBoxNozzle2VisionX";
			resources.ApplyResources(this.textBoxNozzle1VisionY, "textBoxNozzle1VisionY");
			this.textBoxNozzle1VisionY.Name = "textBoxNozzle1VisionY";
			resources.ApplyResources(this.textBoxNozzle1VisionX, "textBoxNozzle1VisionX");
			this.textBoxNozzle1VisionX.Name = "textBoxNozzle1VisionX";
			this.buttonNozzle2Test.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonNozzle2Test, "buttonNozzle2Test");
			this.buttonNozzle2Test.Name = "buttonNozzle2Test";
			this.buttonNozzle2Test.UseVisualStyleBackColor = true;
			this.buttonNozzle2Test.Click += new EventHandler(this.buttonNozzle2Test_Click);
			this.groupBox1.Controls.Add(this.label32);
			this.groupBox1.Controls.Add(this.label31);
			this.groupBox1.Controls.Add(this.textBoxPcbDealy2);
			this.groupBox1.Controls.Add(this.textBoxPcbDealy1);
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			resources.ApplyResources(this.label32, "label32");
			this.label32.Name = "label32";
			resources.ApplyResources(this.label31, "label31");
			this.label31.Name = "label31";
			resources.ApplyResources(this.textBoxPcbDealy2, "textBoxPcbDealy2");
			this.textBoxPcbDealy2.Name = "textBoxPcbDealy2";
			resources.ApplyResources(this.textBoxPcbDealy1, "textBoxPcbDealy1");
			this.textBoxPcbDealy1.Name = "textBoxPcbDealy1";
			resources.ApplyResources(this.textBoxStackDealy, "textBoxStackDealy");
			this.textBoxStackDealy.Name = "textBoxStackDealy";
			resources.ApplyResources(this.label30, "label30");
			this.label30.Name = "label30";
			resources.ApplyResources(this.textBoxNozzleOffsetY, "textBoxNozzleOffsetY");
			this.textBoxNozzleOffsetY.Name = "textBoxNozzleOffsetY";
			resources.ApplyResources(this.textBoxNozzleOffsetX, "textBoxNozzleOffsetX");
			this.textBoxNozzleOffsetX.Name = "textBoxNozzleOffsetX";
			resources.ApplyResources(this.labelNozzleOffset, "labelNozzleOffset");
			this.labelNozzleOffset.Name = "labelNozzleOffset";
			this.tabPage5.Controls.Add(this.groupBox4);
			this.tabPage5.Controls.Add(this.groupBox3);
			resources.ApplyResources(this.tabPage5, "tabPage5");
			this.tabPage5.Name = "tabPage5";
			this.tabPage5.UseVisualStyleBackColor = true;
			this.groupBox4.Controls.Add(this.textBoxVisionZ);
			this.groupBox4.Controls.Add(this.label18);
			this.groupBox4.Controls.Add(this.textBoxVisionY);
			this.groupBox4.Controls.Add(this.label27);
			this.groupBox4.Controls.Add(this.textBoxVisionX);
			this.groupBox4.Controls.Add(this.label24);
			this.groupBox4.Controls.Add(this.label8);
			this.groupBox4.Controls.Add(this.textBoxPixelScaleY1);
			this.groupBox4.Controls.Add(this.textBoxPixelScaleX1);
			this.groupBox4.Controls.Add(this.comboBoxFeedCamera1);
			this.groupBox4.Controls.Add(this.label2);
			this.groupBox4.Controls.Add(this.textBoxVisionValueComp);
			this.groupBox4.Controls.Add(this.textBoxCameraAngle1);
			resources.ApplyResources(this.groupBox4, "groupBox4");
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.TabStop = false;
			resources.ApplyResources(this.textBoxVisionZ, "textBoxVisionZ");
			this.textBoxVisionZ.Name = "textBoxVisionZ";
			resources.ApplyResources(this.label18, "label18");
			this.label18.Name = "label18";
			resources.ApplyResources(this.textBoxVisionY, "textBoxVisionY");
			this.textBoxVisionY.Name = "textBoxVisionY";
			resources.ApplyResources(this.label27, "label27");
			this.label27.Name = "label27";
			resources.ApplyResources(this.textBoxVisionX, "textBoxVisionX");
			this.textBoxVisionX.Name = "textBoxVisionX";
			resources.ApplyResources(this.label24, "label24");
			this.label24.Name = "label24";
			resources.ApplyResources(this.label8, "label8");
			this.label8.Name = "label8";
			resources.ApplyResources(this.textBoxPixelScaleY1, "textBoxPixelScaleY1");
			this.textBoxPixelScaleY1.Name = "textBoxPixelScaleY1";
			resources.ApplyResources(this.textBoxPixelScaleX1, "textBoxPixelScaleX1");
			this.textBoxPixelScaleX1.Name = "textBoxPixelScaleX1";
			this.comboBoxFeedCamera1.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxFeedCamera1, "comboBoxFeedCamera1");
			this.comboBoxFeedCamera1.Name = "comboBoxFeedCamera1";
			this.comboBoxFeedCamera1.Click += new EventHandler(this.comboBoxFeedCamera_Click);
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			resources.ApplyResources(this.textBoxVisionValueComp, "textBoxVisionValueComp");
			this.textBoxVisionValueComp.Name = "textBoxVisionValueComp";
			resources.ApplyResources(this.textBoxCameraAngle1, "textBoxCameraAngle1");
			this.textBoxCameraAngle1.Name = "textBoxCameraAngle1";
			this.groupBox3.Controls.Add(this.label7);
			this.groupBox3.Controls.Add(this.label29);
			this.groupBox3.Controls.Add(this.label1);
			this.groupBox3.Controls.Add(this.textBoxVisionOffsetX);
			this.groupBox3.Controls.Add(this.comboBoxFeedCamera2);
			this.groupBox3.Controls.Add(this.textBoxVisionOffsetY);
			this.groupBox3.Controls.Add(this.textBoxPixelScaleY2);
			this.groupBox3.Controls.Add(this.textBoxPixelScaleX2);
			this.groupBox3.Controls.Add(this.textBoxVisionValueNozzle);
			this.groupBox3.Controls.Add(this.labelVisionValue);
			resources.ApplyResources(this.groupBox3, "groupBox3");
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.TabStop = false;
			resources.ApplyResources(this.label7, "label7");
			this.label7.Name = "label7";
			resources.ApplyResources(this.label29, "label29");
			this.label29.Name = "label29";
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			resources.ApplyResources(this.textBoxVisionOffsetX, "textBoxVisionOffsetX");
			this.textBoxVisionOffsetX.Name = "textBoxVisionOffsetX";
			this.comboBoxFeedCamera2.DropDownStyle = ComboBoxStyle.DropDownList;
			resources.ApplyResources(this.comboBoxFeedCamera2, "comboBoxFeedCamera2");
			this.comboBoxFeedCamera2.Name = "comboBoxFeedCamera2";
			this.comboBoxFeedCamera2.Click += new EventHandler(this.comboBoxFeedCamera_Click);
			resources.ApplyResources(this.textBoxVisionOffsetY, "textBoxVisionOffsetY");
			this.textBoxVisionOffsetY.Name = "textBoxVisionOffsetY";
			resources.ApplyResources(this.textBoxPixelScaleY2, "textBoxPixelScaleY2");
			this.textBoxPixelScaleY2.Name = "textBoxPixelScaleY2";
			resources.ApplyResources(this.textBoxPixelScaleX2, "textBoxPixelScaleX2");
			this.textBoxPixelScaleX2.Name = "textBoxPixelScaleX2";
			resources.ApplyResources(this.textBoxVisionValueNozzle, "textBoxVisionValueNozzle");
			this.textBoxVisionValueNozzle.Name = "textBoxVisionValueNozzle";
			resources.ApplyResources(this.labelVisionValue, "labelVisionValue");
			this.labelVisionValue.Name = "labelVisionValue";
			this.tabPage6.Controls.Add(this.textBoxBackPrickOffsetY);
			this.tabPage6.Controls.Add(this.textBoxBackPrickOffsetX);
			this.tabPage6.Controls.Add(this.label34);
			this.tabPage6.Controls.Add(this.textBoxLeftPrickOffsetY);
			this.tabPage6.Controls.Add(this.textBoxLeftPrickOffsetX);
			this.tabPage6.Controls.Add(this.label28);
			this.tabPage6.Controls.Add(this.checkBoxPressureCheck);
			this.tabPage6.Controls.Add(this.textBoxIpPort);
			this.tabPage6.Controls.Add(this.label13);
			this.tabPage6.Controls.Add(this.textBoxIP3);
			this.tabPage6.Controls.Add(this.textBoxIP1);
			this.tabPage6.Controls.Add(this.label9);
			this.tabPage6.Controls.Add(this.label12);
			this.tabPage6.Controls.Add(this.label10);
			this.tabPage6.Controls.Add(this.textBoxIP2);
			this.tabPage6.Controls.Add(this.textBoxIP4);
			this.tabPage6.Controls.Add(this.label11);
			resources.ApplyResources(this.tabPage6, "tabPage6");
			this.tabPage6.Name = "tabPage6";
			this.tabPage6.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.textBoxBackPrickOffsetY, "textBoxBackPrickOffsetY");
			this.textBoxBackPrickOffsetY.Name = "textBoxBackPrickOffsetY";
			resources.ApplyResources(this.textBoxBackPrickOffsetX, "textBoxBackPrickOffsetX");
			this.textBoxBackPrickOffsetX.Name = "textBoxBackPrickOffsetX";
			resources.ApplyResources(this.label34, "label34");
			this.label34.Name = "label34";
			resources.ApplyResources(this.textBoxLeftPrickOffsetY, "textBoxLeftPrickOffsetY");
			this.textBoxLeftPrickOffsetY.Name = "textBoxLeftPrickOffsetY";
			resources.ApplyResources(this.textBoxLeftPrickOffsetX, "textBoxLeftPrickOffsetX");
			this.textBoxLeftPrickOffsetX.Name = "textBoxLeftPrickOffsetX";
			resources.ApplyResources(this.label28, "label28");
			this.label28.Name = "label28";
			resources.ApplyResources(this.checkBoxPressureCheck, "checkBoxPressureCheck");
			this.checkBoxPressureCheck.Name = "checkBoxPressureCheck";
			this.checkBoxPressureCheck.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.buttonCancel, "buttonCancel");
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.BackgroundImage = Resource.ButtonUp;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new EventHandler(this.buttonSave_Click);
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = SystemColors.Control;
			base.Controls.Add(this.tabControl);
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonSave);
			base.Name = "ParamEdit";
			base.ShowIcon = false;
			base.FormClosed += new FormClosedEventHandler(this.ParamEdit_FormClosed);
			base.Load += new EventHandler(this.ParamEdit_Load);
			this.tabControl.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.tabPage3.ResumeLayout(false);
			this.groupBoxIcStack2.ResumeLayout(false);
			this.groupBoxIcStack2.PerformLayout();
			this.groupBoxIcStack1.ResumeLayout(false);
			this.groupBoxIcStack1.PerformLayout();
			this.tabPage4.ResumeLayout(false);
			this.tabPage4.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.tabPage5.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.tabPage6.ResumeLayout(false);
			this.tabPage6.PerformLayout();
			base.ResumeLayout(false);
		}
	}
}
