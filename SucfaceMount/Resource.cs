using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SucfaceMount
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	internal class Resource
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(Resource.resourceMan, null))
				{
					ResourceManager resourceManager = new ResourceManager("SucfaceMount.Resource", typeof(Resource).Assembly);
					Resource.resourceMan = resourceManager;
				}
				return Resource.resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return Resource.resourceCulture;
			}
			set
			{
				Resource.resourceCulture = value;
			}
		}

		internal static Bitmap aboutUs
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("aboutUs", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Arrow
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("Arrow", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BIAboutUs
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BIAboutUs", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BIAuto
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BIAuto", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BIGoHome
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BIGoHome", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BILoad
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BILoad", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BIManual
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BIManual", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BIPCB
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BIPCB", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BISave
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BISave", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap BISettings
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("BISettings", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ButtonDown
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("ButtonDown", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap ButtonUp
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("ButtonUp", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap go_down
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("go_down", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap go_next
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("go_next", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap go_previous
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("go_previous", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap go_up
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("go_up", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap GreenLED
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("GreenLED", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap media_eject
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("media_eject", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap media_playback_pause
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("media_playback_pause", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap media_playback_start
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("media_playback_start", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap media_playback_stop
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("media_playback_stop", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap RedLED
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("RedLED", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap Transform
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("Transform", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal static Bitmap TransparentLED
		{
			get
			{
				object @object = Resource.ResourceManager.GetObject("TransparentLED", Resource.resourceCulture);
				return (Bitmap)@object;
			}
		}

		internal Resource()
		{
		}
	}
}
