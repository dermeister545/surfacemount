using System;

namespace SucfaceMount
{
	public enum ControlBufType : byte
	{
		STOP_BUF,
		RUN_BUF,
		PAUSE_BUF,
		NULL_BUF
	}
}
