using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class VisionOffsetTest : Form
	{
		private enum VisionOffsetTestStateType
		{
			None,
			Stage1,
			Stage2,
			Stage3,
			Stage4,
			Stage5,
			Stage6,
			Stage7,
			Stage8,
			Stage9,
			Stage10,
			Stage11,
			Stage12,
			Stage13,
			Stage14,
			OK,
			Done
		}

		private const int VisionTestNum = 8;

		private const int FAxis = 0;

		private const int ZAxis = 1;

		private const int XAxis = 2;

		private const int A1Axis = 3;

		private const int YAxis = 4;

		private const int A2Axis = 5;

		private IContainer components;

		private Label labelArrow1;

		private Button buttonOK;

		private Button buttonCancel;

		private Button buttonRun;

		private Label labelStep4;

		private Label labelStep3;

		private Timer timerUI;

		private Label labelStep2;

		private Label labelStep1;

		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		private ResourceManager LocRM = new ResourceManager(typeof(VisionOffsetTest));

		public double VisionX;

		public double VisionY;

		public double Nozzle2OffsetX;

		public double Nozzle2OffsetY;

		public double Nozzle1CenterOffsetX;

		public double Nozzle1CenterOffsetY;

		public double Nozzle2CenterOffsetX;

		public double Nozzle2CenterOffsetY;

		private bool IsTestDone;

		private MainForm owner;

		private TcpControler controler;

		private VisionOffsetTest.VisionOffsetTestStateType VisionOffsetTestState;

		private int RunVisionDelayCounter;

		private double[] VisionCenterX = new double[8];

		private double[] VisionCenterY = new double[8];

		private int VisionTestCounter;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
            ComponentResourceManager resources = new ComponentResourceManager(typeof(VisionOffsetTest));
			this.labelStep4 = new Label();
			this.labelStep3 = new Label();
			this.timerUI = new Timer(this.components);
			this.labelStep2 = new Label();
			this.labelStep1 = new Label();
			this.labelArrow1 = new Label();
			this.buttonOK = new Button();
			this.buttonCancel = new Button();
			this.buttonRun = new Button();
			base.SuspendLayout();
			resources.ApplyResources(this.labelStep4, "labelStep4");
			this.labelStep4.Name = "labelStep4";
			resources.ApplyResources(this.labelStep3, "labelStep3");
			this.labelStep3.Name = "labelStep3";
			this.timerUI.Tick += new EventHandler(this.timerUI_Tick);
			resources.ApplyResources(this.labelStep2, "labelStep2");
			this.labelStep2.Name = "labelStep2";
			resources.ApplyResources(this.labelStep1, "labelStep1");
			this.labelStep1.Name = "labelStep1";
			resources.ApplyResources(this.labelArrow1, "labelArrow1");
			this.labelArrow1.Image = Resource.Arrow;
			this.labelArrow1.Name = "labelArrow1";
			resources.ApplyResources(this.buttonOK, "buttonOK");
			this.buttonOK.BackgroundImage = Resource.ButtonUp;
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.UseVisualStyleBackColor = true;
			this.buttonOK.Click += new EventHandler(this.buttonOK_Click);
			resources.ApplyResources(this.buttonCancel, "buttonCancel");
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			resources.ApplyResources(this.buttonRun, "buttonRun");
			this.buttonRun.BackgroundImage = Resource.ButtonUp;
			this.buttonRun.Name = "buttonRun";
			this.buttonRun.UseVisualStyleBackColor = true;
			this.buttonRun.Click += new EventHandler(this.buttonRun_Click);
			resources.ApplyResources(this, "$this");
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.labelArrow1);
			base.Controls.Add(this.buttonOK);
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonRun);
			base.Controls.Add(this.labelStep4);
			base.Controls.Add(this.labelStep3);
			base.Controls.Add(this.labelStep2);
			base.Controls.Add(this.labelStep1);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "VisionOffsetTest";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.Load += new EventHandler(this.VisionOffsetTest_Load);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public VisionOffsetTest(MainForm o)
		{
			this.owner = o;
			this.InitializeComponent();
		}

		private void VisionOffsetTest_Load(object sender, EventArgs e)
		{
			Label expr_06 = this.labelStep1;
			string text = expr_06.Text;
			expr_06.Text = string.Concat(new string[]
			{
				text,
				"(",
				this.VisionX.ToString("F2"),
				",",
				this.VisionY.ToString("F2"),
				")"
			});
			Label expr_67 = this.labelStep3;
			string text2 = expr_67.Text;
			expr_67.Text = string.Concat(new string[]
			{
				text2,
				"(",
				(this.VisionX + this.Nozzle2OffsetX).ToString("F2"),
				",",
				(this.VisionY + this.Nozzle2OffsetY).ToString("F2"),
				")"
			});
		}

		private void buttonRun_Click(object sender, EventArgs e)
		{
			if (this.buttonRun.Text == this.LocRM.GetString("buttonRun.Text"))
			{
				if (this.controler == null)
				{
					this.controler = this.owner.controler;
				}
				if (this.VisionOffsetTestState == VisionOffsetTest.VisionOffsetTestStateType.None)
				{
					this.controler.WriteSpeed_IMM(5, 100);
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage1;
					this.timerUI.Enabled = true;
				}
				this.buttonRun.Text = this.LocStrings.GetString("buttonRunStop");
				this.buttonRun.BackgroundImage = Resource.ButtonDown;
				return;
			}
			this.owner.controler.MHalt(TcpControler.AxisType.AxisAll);
			this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.None;
			this.buttonRun.Text = this.LocRM.GetString("buttonRun.Text");
			this.buttonRun.BackgroundImage = Resource.ButtonUp;
		}

		private void buttonOK_Click(object sender, EventArgs e)
		{
			this.owner.controler.MHalt(TcpControler.AxisType.AxisAll);
			if (this.VisionOffsetTestState == VisionOffsetTest.VisionOffsetTestStateType.None && this.IsTestDone)
			{
				base.DialogResult = DialogResult.OK;
			}
			else
			{
				base.DialogResult = DialogResult.Cancel;
			}
			base.Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			this.owner.controler.MHalt(TcpControler.AxisType.AxisAll);
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void timerUI_Tick(object sender, EventArgs e)
		{
			switch (this.VisionOffsetTestState)
			{
			case VisionOffsetTest.VisionOffsetTestStateType.None:
				this.owner.MainVisionTest = true;
				this.buttonOK.Enabled = true;
				this.buttonRun.Text = this.LocRM.GetString("buttonRun.Text");
				this.buttonRun.BackgroundImage = Resource.ButtonUp;
				this.timerUI.Enabled = false;
				return;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage1:
				this.buttonOK.Enabled = false;
				this.owner.MainVisionTest = false;
				this.labelArrow1.Location = new Point(this.labelArrow1.Location.X, this.labelStep1.Location.Y + this.labelStep1.Size.Height / 2 - this.labelArrow1.Size.Height / 2);
				if (this.controler.TPTP(1, 3, 5, 0.0, -135.0, -135.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage2;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage2:
				if (this.controler.SPTP(2, this.VisionX, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage3;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage3:
				if (this.controler.SPTP(4, this.VisionY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage4;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage4:
				if (this.controler.SPTP(1, this.owner.NozzleVisionZ, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionTestCounter = 0;
					this.labelArrow1.Location = new Point(this.labelArrow1.Location.X, this.labelStep2.Location.Y + this.labelStep2.Size.Height / 2 - this.labelArrow1.Size.Height / 2);
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage5;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage5:
				if (this.RunVisionDelayCounter == 0)
				{
					this.RunVisionDelayCounter = 5;
				}
				this.RunVisionDelayCounter--;
				if (this.RunVisionDelayCounter == 0)
				{
					if (this.owner.mVision1.Camera1 != null)
					{
						this.owner.mVision1.CompThreshold = (int)this.owner.VisionValueNozzle;
						this.owner.HeartBeatBackup = this.owner.mVision1.HeartBeat;
						this.owner.mVision1.CheckNozzleRun();
					}
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage6;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage6:
				if (this.owner.HeartBeatBackup != this.owner.mVision1.HeartBeat)
				{
					this.VisionCenterX[this.VisionTestCounter] = this.owner.mVision1.CompOffsetW1;
					this.VisionCenterY[this.VisionTestCounter] = this.owner.mVision1.CompOffsetH1;
					this.VisionTestCounter++;
					if (this.VisionTestCounter >= 8)
					{
						this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage8;
						return;
					}
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage7;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage7:
				if (this.controler.SPTP(3, (double)(this.VisionTestCounter * 45 - 135), CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage5;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage8:
				if (this.controler.SPTP(3, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.Nozzle1CenterOffsetX = this.VisionCenterX.Average();
					this.Nozzle1CenterOffsetY = this.VisionCenterY.Average();
					this.labelStep2.Text = string.Concat(new string[]
					{
						this.LocRM.GetString("labelStep2.Text"),
						"(",
						this.Nozzle1CenterOffsetX.ToString("F2"),
						",",
						this.Nozzle1CenterOffsetY.ToString("F2"),
						")"
					});
					this.labelArrow1.Location = new Point(this.labelArrow1.Location.X, this.labelStep3.Location.Y + this.labelStep3.Size.Height / 2 - this.labelArrow1.Size.Height / 2);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM);
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage9;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage9:
				if (this.controler.DPTP(2, 4, this.VisionX + this.Nozzle2OffsetX, this.VisionY + this.Nozzle2OffsetY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage10;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage10:
				if (this.controler.SPTP(1, -this.owner.NozzleVisionZ, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionTestCounter = 0;
					this.labelArrow1.Location = new Point(this.labelArrow1.Location.X, this.labelStep4.Location.Y + this.labelStep4.Size.Height / 2 - this.labelArrow1.Size.Height / 2);
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage11;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage11:
				if (this.RunVisionDelayCounter == 0)
				{
					this.RunVisionDelayCounter = 5;
				}
				this.RunVisionDelayCounter--;
				if (this.RunVisionDelayCounter == 0)
				{
					if (this.owner.mVision1.Camera1 != null)
					{
						this.owner.mVision1.CompThreshold = (int)this.owner.VisionValueNozzle;
						this.owner.HeartBeatBackup = this.owner.mVision1.HeartBeat;
						this.owner.mVision1.CheckNozzleRun();
					}
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage12;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage12:
				if (this.owner.HeartBeatBackup != this.owner.mVision1.HeartBeat)
				{
					this.VisionCenterX[this.VisionTestCounter] = this.owner.mVision1.CompOffsetW1;
					this.VisionCenterY[this.VisionTestCounter] = this.owner.mVision1.CompOffsetH1;
					this.VisionTestCounter++;
					if (this.VisionTestCounter >= 8)
					{
						this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage14;
						return;
					}
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage13;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage13:
				if (this.controler.SPTP(5, (double)(this.VisionTestCounter * 45 - 135), CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.Stage11;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.Stage14:
				if (this.controler.SPTP(5, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.Nozzle2CenterOffsetX = this.VisionCenterX.Average();
					this.Nozzle2CenterOffsetY = this.VisionCenterY.Average();
					this.labelStep4.Text = string.Concat(new string[]
					{
						this.LocRM.GetString("labelStep4.Text"),
						"(",
						this.Nozzle2CenterOffsetX.ToString("F2"),
						",",
						this.Nozzle2CenterOffsetY.ToString("F2"),
						")"
					});
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM);
					this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.OK;
					return;
				}
				break;
			case VisionOffsetTest.VisionOffsetTestStateType.OK:
				this.IsTestDone = true;
				this.VisionOffsetTestState = VisionOffsetTest.VisionOffsetTestStateType.None;
				break;
			default:
				return;
			}
		}
	}
}
