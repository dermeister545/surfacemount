using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class MakeArray : Form
	{
		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		public double xOffset;

		public double yOffset;

		public int xNum;

		public int yNum;

		private IContainer components;

		public Button buttonNo;

		public Button buttonYes;

		private TextBox textBoxXOffset;

		private Label label1;

		private TextBox textBoxYOffset;

		private Label label2;

		private TextBox textBoxXNum;

		private Label label3;

		private TextBox textBoxYNum;

		private Label label4;

		public MakeArray()
		{
			this.InitializeComponent();
		}

		public MakeArray(int x, int y) : this()
		{
			base.Location = new Point(x - base.Size.Width / 2, y - base.Size.Height / 2);
			base.DialogResult = DialogResult.No;
		}

		private void buttonNo_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.No;
			base.Close();
		}

		private void buttonYes_Click(object sender, EventArgs e)
		{
			bool flag = false;
			if (!int.TryParse(this.textBoxXNum.Text, out this.xNum))
			{
				flag = true;
			}
			if (!int.TryParse(this.textBoxYNum.Text, out this.yNum))
			{
				flag = true;
			}
			if (!double.TryParse(this.textBoxXOffset.Text, out this.xOffset))
			{
				flag = true;
			}
			if (!double.TryParse(this.textBoxYOffset.Text, out this.yOffset))
			{
				flag = true;
			}
			if (flag)
			{
				new MyMessageBox(null, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.xNum <= 0 || this.yNum <= 0)
			{
				new MyMessageBox(null, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			base.DialogResult = DialogResult.Yes;
			base.Close();
		}

		private void label4_Click(object sender, EventArgs e)
		{
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager resources = new ComponentResourceManager(typeof(MakeArray));
			this.buttonNo = new Button();
			this.buttonYes = new Button();
			this.textBoxXOffset = new TextBox();
			this.label1 = new Label();
			this.textBoxYOffset = new TextBox();
			this.label2 = new Label();
			this.textBoxXNum = new TextBox();
			this.label3 = new Label();
			this.textBoxYNum = new TextBox();
			this.label4 = new Label();
			base.SuspendLayout();
			resources.ApplyResources(this.buttonNo, "buttonNo");
			this.buttonNo.Name = "buttonNo";
			this.buttonNo.UseVisualStyleBackColor = true;
			this.buttonNo.Click += new EventHandler(this.buttonNo_Click);
			resources.ApplyResources(this.buttonYes, "buttonYes");
			this.buttonYes.Name = "buttonYes";
			this.buttonYes.UseVisualStyleBackColor = true;
			this.buttonYes.Click += new EventHandler(this.buttonYes_Click);
			resources.ApplyResources(this.textBoxXOffset, "textBoxXOffset");
			this.textBoxXOffset.Name = "textBoxXOffset";
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			resources.ApplyResources(this.textBoxYOffset, "textBoxYOffset");
			this.textBoxYOffset.Name = "textBoxYOffset";
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			resources.ApplyResources(this.textBoxXNum, "textBoxXNum");
			this.textBoxXNum.Name = "textBoxXNum";
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			resources.ApplyResources(this.textBoxYNum, "textBoxYNum");
			this.textBoxYNum.Name = "textBoxYNum";
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			this.label4.Click += new EventHandler(this.label4_Click);
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.textBoxYNum);
			base.Controls.Add(this.label4);
			base.Controls.Add(this.textBoxXNum);
			base.Controls.Add(this.label3);
			base.Controls.Add(this.textBoxYOffset);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.textBoxXOffset);
			base.Controls.Add(this.label1);
			base.Controls.Add(this.buttonNo);
			base.Controls.Add(this.buttonYes);
			base.Name = "MakeArray";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.TopMost = true;
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
