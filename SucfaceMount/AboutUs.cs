using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class AboutUs : Form
	{
		private IContainer components;

		private PictureBox pictureBox1;

		private Label label1;

		private Button buttonSave;

		public AboutUs()
		{
			this.InitializeComponent();
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			base.Close();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager resources = new ComponentResourceManager(typeof(AboutUs));
			this.pictureBox1 = new PictureBox();
			this.label1 = new Label();
			this.buttonSave = new Button();
			((ISupportInitialize)this.pictureBox1).BeginInit();
			base.SuspendLayout();
			resources.ApplyResources(this.pictureBox1, "pictureBox1");
			this.pictureBox1.BackgroundImage = Resource.aboutUs;
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.TabStop = false;
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.BackgroundImage = Resource.ButtonUp;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new EventHandler(this.buttonSave_Click);
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = Color.FromArgb(240, 240, 240);
			base.Controls.Add(this.buttonSave);
			base.Controls.Add(this.label1);
			base.Controls.Add(this.pictureBox1);
			base.Name = "AboutUs";
			base.ShowIcon = false;
			((ISupportInitialize)this.pictureBox1).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
