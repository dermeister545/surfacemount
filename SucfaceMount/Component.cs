using System;
using System.Resources;

namespace SucfaceMount
{
	internal class Component
	{
		private string designator;

		private string footprint;

		private string comment;

		private string layer;

		private double midX;

		private double midY;

		private double rotation;

		private string speed;

		private double height;

		private string visionType;

		private bool isPressureCheck;

		private string nozzleNum;

		private string stackNum;

		private string explanation;

		private bool isSMD;

		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		public string Designator
		{
			get
			{
				return this.designator;
			}
			set
			{
				this.designator = value;
			}
		}

		public string Footprint
		{
			get
			{
				return this.footprint;
			}
			set
			{
				this.footprint = value;
			}
		}

		public string Comment
		{
			get
			{
				return this.comment;
			}
			set
			{
				this.comment = value;
			}
		}

		public string Layer
		{
			get
			{
				return this.layer;
			}
			set
			{
				this.layer = value;
			}
		}

		public double MidX
		{
			get
			{
				return this.midX;
			}
			set
			{
				this.midX = value;
			}
		}

		public double MidY
		{
			get
			{
				return this.midY;
			}
			set
			{
				this.midY = value;
			}
		}

		public double Rotation
		{
			get
			{
				return this.rotation;
			}
			set
			{
				this.rotation = value;
			}
		}

		public string Speed
		{
			get
			{
				return this.speed;
			}
			set
			{
				this.speed = value;
			}
		}

		public double Height
		{
			get
			{
				return this.height;
			}
			set
			{
				this.height = value;
			}
		}

		public string VisionType
		{
			get
			{
				return this.visionType;
			}
			set
			{
				this.visionType = value;
			}
		}

		public bool IsPressureCheck
		{
			get
			{
				return this.isPressureCheck;
			}
			set
			{
				this.isPressureCheck = value;
			}
		}

		public string NozzleNum
		{
			get
			{
				return this.nozzleNum;
			}
			set
			{
				this.nozzleNum = value;
			}
		}

		public string StackNum
		{
			get
			{
				return this.stackNum;
			}
			set
			{
				this.stackNum = value;
			}
		}

		public string Explanation
		{
			get
			{
				return this.explanation;
			}
			set
			{
				this.explanation = value;
			}
		}

		public bool IsSMD
		{
			get
			{
				return this.isSMD;
			}
			set
			{
				this.isSMD = value;
			}
		}

		public Component()
		{
			this.designator = "";
			this.comment = "";
			this.nozzleNum = "1";
			this.stackNum = "L1";
			this.midX = 0.0;
			this.midY = 0.0;
			this.rotation = 0.0;
			this.speed = "100";
			this.visionType = this.LocStrings.GetString("displaygridViewFileVisionsNone");
			this.explanation = "";
			this.isSMD = true;
		}
	}
}
