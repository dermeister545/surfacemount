using MVisionControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class MainForm : Form
	{
		private enum RunStateType
		{
			None,
			Ready,
			VisionMark,
			VisionNozzle,
			ChangeNozzle,
			VisionNozzleCheck1,
			VisionNozzleCheck2,
			FindNextComponent,
			FeedComponent1,
			PickComponent1,
			FeedComponent2,
			PickComponent2,
			FeedComponent1And2,
			PickComponent1And2,
			DiscardComponent,
			VisionCenter1,
			VisionCenter2,
			PlaceComponent1,
			PlaceComponent2,
			Done
		}

		private enum ComponentStateType
		{
			None,
			Suck,
			Discard,
			Discarded,
			Done
		}

		private class SMTComponent
		{
			public int Num;

			public int Nozzle;

			public bool IsFrontStack;

			public int Stack;

			public double X;

			public double Y;

			public double Height;

			public double Angle;

			public int Speed;

			public int VisonType;

			public bool IsPressureCheck;

			public double PickAngle;

			public double OffsetX;

			public double OffsetY;

			private int aAxis;

			public int ZAxisDir = 1;

			public MainForm.ComponentStateType State;

			public bool IsVisionDone;

			public int AAxis
			{
				get
				{
					return this.aAxis;
				}
				set
				{
					this.aAxis = value;
					if (value == 5)
					{
						this.ZAxisDir = -1;
						return;
					}
					this.ZAxisDir = 1;
				}
			}
		}

		private const int FAxis = 0;

		private const int ZAxis = 1;

		private const int XAxis = 2;

		private const int A1Axis = 3;

		private const int YAxis = 4;

		private const int A2Axis = 5;

		private const int VisionNozzleTestNum = 8;

		private IContainer components;

		private Label labelFile;

		private Panel panelMain;

		private Panel panelControl;

		private Panel panelVision;

		private Label labelA1Pos;

		private Label labelA1;

		private Label labelYPos;

		private Label labelY;

		private Label labelZ1Pos;

		private Label labelZ1;

		private Label labelXPos;

		private Label labelX;

		private Button buttonSuspend;

		private Button buttonRun;

		private Button buttonStep;

		private Label labelPressure1;

		private Label labelStrip;

		private Label labelPrickHome;

		private Label labelBlowing1;

		private Label labelVacuum1;

		private OpenFileDialog openFileDialog;

		private ContextMenuStrip contextMenuStripFile;

		private ToolStripMenuItem ToolStripMenuItemAdd;

		private ToolStripMenuItem ToolStripMenuItemSelectSame;

		private ToolStripMenuItem ToolStripMenuItemDelete;

		private SaveFileDialog saveFileDialog;

		private Button buttonNozzle1Down;

		private Button buttonNozzle2Down;

		private Button buttonPrick;

		private Button buttonStrip;

		private Button buttonBlowing1;

		private Button buttonVacuum1;

		private Button buttonYUp;

		private Button buttonXUp;

		private Button buttonYDown;

		private Button buttonXDown;

		private Button buttonNozzle1Up;

		private Button buttonNozzle2Up;

		private Panel panelRun;

		private Panel panelManual;

		private Timer timerUpdate;

		private Label labelSpeed;

		private TrackBar trackBarSpeed;

		private ToolStripMenuItem ToolStripMenuItemClearSelect;

		private ProgressBar progressBarRun;

		private Label labelTime;

		private Label labelCompNum;

		private Button buttonPump;

		private Label labelAngle;

		private Label labelOffsetH;

		private Label labelOffetW;

		private Button buttonSave;

		private Button buttonManual;

		private Button buttonLoad;

		public Button buttonParam;

		private Button buttonGoHome;

		public Button buttonPCB;

		private Panel panelButton;

		private Label label7;

		private Label label6;

		private Label label5;

		private Label label4;

		private Label label2;

		private Label labelHeight;

		private Label labelWidth;

		private Button buttonSysParam;

		private Button buttonAuto;

		private Panel panelAuto;

		private Button buttonStopPTPX;

		private Button buttonPTPX;

		private Button buttonStopPTPA1;

		private Button buttonPTPA1;

		private Button buttonStopPTPZ1;

		private Button buttonPTPZ1;

		private Button buttonStopPTPY;

		private Button buttonPTPY;

		private TextBox textBoxA1;

		private TextBox textBoxZ1;

		private TextBox textBoxY;

		private TextBox textBoxX;

		private Label label8;

		private Label label3;

		private Label label1;

		private ToolStripMenuItem ToolStripMenuItemAddCur;

		private ToolStripMenuItem ToolStripMenuItemSetCur;

		private ToolStripSeparator toolStripSeparator1;

		private ToolStripSeparator toolStripSeparator2;

		private Label label9;

		private Button buttonManualSpeed;

		public MVision mVision1;

		private Button buttonA1Up;

		private Button buttonA1Down;

		private ToolStripSeparator toolStripSeparator3;

		private ToolStripMenuItem ToolStripMenuItemAutoSort;

		private Button buttonVacuum2;

		private Button buttonBlowing2;

		private Button buttonA2Up;

		private Button buttonA2Down;

		private Label labelVacuum2;

		private Label labelPressure2;

		private Label labelBlowing2;

		private Button buttonStopPTPA2;

		private Button buttonPTPA2;

		private TextBox textBoxA2;

		private Label label10;

		private Label labelZ2Pos;

		private Label labelZ2;

		private Label labelA2Pos;

		private Label labelA2;

		private Button buttonStopPTPZ2;

		private Button buttonPTPZ2;

		private TextBox textBoxZ2;

		private Label label11;

		private Button buttonGetNozzleCenter;

		private ToolStripMenuItem ToolStripMenuItemAddVisionCur;

		private ToolStripMenuItem ToolStripMenuItemSetVisionCur;

		private Button buttonSetNozzleCenter;

		private Label labelSelectedCount;

		private ToolStripSeparator toolStripSeparator4;

		private ToolStripMenuItem ToolStripMenuItemVisionAlignment;

		private ToolStripSeparator toolStripSeparator5;

		private ToolStripMenuItem ToolStripMenuItemAddVisionCurMark;

		private ToolStripMenuItem ToolStripMenuItemSetVisionCurMark;

		private ContextMenuStrip contextMenuStack;

		private ToolStripMenuItem 左边料栈ToolStripMenuItem;

		private ToolStripMenuItem toolStripMenuItem2;

		private ToolStripMenuItem toolStripMenuItem3;

		private ToolStripMenuItem toolStripMenuItem4;

		private ToolStripMenuItem toolStripMenuItem5;

		private ToolStripMenuItem toolStripMenuItem6;

		private ToolStripMenuItem toolStripMenuItem7;

		private ToolStripMenuItem toolStripMenuItem8;

		private ToolStripMenuItem toolStripMenuItem9;

		private ToolStripMenuItem toolStripMenuItem10;

		private ToolStripMenuItem toolStripMenuItem11;

		private ToolStripMenuItem toolStripMenuItem12;

		private ToolStripMenuItem toolStripMenuItem13;

		private ToolStripMenuItem toolStripMenuItem14;

		private ToolStripMenuItem toolStripMenuItem15;

		private ToolStripMenuItem toolStripMenuItem16;

		private ToolStripMenuItem toolStripMenuItem17;

		private ToolStripMenuItem toolStripMenuItem18;

		private ToolStripMenuItem toolStripMenuItem19;

		private ToolStripMenuItem toolStripMenuItem20;

		private ToolStripMenuItem toolStripMenuItem21;

		private ToolStripMenuItem toolStripMenuItem22;

		private ToolStripMenuItem toolStripMenuItem23;

		private ToolStripMenuItem toolStripMenuItem24;

		private ToolStripMenuItem toolStripMenuItem25;

		private ToolStripMenuItem toolStripMenuItem26;

		private ToolStripMenuItem toolStripMenuItem27;

		private ToolStripMenuItem toolStripMenuItem28;

		private ToolStripMenuItem toolStripMenuItem29;

		private ToolStripMenuItem toolStripMenuItem30;

		private ToolStripMenuItem toolStripMenuItem31;

		private ToolStripMenuItem 后边料栈ToolStripMenuItem;

		private ToolStripMenuItem 托盘ToolStripMenuItem;

		private ToolStripMenuItem toolStripMenuItem32;

		private ToolStripMenuItem toolStripMenuItem33;

		private ToolStripMenuItem toolStripMenuItem34;

		private ToolStripMenuItem toolStripMenuItem35;

		private ToolStripMenuItem toolStripMenuItem36;

		private ToolStripMenuItem toolStripMenuItem37;

		private ToolStripMenuItem toolStripMenuItem38;

		private ToolStripMenuItem toolStripMenuItem39;

		private ToolStripMenuItem toolStripMenuItem40;

		private ToolStripMenuItem toolStripMenuItem41;

		private ToolStripMenuItem toolStripMenuItem42;

		private ToolStripMenuItem toolStripMenuItem43;

		private ToolStripMenuItem toolStripMenuItem44;

		private ToolStripMenuItem toolStripMenuItem45;

		private ToolStripMenuItem toolStripMenuItem46;

		private ToolStripMenuItem toolStripMenuItem47;

		private ToolStripMenuItem toolStripMenuItem48;

		private ToolStripMenuItem toolStripMenuItem49;

		private ToolStripMenuItem toolStripMenuItem50;

		private ToolStripMenuItem toolStripMenuItem51;

		private ToolStripMenuItem toolStripMenuItem52;

		private ToolStripMenuItem toolStripMenuItem53;

		private ToolStripMenuItem toolStripMenuItem54;

		private ToolStripMenuItem toolStripMenuItem55;

		private ToolStripMenuItem toolStripMenuItem56;

		private ToolStripMenuItem toolStripMenuItem57;

		private ToolStripMenuItem toolStripMenuItem58;

		private ToolStripMenuItem toolStripMenuItem59;

		private ToolStripMenuItem toolStripMenuItem60;

		private ToolStripMenuItem toolStripMenuItem61;

		private ToolStripMenuItem toolStripMenuItem62;

		private ToolStripMenuItem toolStripMenuItem63;

		private ToolStripMenuItem toolStripMenuItem64;

		private ToolStripMenuItem toolStripMenuItem65;

		private ToolStripMenuItem toolStripMenuItem66;

		private ToolStripMenuItem toolStripMenuItem67;

		private ToolStripMenuItem toolStripMenuItem68;

		private ToolStripMenuItem toolStripMenuItem69;

		private ToolStripMenuItem toolStripMenuItem70;

		private ToolStripMenuItem toolStripMenuItem71;

		private ToolStripMenuItem ToolStripMenuItemArray;

		private Panel panelGridView;

		private DataGridView gridViewFile;

		private DataGridViewTextBoxColumn ID;

		private DataGridViewComboBoxColumn NozzleNums;

		private DataGridViewButtonColumn StackNums;

		private DataGridViewTextBoxColumn MidXs;

		private DataGridViewTextBoxColumn MidYs;

		private DataGridViewTextBoxColumn Angles;

		private DataGridViewTextBoxColumn Heights;

		private DataGridViewComboBoxColumn Speeds;

		private DataGridViewComboBoxColumn VisionsType;

		private DataGridViewCheckBoxColumn IsPressureCheck;

		private DataGridViewCheckBoxColumn IsDone;

		private DataGridViewTextBoxColumn Comments;

		private ToolStripMenuItem toolStripMenuItem72;

		private ToolStripMenuItem toolStripMenuItem73;

		private ToolStripMenuItem toolStripMenuItem74;

		private ToolStripMenuItem toolStripMenuItem75;

		private ToolStripMenuItem toolStripMenuItem76;

		private ToolStripMenuItem toolStripMenuItem77;

		private ToolStripMenuItem toolStripMenuItem78;

		private ToolStripMenuItem toolStripMenuItem79;

		private ToolStripMenuItem toolStripMenuItem80;

		private ToolStripMenuItem toolStripMenuItem81;

		private ToolStripMenuItem toolStripMenuItem82;

		private ToolStripMenuItem toolStripMenuItem83;

		private ToolStripMenuItem toolStripMenuItem84;

		private ToolStripMenuItem toolStripMenuItem85;

		private ToolStripMenuItem toolStripMenuItem86;

		private ToolStripMenuItem toolStripMenuItem87;

		private ToolStripMenuItem toolStripMenuItem88;

		private ToolStripMenuItem toolStripMenuItem89;

		private ToolStripMenuItem toolStripMenuItem90;

		private ToolStripMenuItem toolStripMenuItem91;

		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		private ResourceManager LocRM = new ResourceManager(typeof(MainForm));

		private string CsvFilePath;

		private string[] StrCsvTopArray;

		private List<Component> ComponentArray = new List<Component>();

		private SelectStackDialog selectStackDialog = new SelectStackDialog();

		public TcpControler controler = new TcpControler();

		public uint macAdd1;

		public uint macAdd2;

		public string MacStr;

		public uint ipAdd = 134260928u;

		public uint ipPort = 701u;

		public double HomePosX = 340.84;

		public double HomePosY = 364.28;

		public double VisionX = -59.85;

		public double VisionY = -40.81;

		public double VisionZ = -4.0;

		public double NozzleVisionZ = -4.0;

		public double NozzleOffsetX;

		public double NozzleOffsetY;

		public int BlowingDealy1;

		public int BlowingDealy2;

		public uint ZAxisStackDealy = 10u;

		public uint ZAxisPcbDealy1 = 10u;

		public uint ZAxisPcbDealy2 = 10u;

		public uint CameraKey1;

		public uint CameraKey2;

		public double VisionOffsetX = -30.1;

		public double VisionOffsetY = 28.86;

		public double LeftPrickOffsetX = -20.5;

		public double LeftPrickOffsetY = -4.78;

		public double BackPrickOffsetX = -20.5;

		public double BackPrickOffsetY = -4.78;

		public double DiscardCompPosX = 100.0;

		public double DiscardCompPosY = 100.0;

		public double VisionValueNozzle = 40.0;

		public double FeedBack = 0.3;

		public double VisionValueComp = 100.0;

		public double ZAxisNozzleDownPCBPos = -5.0;

		public double ZAxisNozzleDownStackPos = -5.0;

		public double ZAxisNozzleDownFrontStackPos = -5.0;

		public double ZAxisNozzleDownDiscardPos = -5.0;

		public double ZAxisPrickDownPos = 5.0;

		public bool DoubleVisionCheck;

		public bool PressureCheckTotal;

		private double pixelScaleX1 = 1.0;

		private double pixelScaleY1 = 1.0;

		private double pixelScaleX2 = 1.0;

		private double pixelScaleY2 = 1.0;

		public NozzleType Nozzle = default(NozzleType);

		public StackType LeftAndBackStack = default(StackType);

		public IcStackType IcStack = default(IcStackType);

		public PuzzleType Puzzle = default(PuzzleType);

		public OtherType Other = default(OtherType);

		public MarkType Mark = default(MarkType);

		private int newObj = 1;

		private bool useTopLayer;

		private bool useBottomLayer;

		private bool useXMirror;

		private bool useSMD;

		private double[] NozzleSizeMin;

		private double[] NozzleSizeMax;

		public bool HeartBeatBackup;

		private bool runTimer;

		private long startTime;

		private int doneNum;

		private bool isConnect;

		private int[] IcStackCount = new int[30];

		private double IcStackX;

		private double IcStackY;

		public double MinNozzleX;

		public double MaxNozzleY;

		public double ReadyToVisionY = 4.0;

		public double MinReadyY;

		private int manualPanel;

		private bool IsHoming;

		public bool IsPCBEdit;

		private PCBEdit PCBForm;

		public bool IsParamEdit;

		private ParamEdit ParamForm;

		public bool MainVisionTest = true;

		private List<Point> SelCells = new List<Point>();

		private int RowStackSel;

		private bool ButtonXUp;

		private bool ButtonXDown;

		private int DealyXUp;

		private int DealyXDown;

		private bool DealyXUpDone;

		private bool DealyXDownDone;

		private bool ButtonYUp;

		private bool ButtonYDown;

		private int DealyYUp;

		private int DealyYDown;

		private bool DealyYUpDone;

		private bool DealyYDownDone;

		private bool isVacuum1On;

		private bool isVacuum2On;

		private bool isPrickOn;

		private bool isBlowing1On;

		private bool isBlowing2On;

		private bool isPumpOn;

		private MyMessageBoxYesNo StopRunForm;

		private MyMessageBoxYesNo ResetNeedForm;

		private MainForm.RunStateType runState;

		private int SubState;

		private bool runNozzleSignal;

		private bool runSignal;

		private bool stepSignal;

		private bool suspendSignal;

		private int NozzleNum;

		private int ChangeToNozzleNum;

		private int[] StackMove = new int[60];

		private int Feed2MM;

		private int CurCompNum;

		private MainForm.SMTComponent CurComp1 = new MainForm.SMTComponent();

		private MainForm.SMTComponent CurComp2 = new MainForm.SMTComponent();

		private int ErrorCounter;

		private int ZAxisHomeErrorCounter;

		private int SpeedBackUp = 1;

		private int RunDelayCounter;

		private double SuspendPosX;

		private double SuspendPosY;

		private double[] VisionNozzle1CenterX = new double[8];

		private double[] VisionNozzle1CenterY = new double[8];

		private double[] VisionNozzle2CenterX = new double[8];

		private double[] VisionNozzle2CenterY = new double[8];

		private double AvrVisionNozzle1CenterX;

		private double AvrVisionNozzle1CenterY;

		private double AvrVisionNozzle2CenterX;

		private double AvrVisionNozzle2CenterY;

		public double Nozzle1VisionX;

		public double Nozzle1VisionY;

		public double Nozzle2VisionX;

		public double Nozzle2VisionY;

		private bool isNozzleOffsetUsed;

		private int VisionNozzleTestCounter;

		private double NozzleOffsetXAtAngle;

		private double NozzleOffsetYAtAngle;

		public int PuzzleNum = 1;

		private int CurPuzzleNum1;

		private int CurPuzzleNum2;

		private int CurVisionMarkPuzzleNum;

		private double MarkOffsetX;

		private double MarkOffsetY;

		private int GetMarkCounter;

		private double VisionOffsetTempX;

		private double VisionOffsetTempY;

		private double VisionOffsetTempAngle;

		private double VisionCenterAngles;

		private int VisionCounter;

		private int VisionRightCounter;

		private double AssistPosX;

		private double AssistPosY;

		private MainForm.RunStateType RunStateBackUp;

		private int CountFor100MS;

		private int CountFor1S;

		private double[] PosBack = new double[6];

		private bool labelPressureBack;

		private bool labelPressure2Back;

		private bool labelVacuumBack;

		private bool labelVacuum2Back;

		private bool labelLaserBack;

		private bool labelBlowingBack;

		private bool labelBlowing2Back;

		private bool labelStripBack;

		private bool labelPrickHomeBack;

		private bool labelNozzleHomeBack;

		private bool labelNozzleStack1Back;

		private bool labelNozzleStack2Back;

		private bool labelNozzleStack3Back;

		private bool labelNozzleStack4Back;

		private bool XLMTNBack;

		private bool XLMTPBack;

		private bool YLMTNBack;

		private bool YLMTPBack;

		private bool buttonXUpBack;

		private bool buttonXDownBack;

		private bool buttonYUpBack;

		private bool buttonYDownBack;

		private bool buttonSpeedGradeBack;

		private bool buttonStepBack;

		private bool buttonPauseBack;

		private bool buttonStartBack;

		private bool IsEnter;

		private CheckVisionType CheckVision = CheckVisionType.ComponentVision;

		public double CameraAngle1
		{
			get
			{
				return this.mVision1.CompAngle1;
			}
			set
			{
				if (Math.Abs(value) < 200.0)
				{
					this.mVision1.CompAngle1 = value;
				}
			}
		}

		public double MarkVisionOffsetX
		{
			get
			{
				return this.mVision1.MarkVisionOffsetX;
			}
			set
			{
				if (Math.Abs(value) < 200.0)
				{
					this.mVision1.MarkVisionOffsetX = value;
				}
			}
		}

		public double MarkVisionOffsetY
		{
			get
			{
				return this.mVision1.MarkVisionOffsetY;
			}
			set
			{
				if (Math.Abs(value) < 200.0)
				{
					this.mVision1.MarkVisionOffsetY = value;
				}
			}
		}

		public double PixelScaleX1
		{
			get
			{
				return this.pixelScaleX1;
			}
			set
			{
				MVision arg_10_0 = this.mVision1;
				this.pixelScaleX1 = value;
				arg_10_0.PixelScaleX1 = value;
			}
		}

		public double PixelScaleY1
		{
			get
			{
				return this.pixelScaleY1;
			}
			set
			{
				MVision arg_10_0 = this.mVision1;
				this.pixelScaleY1 = value;
				arg_10_0.PixelScaleY1 = value;
			}
		}

		public double PixelScaleX2
		{
			get
			{
				return this.pixelScaleX2;
			}
			set
			{
				MVision arg_10_0 = this.mVision1;
				this.pixelScaleX2 = value;
				arg_10_0.PixelScaleX2 = value;
			}
		}

		public double PixelScaleY2
		{
			get
			{
				return this.pixelScaleY2;
			}
			set
			{
				MVision arg_10_0 = this.mVision1;
				this.pixelScaleY2 = value;
				arg_10_0.PixelScaleY2 = value;
			}
		}

		public bool RunTimer
		{
			get
			{
				return this.runTimer;
			}
			set
			{
				if (value != this.runTimer)
				{
					this.runTimer = value;
					if (this.runTimer)
					{
						this.startTime = DateTime.Now.Ticks;
					}
				}
			}
		}

		public long RunTime
		{
			get
			{
				if (this.RunTimer)
				{
					return (long)new TimeSpan(DateTime.Now.Ticks - this.startTime).TotalSeconds;
				}
				return 0L;
			}
		}

		public int DoneNum
		{
			get
			{
				return this.doneNum;
			}
			set
			{
				this.doneNum = value;
				this.labelCompNum.Text = this.doneNum.ToString() + "/" + this.gridViewFile.RowCount.ToString();
				if (this.doneNum > this.progressBarRun.Maximum)
				{
					this.doneNum = this.progressBarRun.Maximum;
				}
				this.progressBarRun.Value = this.doneNum;
			}
		}

		public bool IsConnect
		{
			get
			{
				return this.isConnect;
			}
			set
			{
				if (this.isConnect != value && value)
				{
					this.LoadParam();
				}
				this.isConnect = value;
			}
		}

		public int ManualSpeed
		{
			get
			{
				return this.controler.ManualSpeedGrade;
			}
			set
			{
				this.controler.ManualSpeedGrade = value;
				switch (value)
				{
				case 0:
					this.buttonManualSpeed.Text = this.LocStrings.GetString("labelSpeedLow");
					return;
				case 1:
					break;
				case 2:
					this.buttonManualSpeed.Text = this.LocStrings.GetString("labelSpeedHigh");
					break;
				default:
					return;
				}
			}
		}

		public int ManualPanel
		{
			get
			{
				return this.manualPanel;
			}
			set
			{
				switch (value)
				{
				case 0:
					this.panelManual.Visible = false;
					this.panelAuto.Visible = false;
					this.panelRun.Visible = true;
					this.buttonManual.BackgroundImage = Resource.ButtonUp;
					this.buttonAuto.BackgroundImage = Resource.ButtonUp;
					break;
				case 1:
					this.panelAuto.Visible = false;
					this.panelRun.Visible = false;
					this.panelManual.Visible = true;
					this.buttonManual.BackgroundImage = Resource.ButtonDown;
					this.buttonAuto.BackgroundImage = Resource.ButtonUp;
					break;
				case 2:
					this.panelManual.Visible = false;
					this.panelRun.Visible = false;
					this.panelAuto.Visible = true;
					this.buttonManual.BackgroundImage = Resource.ButtonUp;
					this.buttonAuto.BackgroundImage = Resource.ButtonDown;
					break;
				}
				this.manualPanel = value;
			}
		}

		private MainForm.RunStateType RunState
		{
			get
			{
				return this.runState;
			}
			set
			{
				this.runState = value;
				this.SubState = 0;
			}
		}

		public bool RunNozzleSignal
		{
			get
			{
				return this.runNozzleSignal;
			}
			set
			{
				this.runNozzleSignal = value;
				if (this.runNozzleSignal)
				{
					this.controler.SetDO_ON_IMM(32u);
					this.buttonGetNozzleCenter.BackgroundImage = Resource.ButtonDown;
					this.buttonRun.Enabled = false;
					return;
				}
				this.controler.SetDO_OFF_IMM(32u);
				this.buttonGetNozzleCenter.BackgroundImage = Resource.ButtonUp;
				this.buttonRun.Enabled = true;
			}
		}

		public bool RunSignal
		{
			get
			{
				return this.runSignal;
			}
			set
			{
				this.runSignal = value;
				if (this.runSignal)
				{
					this.controler.SetDO_ON_IMM(32u);
					this.buttonRun.BackgroundImage = Resource.ButtonDown;
					this.buttonRun.Text = this.LocStrings.GetString("buttonStopText");
					this.buttonGetNozzleCenter.Enabled = false;
					return;
				}
				this.controler.SetDO_OFF_IMM(16416u);
				this.buttonRun.BackgroundImage = Resource.ButtonUp;
				this.buttonRun.Text = this.LocStrings.GetString("buttonRunText");
				this.buttonGetNozzleCenter.Enabled = true;
			}
		}

		public bool StepSignal
		{
			get
			{
				return this.stepSignal;
			}
			set
			{
				this.stepSignal = value;
			}
		}

		public bool SuspendSignal
		{
			get
			{
				return this.suspendSignal;
			}
			set
			{
				if (this.suspendSignal != value)
				{
					this.suspendSignal = value;
					if (this.suspendSignal)
					{
						if (this.IsConnect && this.controler.ControlState == ControlBufType.RUN_BUF)
						{
							this.controler.ControlCmdBuf(ControlBufType.PAUSE_BUF);
						}
						this.controler.SetDO_ON_IMM(64u);
						this.buttonSuspend.BackgroundImage = Resource.ButtonDown;
						return;
					}
					if (this.IsConnect && this.controler.ControlState == ControlBufType.PAUSE_BUF)
					{
						this.controler.ControlCmdBuf(ControlBufType.RUN_BUF);
					}
					this.controler.SetDO_OFF_IMM(64u);
					this.buttonSuspend.BackgroundImage = Resource.ButtonUp;
				}
			}
		}

		public bool IsNozzleOffsetUsed
		{
			get
			{
				return this.isNozzleOffsetUsed;
			}
			set
			{
				this.isNozzleOffsetUsed = value;
				if (this.isNozzleOffsetUsed)
				{
					this.buttonSetNozzleCenter.BackgroundImage = Resource.ButtonDown;
					return;
				}
				this.buttonSetNozzleCenter.BackgroundImage = Resource.ButtonUp;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager resources = new ComponentResourceManager(typeof(MainForm));
			DataGridViewCellStyle dataGridViewCellStyle = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle2 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle3 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle4 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle5 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle6 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle7 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle8 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle9 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle10 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle11 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle12 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle13 = new DataGridViewCellStyle();
			DataGridViewCellStyle dataGridViewCellStyle14 = new DataGridViewCellStyle();
			this.labelFile = new Label();
			this.contextMenuStripFile = new ContextMenuStrip(this.components);
			this.ToolStripMenuItemAdd = new ToolStripMenuItem();
			this.ToolStripMenuItemAddCur = new ToolStripMenuItem();
			this.ToolStripMenuItemAddVisionCur = new ToolStripMenuItem();
			this.ToolStripMenuItemAddVisionCurMark = new ToolStripMenuItem();
			this.toolStripSeparator4 = new ToolStripSeparator();
			this.ToolStripMenuItemSetCur = new ToolStripMenuItem();
			this.ToolStripMenuItemSetVisionCur = new ToolStripMenuItem();
			this.ToolStripMenuItemSetVisionCurMark = new ToolStripMenuItem();
			this.toolStripSeparator1 = new ToolStripSeparator();
			this.ToolStripMenuItemVisionAlignment = new ToolStripMenuItem();
			this.toolStripSeparator5 = new ToolStripSeparator();
			this.ToolStripMenuItemSelectSame = new ToolStripMenuItem();
			this.ToolStripMenuItemClearSelect = new ToolStripMenuItem();
			this.toolStripSeparator2 = new ToolStripSeparator();
			this.ToolStripMenuItemDelete = new ToolStripMenuItem();
			this.toolStripSeparator3 = new ToolStripSeparator();
			this.ToolStripMenuItemAutoSort = new ToolStripMenuItem();
			this.ToolStripMenuItemArray = new ToolStripMenuItem();
			this.labelAngle = new Label();
			this.labelOffsetH = new Label();
			this.labelOffetW = new Label();
			this.panelMain = new Panel();
			this.panelGridView = new Panel();
			this.gridViewFile = new DataGridView();
			this.ID = new DataGridViewTextBoxColumn();
			this.NozzleNums = new DataGridViewComboBoxColumn();
			this.StackNums = new DataGridViewButtonColumn();
			this.MidXs = new DataGridViewTextBoxColumn();
			this.MidYs = new DataGridViewTextBoxColumn();
			this.Angles = new DataGridViewTextBoxColumn();
			this.Heights = new DataGridViewTextBoxColumn();
			this.Speeds = new DataGridViewComboBoxColumn();
			this.VisionsType = new DataGridViewComboBoxColumn();
			this.IsPressureCheck = new DataGridViewCheckBoxColumn();
			this.IsDone = new DataGridViewCheckBoxColumn();
			this.Comments = new DataGridViewTextBoxColumn();
			this.labelSelectedCount = new Label();
			this.panelControl = new Panel();
			this.labelZ2Pos = new Label();
			this.labelZ2 = new Label();
			this.labelA2Pos = new Label();
			this.labelA2 = new Label();
			this.labelPressure2 = new Label();
			this.labelBlowing2 = new Label();
			this.labelVacuum2 = new Label();
			this.labelStrip = new Label();
			this.labelPrickHome = new Label();
			this.labelBlowing1 = new Label();
			this.labelVacuum1 = new Label();
			this.labelPressure1 = new Label();
			this.labelA1Pos = new Label();
			this.labelYPos = new Label();
			this.labelY = new Label();
			this.labelZ1Pos = new Label();
			this.labelZ1 = new Label();
			this.labelXPos = new Label();
			this.labelX = new Label();
			this.panelVision = new Panel();
			this.label7 = new Label();
			this.label6 = new Label();
			this.label5 = new Label();
			this.label4 = new Label();
			this.label2 = new Label();
			this.labelHeight = new Label();
			this.labelWidth = new Label();
			this.mVision1 = new MVision();
			this.labelA1 = new Label();
			this.panelManual = new Panel();
			this.buttonA2Up = new Button();
			this.buttonBlowing2 = new Button();
			this.buttonVacuum2 = new Button();
			this.buttonA1Up = new Button();
			this.buttonA1Down = new Button();
			this.buttonManualSpeed = new Button();
			this.buttonPump = new Button();
			this.buttonBlowing1 = new Button();
			this.buttonStrip = new Button();
			this.buttonPrick = new Button();
			this.buttonNozzle2Down = new Button();
			this.buttonNozzle2Up = new Button();
			this.buttonNozzle1Up = new Button();
			this.buttonVacuum1 = new Button();
			this.buttonNozzle1Down = new Button();
			this.buttonXDown = new Button();
			this.buttonYDown = new Button();
			this.buttonXUp = new Button();
			this.buttonYUp = new Button();
			this.buttonA2Down = new Button();
			this.panelRun = new Panel();
			this.buttonSetNozzleCenter = new Button();
			this.buttonGetNozzleCenter = new Button();
			this.labelTime = new Label();
			this.labelCompNum = new Label();
			this.progressBarRun = new ProgressBar();
			this.labelSpeed = new Label();
			this.buttonRun = new Button();
			this.buttonSuspend = new Button();
			this.buttonStep = new Button();
			this.trackBarSpeed = new TrackBar();
			this.panelAuto = new Panel();
			this.buttonStopPTPZ2 = new Button();
			this.buttonPTPZ2 = new Button();
			this.textBoxZ2 = new TextBox();
			this.label11 = new Label();
			this.label10 = new Label();
			this.buttonStopPTPA2 = new Button();
			this.buttonPTPA2 = new Button();
			this.textBoxA2 = new TextBox();
			this.label9 = new Label();
			this.buttonStopPTPA1 = new Button();
			this.buttonPTPA1 = new Button();
			this.buttonStopPTPZ1 = new Button();
			this.buttonPTPZ1 = new Button();
			this.buttonStopPTPY = new Button();
			this.buttonPTPY = new Button();
			this.textBoxA1 = new TextBox();
			this.textBoxZ1 = new TextBox();
			this.textBoxY = new TextBox();
			this.textBoxX = new TextBox();
			this.label8 = new Label();
			this.label3 = new Label();
			this.label1 = new Label();
			this.buttonStopPTPX = new Button();
			this.buttonPTPX = new Button();
			this.openFileDialog = new OpenFileDialog();
			this.saveFileDialog = new SaveFileDialog();
			this.timerUpdate = new Timer(this.components);
			this.panelButton = new Panel();
			this.buttonAuto = new Button();
			this.buttonSysParam = new Button();
			this.buttonPCB = new Button();
			this.buttonGoHome = new Button();
			this.buttonParam = new Button();
			this.buttonLoad = new Button();
			this.buttonManual = new Button();
			this.buttonSave = new Button();
			this.contextMenuStack = new ContextMenuStrip(this.components);
			this.左边料栈ToolStripMenuItem = new ToolStripMenuItem();
			this.toolStripMenuItem2 = new ToolStripMenuItem();
			this.toolStripMenuItem3 = new ToolStripMenuItem();
			this.toolStripMenuItem4 = new ToolStripMenuItem();
			this.toolStripMenuItem5 = new ToolStripMenuItem();
			this.toolStripMenuItem6 = new ToolStripMenuItem();
			this.toolStripMenuItem7 = new ToolStripMenuItem();
			this.toolStripMenuItem8 = new ToolStripMenuItem();
			this.toolStripMenuItem9 = new ToolStripMenuItem();
			this.toolStripMenuItem10 = new ToolStripMenuItem();
			this.toolStripMenuItem11 = new ToolStripMenuItem();
			this.toolStripMenuItem12 = new ToolStripMenuItem();
			this.toolStripMenuItem13 = new ToolStripMenuItem();
			this.toolStripMenuItem14 = new ToolStripMenuItem();
			this.toolStripMenuItem15 = new ToolStripMenuItem();
			this.toolStripMenuItem16 = new ToolStripMenuItem();
			this.toolStripMenuItem17 = new ToolStripMenuItem();
			this.toolStripMenuItem18 = new ToolStripMenuItem();
			this.toolStripMenuItem19 = new ToolStripMenuItem();
			this.toolStripMenuItem20 = new ToolStripMenuItem();
			this.toolStripMenuItem21 = new ToolStripMenuItem();
			this.toolStripMenuItem22 = new ToolStripMenuItem();
			this.toolStripMenuItem23 = new ToolStripMenuItem();
			this.toolStripMenuItem24 = new ToolStripMenuItem();
			this.toolStripMenuItem25 = new ToolStripMenuItem();
			this.toolStripMenuItem26 = new ToolStripMenuItem();
			this.toolStripMenuItem27 = new ToolStripMenuItem();
			this.toolStripMenuItem28 = new ToolStripMenuItem();
			this.toolStripMenuItem29 = new ToolStripMenuItem();
			this.toolStripMenuItem30 = new ToolStripMenuItem();
			this.toolStripMenuItem31 = new ToolStripMenuItem();
			this.后边料栈ToolStripMenuItem = new ToolStripMenuItem();
			this.toolStripMenuItem32 = new ToolStripMenuItem();
			this.toolStripMenuItem33 = new ToolStripMenuItem();
			this.toolStripMenuItem34 = new ToolStripMenuItem();
			this.toolStripMenuItem35 = new ToolStripMenuItem();
			this.toolStripMenuItem36 = new ToolStripMenuItem();
			this.toolStripMenuItem37 = new ToolStripMenuItem();
			this.toolStripMenuItem38 = new ToolStripMenuItem();
			this.toolStripMenuItem39 = new ToolStripMenuItem();
			this.toolStripMenuItem40 = new ToolStripMenuItem();
			this.toolStripMenuItem41 = new ToolStripMenuItem();
			this.toolStripMenuItem42 = new ToolStripMenuItem();
			this.toolStripMenuItem43 = new ToolStripMenuItem();
			this.toolStripMenuItem44 = new ToolStripMenuItem();
			this.toolStripMenuItem45 = new ToolStripMenuItem();
			this.toolStripMenuItem46 = new ToolStripMenuItem();
			this.toolStripMenuItem47 = new ToolStripMenuItem();
			this.toolStripMenuItem48 = new ToolStripMenuItem();
			this.toolStripMenuItem49 = new ToolStripMenuItem();
			this.toolStripMenuItem50 = new ToolStripMenuItem();
			this.toolStripMenuItem51 = new ToolStripMenuItem();
			this.toolStripMenuItem52 = new ToolStripMenuItem();
			this.toolStripMenuItem53 = new ToolStripMenuItem();
			this.toolStripMenuItem54 = new ToolStripMenuItem();
			this.toolStripMenuItem55 = new ToolStripMenuItem();
			this.toolStripMenuItem56 = new ToolStripMenuItem();
			this.toolStripMenuItem57 = new ToolStripMenuItem();
			this.toolStripMenuItem58 = new ToolStripMenuItem();
			this.toolStripMenuItem59 = new ToolStripMenuItem();
			this.toolStripMenuItem60 = new ToolStripMenuItem();
			this.toolStripMenuItem61 = new ToolStripMenuItem();
			this.托盘ToolStripMenuItem = new ToolStripMenuItem();
			this.toolStripMenuItem62 = new ToolStripMenuItem();
			this.toolStripMenuItem63 = new ToolStripMenuItem();
			this.toolStripMenuItem64 = new ToolStripMenuItem();
			this.toolStripMenuItem65 = new ToolStripMenuItem();
			this.toolStripMenuItem66 = new ToolStripMenuItem();
			this.toolStripMenuItem67 = new ToolStripMenuItem();
			this.toolStripMenuItem68 = new ToolStripMenuItem();
			this.toolStripMenuItem69 = new ToolStripMenuItem();
			this.toolStripMenuItem70 = new ToolStripMenuItem();
			this.toolStripMenuItem71 = new ToolStripMenuItem();
			this.toolStripMenuItem72 = new ToolStripMenuItem();
			this.toolStripMenuItem73 = new ToolStripMenuItem();
			this.toolStripMenuItem74 = new ToolStripMenuItem();
			this.toolStripMenuItem75 = new ToolStripMenuItem();
			this.toolStripMenuItem76 = new ToolStripMenuItem();
			this.toolStripMenuItem77 = new ToolStripMenuItem();
			this.toolStripMenuItem78 = new ToolStripMenuItem();
			this.toolStripMenuItem79 = new ToolStripMenuItem();
			this.toolStripMenuItem80 = new ToolStripMenuItem();
			this.toolStripMenuItem81 = new ToolStripMenuItem();
			this.toolStripMenuItem82 = new ToolStripMenuItem();
			this.toolStripMenuItem83 = new ToolStripMenuItem();
			this.toolStripMenuItem84 = new ToolStripMenuItem();
			this.toolStripMenuItem85 = new ToolStripMenuItem();
			this.toolStripMenuItem86 = new ToolStripMenuItem();
			this.toolStripMenuItem87 = new ToolStripMenuItem();
			this.toolStripMenuItem88 = new ToolStripMenuItem();
			this.toolStripMenuItem89 = new ToolStripMenuItem();
			this.toolStripMenuItem90 = new ToolStripMenuItem();
			this.toolStripMenuItem91 = new ToolStripMenuItem();
			this.contextMenuStripFile.SuspendLayout();
			this.panelMain.SuspendLayout();
			this.panelGridView.SuspendLayout();
			((ISupportInitialize)this.gridViewFile).BeginInit();
			this.panelControl.SuspendLayout();
			this.panelVision.SuspendLayout();
			this.panelManual.SuspendLayout();
			this.panelRun.SuspendLayout();
			((ISupportInitialize)this.trackBarSpeed).BeginInit();
			this.panelAuto.SuspendLayout();
			this.panelButton.SuspendLayout();
			this.contextMenuStack.SuspendLayout();
			base.SuspendLayout();
			this.labelFile.BackColor = SystemColors.Control;
			this.labelFile.BorderStyle = BorderStyle.FixedSingle;
			resources.ApplyResources(this.labelFile, "labelFile");
			this.labelFile.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelFile.Name = "labelFile";
			resources.ApplyResources(this.contextMenuStripFile, "contextMenuStripFile");
			this.contextMenuStripFile.Items.AddRange(new ToolStripItem[]
			{
				this.ToolStripMenuItemAdd,
				this.ToolStripMenuItemAddCur,
				this.ToolStripMenuItemAddVisionCur,
				this.ToolStripMenuItemAddVisionCurMark,
				this.toolStripSeparator4,
				this.ToolStripMenuItemSetCur,
				this.ToolStripMenuItemSetVisionCur,
				this.ToolStripMenuItemSetVisionCurMark,
				this.toolStripSeparator1,
				this.ToolStripMenuItemVisionAlignment,
				this.toolStripSeparator5,
				this.ToolStripMenuItemSelectSame,
				this.ToolStripMenuItemClearSelect,
				this.toolStripSeparator2,
				this.ToolStripMenuItemDelete,
				this.toolStripSeparator3,
				this.ToolStripMenuItemAutoSort,
				this.ToolStripMenuItemArray
			});
			this.contextMenuStripFile.Name = "contextMenuStripFile";
			this.contextMenuStripFile.ShowImageMargin = false;
			this.ToolStripMenuItemAdd.Name = "ToolStripMenuItemAdd";
			resources.ApplyResources(this.ToolStripMenuItemAdd, "ToolStripMenuItemAdd");
			this.ToolStripMenuItemAdd.Click += new EventHandler(this.ToolStripMenuItemAdd_Click);
			this.ToolStripMenuItemAddCur.Name = "ToolStripMenuItemAddCur";
			resources.ApplyResources(this.ToolStripMenuItemAddCur, "ToolStripMenuItemAddCur");
			this.ToolStripMenuItemAddCur.Click += new EventHandler(this.ToolStripMenuItemAddCur_Click);
			this.ToolStripMenuItemAddVisionCur.Name = "ToolStripMenuItemAddVisionCur";
			resources.ApplyResources(this.ToolStripMenuItemAddVisionCur, "ToolStripMenuItemAddVisionCur");
			this.ToolStripMenuItemAddVisionCur.Click += new EventHandler(this.ToolStripMenuItemAddVisionCur_Click);
			this.ToolStripMenuItemAddVisionCurMark.Name = "ToolStripMenuItemAddVisionCurMark";
			resources.ApplyResources(this.ToolStripMenuItemAddVisionCurMark, "ToolStripMenuItemAddVisionCurMark");
			this.ToolStripMenuItemAddVisionCurMark.Click += new EventHandler(this.ToolStripMenuItemAddVisionCurMark_Click);
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
			this.ToolStripMenuItemSetCur.Name = "ToolStripMenuItemSetCur";
			resources.ApplyResources(this.ToolStripMenuItemSetCur, "ToolStripMenuItemSetCur");
			this.ToolStripMenuItemSetCur.Click += new EventHandler(this.ToolStripMenuItemSetCur_Click);
			this.ToolStripMenuItemSetVisionCur.Name = "ToolStripMenuItemSetVisionCur";
			resources.ApplyResources(this.ToolStripMenuItemSetVisionCur, "ToolStripMenuItemSetVisionCur");
			this.ToolStripMenuItemSetVisionCur.Click += new EventHandler(this.toolStripMenuItemSetVisionCur_Click);
			this.ToolStripMenuItemSetVisionCurMark.Name = "ToolStripMenuItemSetVisionCurMark";
			resources.ApplyResources(this.ToolStripMenuItemSetVisionCurMark, "ToolStripMenuItemSetVisionCurMark");
			this.ToolStripMenuItemSetVisionCurMark.Click += new EventHandler(this.ToolStripMenuItemSetVisionCurMark_Click);
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
			this.ToolStripMenuItemVisionAlignment.Name = "ToolStripMenuItemVisionAlignment";
			resources.ApplyResources(this.ToolStripMenuItemVisionAlignment, "ToolStripMenuItemVisionAlignment");
			this.ToolStripMenuItemVisionAlignment.Click += new EventHandler(this.toolStripMenuItemVisionAlignment_Click);
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
			this.ToolStripMenuItemSelectSame.Name = "ToolStripMenuItemSelectSame";
			resources.ApplyResources(this.ToolStripMenuItemSelectSame, "ToolStripMenuItemSelectSame");
			this.ToolStripMenuItemSelectSame.Click += new EventHandler(this.ToolStripMenuItemSelectSame_Click);
			this.ToolStripMenuItemClearSelect.Name = "ToolStripMenuItemClearSelect";
			resources.ApplyResources(this.ToolStripMenuItemClearSelect, "ToolStripMenuItemClearSelect");
			this.ToolStripMenuItemClearSelect.Click += new EventHandler(this.ToolStripMenuItemClearSelect_Click);
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
			this.ToolStripMenuItemDelete.Name = "ToolStripMenuItemDelete";
			resources.ApplyResources(this.ToolStripMenuItemDelete, "ToolStripMenuItemDelete");
			this.ToolStripMenuItemDelete.Click += new EventHandler(this.ToolStripMenuItemDelete_Click);
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
			this.ToolStripMenuItemAutoSort.Name = "ToolStripMenuItemAutoSort";
			resources.ApplyResources(this.ToolStripMenuItemAutoSort, "ToolStripMenuItemAutoSort");
			this.ToolStripMenuItemAutoSort.Click += new EventHandler(this.ToolStripMenuItemAutoSort_Click);
			this.ToolStripMenuItemArray.Name = "ToolStripMenuItemArray";
			resources.ApplyResources(this.ToolStripMenuItemArray, "ToolStripMenuItemArray");
			this.ToolStripMenuItemArray.Click += new EventHandler(this.ToolStripMenuItemArray_Click);
			resources.ApplyResources(this.labelAngle, "labelAngle");
			this.labelAngle.ForeColor = Color.White;
			this.labelAngle.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelAngle.Name = "labelAngle";
			resources.ApplyResources(this.labelOffsetH, "labelOffsetH");
			this.labelOffsetH.ForeColor = Color.White;
			this.labelOffsetH.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelOffsetH.Name = "labelOffsetH";
			resources.ApplyResources(this.labelOffetW, "labelOffetW");
			this.labelOffetW.ForeColor = Color.White;
			this.labelOffetW.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelOffetW.Name = "labelOffetW";
			resources.ApplyResources(this.panelMain, "panelMain");
			this.panelMain.Controls.Add(this.panelGridView);
			this.panelMain.Controls.Add(this.labelSelectedCount);
			this.panelMain.Controls.Add(this.labelFile);
			this.panelMain.Controls.Add(this.panelControl);
			this.panelMain.Name = "panelMain";
			resources.ApplyResources(this.panelGridView, "panelGridView");
			this.panelGridView.Controls.Add(this.gridViewFile);
			this.panelGridView.Name = "panelGridView";
			this.gridViewFile.AllowUserToAddRows = false;
			this.gridViewFile.AllowUserToDeleteRows = false;
			this.gridViewFile.AllowUserToResizeRows = false;
			this.gridViewFile.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
			this.gridViewFile.BackgroundColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle.BackColor = SystemColors.Control;
			dataGridViewCellStyle.Font = new Font("Arial", 10f);
			dataGridViewCellStyle.ForeColor = SystemColors.WindowText;
			dataGridViewCellStyle.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle.WrapMode = DataGridViewTriState.True;
			this.gridViewFile.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle;
			resources.ApplyResources(this.gridViewFile, "gridViewFile");
			this.gridViewFile.Columns.AddRange(new DataGridViewColumn[]
			{
				this.ID,
				this.NozzleNums,
				this.StackNums,
				this.MidXs,
				this.MidYs,
				this.Angles,
				this.Heights,
				this.Speeds,
				this.VisionsType,
				this.IsPressureCheck,
				this.IsDone,
				this.Comments
			});
			dataGridViewCellStyle2.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle2.BackColor = SystemColors.Window;
			dataGridViewCellStyle2.Font = new Font("Arial", 9f);
			dataGridViewCellStyle2.ForeColor = SystemColors.ControlText;
			dataGridViewCellStyle2.SelectionBackColor = SystemColors.Highlight;
			dataGridViewCellStyle2.SelectionForeColor = SystemColors.HighlightText;
			dataGridViewCellStyle2.WrapMode = DataGridViewTriState.False;
			this.gridViewFile.DefaultCellStyle = dataGridViewCellStyle2;
			this.gridViewFile.EditMode = DataGridViewEditMode.EditOnF2;
			this.gridViewFile.Name = "gridViewFile";
			this.gridViewFile.RowTemplate.Height = 23;
			this.gridViewFile.CellContentClick += new DataGridViewCellEventHandler(this.gridViewFile_CellContentClick);
			this.gridViewFile.CellMouseDoubleClick += new DataGridViewCellMouseEventHandler(this.gridViewFile_CellMouseDoubleClick);
			this.gridViewFile.CellMouseDown += new DataGridViewCellMouseEventHandler(this.gridViewFile_CellMouseDown);
			this.gridViewFile.CellValueChanged += new DataGridViewCellEventHandler(this.gridViewFile_CellValueChanged);
			this.gridViewFile.CurrentCellDirtyStateChanged += new EventHandler(this.gridViewFile_CurrentCellDirtyStateChanged);
			this.gridViewFile.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.gridViewFile_EditingControlShowing);
			this.gridViewFile.SelectionChanged += new EventHandler(this.gridViewFile_SelectionChanged);
			this.gridViewFile.SortCompare += new DataGridViewSortCompareEventHandler(this.gridViewFile_SortCompare);
			this.gridViewFile.KeyDown += new KeyEventHandler(this.gridViewFile_KeyDown);
			this.gridViewFile.MouseDown += new MouseEventHandler(this.gridViewFile_MouseDown);
			this.ID.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle3.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle3.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle3.Font = new Font("Arial", 11f);
			dataGridViewCellStyle3.SelectionForeColor = Color.White;
			this.ID.DefaultCellStyle = dataGridViewCellStyle3;
			this.ID.FillWeight = 20f;
			resources.ApplyResources(this.ID, "ID");
			this.ID.Name = "ID";
			this.NozzleNums.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle4.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle4.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle4.Font = new Font("Arial", 11f);
			this.NozzleNums.DefaultCellStyle = dataGridViewCellStyle4;
			this.NozzleNums.FillWeight = 10f;
			this.NozzleNums.FlatStyle = FlatStyle.Flat;
			resources.ApplyResources(this.NozzleNums, "NozzleNums");
			this.NozzleNums.Items.AddRange(new object[]
			{
				"1",
				"2",
				"1/2"
			});
			this.NozzleNums.Name = "NozzleNums";
			this.NozzleNums.Resizable = DataGridViewTriState.True;
			this.NozzleNums.SortMode = DataGridViewColumnSortMode.Automatic;
			this.StackNums.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle5.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle5.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle5.Font = new Font("Arial", 11f);
			this.StackNums.DefaultCellStyle = dataGridViewCellStyle5;
			this.StackNums.FillWeight = 10f;
			resources.ApplyResources(this.StackNums, "StackNums");
			this.StackNums.Name = "StackNums";
			this.StackNums.SortMode = DataGridViewColumnSortMode.Automatic;
			this.MidXs.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle6.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle6.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle6.Font = new Font("Arial", 11f);
			this.MidXs.DefaultCellStyle = dataGridViewCellStyle6;
			this.MidXs.FillWeight = 20f;
			resources.ApplyResources(this.MidXs, "MidXs");
			this.MidXs.Name = "MidXs";
			this.MidYs.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle7.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle7.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle7.Font = new Font("Arial", 11f);
			this.MidYs.DefaultCellStyle = dataGridViewCellStyle7;
			this.MidYs.FillWeight = 20f;
			resources.ApplyResources(this.MidYs, "MidYs");
			this.MidYs.Name = "MidYs";
			this.Angles.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle8.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle8.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle8.Font = new Font("Arial", 11f);
			this.Angles.DefaultCellStyle = dataGridViewCellStyle8;
			this.Angles.FillWeight = 15f;
			resources.ApplyResources(this.Angles, "Angles");
			this.Angles.Name = "Angles";
			this.Heights.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle9.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle9.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle9.Font = new Font("Arial", 11f);
			this.Heights.DefaultCellStyle = dataGridViewCellStyle9;
			this.Heights.FillWeight = 20f;
			resources.ApplyResources(this.Heights, "Heights");
			this.Heights.Name = "Heights";
			this.Speeds.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle10.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle10.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle10.Font = new Font("Arial", 11f);
			this.Speeds.DefaultCellStyle = dataGridViewCellStyle10;
			this.Speeds.FillWeight = 10f;
			this.Speeds.FlatStyle = FlatStyle.Flat;
			resources.ApplyResources(this.Speeds, "Speeds");
			this.Speeds.Items.AddRange(new object[]
			{
				"100",
				"90",
				"80",
				"70",
				"60",
				"50",
				"40",
				"30",
				"20",
				"10",
				"5"
			});
			this.Speeds.Name = "Speeds";
			this.VisionsType.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle11.Alignment = DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle11.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle11.Font = new Font("Arial", 11f);
			this.VisionsType.DefaultCellStyle = dataGridViewCellStyle11;
			this.VisionsType.FillWeight = 15f;
			this.VisionsType.FlatStyle = FlatStyle.Flat;
			resources.ApplyResources(this.VisionsType, "VisionsType");
			this.VisionsType.Name = "VisionsType";
			this.IsPressureCheck.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle12.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle12.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle12.NullValue = false;
			this.IsPressureCheck.DefaultCellStyle = dataGridViewCellStyle12;
			this.IsPressureCheck.FillWeight = 10f;
			resources.ApplyResources(this.IsPressureCheck, "IsPressureCheck");
			this.IsPressureCheck.Name = "IsPressureCheck";
			this.IsDone.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle13.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle13.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle13.NullValue = false;
			this.IsDone.DefaultCellStyle = dataGridViewCellStyle13;
			this.IsDone.FillWeight = 10f;
			resources.ApplyResources(this.IsDone, "IsDone");
			this.IsDone.Name = "IsDone";
			this.Comments.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			dataGridViewCellStyle14.Alignment = DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle14.BackColor = Color.FromArgb(240, 240, 240);
			dataGridViewCellStyle14.Font = new Font("Arial", 11f);
			this.Comments.DefaultCellStyle = dataGridViewCellStyle14;
			this.Comments.FillWeight = 250f;
			resources.ApplyResources(this.Comments, "Comments");
			this.Comments.Name = "Comments";
			resources.ApplyResources(this.labelSelectedCount, "labelSelectedCount");
			this.labelSelectedCount.Name = "labelSelectedCount";
			this.panelControl.Controls.Add(this.labelZ2Pos);
			this.panelControl.Controls.Add(this.labelZ2);
			this.panelControl.Controls.Add(this.labelA2Pos);
			this.panelControl.Controls.Add(this.labelA2);
			this.panelControl.Controls.Add(this.labelPressure2);
			this.panelControl.Controls.Add(this.labelBlowing2);
			this.panelControl.Controls.Add(this.labelVacuum2);
			this.panelControl.Controls.Add(this.labelStrip);
			this.panelControl.Controls.Add(this.labelPrickHome);
			this.panelControl.Controls.Add(this.labelBlowing1);
			this.panelControl.Controls.Add(this.labelVacuum1);
			this.panelControl.Controls.Add(this.labelPressure1);
			this.panelControl.Controls.Add(this.labelA1Pos);
			this.panelControl.Controls.Add(this.labelYPos);
			this.panelControl.Controls.Add(this.labelY);
			this.panelControl.Controls.Add(this.labelZ1Pos);
			this.panelControl.Controls.Add(this.labelZ1);
			this.panelControl.Controls.Add(this.labelXPos);
			this.panelControl.Controls.Add(this.labelX);
			this.panelControl.Controls.Add(this.panelVision);
			this.panelControl.Controls.Add(this.labelA1);
			this.panelControl.Controls.Add(this.panelManual);
			this.panelControl.Controls.Add(this.panelRun);
			this.panelControl.Controls.Add(this.panelAuto);
			resources.ApplyResources(this.panelControl, "panelControl");
			this.panelControl.Name = "panelControl";
			resources.ApplyResources(this.labelZ2Pos, "labelZ2Pos");
			this.labelZ2Pos.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelZ2Pos.Name = "labelZ2Pos";
			resources.ApplyResources(this.labelZ2, "labelZ2");
			this.labelZ2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelZ2.Name = "labelZ2";
			resources.ApplyResources(this.labelA2Pos, "labelA2Pos");
			this.labelA2Pos.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelA2Pos.Name = "labelA2Pos";
			resources.ApplyResources(this.labelA2, "labelA2");
			this.labelA2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelA2.Name = "labelA2";
			resources.ApplyResources(this.labelPressure2, "labelPressure2");
			this.labelPressure2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelPressure2.Name = "labelPressure2";
			resources.ApplyResources(this.labelBlowing2, "labelBlowing2");
			this.labelBlowing2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelBlowing2.Name = "labelBlowing2";
			resources.ApplyResources(this.labelVacuum2, "labelVacuum2");
			this.labelVacuum2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelVacuum2.Name = "labelVacuum2";
			resources.ApplyResources(this.labelStrip, "labelStrip");
			this.labelStrip.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelStrip.Name = "labelStrip";
			resources.ApplyResources(this.labelPrickHome, "labelPrickHome");
			this.labelPrickHome.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelPrickHome.Name = "labelPrickHome";
			resources.ApplyResources(this.labelBlowing1, "labelBlowing1");
			this.labelBlowing1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelBlowing1.Name = "labelBlowing1";
			resources.ApplyResources(this.labelVacuum1, "labelVacuum1");
			this.labelVacuum1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelVacuum1.Name = "labelVacuum1";
			resources.ApplyResources(this.labelPressure1, "labelPressure1");
			this.labelPressure1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelPressure1.Name = "labelPressure1";
			resources.ApplyResources(this.labelA1Pos, "labelA1Pos");
			this.labelA1Pos.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelA1Pos.Name = "labelA1Pos";
			resources.ApplyResources(this.labelYPos, "labelYPos");
			this.labelYPos.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelYPos.Name = "labelYPos";
			resources.ApplyResources(this.labelY, "labelY");
			this.labelY.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelY.Name = "labelY";
			resources.ApplyResources(this.labelZ1Pos, "labelZ1Pos");
			this.labelZ1Pos.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelZ1Pos.Name = "labelZ1Pos";
			resources.ApplyResources(this.labelZ1, "labelZ1");
			this.labelZ1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelZ1.Name = "labelZ1";
			resources.ApplyResources(this.labelXPos, "labelXPos");
			this.labelXPos.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelXPos.Name = "labelXPos";
			resources.ApplyResources(this.labelX, "labelX");
			this.labelX.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelX.Name = "labelX";
			this.panelVision.BackColor = Color.Black;
			this.panelVision.Controls.Add(this.label7);
			this.panelVision.Controls.Add(this.label6);
			this.panelVision.Controls.Add(this.label5);
			this.panelVision.Controls.Add(this.label4);
			this.panelVision.Controls.Add(this.label2);
			this.panelVision.Controls.Add(this.labelHeight);
			this.panelVision.Controls.Add(this.labelWidth);
			this.panelVision.Controls.Add(this.labelAngle);
			this.panelVision.Controls.Add(this.labelOffsetH);
			this.panelVision.Controls.Add(this.labelOffetW);
			this.panelVision.Controls.Add(this.mVision1);
			resources.ApplyResources(this.panelVision, "panelVision");
			this.panelVision.Name = "panelVision";
			resources.ApplyResources(this.label7, "label7");
			this.label7.ForeColor = Color.White;
			this.label7.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label7.Name = "label7";
			resources.ApplyResources(this.label6, "label6");
			this.label6.ForeColor = Color.White;
			this.label6.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label6.Name = "label6";
			resources.ApplyResources(this.label5, "label5");
			this.label5.ForeColor = Color.White;
			this.label5.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label5.Name = "label5";
			resources.ApplyResources(this.label4, "label4");
			this.label4.ForeColor = Color.White;
			this.label4.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label4.Name = "label4";
			resources.ApplyResources(this.label2, "label2");
			this.label2.ForeColor = Color.White;
			this.label2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label2.Name = "label2";
			resources.ApplyResources(this.labelHeight, "labelHeight");
			this.labelHeight.ForeColor = Color.White;
			this.labelHeight.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelHeight.Name = "labelHeight";
			resources.ApplyResources(this.labelWidth, "labelWidth");
			this.labelWidth.ForeColor = Color.White;
			this.labelWidth.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelWidth.Name = "labelWidth";
			this.mVision1.Answer = 0;
			this.mVision1.BorderStyle = BorderStyle.FixedSingle;
			this.mVision1.CamNum = 0;
			this.mVision1.CompAngle1 = 0.0;
			this.mVision1.CompSum = 0;
			this.mVision1.CompThreshold = 0;
			resources.ApplyResources(this.mVision1, "mVision1");
			this.mVision1.GoDone = true;
			this.mVision1.MarkVisionOffsetX = 0.0;
			this.mVision1.MarkVisionOffsetY = 0.0;
			this.mVision1.ModeShape = -1;
			this.mVision1.Name = "mVision1";
			this.mVision1.TempDia = 0;
			this.mVision1.DoubleClick += new EventHandler(this.mVision1_DoubleClick);
			resources.ApplyResources(this.labelA1, "labelA1");
			this.labelA1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelA1.Name = "labelA1";
			resources.ApplyResources(this.panelManual, "panelManual");
			this.panelManual.Controls.Add(this.buttonA2Up);
			this.panelManual.Controls.Add(this.buttonBlowing2);
			this.panelManual.Controls.Add(this.buttonVacuum2);
			this.panelManual.Controls.Add(this.buttonA1Up);
			this.panelManual.Controls.Add(this.buttonA1Down);
			this.panelManual.Controls.Add(this.buttonManualSpeed);
			this.panelManual.Controls.Add(this.buttonPump);
			this.panelManual.Controls.Add(this.buttonBlowing1);
			this.panelManual.Controls.Add(this.buttonStrip);
			this.panelManual.Controls.Add(this.buttonPrick);
			this.panelManual.Controls.Add(this.buttonNozzle2Down);
			this.panelManual.Controls.Add(this.buttonNozzle2Up);
			this.panelManual.Controls.Add(this.buttonNozzle1Up);
			this.panelManual.Controls.Add(this.buttonVacuum1);
			this.panelManual.Controls.Add(this.buttonNozzle1Down);
			this.panelManual.Controls.Add(this.buttonXDown);
			this.panelManual.Controls.Add(this.buttonYDown);
			this.panelManual.Controls.Add(this.buttonXUp);
			this.panelManual.Controls.Add(this.buttonYUp);
			this.panelManual.Controls.Add(this.buttonA2Down);
			this.panelManual.Name = "panelManual";
			resources.ApplyResources(this.buttonA2Up, "buttonA2Up");
			this.buttonA2Up.BackgroundImage = Resource.ButtonUp;
			this.buttonA2Up.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonA2Up.Name = "buttonA2Up";
			this.buttonA2Up.UseVisualStyleBackColor = true;
			this.buttonA2Up.MouseDown += new MouseEventHandler(this.buttonA2Up_MouseDown);
			this.buttonA2Up.MouseUp += new MouseEventHandler(this.buttonA2Up_MouseUp);
			resources.ApplyResources(this.buttonBlowing2, "buttonBlowing2");
			this.buttonBlowing2.BackgroundImage = Resource.ButtonUp;
			this.buttonBlowing2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonBlowing2.Name = "buttonBlowing2";
			this.buttonBlowing2.UseVisualStyleBackColor = true;
			this.buttonBlowing2.Click += new EventHandler(this.buttonBlowing2_Click);
			resources.ApplyResources(this.buttonVacuum2, "buttonVacuum2");
			this.buttonVacuum2.BackgroundImage = Resource.ButtonUp;
			this.buttonVacuum2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonVacuum2.Name = "buttonVacuum2";
			this.buttonVacuum2.UseVisualStyleBackColor = true;
			this.buttonVacuum2.Click += new EventHandler(this.buttonVacuum2_Click);
			resources.ApplyResources(this.buttonA1Up, "buttonA1Up");
			this.buttonA1Up.BackgroundImage = Resource.ButtonUp;
			this.buttonA1Up.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonA1Up.Name = "buttonA1Up";
			this.buttonA1Up.UseVisualStyleBackColor = true;
			this.buttonA1Up.MouseDown += new MouseEventHandler(this.buttonA1Up_MouseDown);
			this.buttonA1Up.MouseUp += new MouseEventHandler(this.buttonA1Up_MouseUp);
			resources.ApplyResources(this.buttonA1Down, "buttonA1Down");
			this.buttonA1Down.BackgroundImage = Resource.ButtonUp;
			this.buttonA1Down.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonA1Down.Name = "buttonA1Down";
			this.buttonA1Down.UseVisualStyleBackColor = true;
			this.buttonA1Down.MouseDown += new MouseEventHandler(this.buttonA1Down_MouseDown);
			this.buttonA1Down.MouseUp += new MouseEventHandler(this.buttonA1Down_MouseUp);
			resources.ApplyResources(this.buttonManualSpeed, "buttonManualSpeed");
			this.buttonManualSpeed.BackgroundImage = Resource.ButtonUp;
			this.buttonManualSpeed.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonManualSpeed.Name = "buttonManualSpeed";
			this.buttonManualSpeed.UseVisualStyleBackColor = true;
			this.buttonManualSpeed.Click += new EventHandler(this.buttonManualSpeed_Click);
			resources.ApplyResources(this.buttonPump, "buttonPump");
			this.buttonPump.BackgroundImage = Resource.ButtonUp;
			this.buttonPump.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPump.Name = "buttonPump";
			this.buttonPump.UseVisualStyleBackColor = true;
			this.buttonPump.Click += new EventHandler(this.buttonPump_Click);
			resources.ApplyResources(this.buttonBlowing1, "buttonBlowing1");
			this.buttonBlowing1.BackgroundImage = Resource.ButtonUp;
			this.buttonBlowing1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonBlowing1.Name = "buttonBlowing1";
			this.buttonBlowing1.UseVisualStyleBackColor = true;
			this.buttonBlowing1.Click += new EventHandler(this.buttonBlowing1_Click);
			resources.ApplyResources(this.buttonStrip, "buttonStrip");
			this.buttonStrip.BackgroundImage = Resource.ButtonUp;
			this.buttonStrip.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStrip.Name = "buttonStrip";
			this.buttonStrip.UseVisualStyleBackColor = true;
			this.buttonStrip.Click += new EventHandler(this.buttonStrip_Click);
			resources.ApplyResources(this.buttonPrick, "buttonPrick");
			this.buttonPrick.BackgroundImage = Resource.ButtonUp;
			this.buttonPrick.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPrick.Name = "buttonPrick";
			this.buttonPrick.UseVisualStyleBackColor = true;
			this.buttonPrick.Click += new EventHandler(this.buttonPrick_Click);
			resources.ApplyResources(this.buttonNozzle2Down, "buttonNozzle2Down");
			this.buttonNozzle2Down.BackgroundImage = Resource.ButtonUp;
			this.buttonNozzle2Down.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonNozzle2Down.Name = "buttonNozzle2Down";
			this.buttonNozzle2Down.UseVisualStyleBackColor = true;
			this.buttonNozzle2Down.MouseDown += new MouseEventHandler(this.buttonNozzle2Down_MouseDown);
			this.buttonNozzle2Down.MouseUp += new MouseEventHandler(this.buttonNozzle2Down_MouseUp);
			resources.ApplyResources(this.buttonNozzle2Up, "buttonNozzle2Up");
			this.buttonNozzle2Up.BackgroundImage = Resource.ButtonUp;
			this.buttonNozzle2Up.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonNozzle2Up.Name = "buttonNozzle2Up";
			this.buttonNozzle2Up.UseVisualStyleBackColor = true;
			this.buttonNozzle2Up.MouseDown += new MouseEventHandler(this.buttonNozzle2Up_MouseDown);
			this.buttonNozzle2Up.MouseUp += new MouseEventHandler(this.buttonNozzle2Up_MouseUp);
			resources.ApplyResources(this.buttonNozzle1Up, "buttonNozzle1Up");
			this.buttonNozzle1Up.BackgroundImage = Resource.ButtonUp;
			this.buttonNozzle1Up.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonNozzle1Up.Name = "buttonNozzle1Up";
			this.buttonNozzle1Up.UseVisualStyleBackColor = true;
			this.buttonNozzle1Up.MouseDown += new MouseEventHandler(this.buttonNozzleUp_MouseDown);
			this.buttonNozzle1Up.MouseUp += new MouseEventHandler(this.buttonNozzleUp_MouseUp);
			resources.ApplyResources(this.buttonVacuum1, "buttonVacuum1");
			this.buttonVacuum1.BackgroundImage = Resource.ButtonUp;
			this.buttonVacuum1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonVacuum1.Name = "buttonVacuum1";
			this.buttonVacuum1.UseVisualStyleBackColor = true;
			this.buttonVacuum1.Click += new EventHandler(this.buttonVacuum1_Click);
			resources.ApplyResources(this.buttonNozzle1Down, "buttonNozzle1Down");
			this.buttonNozzle1Down.BackgroundImage = Resource.ButtonUp;
			this.buttonNozzle1Down.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonNozzle1Down.Name = "buttonNozzle1Down";
			this.buttonNozzle1Down.UseVisualStyleBackColor = true;
			this.buttonNozzle1Down.MouseDown += new MouseEventHandler(this.buttonNozzleDown_MouseDown);
			this.buttonNozzle1Down.MouseUp += new MouseEventHandler(this.buttonNozzleDown_MouseUp);
			resources.ApplyResources(this.buttonXDown, "buttonXDown");
			this.buttonXDown.BackgroundImage = Resource.ButtonUp;
			this.buttonXDown.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonXDown.Name = "buttonXDown";
			this.buttonXDown.UseVisualStyleBackColor = true;
			this.buttonXDown.MouseDown += new MouseEventHandler(this.buttonXDown_MouseDown);
			this.buttonXDown.MouseUp += new MouseEventHandler(this.buttonXDown_MouseUp);
			resources.ApplyResources(this.buttonYDown, "buttonYDown");
			this.buttonYDown.BackgroundImage = Resource.ButtonUp;
			this.buttonYDown.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonYDown.Name = "buttonYDown";
			this.buttonYDown.UseVisualStyleBackColor = true;
			this.buttonYDown.MouseDown += new MouseEventHandler(this.buttonYDown_MouseDown);
			this.buttonYDown.MouseUp += new MouseEventHandler(this.buttonYDown_MouseUp);
			resources.ApplyResources(this.buttonXUp, "buttonXUp");
			this.buttonXUp.BackgroundImage = Resource.ButtonUp;
			this.buttonXUp.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonXUp.Name = "buttonXUp";
			this.buttonXUp.UseVisualStyleBackColor = true;
			this.buttonXUp.MouseDown += new MouseEventHandler(this.buttonXUp_MouseDown);
			this.buttonXUp.MouseUp += new MouseEventHandler(this.buttonXUp_MouseUp);
			resources.ApplyResources(this.buttonYUp, "buttonYUp");
			this.buttonYUp.BackgroundImage = Resource.ButtonUp;
			this.buttonYUp.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonYUp.Name = "buttonYUp";
			this.buttonYUp.UseVisualStyleBackColor = true;
			this.buttonYUp.MouseDown += new MouseEventHandler(this.buttonYUp_MouseDown);
			this.buttonYUp.MouseUp += new MouseEventHandler(this.buttonYUp_MouseUp);
			resources.ApplyResources(this.buttonA2Down, "buttonA2Down");
			this.buttonA2Down.BackgroundImage = Resource.ButtonUp;
			this.buttonA2Down.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonA2Down.Name = "buttonA2Down";
			this.buttonA2Down.UseVisualStyleBackColor = true;
			this.buttonA2Down.MouseDown += new MouseEventHandler(this.buttonA2Down_MouseDown);
			this.buttonA2Down.MouseUp += new MouseEventHandler(this.buttonA2Down_MouseUp);
			resources.ApplyResources(this.panelRun, "panelRun");
			this.panelRun.Controls.Add(this.buttonSetNozzleCenter);
			this.panelRun.Controls.Add(this.buttonGetNozzleCenter);
			this.panelRun.Controls.Add(this.labelTime);
			this.panelRun.Controls.Add(this.labelCompNum);
			this.panelRun.Controls.Add(this.progressBarRun);
			this.panelRun.Controls.Add(this.labelSpeed);
			this.panelRun.Controls.Add(this.buttonRun);
			this.panelRun.Controls.Add(this.buttonSuspend);
			this.panelRun.Controls.Add(this.buttonStep);
			this.panelRun.Controls.Add(this.trackBarSpeed);
			this.panelRun.Name = "panelRun";
			resources.ApplyResources(this.buttonSetNozzleCenter, "buttonSetNozzleCenter");
			this.buttonSetNozzleCenter.BackgroundImage = Resource.ButtonUp;
			this.buttonSetNozzleCenter.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonSetNozzleCenter.Name = "buttonSetNozzleCenter";
			this.buttonSetNozzleCenter.UseVisualStyleBackColor = true;
			this.buttonSetNozzleCenter.Click += new EventHandler(this.buttonSetNozzleCenter_Click);
			resources.ApplyResources(this.buttonGetNozzleCenter, "buttonGetNozzleCenter");
			this.buttonGetNozzleCenter.BackgroundImage = Resource.ButtonUp;
			this.buttonGetNozzleCenter.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonGetNozzleCenter.Name = "buttonGetNozzleCenter";
			this.buttonGetNozzleCenter.UseVisualStyleBackColor = true;
			this.buttonGetNozzleCenter.Click += new EventHandler(this.buttonGetNozzleCenter_Click);
			resources.ApplyResources(this.labelTime, "labelTime");
			this.labelTime.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelTime.Name = "labelTime";
			resources.ApplyResources(this.labelCompNum, "labelCompNum");
			this.labelCompNum.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelCompNum.Name = "labelCompNum";
			resources.ApplyResources(this.progressBarRun, "progressBarRun");
			this.progressBarRun.Name = "progressBarRun";
			resources.ApplyResources(this.labelSpeed, "labelSpeed");
			this.labelSpeed.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.labelSpeed.Name = "labelSpeed";
			resources.ApplyResources(this.buttonRun, "buttonRun");
			this.buttonRun.BackgroundImage = Resource.ButtonUp;
			this.buttonRun.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonRun.Name = "buttonRun";
			this.buttonRun.UseVisualStyleBackColor = true;
			this.buttonRun.Click += new EventHandler(this.buttonRun_Click);
			resources.ApplyResources(this.buttonSuspend, "buttonSuspend");
			this.buttonSuspend.BackgroundImage = Resource.ButtonUp;
			this.buttonSuspend.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonSuspend.Name = "buttonSuspend";
			this.buttonSuspend.UseVisualStyleBackColor = true;
			this.buttonSuspend.Click += new EventHandler(this.buttonSuspend_Click);
			resources.ApplyResources(this.buttonStep, "buttonStep");
			this.buttonStep.BackgroundImage = Resource.ButtonUp;
			this.buttonStep.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStep.Name = "buttonStep";
			this.buttonStep.UseVisualStyleBackColor = true;
			this.buttonStep.Click += new EventHandler(this.buttonStep_Click);
			resources.ApplyResources(this.trackBarSpeed, "trackBarSpeed");
			this.trackBarSpeed.LargeChange = 1;
			this.trackBarSpeed.Maximum = 11;
			this.trackBarSpeed.Name = "trackBarSpeed";
			this.trackBarSpeed.TabStop = false;
			this.trackBarSpeed.TickStyle = TickStyle.None;
			this.trackBarSpeed.Value = 3;
			this.trackBarSpeed.ValueChanged += new EventHandler(this.trackBarSpeed_ValueChanged);
			resources.ApplyResources(this.panelAuto, "panelAuto");
			this.panelAuto.Controls.Add(this.buttonStopPTPZ2);
			this.panelAuto.Controls.Add(this.buttonPTPZ2);
			this.panelAuto.Controls.Add(this.textBoxZ2);
			this.panelAuto.Controls.Add(this.label11);
			this.panelAuto.Controls.Add(this.label10);
			this.panelAuto.Controls.Add(this.buttonStopPTPA2);
			this.panelAuto.Controls.Add(this.buttonPTPA2);
			this.panelAuto.Controls.Add(this.textBoxA2);
			this.panelAuto.Controls.Add(this.label9);
			this.panelAuto.Controls.Add(this.buttonStopPTPA1);
			this.panelAuto.Controls.Add(this.buttonPTPA1);
			this.panelAuto.Controls.Add(this.buttonStopPTPZ1);
			this.panelAuto.Controls.Add(this.buttonPTPZ1);
			this.panelAuto.Controls.Add(this.buttonStopPTPY);
			this.panelAuto.Controls.Add(this.buttonPTPY);
			this.panelAuto.Controls.Add(this.textBoxA1);
			this.panelAuto.Controls.Add(this.textBoxZ1);
			this.panelAuto.Controls.Add(this.textBoxY);
			this.panelAuto.Controls.Add(this.textBoxX);
			this.panelAuto.Controls.Add(this.label8);
			this.panelAuto.Controls.Add(this.label3);
			this.panelAuto.Controls.Add(this.label1);
			this.panelAuto.Controls.Add(this.buttonStopPTPX);
			this.panelAuto.Controls.Add(this.buttonPTPX);
			this.panelAuto.Name = "panelAuto";
			resources.ApplyResources(this.buttonStopPTPZ2, "buttonStopPTPZ2");
			this.buttonStopPTPZ2.BackgroundImage = Resource.ButtonUp;
			this.buttonStopPTPZ2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStopPTPZ2.Name = "buttonStopPTPZ2";
			this.buttonStopPTPZ2.UseVisualStyleBackColor = true;
			this.buttonStopPTPZ2.Click += new EventHandler(this.buttonStopPTPZ2_Click);
			resources.ApplyResources(this.buttonPTPZ2, "buttonPTPZ2");
			this.buttonPTPZ2.BackgroundImage = Resource.ButtonUp;
			this.buttonPTPZ2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPTPZ2.Name = "buttonPTPZ2";
			this.buttonPTPZ2.UseVisualStyleBackColor = true;
			this.buttonPTPZ2.Click += new EventHandler(this.buttonPTPZ2_Click);
			resources.ApplyResources(this.textBoxZ2, "textBoxZ2");
			this.textBoxZ2.Name = "textBoxZ2";
			resources.ApplyResources(this.label11, "label11");
			this.label11.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label11.Name = "label11";
			resources.ApplyResources(this.label10, "label10");
			this.label10.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label10.Name = "label10";
			resources.ApplyResources(this.buttonStopPTPA2, "buttonStopPTPA2");
			this.buttonStopPTPA2.BackgroundImage = Resource.ButtonUp;
			this.buttonStopPTPA2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStopPTPA2.Name = "buttonStopPTPA2";
			this.buttonStopPTPA2.UseVisualStyleBackColor = true;
			this.buttonStopPTPA2.Click += new EventHandler(this.buttonStopPTPA2_Click);
			resources.ApplyResources(this.buttonPTPA2, "buttonPTPA2");
			this.buttonPTPA2.BackgroundImage = Resource.ButtonUp;
			this.buttonPTPA2.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPTPA2.Name = "buttonPTPA2";
			this.buttonPTPA2.UseVisualStyleBackColor = true;
			this.buttonPTPA2.Click += new EventHandler(this.buttonPTPA2_Click);
			resources.ApplyResources(this.textBoxA2, "textBoxA2");
			this.textBoxA2.Name = "textBoxA2";
			resources.ApplyResources(this.label9, "label9");
			this.label9.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label9.Name = "label9";
			resources.ApplyResources(this.buttonStopPTPA1, "buttonStopPTPA1");
			this.buttonStopPTPA1.BackgroundImage = Resource.ButtonUp;
			this.buttonStopPTPA1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStopPTPA1.Name = "buttonStopPTPA1";
			this.buttonStopPTPA1.UseVisualStyleBackColor = true;
			this.buttonStopPTPA1.Click += new EventHandler(this.buttonStopPTPA1_Click);
			resources.ApplyResources(this.buttonPTPA1, "buttonPTPA1");
			this.buttonPTPA1.BackgroundImage = Resource.ButtonUp;
			this.buttonPTPA1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPTPA1.Name = "buttonPTPA1";
			this.buttonPTPA1.UseVisualStyleBackColor = true;
			this.buttonPTPA1.Click += new EventHandler(this.buttonPTPA1_Click);
			resources.ApplyResources(this.buttonStopPTPZ1, "buttonStopPTPZ1");
			this.buttonStopPTPZ1.BackgroundImage = Resource.ButtonUp;
			this.buttonStopPTPZ1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStopPTPZ1.Name = "buttonStopPTPZ1";
			this.buttonStopPTPZ1.UseVisualStyleBackColor = true;
			this.buttonStopPTPZ1.Click += new EventHandler(this.buttonStopPTPZ1_Click);
			resources.ApplyResources(this.buttonPTPZ1, "buttonPTPZ1");
			this.buttonPTPZ1.BackgroundImage = Resource.ButtonUp;
			this.buttonPTPZ1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPTPZ1.Name = "buttonPTPZ1";
			this.buttonPTPZ1.UseVisualStyleBackColor = true;
			this.buttonPTPZ1.Click += new EventHandler(this.buttonPTPZ1_Click);
			resources.ApplyResources(this.buttonStopPTPY, "buttonStopPTPY");
			this.buttonStopPTPY.BackgroundImage = Resource.ButtonUp;
			this.buttonStopPTPY.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStopPTPY.Name = "buttonStopPTPY";
			this.buttonStopPTPY.UseVisualStyleBackColor = true;
			this.buttonStopPTPY.Click += new EventHandler(this.buttonStopPTPY_Click);
			resources.ApplyResources(this.buttonPTPY, "buttonPTPY");
			this.buttonPTPY.BackgroundImage = Resource.ButtonUp;
			this.buttonPTPY.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPTPY.Name = "buttonPTPY";
			this.buttonPTPY.UseVisualStyleBackColor = true;
			this.buttonPTPY.Click += new EventHandler(this.buttonPTPY_Click);
			resources.ApplyResources(this.textBoxA1, "textBoxA1");
			this.textBoxA1.Name = "textBoxA1";
			resources.ApplyResources(this.textBoxZ1, "textBoxZ1");
			this.textBoxZ1.Name = "textBoxZ1";
			resources.ApplyResources(this.textBoxY, "textBoxY");
			this.textBoxY.Name = "textBoxY";
			resources.ApplyResources(this.textBoxX, "textBoxX");
			this.textBoxX.Name = "textBoxX";
			resources.ApplyResources(this.label8, "label8");
			this.label8.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label8.Name = "label8";
			resources.ApplyResources(this.label3, "label3");
			this.label3.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label3.Name = "label3";
			resources.ApplyResources(this.label1, "label1");
			this.label1.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.label1.Name = "label1";
			resources.ApplyResources(this.buttonStopPTPX, "buttonStopPTPX");
			this.buttonStopPTPX.BackgroundImage = Resource.ButtonUp;
			this.buttonStopPTPX.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonStopPTPX.Name = "buttonStopPTPX";
			this.buttonStopPTPX.UseVisualStyleBackColor = true;
			this.buttonStopPTPX.Click += new EventHandler(this.buttonStopPTPX_Click);
			resources.ApplyResources(this.buttonPTPX, "buttonPTPX");
			this.buttonPTPX.BackgroundImage = Resource.ButtonUp;
			this.buttonPTPX.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPTPX.Name = "buttonPTPX";
			this.buttonPTPX.UseVisualStyleBackColor = true;
			this.buttonPTPX.Click += new EventHandler(this.buttonPTPX_Click);
			this.openFileDialog.DefaultExt = "csv";
			this.openFileDialog.FileName = "openFileDialog";
			resources.ApplyResources(this.openFileDialog, "openFileDialog");
			this.openFileDialog.Title = MyStrings.displaygridViewFileVisionsAccurate;
			this.saveFileDialog.DefaultExt = "csv";
			resources.ApplyResources(this.saveFileDialog, "saveFileDialog");
			this.timerUpdate.Interval = 10;
			this.timerUpdate.Tick += new EventHandler(this.timerUpdate_Tick);
			this.panelButton.BackColor = SystemColors.Control;
			this.panelButton.CausesValidation = false;
			this.panelButton.Controls.Add(this.buttonAuto);
			this.panelButton.Controls.Add(this.buttonSysParam);
			this.panelButton.Controls.Add(this.buttonPCB);
			this.panelButton.Controls.Add(this.buttonGoHome);
			this.panelButton.Controls.Add(this.buttonParam);
			this.panelButton.Controls.Add(this.buttonLoad);
			this.panelButton.Controls.Add(this.buttonManual);
			this.panelButton.Controls.Add(this.buttonSave);
			resources.ApplyResources(this.panelButton, "panelButton");
			this.panelButton.Name = "panelButton";
			this.buttonAuto.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonAuto, "buttonAuto");
			this.buttonAuto.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonAuto.Name = "buttonAuto";
			this.buttonAuto.UseVisualStyleBackColor = true;
			this.buttonAuto.Click += new EventHandler(this.buttonAuto_Click);
			this.buttonSysParam.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonSysParam, "buttonSysParam");
			this.buttonSysParam.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonSysParam.Name = "buttonSysParam";
			this.buttonSysParam.UseVisualStyleBackColor = true;
			this.buttonSysParam.Click += new EventHandler(this.buttonSysParam_Click);
			this.buttonPCB.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonPCB, "buttonPCB");
			this.buttonPCB.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonPCB.Name = "buttonPCB";
			this.buttonPCB.UseVisualStyleBackColor = true;
			this.buttonPCB.Click += new EventHandler(this.buttonPCB_Click);
			this.buttonGoHome.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonGoHome, "buttonGoHome");
			this.buttonGoHome.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonGoHome.Name = "buttonGoHome";
			this.buttonGoHome.UseVisualStyleBackColor = true;
			this.buttonGoHome.Click += new EventHandler(this.buttonGoHome_Click);
			this.buttonParam.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonParam, "buttonParam");
			this.buttonParam.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonParam.Name = "buttonParam";
			this.buttonParam.UseVisualStyleBackColor = true;
			this.buttonParam.Click += new EventHandler(this.buttonParam_Click);
			this.buttonLoad.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonLoad, "buttonLoad");
			this.buttonLoad.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonLoad.Name = "buttonLoad";
			this.buttonLoad.UseVisualStyleBackColor = true;
			this.buttonLoad.Click += new EventHandler(this.buttonLoad_Click);
			this.buttonManual.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonManual, "buttonManual");
			this.buttonManual.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonManual.Name = "buttonManual";
			this.buttonManual.UseVisualStyleBackColor = true;
			this.buttonManual.Click += new EventHandler(this.buttonManual_Click);
			this.buttonSave.BackgroundImage = Resource.ButtonUp;
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.ImageKey = MyStrings.displaygridViewFileVisionsAccurate;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new EventHandler(this.buttonSave_Click);
			this.contextMenuStack.Items.AddRange(new ToolStripItem[]
			{
				this.左边料栈ToolStripMenuItem,
				this.后边料栈ToolStripMenuItem,
				this.托盘ToolStripMenuItem
			});
			this.contextMenuStack.Name = "contextMenuStack";
			resources.ApplyResources(this.contextMenuStack, "contextMenuStack");
			this.左边料栈ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.toolStripMenuItem2,
				this.toolStripMenuItem3,
				this.toolStripMenuItem4,
				this.toolStripMenuItem5,
				this.toolStripMenuItem6,
				this.toolStripMenuItem7,
				this.toolStripMenuItem8,
				this.toolStripMenuItem9,
				this.toolStripMenuItem10,
				this.toolStripMenuItem11,
				this.toolStripMenuItem12,
				this.toolStripMenuItem13,
				this.toolStripMenuItem14,
				this.toolStripMenuItem15,
				this.toolStripMenuItem16,
				this.toolStripMenuItem17,
				this.toolStripMenuItem18,
				this.toolStripMenuItem19,
				this.toolStripMenuItem20,
				this.toolStripMenuItem21,
				this.toolStripMenuItem22,
				this.toolStripMenuItem23,
				this.toolStripMenuItem24,
				this.toolStripMenuItem25,
				this.toolStripMenuItem26,
				this.toolStripMenuItem27,
				this.toolStripMenuItem28,
				this.toolStripMenuItem29,
				this.toolStripMenuItem30,
				this.toolStripMenuItem31
			});
			this.左边料栈ToolStripMenuItem.Name = "左边料栈ToolStripMenuItem";
			resources.ApplyResources(this.左边料栈ToolStripMenuItem, "左边料栈ToolStripMenuItem");
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
			this.toolStripMenuItem2.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
			this.toolStripMenuItem3.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
			this.toolStripMenuItem4.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem5.Name = "toolStripMenuItem5";
			resources.ApplyResources(this.toolStripMenuItem5, "toolStripMenuItem5");
			this.toolStripMenuItem5.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem6.Name = "toolStripMenuItem6";
			resources.ApplyResources(this.toolStripMenuItem6, "toolStripMenuItem6");
			this.toolStripMenuItem6.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem7.Name = "toolStripMenuItem7";
			resources.ApplyResources(this.toolStripMenuItem7, "toolStripMenuItem7");
			this.toolStripMenuItem7.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem8.Name = "toolStripMenuItem8";
			resources.ApplyResources(this.toolStripMenuItem8, "toolStripMenuItem8");
			this.toolStripMenuItem8.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem9.Name = "toolStripMenuItem9";
			resources.ApplyResources(this.toolStripMenuItem9, "toolStripMenuItem9");
			this.toolStripMenuItem9.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem10.Name = "toolStripMenuItem10";
			resources.ApplyResources(this.toolStripMenuItem10, "toolStripMenuItem10");
			this.toolStripMenuItem10.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem11.Name = "toolStripMenuItem11";
			resources.ApplyResources(this.toolStripMenuItem11, "toolStripMenuItem11");
			this.toolStripMenuItem11.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem12.Name = "toolStripMenuItem12";
			resources.ApplyResources(this.toolStripMenuItem12, "toolStripMenuItem12");
			this.toolStripMenuItem12.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem13.Name = "toolStripMenuItem13";
			resources.ApplyResources(this.toolStripMenuItem13, "toolStripMenuItem13");
			this.toolStripMenuItem13.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem14.Name = "toolStripMenuItem14";
			resources.ApplyResources(this.toolStripMenuItem14, "toolStripMenuItem14");
			this.toolStripMenuItem14.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem15.Name = "toolStripMenuItem15";
			resources.ApplyResources(this.toolStripMenuItem15, "toolStripMenuItem15");
			this.toolStripMenuItem15.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem16.Name = "toolStripMenuItem16";
			resources.ApplyResources(this.toolStripMenuItem16, "toolStripMenuItem16");
			this.toolStripMenuItem16.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem17.Name = "toolStripMenuItem17";
			resources.ApplyResources(this.toolStripMenuItem17, "toolStripMenuItem17");
			this.toolStripMenuItem17.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem18.Name = "toolStripMenuItem18";
			resources.ApplyResources(this.toolStripMenuItem18, "toolStripMenuItem18");
			this.toolStripMenuItem18.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem19.Name = "toolStripMenuItem19";
			resources.ApplyResources(this.toolStripMenuItem19, "toolStripMenuItem19");
			this.toolStripMenuItem19.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem20.Name = "toolStripMenuItem20";
			resources.ApplyResources(this.toolStripMenuItem20, "toolStripMenuItem20");
			this.toolStripMenuItem20.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem21.Name = "toolStripMenuItem21";
			resources.ApplyResources(this.toolStripMenuItem21, "toolStripMenuItem21");
			this.toolStripMenuItem21.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem22.Name = "toolStripMenuItem22";
			resources.ApplyResources(this.toolStripMenuItem22, "toolStripMenuItem22");
			this.toolStripMenuItem22.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem23.Name = "toolStripMenuItem23";
			resources.ApplyResources(this.toolStripMenuItem23, "toolStripMenuItem23");
			this.toolStripMenuItem23.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem24.Name = "toolStripMenuItem24";
			resources.ApplyResources(this.toolStripMenuItem24, "toolStripMenuItem24");
			this.toolStripMenuItem24.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem25.Name = "toolStripMenuItem25";
			resources.ApplyResources(this.toolStripMenuItem25, "toolStripMenuItem25");
			this.toolStripMenuItem25.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem26.Name = "toolStripMenuItem26";
			resources.ApplyResources(this.toolStripMenuItem26, "toolStripMenuItem26");
			this.toolStripMenuItem26.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem27.Name = "toolStripMenuItem27";
			resources.ApplyResources(this.toolStripMenuItem27, "toolStripMenuItem27");
			this.toolStripMenuItem27.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem28.Name = "toolStripMenuItem28";
			resources.ApplyResources(this.toolStripMenuItem28, "toolStripMenuItem28");
			this.toolStripMenuItem28.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem29.Name = "toolStripMenuItem29";
			resources.ApplyResources(this.toolStripMenuItem29, "toolStripMenuItem29");
			this.toolStripMenuItem29.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem30.Name = "toolStripMenuItem30";
			resources.ApplyResources(this.toolStripMenuItem30, "toolStripMenuItem30");
			this.toolStripMenuItem30.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.toolStripMenuItem31.Name = "toolStripMenuItem31";
			resources.ApplyResources(this.toolStripMenuItem31, "toolStripMenuItem31");
			this.toolStripMenuItem31.Click += new EventHandler(this.toolStripMenuItemLeft_Click);
			this.后边料栈ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.toolStripMenuItem32,
				this.toolStripMenuItem33,
				this.toolStripMenuItem34,
				this.toolStripMenuItem35,
				this.toolStripMenuItem36,
				this.toolStripMenuItem37,
				this.toolStripMenuItem38,
				this.toolStripMenuItem39,
				this.toolStripMenuItem40,
				this.toolStripMenuItem41,
				this.toolStripMenuItem42,
				this.toolStripMenuItem43,
				this.toolStripMenuItem44,
				this.toolStripMenuItem45,
				this.toolStripMenuItem46,
				this.toolStripMenuItem47,
				this.toolStripMenuItem48,
				this.toolStripMenuItem49,
				this.toolStripMenuItem50,
				this.toolStripMenuItem51,
				this.toolStripMenuItem52,
				this.toolStripMenuItem53,
				this.toolStripMenuItem54,
				this.toolStripMenuItem55,
				this.toolStripMenuItem56,
				this.toolStripMenuItem57,
				this.toolStripMenuItem58,
				this.toolStripMenuItem59,
				this.toolStripMenuItem60,
				this.toolStripMenuItem61
			});
			this.后边料栈ToolStripMenuItem.Name = "后边料栈ToolStripMenuItem";
			resources.ApplyResources(this.后边料栈ToolStripMenuItem, "后边料栈ToolStripMenuItem");
			this.toolStripMenuItem32.Name = "toolStripMenuItem32";
			resources.ApplyResources(this.toolStripMenuItem32, "toolStripMenuItem32");
			this.toolStripMenuItem32.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem33.Name = "toolStripMenuItem33";
			resources.ApplyResources(this.toolStripMenuItem33, "toolStripMenuItem33");
			this.toolStripMenuItem33.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem34.Name = "toolStripMenuItem34";
			resources.ApplyResources(this.toolStripMenuItem34, "toolStripMenuItem34");
			this.toolStripMenuItem34.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem35.Name = "toolStripMenuItem35";
			resources.ApplyResources(this.toolStripMenuItem35, "toolStripMenuItem35");
			this.toolStripMenuItem35.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem36.Name = "toolStripMenuItem36";
			resources.ApplyResources(this.toolStripMenuItem36, "toolStripMenuItem36");
			this.toolStripMenuItem36.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem37.Name = "toolStripMenuItem37";
			resources.ApplyResources(this.toolStripMenuItem37, "toolStripMenuItem37");
			this.toolStripMenuItem37.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem38.Name = "toolStripMenuItem38";
			resources.ApplyResources(this.toolStripMenuItem38, "toolStripMenuItem38");
			this.toolStripMenuItem38.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem39.Name = "toolStripMenuItem39";
			resources.ApplyResources(this.toolStripMenuItem39, "toolStripMenuItem39");
			this.toolStripMenuItem39.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem40.Name = "toolStripMenuItem40";
			resources.ApplyResources(this.toolStripMenuItem40, "toolStripMenuItem40");
			this.toolStripMenuItem40.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem41.Name = "toolStripMenuItem41";
			resources.ApplyResources(this.toolStripMenuItem41, "toolStripMenuItem41");
			this.toolStripMenuItem41.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem42.Name = "toolStripMenuItem42";
			resources.ApplyResources(this.toolStripMenuItem42, "toolStripMenuItem42");
			this.toolStripMenuItem42.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem43.Name = "toolStripMenuItem43";
			resources.ApplyResources(this.toolStripMenuItem43, "toolStripMenuItem43");
			this.toolStripMenuItem43.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem44.Name = "toolStripMenuItem44";
			resources.ApplyResources(this.toolStripMenuItem44, "toolStripMenuItem44");
			this.toolStripMenuItem44.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem45.Name = "toolStripMenuItem45";
			resources.ApplyResources(this.toolStripMenuItem45, "toolStripMenuItem45");
			this.toolStripMenuItem45.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem46.Name = "toolStripMenuItem46";
			resources.ApplyResources(this.toolStripMenuItem46, "toolStripMenuItem46");
			this.toolStripMenuItem46.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem47.Name = "toolStripMenuItem47";
			resources.ApplyResources(this.toolStripMenuItem47, "toolStripMenuItem47");
			this.toolStripMenuItem47.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem48.Name = "toolStripMenuItem48";
			resources.ApplyResources(this.toolStripMenuItem48, "toolStripMenuItem48");
			this.toolStripMenuItem48.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem49.Name = "toolStripMenuItem49";
			resources.ApplyResources(this.toolStripMenuItem49, "toolStripMenuItem49");
			this.toolStripMenuItem49.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem50.Name = "toolStripMenuItem50";
			resources.ApplyResources(this.toolStripMenuItem50, "toolStripMenuItem50");
			this.toolStripMenuItem50.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem51.Name = "toolStripMenuItem51";
			resources.ApplyResources(this.toolStripMenuItem51, "toolStripMenuItem51");
			this.toolStripMenuItem51.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem52.Name = "toolStripMenuItem52";
			resources.ApplyResources(this.toolStripMenuItem52, "toolStripMenuItem52");
			this.toolStripMenuItem52.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem53.Name = "toolStripMenuItem53";
			resources.ApplyResources(this.toolStripMenuItem53, "toolStripMenuItem53");
			this.toolStripMenuItem53.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem54.Name = "toolStripMenuItem54";
			resources.ApplyResources(this.toolStripMenuItem54, "toolStripMenuItem54");
			this.toolStripMenuItem54.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem55.Name = "toolStripMenuItem55";
			resources.ApplyResources(this.toolStripMenuItem55, "toolStripMenuItem55");
			this.toolStripMenuItem55.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem56.Name = "toolStripMenuItem56";
			resources.ApplyResources(this.toolStripMenuItem56, "toolStripMenuItem56");
			this.toolStripMenuItem56.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem57.Name = "toolStripMenuItem57";
			resources.ApplyResources(this.toolStripMenuItem57, "toolStripMenuItem57");
			this.toolStripMenuItem57.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem58.Name = "toolStripMenuItem58";
			resources.ApplyResources(this.toolStripMenuItem58, "toolStripMenuItem58");
			this.toolStripMenuItem58.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem59.Name = "toolStripMenuItem59";
			resources.ApplyResources(this.toolStripMenuItem59, "toolStripMenuItem59");
			this.toolStripMenuItem59.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem60.Name = "toolStripMenuItem60";
			resources.ApplyResources(this.toolStripMenuItem60, "toolStripMenuItem60");
			this.toolStripMenuItem60.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.toolStripMenuItem61.Name = "toolStripMenuItem61";
			resources.ApplyResources(this.toolStripMenuItem61, "toolStripMenuItem61");
			this.toolStripMenuItem61.Click += new EventHandler(this.toolStripMenuItemBack_Click);
			this.托盘ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[]
			{
				this.toolStripMenuItem62,
				this.toolStripMenuItem63,
				this.toolStripMenuItem64,
				this.toolStripMenuItem65,
				this.toolStripMenuItem66,
				this.toolStripMenuItem67,
				this.toolStripMenuItem68,
				this.toolStripMenuItem69,
				this.toolStripMenuItem70,
				this.toolStripMenuItem71,
				this.toolStripMenuItem72,
				this.toolStripMenuItem73,
				this.toolStripMenuItem74,
				this.toolStripMenuItem75,
				this.toolStripMenuItem76,
				this.toolStripMenuItem77,
				this.toolStripMenuItem78,
				this.toolStripMenuItem79,
				this.toolStripMenuItem80,
				this.toolStripMenuItem81,
				this.toolStripMenuItem82,
				this.toolStripMenuItem83,
				this.toolStripMenuItem84,
				this.toolStripMenuItem85,
				this.toolStripMenuItem86,
				this.toolStripMenuItem87,
				this.toolStripMenuItem88,
				this.toolStripMenuItem89,
				this.toolStripMenuItem90,
				this.toolStripMenuItem91
			});
			this.托盘ToolStripMenuItem.Name = "托盘ToolStripMenuItem";
			resources.ApplyResources(this.托盘ToolStripMenuItem, "托盘ToolStripMenuItem");
			this.toolStripMenuItem62.Name = "toolStripMenuItem62";
			resources.ApplyResources(this.toolStripMenuItem62, "toolStripMenuItem62");
			this.toolStripMenuItem62.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem63.Name = "toolStripMenuItem63";
			resources.ApplyResources(this.toolStripMenuItem63, "toolStripMenuItem63");
			this.toolStripMenuItem63.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem64.Name = "toolStripMenuItem64";
			resources.ApplyResources(this.toolStripMenuItem64, "toolStripMenuItem64");
			this.toolStripMenuItem64.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem65.Name = "toolStripMenuItem65";
			resources.ApplyResources(this.toolStripMenuItem65, "toolStripMenuItem65");
			this.toolStripMenuItem65.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem66.Name = "toolStripMenuItem66";
			resources.ApplyResources(this.toolStripMenuItem66, "toolStripMenuItem66");
			this.toolStripMenuItem66.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem67.Name = "toolStripMenuItem67";
			resources.ApplyResources(this.toolStripMenuItem67, "toolStripMenuItem67");
			this.toolStripMenuItem67.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem68.Name = "toolStripMenuItem68";
			resources.ApplyResources(this.toolStripMenuItem68, "toolStripMenuItem68");
			this.toolStripMenuItem68.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem69.Name = "toolStripMenuItem69";
			resources.ApplyResources(this.toolStripMenuItem69, "toolStripMenuItem69");
			this.toolStripMenuItem69.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem70.Name = "toolStripMenuItem70";
			resources.ApplyResources(this.toolStripMenuItem70, "toolStripMenuItem70");
			this.toolStripMenuItem70.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem71.Name = "toolStripMenuItem71";
			resources.ApplyResources(this.toolStripMenuItem71, "toolStripMenuItem71");
			this.toolStripMenuItem71.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem72.Name = "toolStripMenuItem72";
			resources.ApplyResources(this.toolStripMenuItem72, "toolStripMenuItem72");
			this.toolStripMenuItem72.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem73.Name = "toolStripMenuItem73";
			resources.ApplyResources(this.toolStripMenuItem73, "toolStripMenuItem73");
			this.toolStripMenuItem73.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem74.Name = "toolStripMenuItem74";
			resources.ApplyResources(this.toolStripMenuItem74, "toolStripMenuItem74");
			this.toolStripMenuItem74.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem75.Name = "toolStripMenuItem75";
			resources.ApplyResources(this.toolStripMenuItem75, "toolStripMenuItem75");
			this.toolStripMenuItem75.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem76.Name = "toolStripMenuItem76";
			resources.ApplyResources(this.toolStripMenuItem76, "toolStripMenuItem76");
			this.toolStripMenuItem76.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem77.Name = "toolStripMenuItem77";
			resources.ApplyResources(this.toolStripMenuItem77, "toolStripMenuItem77");
			this.toolStripMenuItem77.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem78.Name = "toolStripMenuItem78";
			resources.ApplyResources(this.toolStripMenuItem78, "toolStripMenuItem78");
			this.toolStripMenuItem78.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem79.Name = "toolStripMenuItem79";
			resources.ApplyResources(this.toolStripMenuItem79, "toolStripMenuItem79");
			this.toolStripMenuItem79.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem80.Name = "toolStripMenuItem80";
			resources.ApplyResources(this.toolStripMenuItem80, "toolStripMenuItem80");
			this.toolStripMenuItem80.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem81.Name = "toolStripMenuItem81";
			resources.ApplyResources(this.toolStripMenuItem81, "toolStripMenuItem81");
			this.toolStripMenuItem81.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem82.Name = "toolStripMenuItem82";
			resources.ApplyResources(this.toolStripMenuItem82, "toolStripMenuItem82");
			this.toolStripMenuItem82.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem83.Name = "toolStripMenuItem83";
			resources.ApplyResources(this.toolStripMenuItem83, "toolStripMenuItem83");
			this.toolStripMenuItem83.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem84.Name = "toolStripMenuItem84";
			resources.ApplyResources(this.toolStripMenuItem84, "toolStripMenuItem84");
			this.toolStripMenuItem84.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem85.Name = "toolStripMenuItem85";
			resources.ApplyResources(this.toolStripMenuItem85, "toolStripMenuItem85");
			this.toolStripMenuItem85.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem86.Name = "toolStripMenuItem86";
			resources.ApplyResources(this.toolStripMenuItem86, "toolStripMenuItem86");
			this.toolStripMenuItem86.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem87.Name = "toolStripMenuItem87";
			resources.ApplyResources(this.toolStripMenuItem87, "toolStripMenuItem87");
			this.toolStripMenuItem87.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem88.Name = "toolStripMenuItem88";
			resources.ApplyResources(this.toolStripMenuItem88, "toolStripMenuItem88");
			this.toolStripMenuItem88.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem89.Name = "toolStripMenuItem89";
			resources.ApplyResources(this.toolStripMenuItem89, "toolStripMenuItem89");
			this.toolStripMenuItem89.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem90.Name = "toolStripMenuItem90";
			resources.ApplyResources(this.toolStripMenuItem90, "toolStripMenuItem90");
			this.toolStripMenuItem90.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			this.toolStripMenuItem91.Name = "toolStripMenuItem91";
			resources.ApplyResources(this.toolStripMenuItem91, "toolStripMenuItem91");
			this.toolStripMenuItem91.Click += new EventHandler(this.toolStripMenuItemFront_Click);
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.panelMain);
			base.Controls.Add(this.panelButton);
			base.KeyPreview = true;
			base.Name = "MainForm";
			base.ShowIcon = false;
			base.FormClosing += new FormClosingEventHandler(this.MainForm_FormClosing);
			base.Load += new EventHandler(this.MainForm_Load);
			this.contextMenuStripFile.ResumeLayout(false);
			this.panelMain.ResumeLayout(false);
			this.panelGridView.ResumeLayout(false);
			((ISupportInitialize)this.gridViewFile).EndInit();
			this.panelControl.ResumeLayout(false);
			this.panelVision.ResumeLayout(false);
			this.panelVision.PerformLayout();
			this.panelManual.ResumeLayout(false);
			this.panelRun.ResumeLayout(false);
			this.panelRun.PerformLayout();
			((ISupportInitialize)this.trackBarSpeed).EndInit();
			this.panelAuto.ResumeLayout(false);
			this.panelAuto.PerformLayout();
			this.panelButton.ResumeLayout(false);
			this.contextMenuStack.ResumeLayout(false);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public void GetIcStackXY(int Num, out double x, out double y)
		{
			x = (y = 0.0);
			if (Num >= 0 && Num < 30)
			{
				if (this.IcStack.ArrayX[Num] <= 1 && this.IcStack.ArrayY[Num] <= 1)
				{
					x = this.IcStack.StartX[Num];
					y = this.IcStack.StartY[Num];
					this.IcStackCount[Num] = 0;
					return;
				}
				if (this.IcStackCount[Num] >= this.IcStack.ArrayX[Num] * this.IcStack.ArrayY[Num])
				{
					this.IcStackCount[Num] = 0;
				}
				int num = this.IcStackCount[Num] % this.IcStack.ArrayX[Num];
				int num2 = this.IcStackCount[Num] / this.IcStack.ArrayX[Num];
				if (this.IcStack.ArrayX[Num] == 1)
				{
					x = this.IcStack.StartX[Num];
				}
				else
				{
					x = this.IcStack.StartX[Num] + (this.IcStack.EndX[Num] - this.IcStack.StartX[Num]) / (double)(this.IcStack.ArrayX[Num] - 1) * (double)num;
				}
				if (this.IcStack.ArrayY[Num] == 1)
				{
					y = this.IcStack.StartY[Num];
				}
				else
				{
					y = this.IcStack.StartY[Num] + (this.IcStack.EndY[Num] - this.IcStack.StartY[Num]) / (double)(this.IcStack.ArrayY[Num] - 1) * (double)num2;
				}
				this.IcStackCount[Num]++;
			}
		}

		public MainForm()
		{
			this.InitializeComponent();
			this.VisionsType.Items.AddRange(new object[]
			{
				this.LocStrings.GetString("displaygridViewFileVisionsNone"),
				this.LocStrings.GetString("displaygridViewFileVisionsQuick"),
				this.LocStrings.GetString("displaygridViewFileVisionsAccurate")
			});
			this.labelVacuum1.Image = Resource.TransparentLED;
			this.labelVacuum2.Image = Resource.TransparentLED;
			this.labelBlowing1.Image = Resource.TransparentLED;
			this.labelBlowing2.Image = Resource.TransparentLED;
			this.labelPressure1.Image = Resource.TransparentLED;
			this.labelPressure2.Image = Resource.TransparentLED;
			this.labelPrickHome.Image = Resource.TransparentLED;
			this.labelStrip.Image = Resource.TransparentLED;
			this.buttonLoad.Image = Resource.BILoad;
			this.buttonSave.Image = Resource.BISave;
			this.buttonManual.Image = Resource.BIManual;
			this.buttonAuto.Image = Resource.BIAuto;
			this.buttonGoHome.Image = Resource.BIGoHome;
			this.buttonPCB.Image = Resource.BIPCB;
			this.buttonParam.Image = Resource.BISettings;
			this.buttonSysParam.Image = Resource.BIAboutUs;
			this.buttonNozzle1Up.Image = Resource.go_up;
			this.buttonNozzle1Down.Image = Resource.go_down;
			this.buttonNozzle2Up.Image = Resource.go_up;
			this.buttonNozzle2Down.Image = Resource.go_down;
			this.buttonYUp.Image = Resource.go_up;
			this.buttonYDown.Image = Resource.go_down;
			this.buttonXDown.Image = Resource.go_previous;
			this.buttonXUp.Image = Resource.go_next;
			this.buttonStep.Image = Resource.media_eject;
			this.buttonRun.Image = Resource.media_playback_start;
			this.buttonSuspend.Image = Resource.media_playback_pause;
			this.buttonPTPX.Image = Resource.media_playback_start;
			this.buttonPTPY.Image = Resource.media_playback_start;
			this.buttonPTPZ1.Image = Resource.media_playback_start;
			this.buttonPTPZ2.Image = Resource.media_playback_start;
			this.buttonPTPA1.Image = Resource.media_playback_start;
			this.buttonPTPA2.Image = Resource.media_playback_start;
			this.buttonStopPTPX.Image = Resource.media_playback_stop;
			this.buttonStopPTPY.Image = Resource.media_playback_stop;
			this.buttonStopPTPZ1.Image = Resource.media_playback_stop;
			this.buttonStopPTPZ2.Image = Resource.media_playback_stop;
			this.buttonStopPTPA1.Image = Resource.media_playback_stop;
			this.buttonStopPTPA2.Image = Resource.media_playback_stop;
			this.gridViewFile.SetDoubleBuffered(true);
			this.gridViewFile.Height = 600;
			this.gridViewFile.BringToFront();
			this.controler.IpAdd = this.ipAdd;
			this.controler.Port = this.ipPort;
			this.controler.owner = this;
			this.Nozzle.X = new double[]
			{
				212.1,
				254.0,
				296.16,
				337.81
			};
			this.Nozzle.Y = new double[]
			{
				-52.4,
				-52.0,
				-52.2,
				-52.2
			};
			this.Nozzle.Z = new double[]
			{
				-12.78,
				-12.78,
				-12.78,
				-12.78
			};
			this.NozzleSizeMin = new double[]
			{
				0.55,
				1.4,
				1.9,
				8.0
			};
			this.NozzleSizeMax = new double[]
			{
				1.2,
				1.8,
				2.5,
				11.0
			};
			this.LeftAndBackStack.X = (double[])Const.StackX.Clone();
			this.LeftAndBackStack.Y = (double[])Const.StackY.Clone();
			this.LeftAndBackStack.Feed = (int[])Const.StackFeed.Clone();
			this.LeftAndBackStack.Width = (int[])Const.StackWidth.Clone();
			this.LeftAndBackStack.PrickOffsetY = new double[60];
			for (int i = 0; i < this.LeftAndBackStack.Width.Length; i++)
			{
				int num = this.LeftAndBackStack.Width[i];
				if (num <= 12)
				{
					if (num != 8)
					{
						if (num == 12)
						{
							this.LeftAndBackStack.PrickOffsetY[i] = 5.5;
						}
					}
					else
					{
						this.LeftAndBackStack.PrickOffsetY[i] = 3.5;
					}
				}
				else if (num != 16)
				{
					if (num == 24)
					{
						this.LeftAndBackStack.PrickOffsetY[i] = 11.0;
					}
				}
				else
				{
					this.LeftAndBackStack.PrickOffsetY[i] = 7.5;
				}
			}
			this.IcStack.StartX = (double[])Const.IcStackStartX.Clone();
			this.IcStack.StartY = (double[])Const.IcStackStartY.Clone();
			this.IcStack.EndX = (double[])Const.IcStackEndX.Clone();
			this.IcStack.EndY = (double[])Const.IcStackEndY.Clone();
			this.IcStack.ArrayX = (int[])Const.IcStackArrayX.Clone();
			this.IcStack.ArrayY = (int[])Const.IcStackArrayY.Clone();
			this.initFileParam();
			this.buttonSysParam.Text = this.LocStrings.GetString("AboutUs");
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			if (this.controler.CheckConnect())
			{
				this.IsConnect = true;
				this.controler.ControlCmdBuf(ControlBufType.STOP_BUF);
				this.controler.SetDO_OFF_IMM(18432u);
			}
			if (this.MacStr == null)
			{
				this.Text += "   V2.12";
			}
			else
			{
				this.Text = this.Text + "    SN:" + this.MacStr + "   V2.12";
			}
			this.mVision1.Initialize(this.CameraKey1.ToString("D9"), this.CameraKey2.ToString("D9"));
			this.timerUpdate.Enabled = true;
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			this.mVision1.Release();
		}

		private void initFileParam()
		{
			this.Puzzle.IsMark1Used = new bool[50];
			this.Puzzle.IsMark2Used = new bool[50];
			this.Puzzle.Mark1X = new double[50];
			this.Puzzle.Mark1Y = new double[50];
			this.Puzzle.Mark1MeasureX = new double[50];
			this.Puzzle.Mark1MeasureY = new double[50];
			this.Puzzle.Mark2X = new double[50];
			this.Puzzle.Mark2Y = new double[50];
			this.Puzzle.Mark2MeasureX = new double[50];
			this.Puzzle.Mark2MeasureY = new double[50];
			this.Puzzle.Angle1 = new double[50];
			this.Puzzle.Angle2 = new double[50];
			this.Puzzle.Scale = new double[50];
			this.Other.PCBThickness = 1.6;
			this.Mark.Mode = MarkModeType.NoMark;
			this.Mark.Shape = MarkShapeType.Round;
			this.Mark.IsAutoPause = true;
			this.Mark.Exposure = 6;
		}

		public int PreCalcParam()
		{
			for (int i = 0; i < 50; i++)
			{
				if (!this.Puzzle.IsMark1Used[i] && this.Puzzle.IsMark2Used[i])
				{
					this.Puzzle.IsMark1Used[i] = true;
					this.Puzzle.IsMark2Used[i] = false;
					double num = this.Puzzle.Mark1X[i];
					this.Puzzle.Mark1X[i] = this.Puzzle.Mark2X[i];
					this.Puzzle.Mark2X[i] = num;
					num = this.Puzzle.Mark1Y[i];
					this.Puzzle.Mark1Y[i] = this.Puzzle.Mark2Y[i];
					this.Puzzle.Mark2Y[i] = num;
					num = this.Puzzle.Mark1MeasureX[i];
					this.Puzzle.Mark1MeasureX[i] = this.Puzzle.Mark2MeasureX[i];
					this.Puzzle.Mark2MeasureX[i] = num;
					num = this.Puzzle.Mark1MeasureY[i];
					this.Puzzle.Mark1MeasureY[i] = this.Puzzle.Mark2MeasureY[i];
					this.Puzzle.Mark2MeasureY[i] = num;
				}
				if (this.Puzzle.IsMark1Used[i] && this.Puzzle.IsMark2Used[i])
				{
					this.Puzzle.Angle1[i] = Math.Atan2(this.Puzzle.Mark2Y[i] - this.Puzzle.Mark1Y[i], this.Puzzle.Mark2X[i] - this.Puzzle.Mark1X[i]);
					this.Puzzle.Angle2[i] = Math.Atan2(this.Puzzle.Mark2MeasureY[i] - this.Puzzle.Mark1MeasureY[i], this.Puzzle.Mark2MeasureX[i] - this.Puzzle.Mark1MeasureX[i]);
					double num2 = Math.Pow(this.Puzzle.Mark2X[i] - this.Puzzle.Mark1X[i], 2.0) + Math.Pow(this.Puzzle.Mark2Y[i] - this.Puzzle.Mark1Y[i], 2.0);
					if (num2 != 0.0)
					{
						this.Puzzle.Scale[i] = Math.Sqrt((Math.Pow(this.Puzzle.Mark2MeasureX[i] - this.Puzzle.Mark1MeasureX[i], 2.0) + Math.Pow(this.Puzzle.Mark2MeasureY[i] - this.Puzzle.Mark1MeasureY[i], 2.0)) / num2);
					}
					else
					{
						this.Puzzle.Angle1[i] = (this.Puzzle.Angle2[i] = 0.0);
						this.Puzzle.Scale[i] = 1.0;
					}
				}
				else
				{
					this.Puzzle.Angle1[i] = (this.Puzzle.Angle2[i] = 0.0);
					this.Puzzle.Scale[i] = 1.0;
				}
			}
			for (int j = 0; j < 50; j++)
			{
				if (Math.Abs(this.Puzzle.Scale[j] - 1.0) > 0.1)
				{
					return j + 1;
				}
			}
			return 0;
		}

		public void LoadParam()
		{
			if (!this.controler.ReadSpeed())
			{
				this.IsConnect = false;
				return;
			}
			this.controler.SpeedValue = 4;
			this.ManualSpeed = 0;
			this.trackBarSpeed.Value = this.controler.SpeedValue;
			this.labelSpeed.Text = this.LocStrings.GetString("labelSpeed.Text") + (this.controler.SpeedValue + 1).ToString() + "0%";
			this.controler.SetDO_ON_IMM(512u);
			int num;
			if (!this.controler.ReadParam(0u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.macAdd1 = (uint)num;
			if (!this.controler.ReadParam(1u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.macAdd2 = (uint)num;
			this.macAdd2 &= 65535u;
			this.MacStr = this.macAdd2.ToString("X4") + this.macAdd1.ToString("X8");
			this.MacStr = this.MacStr.Insert(10, "-");
			this.MacStr = this.MacStr.Insert(8, "-");
			this.MacStr = this.MacStr.Insert(6, "-");
			this.MacStr = this.MacStr.Insert(4, "-");
			this.MacStr = this.MacStr.Insert(2, "-");
			if (!this.controler.ReadParam(2u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ipAdd = (uint)num;
			if (!this.controler.ReadParam(3u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ipPort = (uint)num;
			if (!this.controler.ReadParam(4u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.HomePosX = (double)num / 1000.0;
			if (!this.controler.ReadParam(5u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.HomePosY = (double)num / 1000.0;
			if (!this.controler.ReadParam(6u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.VisionX = (double)num / 1000.0;
			if (!this.controler.ReadParam(7u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.VisionY = (double)num / 1000.0;
			if (!this.controler.ReadParam(8u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.VisionOffsetX = (double)num / 1000.0;
			if (!this.controler.ReadParam(9u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.VisionOffsetY = (double)num / 1000.0;
			if (!this.controler.ReadParam(10u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.LeftPrickOffsetX = (double)num / 1000.0;
			if (!this.controler.ReadParam(11u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.LeftPrickOffsetY = (double)num / 1000.0;
			if (!this.controler.ReadParam(12u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.DiscardCompPosX = (double)num / 1000.0;
			if (!this.controler.ReadParam(13u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.DiscardCompPosY = (double)num / 1000.0;
			if (!this.controler.ReadParam(14u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisNozzleDownPCBPos = (double)num / 1000.0;
			if (!this.controler.ReadParam(15u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisNozzleDownStackPos = (double)num / 1000.0;
			if (!this.controler.ReadParam(16u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisNozzleDownFrontStackPos = (double)num / 1000.0;
			if (!this.controler.ReadParam(17u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisNozzleDownDiscardPos = (double)num / 1000.0;
			if (!this.controler.ReadParam(18u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisPrickDownPos = (double)num / 1000.0;
			if (!this.controler.ReadParam(19u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.DoubleVisionCheck = (num != 0);
			if (!this.controler.ReadParam(20u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.PixelScaleX1 = (double)num / 10000.0;
			if (!this.controler.ReadParam(21u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.PixelScaleY1 = (double)num / 10000.0;
			if (!this.controler.ReadParam(22u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.VisionZ = (double)num / 1000.0;
			if (!this.controler.ReadParam(23u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.PressureCheckTotal = (num != 0);
			if (!this.controler.ReadParam(24u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.VisionValueNozzle = (double)num / 1000.0;
			if (!this.controler.ReadParam(25u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.VisionValueComp = (double)num / 1000.0;
			if (!this.controler.ReadParam(26u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.NozzleVisionZ = (double)num / 1000.0;
			if (!this.controler.ReadParam(27u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.NozzleOffsetX = (double)num / 1000.0;
			if (!this.controler.ReadParam(28u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.NozzleOffsetY = (double)num / 1000.0;
			if (!this.controler.ReadParam(29u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisStackDealy = (uint)num;
			if (!this.controler.ReadParam(30u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisPcbDealy1 = (uint)num;
			if (!this.controler.ReadParam(31u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.ZAxisPcbDealy2 = (uint)num;
			if (!this.controler.ReadParam(32u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.CameraKey1 = (uint)num;
			if (!this.controler.ReadParam(33u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.CameraKey2 = (uint)num;
			if (!this.controler.ReadParam(34u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.PixelScaleX2 = (double)num / 10000.0;
			if (!this.controler.ReadParam(35u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.PixelScaleY2 = (double)num / 10000.0;
			if (!this.controler.ReadParam(36u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.CameraAngle1 = (double)num / 1000.0;
			if (!this.controler.ReadParam(37u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.FeedBack = (double)num / 1000.0;
			if (!this.controler.ReadParam(38u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.Nozzle1VisionX = (double)num / 1000.0;
			if (!this.controler.ReadParam(39u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.Nozzle1VisionY = (double)num / 1000.0;
			if (!this.controler.ReadParam(40u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.Nozzle2VisionX = (double)num / 1000.0;
			if (!this.controler.ReadParam(41u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.Nozzle2VisionY = (double)num / 1000.0;
			if (!this.controler.ReadParam(42u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.MarkVisionOffsetX = (double)num / 1000.0;
			if (!this.controler.ReadParam(43u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.MarkVisionOffsetY = (double)num / 1000.0;
			if (!this.controler.ReadParam(44u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.BlowingDealy1 = num;
			if (this.BlowingDealy1 < 0)
			{
				this.BlowingDealy1 = 0;
			}
			if (this.BlowingDealy1 > 2)
			{
				this.BlowingDealy1 = 2;
			}
			if (!this.controler.ReadParam(45u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.BlowingDealy2 = num;
			if (this.BlowingDealy2 < 0)
			{
				this.BlowingDealy2 = 0;
			}
			if (this.BlowingDealy2 > 2)
			{
				this.BlowingDealy2 = 2;
			}
			if (!this.controler.ReadParam(46u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.BackPrickOffsetX = (double)num / 1000.0;
			if (!this.controler.ReadParam(47u, out num))
			{
				this.IsConnect = false;
				return;
			}
			this.BackPrickOffsetY = (double)num / 1000.0;
			this.MinNozzleX = 1000.0;
			this.MaxNozzleY = -1000.0;
			for (uint num2 = 0u; num2 < 4u; num2 += 1u)
			{
				if (!this.controler.ReadParam(50u + num2 * 3u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.Nozzle.X[(int)((UIntPtr)num2)] = (double)num / 1000.0;
				if (!this.controler.ReadParam(51u + num2 * 3u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.Nozzle.Y[(int)((UIntPtr)num2)] = (double)num / 1000.0;
				if (!this.controler.ReadParam(52u + num2 * 3u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.Nozzle.Z[(int)((UIntPtr)num2)] = (double)num / 1000.0;
				if (this.Nozzle.X[(int)((UIntPtr)num2)] < this.MinNozzleX)
				{
					this.MinNozzleX = this.Nozzle.X[(int)((UIntPtr)num2)];
				}
				if (this.Nozzle.Y[(int)((UIntPtr)num2)] > this.MaxNozzleY)
				{
					this.MaxNozzleY = this.Nozzle.Y[(int)((UIntPtr)num2)];
				}
			}
			this.MinNozzleX -= 50.0;
			this.MaxNozzleY += 25.0;
			for (uint num3 = 0u; num3 < 60u; num3 += 1u)
			{
				if (!this.controler.ReadParam(100u + num3 * 3u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.LeftAndBackStack.X[(int)((UIntPtr)num3)] = (double)num / 1000.0;
				if (!this.controler.ReadParam(101u + num3 * 3u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.LeftAndBackStack.Y[(int)((UIntPtr)num3)] = (double)num / 1000.0;
				if (!this.controler.ReadParam(102u + num3 * 3u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.LeftAndBackStack.Feed[(int)((UIntPtr)num3)] = (num & 65535);
				this.LeftAndBackStack.Width[(int)((UIntPtr)num3)] = num >> 16;
				int num4 = this.LeftAndBackStack.Width[(int)((UIntPtr)num3)];
				if (num4 <= 12)
				{
					if (num4 != 8)
					{
						if (num4 == 12)
						{
							this.LeftAndBackStack.PrickOffsetY[(int)((UIntPtr)num3)] = 5.5;
						}
					}
					else
					{
						this.LeftAndBackStack.PrickOffsetY[(int)((UIntPtr)num3)] = 3.5;
					}
				}
				else if (num4 != 16)
				{
					if (num4 == 24)
					{
						this.LeftAndBackStack.PrickOffsetY[(int)((UIntPtr)num3)] = 11.0;
					}
				}
				else
				{
					this.LeftAndBackStack.PrickOffsetY[(int)((UIntPtr)num3)] = 7.5;
				}
			}
			for (uint num5 = 0u; num5 < 30u; num5 += 1u)
			{
				if (!this.controler.ReadParam(300u + num5 * 2u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.IcStack.StartX[(int)((UIntPtr)num5)] = (double)num / 1000.0;
				if (!this.controler.ReadParam(301u + num5 * 2u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.IcStack.StartY[(int)((UIntPtr)num5)] = (double)num / 1000.0;
			}
			for (uint num6 = 0u; num6 < 30u; num6 += 1u)
			{
				if (!this.controler.ReadParam(360u + num6 * 2u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.IcStack.EndX[(int)((UIntPtr)num6)] = (double)num / 1000.0;
				if (!this.controler.ReadParam(361u + num6 * 2u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.IcStack.EndY[(int)((UIntPtr)num6)] = (double)num / 1000.0;
			}
			for (uint num7 = 0u; num7 < 30u; num7 += 1u)
			{
				if (!this.controler.ReadParam(420u + num7 * 2u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.IcStack.ArrayX[(int)((UIntPtr)num7)] = num;
				if (!this.controler.ReadParam(421u + num7 * 2u, out num))
				{
					this.IsConnect = false;
					return;
				}
				this.IcStack.ArrayY[(int)((UIntPtr)num7)] = num;
			}
		}

		public void SaveParam()
		{
			if (!this.controler.WriteParam(2u, (int)this.ipAdd))
			{
				return;
			}
			if (!this.controler.WriteParam(3u, (int)this.ipPort))
			{
				return;
			}
			if (!this.controler.WriteParam(6u, (int)(this.VisionX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(7u, (int)(this.VisionY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(8u, (int)(this.VisionOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(9u, (int)(this.VisionOffsetY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(10u, (int)(this.LeftPrickOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(11u, (int)(this.LeftPrickOffsetY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(14u, (int)(this.ZAxisNozzleDownPCBPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(15u, (int)(this.ZAxisNozzleDownStackPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(16u, (int)(this.ZAxisNozzleDownFrontStackPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(17u, (int)(this.ZAxisNozzleDownDiscardPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(18u, (int)(this.ZAxisPrickDownPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(19u, this.DoubleVisionCheck ? 1 : 0))
			{
				return;
			}
			if (!this.controler.WriteParam(20u, (int)(this.PixelScaleX1 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(21u, (int)(this.PixelScaleY1 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(22u, (int)(this.VisionZ * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(23u, this.PressureCheckTotal ? 1 : 0))
			{
				return;
			}
			if (!this.controler.WriteParam(24u, (int)(this.VisionValueNozzle * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(25u, (int)(this.VisionValueComp * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(27u, (int)(this.NozzleOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(28u, (int)(this.NozzleOffsetY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(29u, (int)this.ZAxisStackDealy))
			{
				return;
			}
			if (!this.controler.WriteParam(30u, (int)this.ZAxisPcbDealy1))
			{
				return;
			}
			if (!this.controler.WriteParam(31u, (int)this.ZAxisPcbDealy2))
			{
				return;
			}
			if (!this.controler.WriteParam(32u, (int)this.CameraKey1))
			{
				return;
			}
			if (!this.controler.WriteParam(33u, (int)this.CameraKey2))
			{
				return;
			}
			if (!this.controler.WriteParam(34u, (int)(this.PixelScaleX2 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(35u, (int)(this.PixelScaleY2 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(36u, (int)(this.CameraAngle1 * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(38u, (int)(this.Nozzle1VisionX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(39u, (int)(this.Nozzle1VisionY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(40u, (int)(this.Nozzle2VisionX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(41u, (int)(this.Nozzle2VisionY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(44u, this.BlowingDealy1))
			{
				return;
			}
			if (!this.controler.WriteParam(45u, this.BlowingDealy2))
			{
				return;
			}
			if (!this.controler.WriteParam(46u, (int)(this.BackPrickOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(47u, (int)(this.BackPrickOffsetY * 1000.0)))
			{
				return;
			}
			for (uint num = 0u; num < 4u; num += 1u)
			{
				if (!this.controler.WriteParam(50u + num * 3u, (int)(this.Nozzle.X[(int)((UIntPtr)num)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(51u + num * 3u, (int)(this.Nozzle.Y[(int)((UIntPtr)num)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(52u + num * 3u, (int)(this.Nozzle.Z[(int)((UIntPtr)num)] * 1000.0)))
				{
					return;
				}
			}
			for (uint num2 = 0u; num2 < 60u; num2 += 1u)
			{
				if (!this.controler.WriteParam(100u + num2 * 3u, (int)(this.LeftAndBackStack.X[(int)((UIntPtr)num2)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(101u + num2 * 3u, (int)(this.LeftAndBackStack.Y[(int)((UIntPtr)num2)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(102u + num2 * 3u, (this.LeftAndBackStack.Width[(int)((UIntPtr)num2)] << 16) + this.LeftAndBackStack.Feed[(int)((UIntPtr)num2)]))
				{
					return;
				}
			}
			for (uint num3 = 0u; num3 < 30u; num3 += 1u)
			{
				if (!this.controler.WriteParam(300u + num3 * 2u, (int)(this.IcStack.StartX[(int)((UIntPtr)num3)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(301u + num3 * 2u, (int)(this.IcStack.StartY[(int)((UIntPtr)num3)] * 1000.0)))
				{
					return;
				}
			}
			for (uint num4 = 0u; num4 < 30u; num4 += 1u)
			{
				if (!this.controler.WriteParam(360u + num4 * 2u, (int)(this.IcStack.EndX[(int)((UIntPtr)num4)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(361u + num4 * 2u, (int)(this.IcStack.EndY[(int)((UIntPtr)num4)] * 1000.0)))
				{
					return;
				}
			}
			for (uint num5 = 0u; num5 < 30u; num5 += 1u)
			{
				if (!this.controler.WriteParam(420u + num5 * 2u, this.IcStack.ArrayX[(int)((UIntPtr)num5)]))
				{
					return;
				}
				if (!this.controler.WriteParam(421u + num5 * 2u, this.IcStack.ArrayY[(int)((UIntPtr)num5)]))
				{
					return;
				}
			}
			this.controler.SaveParam();
		}

		public void SaveSysParam()
		{
			if (!this.controler.WriteParam(2u, (int)this.ipAdd))
			{
				return;
			}
			if (!this.controler.WriteParam(3u, (int)this.ipPort))
			{
				return;
			}
			if (!this.controler.WriteParam(4u, (int)(this.HomePosX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(5u, (int)(this.HomePosY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(6u, (int)(this.VisionX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(7u, (int)(this.VisionY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(8u, (int)(this.VisionOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(9u, (int)(this.VisionOffsetY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(10u, (int)(this.LeftPrickOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(11u, (int)(this.LeftPrickOffsetY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(12u, (int)(this.DiscardCompPosX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(13u, (int)(this.DiscardCompPosY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(14u, (int)(this.ZAxisNozzleDownPCBPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(15u, (int)(this.ZAxisNozzleDownStackPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(16u, (int)(this.ZAxisNozzleDownFrontStackPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(17u, (int)(this.ZAxisNozzleDownDiscardPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(18u, (int)(this.ZAxisPrickDownPos * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(19u, this.DoubleVisionCheck ? 1 : 0))
			{
				return;
			}
			if (!this.controler.WriteParam(20u, (int)(this.PixelScaleX1 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(21u, (int)(this.PixelScaleY1 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(22u, (int)(this.VisionZ * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(23u, this.PressureCheckTotal ? 1 : 0))
			{
				return;
			}
			if (!this.controler.WriteParam(24u, (int)(this.VisionValueNozzle * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(25u, (int)(this.VisionValueComp * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(26u, (int)(this.NozzleVisionZ * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(27u, (int)(this.NozzleOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(28u, (int)(this.NozzleOffsetY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(29u, (int)this.ZAxisStackDealy))
			{
				return;
			}
			if (!this.controler.WriteParam(30u, (int)this.ZAxisPcbDealy1))
			{
				return;
			}
			if (!this.controler.WriteParam(31u, (int)this.ZAxisPcbDealy2))
			{
				return;
			}
			if (!this.controler.WriteParam(32u, (int)this.CameraKey1))
			{
				return;
			}
			if (!this.controler.WriteParam(33u, (int)this.CameraKey2))
			{
				return;
			}
			if (!this.controler.WriteParam(34u, (int)(this.PixelScaleX2 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(35u, (int)(this.PixelScaleY2 * 10000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(36u, (int)(this.CameraAngle1 * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(37u, (int)(this.FeedBack * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(38u, (int)(this.Nozzle1VisionX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(39u, (int)(this.Nozzle1VisionY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(40u, (int)(this.Nozzle2VisionX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(41u, (int)(this.Nozzle2VisionY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(42u, (int)(this.MarkVisionOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(43u, (int)(this.MarkVisionOffsetY * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(44u, this.BlowingDealy1))
			{
				return;
			}
			if (!this.controler.WriteParam(45u, this.BlowingDealy2))
			{
				return;
			}
			if (!this.controler.WriteParam(46u, (int)(this.BackPrickOffsetX * 1000.0)))
			{
				return;
			}
			if (!this.controler.WriteParam(47u, (int)(this.BackPrickOffsetY * 1000.0)))
			{
				return;
			}
			for (uint num = 0u; num < 4u; num += 1u)
			{
				if (!this.controler.WriteParam(50u + num * 3u, (int)(this.Nozzle.X[(int)((UIntPtr)num)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(51u + num * 3u, (int)(this.Nozzle.Y[(int)((UIntPtr)num)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(52u + num * 3u, (int)(this.Nozzle.Z[(int)((UIntPtr)num)] * 1000.0)))
				{
					return;
				}
			}
			for (uint num2 = 0u; num2 < 60u; num2 += 1u)
			{
				if (!this.controler.WriteParam(100u + num2 * 3u, (int)(this.LeftAndBackStack.X[(int)((UIntPtr)num2)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(101u + num2 * 3u, (int)(this.LeftAndBackStack.Y[(int)((UIntPtr)num2)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(102u + num2 * 3u, (this.LeftAndBackStack.Width[(int)((UIntPtr)num2)] << 16) + this.LeftAndBackStack.Feed[(int)((UIntPtr)num2)]))
				{
					return;
				}
			}
			for (uint num3 = 0u; num3 < 30u; num3 += 1u)
			{
				if (!this.controler.WriteParam(300u + num3 * 2u, (int)(this.IcStack.StartX[(int)((UIntPtr)num3)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(301u + num3 * 2u, (int)(this.IcStack.StartY[(int)((UIntPtr)num3)] * 1000.0)))
				{
					return;
				}
			}
			for (uint num4 = 0u; num4 < 30u; num4 += 1u)
			{
				if (!this.controler.WriteParam(360u + num4 * 2u, (int)(this.IcStack.EndX[(int)((UIntPtr)num4)] * 1000.0)))
				{
					return;
				}
				if (!this.controler.WriteParam(361u + num4 * 2u, (int)(this.IcStack.EndY[(int)((UIntPtr)num4)] * 1000.0)))
				{
					return;
				}
			}
			for (uint num5 = 0u; num5 < 30u; num5 += 1u)
			{
				if (!this.controler.WriteParam(420u + num5 * 2u, this.IcStack.ArrayX[(int)((UIntPtr)num5)]))
				{
					return;
				}
				if (!this.controler.WriteParam(421u + num5 * 2u, this.IcStack.ArrayY[(int)((UIntPtr)num5)]))
				{
					return;
				}
			}
			this.controler.SaveParam();
		}

		private void buttonLoad_Click(object sender, EventArgs e)
		{
			if (this.openFileDialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}
			this.CsvFilePath = this.openFileDialog.FileName;
			string a = Path.GetExtension(this.CsvFilePath).Substring(1).ToUpper();
			FileStream stream = new FileStream(this.CsvFilePath, FileMode.Open, FileAccess.Read);
			StreamReader streamReader = new StreamReader(stream, Encoding.Default);
			int num = -1;
			int num2 = -1;
			int num3 = -1;
			int num4 = -1;
			int num5 = -1;
			int num6 = -1;
			int num7 = -1;
			int num8 = -1;
			int num9 = -1;
			int num10 = -1;
			int num11 = -1;
			int num12 = -1;
			int num13 = -1;
			int num14 = -1;
			int num15 = -1;
			string oldValue;
			string text;
			if (a == "MNT" || a == "MNB")
			{
				oldValue = " ";
				num = 0;
				num3 = 1;
				num4 = 2;
				num6 = 3;
				num7 = 4;
				num2 = 5;
				this.StrCsvTopArray = new string[6];
			}
			else
			{
				while (true)
				{
					text = streamReader.ReadLine();
					if (text == null)
					{
						break;
					}
					if (text.TrimStart(new char[0]).StartsWith("Fields:"))
					{
						goto Block_4;
					}
					if (text.Replace("\",\"", "\t").Replace("\"", "").Split(new char[]
					{
						'\t'
					}).Length >= 5)
					{
						goto Block_5;
					}
					if (text.Replace(",", "\t").Split(new char[]
					{
						'\t'
					}).Length >= 5)
					{
						goto Block_6;
					}
				}
				new MyMessageBox(this.controler, this.LocStrings.GetString("csvErrorFormat"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
				Block_4:
				text = text.Substring(8);
				oldValue = ",";
				goto IL_1CE;
				Block_5:
				oldValue = "\",\"";
				goto IL_1CE;
				Block_6:
				oldValue = ",";
				IL_1CE:
				text = text.ToLower();
				this.StrCsvTopArray = text.Replace(oldValue, "\t").Replace("\"", "").Replace(" ", "").Split(new char[]
				{
					'\t'
				});
				for (int i = 0; i < this.StrCsvTopArray.Length; i++)
				{
					string text2;
					switch (text2 = this.StrCsvTopArray[i])
					{
					case "partid":
					case "refdes":
					case "designator":
					case "name":
						num = i;
						break;
					case "x":
					case "midx":
					case "x1":
					case "sym_x":
						num3 = i;
						break;
					case "y":
					case "midy":
					case "y1":
					case "sym_y":
						num4 = i;
						break;
					case "rotation":
					case "angle":
					case "orient.":
					case "sym_rotate":
						num6 = i;
						break;
					case "package":
					case "footprint":
					case "partdecal":
					case "pattern":
					case "sym_name":
						num2 = i;
						break;
					case "value":
					case "comment":
					case "comp_value":
						num7 = i;
						break;
					case "layer":
					case "sym_mirror":
						num5 = i;
						break;
					case "height":
						num8 = i;
						break;
					case "nozzlenum":
						num10 = i;
						break;
					case "stacknum":
						num11 = i;
						break;
					case "speed":
						num12 = i;
						break;
					case "vision":
						num13 = i;
						break;
					case "pressure":
						num14 = i;
						break;
					case "explanation":
						num9 = i;
						break;
					case "smd":
						num15 = i;
						break;
					}
				}
				if (num == -1)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("csvErrorDesignator"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (num3 == -1)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("csvErrorMidX"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (num4 == -1)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("csvErrorMidY"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (num6 == -1)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("csvErrorRotation"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
			}
			bool isWithTopLayer = false;
			bool isWithBottomLayer = false;
			this.ComponentArray.Clear();
			try
			{
				while ((text = streamReader.ReadLine()) != null && !(text == "Puzzle"))
				{
					if (text.Length != 0 && text[0] != ' ')
					{
						if (a == "MNT" || a == "MNB")
						{
							text = text.Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");
						}
						string[] array = text.Replace(oldValue, "\t").Replace("\"", "").Split(new char[]
						{
							'\t'
						});
						if (array.Length == this.StrCsvTopArray.Length)
						{
							Component component = new Component();
							component.Designator = array[num];
							if (num2 != -1)
							{
								component.Footprint = array[num2];
							}
							if (array[num3].Length != 0)
							{
								component.MidX = double.Parse(array[num3].Replace(",", ".").Replace("mm", ""));
								if (array[num4].Length != 0)
								{
									component.MidY = double.Parse(array[num4].Replace(",", ".").Replace("mm", ""));
									if (array[num6].Length != 0)
									{
										component.Rotation = double.Parse(array[num6].Replace(",", "."));
										component.Rotation -= 90.0;
										if (component.Rotation < 0.0)
										{
											component.Rotation += 360.0;
										}
										if (num7 != -1)
										{
											component.Comment = array[num7];
										}
										if (num5 != -1)
										{
											if (array[num5].Length > 1)
											{
												component.Layer = array[num5].ToUpper().Remove(1);
											}
											else
											{
												component.Layer = array[num5].ToUpper();
											}
											if (component.Layer == "N")
											{
												component.Layer = "T";
											}
											if (component.Layer == "Y")
											{
												component.Layer = "B";
											}
											if (component.Layer == "T")
											{
												isWithTopLayer = true;
											}
											else if (component.Layer == "B")
											{
												isWithBottomLayer = true;
											}
										}
										if (num8 != -1)
										{
											component.Height = double.Parse(array[num8]);
										}
										if (num10 != -1)
										{
											if (array[num10] != "1" && array[num10] != "2" && array[num10] != "1/2")
											{
												component.NozzleNum = "1";
											}
											else
											{
												component.NozzleNum = array[num10];
											}
										}
										if (num11 != -1)
										{
											component.StackNum = array[num11].Replace('前', 'I');
											component.StackNum = component.StackNum.Replace('盘', 'I');
											component.StackNum = component.StackNum.Replace('左', 'L');
											component.StackNum = component.StackNum.Replace('后', 'B');
											component.StackNum = component.StackNum.Replace('F', 'I');
											if (component.StackNum[0] != 'L' && component.StackNum[0] != 'B' && component.StackNum[0] != 'I')
											{
												component.StackNum = component.StackNum.Insert(0, "L");
											}
										}
										if (num9 != -1)
										{
											component.Explanation = array[num9];
										}
										if (num12 != -1)
										{
											component.Speed = array[num12];
										}
										if (num13 != -1)
										{
											bool flag;
											if (bool.TryParse(array[num13], out flag))
											{
												if (!flag)
												{
													component.VisionType = this.LocStrings.GetString("displaygridViewFileVisionsNone");
												}
												else
												{
													component.VisionType = this.LocStrings.GetString("displaygridViewFileVisionsQuick");
												}
											}
											else if (array[num13] == "精确" || array[num13] == "Accurate")
											{
												component.VisionType = this.LocStrings.GetString("displaygridViewFileVisionsAccurate");
											}
											else if (array[num13] == "快速" || array[num13] == "Quick")
											{
												component.VisionType = this.LocStrings.GetString("displaygridViewFileVisionsQuick");
											}
											else
											{
												component.VisionType = this.LocStrings.GetString("displaygridViewFileVisionsNone");
											}
										}
										if (num14 != -1)
										{
											component.IsPressureCheck = bool.Parse(array[num14]);
										}
										else
										{
											component.IsPressureCheck = true;
										}
										if (num15 != -1)
										{
											if (array[num15].ToLower() == "yes")
											{
												component.IsSMD = true;
											}
											else
											{
												component.IsSMD = false;
											}
										}
										else
										{
											component.IsSMD = true;
										}
										this.ComponentArray.Add(component);
									}
								}
							}
						}
					}
				}
			}
			catch
			{
				new MyMessageBox(this.controler, this.LocStrings.GetString("csvErrorFormat"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			this.initFileParam();
			if (text != null)
			{
				while (true)
				{
					string text2;
					if ((text2 = text) == null)
					{
						goto IL_109B;
					}
					if (text2 == "Puzzle")
					{
						text = streamReader.ReadLine();
						int num17 = 0;
						do
						{
							this.Puzzle.Mark1MeasureX[num17++] = double.Parse(text);
							text = streamReader.ReadLine();
							if (!(text != ""))
							{
								break;
							}
						}
						while (num17 < 50);
						IL_D10:
						while (text != "")
						{
							text = streamReader.ReadLine();
						}
						text = streamReader.ReadLine();
						num17 = 0;
						do
						{
							this.Puzzle.Mark1MeasureY[num17++] = double.Parse(text);
							text = streamReader.ReadLine();
							if (!(text != ""))
							{
								break;
							}
						}
						while (num17 < 50);
						IL_D63:
						while (text != "")
						{
							text = streamReader.ReadLine();
						}
						text = streamReader.ReadLine();
						if (text == "Other")
						{
							goto IL_10A2;
						}
						num17 = 0;
						do
						{
							this.Puzzle.Mark1X[num17++] = double.Parse(text);
							text = streamReader.ReadLine();
						}
						while (text != "" && num17 < 50);
						text = streamReader.ReadLine();
						if (text == "Other")
						{
							goto IL_10A2;
						}
						num17 = 0;
						do
						{
							this.Puzzle.Mark1Y[num17++] = double.Parse(text);
							text = streamReader.ReadLine();
						}
						while (text != "" && num17 < 50);
						text = streamReader.ReadLine();
						if (text == "Other")
						{
							goto IL_10A2;
						}
						num17 = 0;
						do
						{
							this.Puzzle.Mark2MeasureX[num17++] = double.Parse(text);
							text = streamReader.ReadLine();
						}
						while (text != "" && num17 < 50);
						text = streamReader.ReadLine();
						if (text == "Other")
						{
							goto IL_10A2;
						}
						num17 = 0;
						do
						{
							this.Puzzle.Mark2MeasureY[num17++] = double.Parse(text);
							text = streamReader.ReadLine();
						}
						while (text != "" && num17 < 50);
						text = streamReader.ReadLine();
						if (text == "Other")
						{
							goto IL_10A2;
						}
						num17 = 0;
						do
						{
							this.Puzzle.Mark2X[num17++] = double.Parse(text);
							text = streamReader.ReadLine();
						}
						while (text != "" && num17 < 50);
						text = streamReader.ReadLine();
						if (!(text == "Other"))
						{
							num17 = 0;
							do
							{
								this.Puzzle.Mark2Y[num17++] = double.Parse(text);
								text = streamReader.ReadLine();
								if (!(text != ""))
								{
									break;
								}
							}
							while (num17 < 50);
							goto IL_109B;
						}
						goto IL_10A2;
						goto IL_D63;
						goto IL_D10;
					}
					if (!(text2 == "Other"))
					{
						if (!(text2 == "Mark"))
						{
							goto IL_109B;
						}
						text = streamReader.ReadLine();
						this.Mark.Mode = (MarkModeType)int.Parse(text);
						text = streamReader.ReadLine();
						this.Mark.Shape = (MarkShapeType)int.Parse(text);
						text = streamReader.ReadLine();
						this.Mark.IsAutoPause = bool.Parse(text);
						text = streamReader.ReadLine();
						if (text == null || !(text != ""))
						{
							goto IL_109B;
						}
						this.Mark.Exposure = int.Parse(text);
						if (this.mVision1.Camera2 != null)
						{
							this.mVision1.Camera2.Exposure = -this.Mark.Exposure;
							goto IL_109B;
						}
						goto IL_109B;
					}
					else
					{
						text = streamReader.ReadLine();
						this.Other.PCBThickness = double.Parse(text);
						text = streamReader.ReadLine();
						if (text == null)
						{
							goto IL_109B;
						}
						text = streamReader.ReadLine();
						if (text == null)
						{
							goto IL_109B;
						}
						int num17 = 0;
						do
						{
							this.Puzzle.IsMark1Used[num17++] = bool.Parse(text);
							text = streamReader.ReadLine();
						}
						while (text != null && text != "" && num17 < 50);
						text = streamReader.ReadLine();
						if (text != null)
						{
							num17 = 0;
							do
							{
								this.Puzzle.IsMark2Used[num17++] = bool.Parse(text);
								text = streamReader.ReadLine();
								if (text == null || !(text != ""))
								{
									break;
								}
							}
							while (num17 < 50);
							goto IL_109B;
						}
						goto IL_109B;
					}
					IL_10A2:
					if (text == null)
					{
						break;
					}
					continue;
					IL_109B:
					text = streamReader.ReadLine();
					goto IL_10A2;
				}
			}
			streamReader.Close();
			this.PreCalcParam();
			if (num5 != -1 || num15 != -1)
			{
				this.selectStackDialog.IsWithTopLayer = isWithTopLayer;
				this.selectStackDialog.IsWithBottomLayer = isWithBottomLayer;
				this.selectStackDialog.IsWithSMD = (num15 != -1);
				this.selectStackDialog.Location = new Point(base.Location.X + base.Size.Width / 2 - this.selectStackDialog.Size.Width / 2, base.Location.Y + base.Size.Height / 2 - this.selectStackDialog.Size.Height / 2);
				if (this.selectStackDialog.ShowDialog() != DialogResult.OK)
				{
					return;
				}
				this.useTopLayer = this.selectStackDialog.UseTopLayer;
				this.useBottomLayer = this.selectStackDialog.UseBottomLayer;
				this.useXMirror = this.selectStackDialog.UseXMirror;
				this.useSMD = this.selectStackDialog.UseSMD;
			}
			else
			{
				this.useTopLayer = false;
				this.useBottomLayer = false;
				this.useXMirror = false;
				this.useSMD = false;
			}
			if (this.useXMirror)
			{
				for (int j = 0; j < this.ComponentArray.Count; j++)
				{
					this.ComponentArray[j].MidX = -this.ComponentArray[j].MidX;
				}
			}
			this.gridViewFile.Rows.Clear();
			object[] array2 = new object[12];
			int num18 = 0;
			for (int k = 0; k < this.ComponentArray.Count; k++)
			{
				if (((this.useTopLayer && this.ComponentArray[k].Layer == "T") || (this.useBottomLayer && this.ComponentArray[k].Layer == "B") || (!this.useTopLayer && !this.useBottomLayer)) && ((this.useSMD && this.ComponentArray[k].IsSMD) || !this.useSMD))
				{
					if (num18 >= this.gridViewFile.RowCount)
					{
						array2[0] = this.ComponentArray[k].Designator;
						array2[1] = this.ComponentArray[k].NozzleNum;
						array2[2] = this.ComponentArray[k].StackNum;
						array2[3] = this.ComponentArray[k].MidX.ToString("F2");
						array2[4] = this.ComponentArray[k].MidY.ToString("F2");
						array2[5] = this.ComponentArray[k].Rotation.ToString("F1");
						array2[6] = this.ComponentArray[k].Height.ToString("F2");
						array2[7] = this.ComponentArray[k].Speed;
						array2[8] = this.ComponentArray[k].VisionType;
						array2[9] = this.ComponentArray[k].IsPressureCheck;
						array2[10] = false;
						if (this.ComponentArray[k].Explanation == "")
						{
							array2[11] = this.ComponentArray[k].Footprint + " " + this.ComponentArray[k].Comment;
						}
						else
						{
							array2[11] = this.ComponentArray[k].Explanation;
						}
						this.gridViewFile.Rows.Add(array2);
					}
					else
					{
						this.gridViewFile.Rows[num18].Cells[0].Value = this.ComponentArray[k].Designator;
						this.gridViewFile.Rows[num18].Cells[1].Value = this.ComponentArray[k].NozzleNum;
						this.gridViewFile.Rows[num18].Cells[2].Value = this.ComponentArray[k].StackNum;
						this.gridViewFile.Rows[num18].Cells[3].Value = this.ComponentArray[k].MidX.ToString("F2");
						this.gridViewFile.Rows[num18].Cells[4].Value = this.ComponentArray[k].MidY.ToString("F2");
						this.gridViewFile.Rows[num18].Cells[5].Value = this.ComponentArray[k].Rotation.ToString("F1");
						this.gridViewFile.Rows[num18].Cells[6].Value = this.ComponentArray[k].Height.ToString("F2");
						this.gridViewFile.Rows[num18].Cells[7].Value = this.ComponentArray[k].Speed;
						this.gridViewFile.Rows[num18].Cells[8].Value = this.ComponentArray[k].VisionType;
						this.gridViewFile.Rows[num18].Cells[9].Value = this.ComponentArray[k].IsPressureCheck;
						this.gridViewFile.Rows[num18].Cells[10].Value = false;
						if (this.ComponentArray[k].Explanation == "")
						{
							this.gridViewFile.Rows[num18].Cells[11].Value = this.ComponentArray[k].Footprint + " " + this.ComponentArray[k].Comment;
						}
						else
						{
							this.gridViewFile.Rows[num18].Cells[11].Value = this.ComponentArray[k].Explanation;
						}
					}
					num18++;
				}
			}
			this.labelFile.Text = Path.GetFileName(this.CsvFilePath);
			this.newObj = 1;
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			string fileName = Path.GetFileName(this.CsvFilePath);
			this.saveFileDialog.InitialDirectory = Path.GetDirectoryName(this.CsvFilePath);
			if (this.useTopLayer && !Path.GetFileNameWithoutExtension(this.CsvFilePath).EndsWith("_Top"))
			{
				fileName = Path.GetFileNameWithoutExtension(this.CsvFilePath) + "_Top.csv";
			}
			if (this.useBottomLayer && !Path.GetFileNameWithoutExtension(this.CsvFilePath).EndsWith("_Bottom"))
			{
				fileName = Path.GetFileNameWithoutExtension(this.CsvFilePath) + "_Bottom.csv";
			}
			this.saveFileDialog.FileName = fileName;
			if (this.saveFileDialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}
			this.CsvFilePath = this.saveFileDialog.FileName;
			FileStream stream = new FileStream(this.CsvFilePath, FileMode.Create, FileAccess.Write);
			StreamWriter streamWriter = new StreamWriter(stream, Encoding.Default);
			streamWriter.Write("\"Designator\",\"NozzleNum\",\"StackNum\",\"Mid X\",");
			streamWriter.WriteLine("\"Mid Y\",\"Rotation\",\"Height\",\"Speed\",\"Vision\",\"Pressure\",\"Explanation\"");
			streamWriter.WriteLine("\"\"");
			for (int i = 0; i < this.gridViewFile.RowCount; i++)
			{
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[0].Value + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[1].Value + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[2].Value + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[3].Value + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[4].Value + "\"");
				streamWriter.Write(",");
				double num = double.Parse(this.gridViewFile.Rows[i].Cells[5].Value.ToString()) + 90.0;
				if (num > 360.0)
				{
					num -= 360.0;
				}
				streamWriter.Write("\"" + num.ToString("F2") + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[6].Value + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[7].Value + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[8].Value + "\"");
				streamWriter.Write(",");
				streamWriter.Write("\"" + this.gridViewFile.Rows[i].Cells[9].Value + "\"");
				streamWriter.Write(",");
				streamWriter.WriteLine("\"" + this.gridViewFile.Rows[i].Cells[11].Value + "\"");
			}
			streamWriter.WriteLine("");
			streamWriter.WriteLine("Puzzle");
			for (int j = 0; j < this.Puzzle.Mark1MeasureX.Length; j++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark1MeasureX[j].ToString("F2"));
			}
			streamWriter.WriteLine("");
			for (int k = 0; k < this.Puzzle.Mark1MeasureY.Length; k++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark1MeasureY[k].ToString("F2"));
			}
			streamWriter.WriteLine("");
			for (int l = 0; l < this.Puzzle.Mark1X.Length; l++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark1X[l].ToString("F2"));
			}
			streamWriter.WriteLine("");
			for (int m = 0; m < this.Puzzle.Mark1Y.Length; m++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark1Y[m].ToString("F2"));
			}
			streamWriter.WriteLine("");
			for (int n = 0; n < this.Puzzle.Mark2MeasureX.Length; n++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark2MeasureX[n].ToString("F2"));
			}
			streamWriter.WriteLine("");
			for (int num2 = 0; num2 < this.Puzzle.Mark2MeasureY.Length; num2++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark2MeasureY[num2].ToString("F2"));
			}
			streamWriter.WriteLine("");
			for (int num3 = 0; num3 < this.Puzzle.Mark2X.Length; num3++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark2X[num3].ToString("F2"));
			}
			streamWriter.WriteLine("");
			for (int num4 = 0; num4 < this.Puzzle.Mark2Y.Length; num4++)
			{
				streamWriter.WriteLine(this.Puzzle.Mark2Y[num4].ToString("F2"));
			}
			streamWriter.WriteLine("");
			streamWriter.WriteLine("Other");
			streamWriter.WriteLine(this.Other.PCBThickness.ToString("F2"));
			streamWriter.WriteLine("");
			for (int num5 = 0; num5 < this.Puzzle.IsMark1Used.Length; num5++)
			{
				streamWriter.WriteLine(this.Puzzle.IsMark1Used[num5].ToString());
			}
			streamWriter.WriteLine("");
			for (int num6 = 0; num6 < this.Puzzle.IsMark2Used.Length; num6++)
			{
				streamWriter.WriteLine(this.Puzzle.IsMark2Used[num6].ToString());
			}
			streamWriter.WriteLine("");
			streamWriter.WriteLine("Mark");
			TextWriter arg_745_0 = streamWriter;
			int mode = (int)this.Mark.Mode;
			arg_745_0.WriteLine(mode.ToString());
			TextWriter arg_75F_0 = streamWriter;
			int shape = (int)this.Mark.Shape;
			arg_75F_0.WriteLine(shape.ToString());
			streamWriter.WriteLine(this.Mark.IsAutoPause.ToString());
			streamWriter.WriteLine(this.Mark.Exposure.ToString());
			streamWriter.Close();
			this.labelFile.Text = Path.GetFileName(this.CsvFilePath);
		}

		private void buttonEdit_Click(object sender, EventArgs e)
		{
		}

		private void buttonManual_Click(object sender, EventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.ManualPanel = 0;
				return;
			}
			if (this.IsConnect && this.RunSignal)
			{
				this.StopRunForm = new MyMessageBoxYesNo(this.controler, this.LocStrings.GetString("labelToStop"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				this.StopRunForm.ShowDialog();
				if (this.StopRunForm.DialogResult == DialogResult.Yes)
				{
					this.StopRunning();
					this.ManualPanel = 1;
				}
				this.StopRunForm = null;
				return;
			}
			this.GoDone(false);
			this.ManualPanel = 1;
		}

		private void buttonAuto_Click(object sender, EventArgs e)
		{
			if (this.ManualPanel == 2)
			{
				this.ManualPanel = 0;
				return;
			}
			if (this.IsConnect && this.RunSignal)
			{
				this.StopRunForm = new MyMessageBoxYesNo(this.controler, this.LocStrings.GetString("labelToStop"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				this.StopRunForm.ShowDialog();
				if (this.StopRunForm.DialogResult == DialogResult.Yes)
				{
					this.StopRunning();
					this.ManualPanel = 2;
				}
				this.StopRunForm = null;
				return;
			}
			this.GoDone(false);
			this.ManualPanel = 2;
		}

		private void buttonGoHome_Click(object sender, EventArgs e)
		{
			this.StopRunning();
			if (this.controler.DI_PrickHome || this.controler.PosMM[1] != 0.0)
			{
				this.controler.SetDO_ON_IMM(2048u);
				MyMessageBox myMessageBox = new MyMessageBox(this.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.IsHoming = true;
			this.ManualPanel = 0;
			GoHomeForm goHomeForm = new GoHomeForm(this.controler, this.HomePosX, this.HomePosY);
			goHomeForm.Location = new Point(base.Location.X + base.Size.Width / 2 - goHomeForm.Size.Width / 2, base.Location.Y + base.Size.Height / 2 - goHomeForm.Size.Height / 2);
			goHomeForm.ShowDialog();
			this.IsHoming = false;
		}

		private void buttonPCB_Click(object sender, EventArgs e)
		{
			if (!this.IsPCBEdit)
			{
				this.PCBForm = new PCBEdit(this);
				this.PCBForm.Location = new Point(base.Location.X + base.Size.Width / 2 - this.PCBForm.Size.Width / 2, base.Location.Y + base.Size.Height / 2 - this.PCBForm.Size.Height / 2);
				this.PCBForm.Show();
				this.IsPCBEdit = true;
				this.buttonPCB.BackgroundImage = Resource.ButtonDown;
				return;
			}
			this.PCBForm.Close();
		}

		private void buttonParam_Click(object sender, EventArgs e)
		{
			if (!this.IsParamEdit)
			{
				this.ParamForm = new ParamEdit(this);
				this.ParamForm.Location = new Point(base.Location.X + base.Size.Width / 2 - this.ParamForm.Size.Width / 2, base.Location.Y + base.Size.Height / 2 - this.ParamForm.Size.Height / 2);
				this.ParamForm.Show();
				this.IsParamEdit = true;
				this.buttonParam.BackgroundImage = Resource.ButtonDown;
				return;
			}
			this.ParamForm.Close();
		}

		private void buttonSysParam_Click(object sender, EventArgs e)
		{
			AboutUs aboutUs = new AboutUs();
			aboutUs.Location = new Point(base.Location.X + base.Size.Width / 2 - aboutUs.Size.Width / 2, base.Location.Y + base.Size.Height / 2 - aboutUs.Size.Height / 2);
			aboutUs.ShowDialog();
		}

		private void ToolStripMenuItemAdd_Click(object sender, EventArgs e)
		{
			object[] values = new object[]
			{
				this.newObj.ToString(),
				"1",
				"L1",
				0.ToString("F2"),
				0.ToString("F2"),
				0.ToString("F0"),
				0.ToString("F2"),
				100.ToString(),
				this.LocStrings.GetString("displaygridViewFileVisionsNone"),
				true,
				false,
				this.LocStrings.GetString("labelCompNew") + this.newObj.ToString()
			};
			this.gridViewFile.Rows.Add(values);
			this.newObj++;
		}

		private void ToolStripMenuItemAddCur_Click(object sender, EventArgs e)
		{
			object[] values = new object[]
			{
				this.newObj.ToString(),
				"1",
				"L1",
				this.controler.PosMM[2].ToString("F2"),
				this.controler.PosMM[4].ToString("F2"),
				0.ToString("F0"),
				0.ToString("F2"),
				100.ToString(),
				this.LocStrings.GetString("displaygridViewFileVisionsNone"),
				true,
				false,
				this.LocStrings.GetString("labelCompNew") + this.newObj.ToString()
			};
			this.gridViewFile.Rows.Add(values);
			this.newObj++;
		}

		private void ToolStripMenuItemSetCur_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				if (!this.controler.IsHomed)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				int rowIndex = this.gridViewFile.SelectedCells[0].RowIndex;
				this.gridViewFile.Rows[rowIndex].Cells[3].Value = this.controler.PosMM[2].ToString("F2");
				this.gridViewFile.Rows[rowIndex].Cells[4].Value = this.controler.PosMM[4].ToString("F2");
			}
		}

		private void ToolStripMenuItemAddVisionCur_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				if (!this.controler.IsHomed)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				object[] values = new object[]
				{
					this.newObj.ToString(),
					"1",
					"L1",
					(this.controler.PosMM[2] + this.VisionOffsetX).ToString("F2"),
					(this.controler.PosMM[4] + this.VisionOffsetY).ToString("F2"),
					0.ToString("F0"),
					0.ToString("F2"),
					100.ToString(),
					this.LocStrings.GetString("displaygridViewFileVisionsNone"),
					true,
					false,
					this.LocStrings.GetString("labelCompNew") + this.newObj.ToString()
				};
				this.gridViewFile.Rows.Add(values);
				this.newObj++;
			}
		}

		private void ToolStripMenuItemAddVisionCurMark_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				if (!this.controler.IsHomed)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (!this.Puzzle.IsMark1Used[0])
				{
					new MyMessageBox(null, this.LocStrings.GetString("markIsNotSet"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num = this.controler.PosMM[2] + this.VisionOffsetX;
				double num2 = this.controler.PosMM[4] + this.VisionOffsetY;
				num -= this.Puzzle.Mark1MeasureX[0];
				num2 -= this.Puzzle.Mark1MeasureY[0];
				double num3 = this.Puzzle.Angle1[0] - this.Puzzle.Angle2[0];
				double num4 = Math.Cos(num3);
				double num5 = Math.Sin(num3);
				double num6 = num4 * num - num5 * num2;
				double num7 = num5 * num + num4 * num2;
				if (this.Puzzle.Scale[0] != 0.0)
				{
					num6 /= this.Puzzle.Scale[0];
					num7 /= this.Puzzle.Scale[0];
				}
				num = num6 + this.Puzzle.Mark1X[0];
				num2 = num7 + this.Puzzle.Mark1Y[0];
				object[] values = new object[]
				{
					this.newObj.ToString(),
					"1",
					"L1",
					num.ToString("F2"),
					num2.ToString("F2"),
					0.ToString("F0"),
					0.ToString("F2"),
					100.ToString(),
					this.LocStrings.GetString("displaygridViewFileVisionsNone"),
					true,
					false,
					this.LocStrings.GetString("labelCompNew") + this.newObj.ToString()
				};
				this.gridViewFile.Rows.Add(values);
				this.newObj++;
			}
		}

		private void toolStripMenuItemSetVisionCur_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				if (!this.controler.IsHomed)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				int rowIndex = this.gridViewFile.SelectedCells[0].RowIndex;
				this.gridViewFile.Rows[rowIndex].Cells[3].Value = (this.controler.PosMM[2] + this.VisionOffsetX).ToString("F2");
				this.gridViewFile.Rows[rowIndex].Cells[4].Value = (this.controler.PosMM[4] + this.VisionOffsetY).ToString("F2");
			}
		}

		private void ToolStripMenuItemSetVisionCurMark_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				if (!this.controler.IsHomed)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (!this.Puzzle.IsMark1Used[0])
				{
					new MyMessageBox(null, this.LocStrings.GetString("markIsNotSet"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num = this.controler.PosMM[2] + this.VisionOffsetX;
				double num2 = this.controler.PosMM[4] + this.VisionOffsetY;
				num -= this.Puzzle.Mark1MeasureX[0];
				num2 -= this.Puzzle.Mark1MeasureY[0];
				double num3 = this.Puzzle.Angle1[0] - this.Puzzle.Angle2[0];
				double num4 = Math.Cos(num3);
				double num5 = Math.Sin(num3);
				double num6 = num4 * num - num5 * num2;
				double num7 = num5 * num + num4 * num2;
				if (this.Puzzle.Scale[0] != 0.0)
				{
					num6 /= this.Puzzle.Scale[0];
					num7 /= this.Puzzle.Scale[0];
				}
				num = num6 + this.Puzzle.Mark1X[0];
				num2 = num7 + this.Puzzle.Mark1Y[0];
				int rowIndex = this.gridViewFile.SelectedCells[0].RowIndex;
				this.gridViewFile.Rows[rowIndex].Cells[3].Value = num.ToString("F2");
				this.gridViewFile.Rows[rowIndex].Cells[4].Value = num2.ToString("F2");
			}
		}

		private void toolStripMenuItemVisionAlignment_Click(object sender, EventArgs e)
		{
			int rowIndex = this.gridViewFile.SelectedCells[0].RowIndex;
			double num;
			double num2;
			if (!double.TryParse((string)this.gridViewFile.Rows[rowIndex].Cells[3].Value, out num) || !double.TryParse((string)this.gridViewFile.Rows[rowIndex].Cells[4].Value, out num2))
			{
				new MyMessageBox(this.controler, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			SelectPCB selectPCB = new SelectPCB(base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
			int num3 = -1;
			for (int i = 0; i < this.Puzzle.IsMark1Used.Length; i++)
			{
				if (this.Puzzle.IsMark1Used[i])
				{
					if (num3 == -1)
					{
						num3 = i;
					}
					selectPCB.comboBoxWidthSelPCB.Items.Add(i.ToString());
				}
			}
			if (selectPCB.comboBoxWidthSelPCB.Items.Count > 1)
			{
				if (selectPCB.ShowDialog() != DialogResult.OK)
				{
					return;
				}
				num3 = int.Parse(selectPCB.comboBoxWidthSelPCB.Text);
			}
			if (num3 != -1)
			{
				num -= this.Puzzle.Mark1X[num3];
				num2 -= this.Puzzle.Mark1Y[num3];
				if (this.Puzzle.Scale[num3] != 0.0)
				{
					num *= this.Puzzle.Scale[num3];
					num2 *= this.Puzzle.Scale[num3];
				}
				double num4 = this.Puzzle.Angle2[num3] - this.Puzzle.Angle1[num3];
				double num5 = Math.Cos(num4);
				double num6 = Math.Sin(num4);
				double num7 = num5 * num - num6 * num2;
				double num8 = num6 * num + num5 * num2;
				num = num7 + this.Puzzle.Mark1MeasureX[num3];
				num2 = num8 + this.Puzzle.Mark1MeasureY[num3];
			}
			if (num < 0.0 || num > this.HomePosX - 1.0 || num2 < 0.0 || num2 > this.HomePosY - 1.0)
			{
				this.controler.SetDO_ON_IMM(2048u);
				new MyMessageBox(this.controler, this.LocStrings.GetString("compPosOverflow"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			this.CheckVision = CheckVisionType.PCBVision;
			this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
			this.controler.DPTP(2, 4, num - this.VisionOffsetX, num2 - this.VisionOffsetY, CmdType.CMD_PTP_IMM);
		}

		private void ToolStripMenuItemSelectSame_Click(object sender, EventArgs e)
		{
			if (this.gridViewFile.SelectedCells.Count == 1)
			{
				int columnIndex = this.gridViewFile.SelectedCells[0].ColumnIndex;
				string b = this.gridViewFile.SelectedCells[0].Value.ToString();
				for (int i = 0; i < this.gridViewFile.RowCount; i++)
				{
					if (this.gridViewFile.Rows[i].Cells[columnIndex].Value.ToString() == b)
					{
						this.gridViewFile.Rows[i].Selected = true;
					}
				}
			}
		}

		private void ToolStripMenuItemDelete_Click(object sender, EventArgs e)
		{
			List<int> list = new List<int>();
			foreach (DataGridViewCell dataGridViewCell in this.gridViewFile.SelectedCells)
			{
				if (!list.Contains(dataGridViewCell.RowIndex))
				{
					list.Add(dataGridViewCell.RowIndex);
				}
			}
			list.Sort();
			for (int i = list.Count - 1; i >= 0; i--)
			{
				this.gridViewFile.Rows.RemoveAt(list[i]);
			}
		}

		private int GetNozzleCode(string str)
		{
			if (str != null)
			{
				if (str == "1")
				{
					return 0;
				}
				if (str == "2")
				{
					return 1;
				}
				if (str == "1/2")
				{
					return 2;
				}
			}
			return 0;
		}

		private int GetStackCode(string str)
		{
			if (str[0] == this.LocStrings.GetString("labelFornt")[0])
			{
				int num = int.Parse(str.Substring(1)) - 1;
				return num + 60;
			}
			if (str[0] == this.LocStrings.GetString("labelBack")[0])
			{
				int num2 = int.Parse(str.Substring(1)) - 1;
				return num2 + 30;
			}
			if (str[0] == this.LocStrings.GetString("labelLeft")[0])
			{
				return int.Parse(str.Substring(1)) - 1;
			}
			return 0;
		}

		private int GetVisionType(string str)
		{
			if (str == this.LocStrings.GetString("displaygridViewFileVisionsNone"))
			{
				return 0;
			}
			if (str == this.LocStrings.GetString("displaygridViewFileVisionsQuick"))
			{
				return 1;
			}
			if (str == this.LocStrings.GetString("displaygridViewFileVisionsAccurate"))
			{
				return 2;
			}
			return 0;
		}

		private void ToolStripMenuItemAutoSort_Click(object sender, EventArgs e)
		{
			DataGridView dataGridView = this.gridViewFile;
			int[] array = new int[dataGridView.RowCount];
			int[,] array2 = new int[3, dataGridView.RowCount + 1];
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			for (int i = 0; i < dataGridView.RowCount; i++)
			{
				string a;
				if ((a = dataGridView.Rows[i].Cells[1].Value.ToString()) != null)
				{
					if (!(a == "1"))
					{
						if (!(a == "2"))
						{
							if (a == "1/2")
							{
								array2[2, num3++] = i;
							}
						}
						else
						{
							array2[1, num2++] = i;
						}
					}
					else
					{
						array2[0, num++] = i;
					}
				}
			}
			array2[0, num] = -1;
			array2[1, num2] = -1;
			array2[2, num3] = -1;
			for (int j = 0; j < 3; j++)
			{
				int num4 = 0;
				while (array2[j, num4] != -1)
				{
					int num5 = num4;
					double num6 = double.Parse((string)dataGridView.Rows[array2[j, num4]].Cells[3].Value);
					double num7 = double.Parse((string)dataGridView.Rows[array2[j, num4]].Cells[4].Value);
					int num8 = num4 + 1;
					while (array2[j, num8] != -1)
					{
						double num9 = double.Parse((string)dataGridView.Rows[array2[j, num8]].Cells[3].Value);
						if (num9 < num6)
						{
							num5 = num8;
							num6 = num9;
							num7 = double.Parse((string)dataGridView.Rows[array2[j, num8]].Cells[4].Value);
						}
						else if (num9 == num6)
						{
							double num10 = double.Parse((string)dataGridView.Rows[array2[j, num8]].Cells[4].Value);
							if (num10 < num7)
							{
								num5 = num8;
								num6 = num9;
								num7 = num10;
							}
						}
						num8++;
					}
					if (num5 != num4)
					{
						int num11 = array2[j, num4];
						array2[j, num4] = array2[j, num5];
						array2[j, num5] = num11;
					}
					num4++;
				}
			}
			int num12 = 30;
			int num13 = 0;
			for (int k = 0; k < num12; k++)
			{
				string b = this.LocStrings.GetString("labelLeft") + (k + 1).ToString();
				int num14 = 0;
				while (array2[2, num14] != -1)
				{
					if (dataGridView.Rows[array2[2, num14]].Cells[2].Value.ToString() == b && dataGridView.Rows[array2[2, num14]].Cells[8].Value.ToString() == this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[2, num14]] = num13;
						num13++;
					}
					num14++;
				}
			}
			int num15 = num13 + 1;
			for (int l = 0; l < num12; l++)
			{
				string b2 = this.LocStrings.GetString("labelLeft") + (l + 1).ToString();
				int num16 = 0;
				while (array2[0, num16] != -1)
				{
					if (dataGridView.Rows[array2[0, num16]].Cells[2].Value.ToString() == b2 && dataGridView.Rows[array2[0, num16]].Cells[8].Value.ToString() == this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[0, num16]] = num13;
						num13 += 2;
					}
					num16++;
				}
			}
			for (int m = 0; m < num12; m++)
			{
				string b3 = this.LocStrings.GetString("labelLeft") + (m + 1).ToString();
				int num17 = 0;
				while (array2[1, num17] != -1)
				{
					if (dataGridView.Rows[array2[1, num17]].Cells[2].Value.ToString() == b3 && dataGridView.Rows[array2[1, num17]].Cells[8].Value.ToString() == this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[1, num17]] = num15;
						num15 += 2;
					}
					num17++;
				}
			}
			if (num13 < num15)
			{
				num13 = num15;
			}
			for (int n = 0; n < num12; n++)
			{
				string b4 = this.LocStrings.GetString("labelBack") + (n + 1).ToString();
				int num18 = 0;
				while (array2[2, num18] != -1)
				{
					if (dataGridView.Rows[array2[2, num18]].Cells[2].Value.ToString() == b4 && dataGridView.Rows[array2[2, num18]].Cells[8].Value.ToString() == this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[2, num18]] = num13;
						num13++;
					}
					num18++;
				}
			}
			num15 = num13 + 1;
			for (int num19 = 0; num19 < num12; num19++)
			{
				string b5 = this.LocStrings.GetString("labelBack") + (num19 + 1).ToString();
				int num20 = 0;
				while (array2[0, num20] != -1)
				{
					if (dataGridView.Rows[array2[0, num20]].Cells[2].Value.ToString() == b5 && dataGridView.Rows[array2[0, num20]].Cells[8].Value.ToString() == this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[0, num20]] = num13;
						num13 += 2;
					}
					num20++;
				}
			}
			for (int num21 = 0; num21 < num12; num21++)
			{
				string b6 = this.LocStrings.GetString("labelBack") + (num21 + 1).ToString();
				int num22 = 0;
				while (array2[1, num22] != -1)
				{
					if (dataGridView.Rows[array2[1, num22]].Cells[2].Value.ToString() == b6 && dataGridView.Rows[array2[1, num22]].Cells[8].Value.ToString() == this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[1, num22]] = num15;
						num15 += 2;
					}
					num22++;
				}
			}
			if (num13 < num15)
			{
				num13 = num15;
			}
			for (int num23 = 0; num23 < num12; num23++)
			{
				string b7 = this.LocStrings.GetString("labelLeft") + (num23 + 1).ToString();
				int num24 = 0;
				while (array2[2, num24] != -1)
				{
					if (dataGridView.Rows[array2[2, num24]].Cells[2].Value.ToString() == b7 && dataGridView.Rows[array2[2, num24]].Cells[8].Value.ToString() != this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[2, num24]] = num13;
						num13++;
					}
					num24++;
				}
			}
			num15 = num13 + 1;
			for (int num25 = 0; num25 < num12; num25++)
			{
				string b8 = this.LocStrings.GetString("labelLeft") + (num25 + 1).ToString();
				int num26 = 0;
				while (array2[0, num26] != -1)
				{
					if (dataGridView.Rows[array2[0, num26]].Cells[2].Value.ToString() == b8 && dataGridView.Rows[array2[0, num26]].Cells[8].Value.ToString() != this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[0, num26]] = num13;
						num13 += 2;
					}
					num26++;
				}
			}
			for (int num27 = 0; num27 < num12; num27++)
			{
				string b9 = this.LocStrings.GetString("labelLeft") + (num27 + 1).ToString();
				int num28 = 0;
				while (array2[1, num28] != -1)
				{
					if (dataGridView.Rows[array2[1, num28]].Cells[2].Value.ToString() == b9 && dataGridView.Rows[array2[1, num28]].Cells[8].Value.ToString() != this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[1, num28]] = num15;
						num15 += 2;
					}
					num28++;
				}
			}
			if (num13 < num15)
			{
				num13 = num15;
			}
			for (int num29 = 0; num29 < num12; num29++)
			{
				string b10 = this.LocStrings.GetString("labelBack") + (num29 + 1).ToString();
				int num30 = 0;
				while (array2[2, num30] != -1)
				{
					if (dataGridView.Rows[array2[2, num30]].Cells[2].Value.ToString() == b10 && dataGridView.Rows[array2[2, num30]].Cells[8].Value.ToString() != this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[2, num30]] = num13;
						num13++;
					}
					num30++;
				}
			}
			num15 = num13 + 1;
			for (int num31 = 0; num31 < num12; num31++)
			{
				string b11 = this.LocStrings.GetString("labelBack") + (num31 + 1).ToString();
				int num32 = 0;
				while (array2[0, num32] != -1)
				{
					if (dataGridView.Rows[array2[0, num32]].Cells[2].Value.ToString() == b11 && dataGridView.Rows[array2[0, num32]].Cells[8].Value.ToString() != this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[0, num32]] = num13;
						num13 += 2;
					}
					num32++;
				}
			}
			for (int num33 = 0; num33 < num12; num33++)
			{
				string b12 = this.LocStrings.GetString("labelBack") + (num33 + 1).ToString();
				int num34 = 0;
				while (array2[1, num34] != -1)
				{
					if (dataGridView.Rows[array2[1, num34]].Cells[2].Value.ToString() == b12 && dataGridView.Rows[array2[1, num34]].Cells[8].Value.ToString() != this.LocStrings.GetString("displaygridViewFileVisionsNone"))
					{
						array[array2[1, num34]] = num15;
						num15 += 2;
					}
					num34++;
				}
			}
			int num35;
			if (num13 < num15)
			{
				num35 = num15;
			}
			else
			{
				num35 = num13;
			}
			for (int num36 = 0; num36 < 30; num36++)
			{
				string b13 = this.LocStrings.GetString("labelFornt") + (num36 + 1).ToString();
				int num37 = 0;
				while (array2[0, num37] != -1)
				{
					if (dataGridView.Rows[array2[0, num37]].Cells[2].Value.ToString() == b13)
					{
						array[array2[0, num37]] = num35;
						num35++;
					}
					num37++;
				}
			}
			for (int num38 = 0; num38 < 30; num38++)
			{
				string b14 = this.LocStrings.GetString("labelFornt") + (num38 + 1).ToString();
				int num39 = 0;
				while (array2[1, num39] != -1)
				{
					if (dataGridView.Rows[array2[1, num39]].Cells[2].Value.ToString() == b14)
					{
						array[array2[1, num39]] = num35;
						num35++;
					}
					num39++;
				}
			}
			for (int num40 = 0; num40 < 30; num40++)
			{
				string b15 = this.LocStrings.GetString("labelFornt") + (num40 + 1).ToString();
				int num41 = 0;
				while (array2[2, num41] != -1)
				{
					if (dataGridView.Rows[array2[2, num41]].Cells[2].Value.ToString() == b15)
					{
						array[array2[2, num41]] = num35;
						num35++;
					}
					num41++;
				}
			}
			for (int num42 = 0; num42 < dataGridView.RowCount; num42++)
			{
				int num43 = num42;
				for (int num44 = num42 + 1; num44 < dataGridView.RowCount; num44++)
				{
					if (array[num44] < array[num43])
					{
						num43 = num44;
					}
				}
				if (num43 != num42)
				{
					DataGridViewRow dataGridViewRow = dataGridView.Rows[num42];
					DataGridViewRow dataGridViewRow2 = dataGridView.Rows[num43];
					dataGridView.Rows.RemoveAt(num43);
					dataGridView.Rows.RemoveAt(num42);
					dataGridView.Rows.Insert(num42, dataGridViewRow2);
					dataGridView.Rows.Insert(num43, dataGridViewRow);
					int num45 = array[num43];
					array[num43] = array[num42];
					array[num42] = num45;
				}
			}
		}

		private void gridViewFile_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Escape)
			{
				this.SelCells.Clear();
				this.gridViewFile.ClearSelection();
			}
		}

		private void gridViewFile_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
		{
			if (e.Column.HeaderText == "X" || e.Column.HeaderText == "Y" || e.Column.HeaderText == this.LocRM.GetString("NozzleNums.HeaderText") || e.Column.HeaderText == this.LocRM.GetString("Angles.HeaderText") || e.Column.HeaderText == this.LocRM.GetString("Heights.HeaderText"))
			{
				double num;
				double.TryParse(e.CellValue1.ToString(), out num);
				double num2;
				double.TryParse(e.CellValue2.ToString(), out num2);
				if (num > num2)
				{
					e.SortResult = 1;
				}
				else if (num == num2)
				{
					e.SortResult = 0;
				}
				else
				{
					e.SortResult = -1;
				}
			}
			else if (e.Column.HeaderText == this.LocRM.GetString("StackNums.HeaderText"))
			{
				int num3;
				if (e.CellValue1.ToString()[0] == '左')
				{
					num3 = 0;
				}
				else if (e.CellValue1.ToString()[0] == '右')
				{
					num3 = 1;
				}
				else
				{
					num3 = 2;
				}
				int num4;
				if (e.CellValue2.ToString()[0] == '左')
				{
					num4 = 0;
				}
				else if (e.CellValue2.ToString()[0] == '右')
				{
					num4 = 1;
				}
				else
				{
					num4 = 2;
				}
				if (num3 > num4)
				{
					e.SortResult = 1;
				}
				else if (num3 < num4)
				{
					e.SortResult = -1;
				}
				else
				{
					double num5;
					double.TryParse(e.CellValue1.ToString().Substring(1), out num5);
					double num6;
					double.TryParse(e.CellValue2.ToString().Substring(1), out num6);
					if (num5 > num6)
					{
						e.SortResult = 1;
					}
					else if (num5 == num6)
					{
						e.SortResult = 0;
					}
					else
					{
						e.SortResult = -1;
					}
				}
			}
			else
			{
				e.SortResult = string.Compare(e.CellValue1.ToString(), e.CellValue2.ToString());
			}
			e.Handled = true;
		}

		private void ToolStripMenuItemClearSelect_Click(object sender, EventArgs e)
		{
			this.SelCells.Clear();
			this.gridViewFile.ClearSelection();
		}

		private void gridViewFile_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (this.gridViewFile.ReadOnly || this.gridViewFile.CurrentCell == null)
			{
				return;
			}
			int arg_2B_0 = this.gridViewFile.CurrentCell.RowIndex;
			int arg_3C_0 = this.gridViewFile.CurrentCell.ColumnIndex;
			if (e.Button == MouseButtons.Right && e.RowIndex >= 0)
			{
				if (e.ColumnIndex >= 0)
				{
					if (!this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected)
					{
						this.gridViewFile.CurrentCell = this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex];
					}
				}
				else if (e.ColumnIndex == -1)
				{
					this.gridViewFile.ClearSelection();
					this.gridViewFile.Rows[e.RowIndex].Selected = true;
				}
				if (e.ColumnIndex == 8 || e.ColumnIndex == 9 || e.ColumnIndex == 10 || (e.ColumnIndex != -1 && !this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].IsInEditMode))
				{
					this.ToolStripMenuItemSelectSame.Enabled = true;
					this.ToolStripMenuItemDelete.Enabled = true;
					this.ToolStripMenuItemSetCur.Enabled = true;
					this.ToolStripMenuItemVisionAlignment.Enabled = true;
					this.ToolStripMenuItemSetVisionCur.Enabled = true;
					this.ToolStripMenuItemSetVisionCurMark.Enabled = true;
					if (this.gridViewFile.SelectedCells.Count == 0)
					{
						this.ToolStripMenuItemClearSelect.Enabled = false;
						this.ToolStripMenuItemSelectSame.Enabled = false;
					}
					else if (this.gridViewFile.SelectedCells.Count == 1)
					{
						this.ToolStripMenuItemClearSelect.Enabled = false;
						this.ToolStripMenuItemSelectSame.Enabled = true;
					}
					else
					{
						this.ToolStripMenuItemClearSelect.Enabled = true;
						this.ToolStripMenuItemSelectSame.Enabled = false;
					}
					this.contextMenuStripFile.Show(Control.MousePosition.X, Control.MousePosition.Y);
				}
			}
			if (e.Button == MouseButtons.Left)
			{
				this.SelCells.Clear();
				if (e.RowIndex >= 0 && e.ColumnIndex >= 0 && this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected)
				{
					foreach (DataGridViewCell dataGridViewCell in this.gridViewFile.SelectedCells)
					{
						this.SelCells.Add(new Point(dataGridViewCell.RowIndex, dataGridViewCell.ColumnIndex));
					}
				}
			}
		}

		private void gridViewFile_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.gridViewFile.ReadOnly)
			{
				return;
			}
			DataGridView.HitTestInfo hitTestInfo = this.gridViewFile.HitTest(e.X, e.Y);
			if (hitTestInfo.Type == DataGridViewHitTestType.None)
			{
				if (e.Button == MouseButtons.Right)
				{
					this.gridViewFile.ClearSelection();
					this.ToolStripMenuItemSelectSame.Enabled = false;
					this.ToolStripMenuItemDelete.Enabled = false;
					if (this.gridViewFile.SelectedCells.Count > 0)
					{
						this.ToolStripMenuItemClearSelect.Enabled = true;
					}
					else
					{
						this.ToolStripMenuItemClearSelect.Enabled = false;
					}
					this.ToolStripMenuItemSetCur.Enabled = false;
					this.ToolStripMenuItemVisionAlignment.Enabled = false;
					this.ToolStripMenuItemSetVisionCur.Enabled = false;
					this.ToolStripMenuItemSetVisionCurMark.Enabled = false;
					this.contextMenuStripFile.Show(Control.MousePosition.X, Control.MousePosition.Y);
					return;
				}
				if (e.Button == MouseButtons.Left)
				{
					this.gridViewFile.ClearSelection();
				}
			}
		}

		private void gridViewFile_CurrentCellDirtyStateChanged(object sender, EventArgs e)
		{
			if (this.gridViewFile.IsCurrentCellDirty)
			{
				this.gridViewFile.CommitEdit(DataGridViewDataErrorContexts.Commit);
			}
		}

		private void gridViewFile_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (this.gridViewFile.ReadOnly)
			{
				return;
			}
			if ((e.ColumnIndex == 3 || e.ColumnIndex == 4 || e.ColumnIndex == 5 || e.ColumnIndex == 6) && e.RowIndex >= 0)
			{
				if (this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].Value == null)
				{
					this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
				}
				double num;
				if (!double.TryParse(this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(), out num))
				{
					this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = 0;
				}
			}
			if (e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 5 || e.ColumnIndex == 6 || e.ColumnIndex == 7 || e.ColumnIndex == 8 || e.ColumnIndex == 9 || e.ColumnIndex == 10)
			{
				foreach (DataGridViewCell dataGridViewCell in this.gridViewFile.SelectedCells)
				{
					if (dataGridViewCell.RowIndex != e.RowIndex && dataGridViewCell.ColumnIndex == e.ColumnIndex)
					{
						dataGridViewCell.Value = this.gridViewFile.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
					}
				}
			}
			this.SelCells.Clear();
		}

		private void gridViewFile_SelectionChanged(object sender, EventArgs e)
		{
			this.gridViewFile.SelectionChanged -= new EventHandler(this.gridViewFile_SelectionChanged);
			if (!this.gridViewFile.ReadOnly && this.SelCells.Count > 0)
			{
				foreach (Point current in this.SelCells)
				{
					if (this.gridViewFile.Rows.Count > current.X)
					{
						this.gridViewFile.Rows[current.X].Cells[current.Y].Selected = true;
					}
				}
			}
			if (this.gridViewFile.SelectedCells.Count > 0)
			{
				int num = 0;
				bool[] array = new bool[this.gridViewFile.RowCount];
				foreach (DataGridViewCell dataGridViewCell in this.gridViewFile.SelectedCells)
				{
					if (!array[dataGridViewCell.RowIndex])
					{
						array[dataGridViewCell.RowIndex] = true;
						num++;
					}
				}
				this.labelSelectedCount.Text = this.LocStrings.GetString("lableSelectLines1") + num.ToString() + this.LocStrings.GetString("lableSelectLines2");
				this.labelSelectedCount.Visible = true;
			}
			else
			{
				this.labelSelectedCount.Visible = false;
			}
			this.gridViewFile.SelectionChanged += new EventHandler(this.gridViewFile_SelectionChanged);
		}

		private void gridViewFile_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			e.CellStyle.BackColor = Color.FromArgb(250, 250, 250);
		}

		private void gridViewFile_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			this.gridViewFile.BeginEdit(true);
		}

		private void gridViewFile_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (this.gridViewFile.Columns[e.ColumnIndex].Name == "StackNums" && e.RowIndex >= 0)
			{
				this.contextMenuStack.Show((Control)sender, this.gridViewFile.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true).X, this.gridViewFile.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true).Bottom);
				this.RowStackSel = e.RowIndex;
			}
		}

		private void toolStripMenuItemLeft_Click(object sender, EventArgs e)
		{
			this.gridViewFile[2, this.RowStackSel].Value = this.LocStrings.GetString("labelLeft") + ((ToolStripMenuItem)sender).Text;
		}

		private void toolStripMenuItemBack_Click(object sender, EventArgs e)
		{
			this.gridViewFile[2, this.RowStackSel].Value = this.LocStrings.GetString("labelBack") + ((ToolStripMenuItem)sender).Text;
		}

		private void toolStripMenuItemFront_Click(object sender, EventArgs e)
		{
			this.gridViewFile[2, this.RowStackSel].Value = this.LocStrings.GetString("labelFornt") + ((ToolStripMenuItem)sender).Text;
		}

		private void ToolStripMenuItemArray_Click(object sender, EventArgs e)
		{
			MakeArray makeArray = new MakeArray(base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
			makeArray.ShowDialog();
			if (makeArray.DialogResult == DialogResult.Yes)
			{
				double xOffset = makeArray.xOffset;
				double yOffset = makeArray.yOffset;
				int xNum = makeArray.xNum;
				int yNum = makeArray.yNum;
				int rowCount = this.gridViewFile.RowCount;
				for (int i = 0; i < rowCount; i++)
				{
					string text = (string)this.gridViewFile.Rows[i].Cells[11].Value;
					if (text.StartsWith("0:"))
					{
						this.gridViewFile.Rows[i].Cells[11].Value = text.Substring(2);
					}
				}
				int j = 0;
				int k = 1;
				if (xNum <= 1)
				{
					if (yNum <= 1)
					{
						goto IL_3E3;
					}
				}
				while (j < yNum)
				{
					while (k < xNum)
					{
						for (int l = 0; l < rowCount; l++)
						{
							object[] array = new object[12];
							array[0] = this.gridViewFile.Rows[l].Cells[0].Value;
							array[1] = this.gridViewFile.Rows[l].Cells[1].Value;
							array[2] = this.gridViewFile.Rows[l].Cells[2].Value;
							double num;
							if (double.TryParse((string)this.gridViewFile.Rows[l].Cells[3].Value, out num))
							{
								array[3] = (num + (double)k * xOffset).ToString("F2");
							}
							else
							{
								array[3] = this.gridViewFile.Rows[l].Cells[3].Value;
							}
							double num2;
							if (double.TryParse((string)this.gridViewFile.Rows[l].Cells[4].Value, out num2))
							{
								array[4] = (num2 + (double)j * yOffset).ToString("F2");
							}
							else
							{
								array[4] = this.gridViewFile.Rows[l].Cells[4].Value;
							}
							array[5] = this.gridViewFile.Rows[l].Cells[5].Value;
							array[6] = this.gridViewFile.Rows[l].Cells[6].Value;
							array[7] = this.gridViewFile.Rows[l].Cells[7].Value;
							array[8] = this.gridViewFile.Rows[l].Cells[8].Value;
							array[9] = this.gridViewFile.Rows[l].Cells[9].Value;
							array[10] = this.gridViewFile.Rows[l].Cells[10].Value;
							array[11] = (j * xNum + k).ToString() + ":" + this.gridViewFile.Rows[l].Cells[11].Value;
							this.gridViewFile.Rows.Add(array);
						}
						k++;
					}
					k = 0;
					j++;
				}
				IL_3E3:
				for (int m = 0; m < rowCount; m++)
				{
					this.gridViewFile.Rows[m].Cells[11].Value = "0:" + this.gridViewFile.Rows[m].Cells[11].Value;
				}
			}
		}

		private void buttonManualSpeed_Click(object sender, EventArgs e)
		{
			if (this.ManualSpeed >= 1)
			{
				this.ManualSpeed = 0;
				return;
			}
			this.ManualSpeed = 2;
		}

		private void buttonXUp_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.DI_PrickHome)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				if (this.ManualSpeed == 0)
				{
					this.controler.SPulse(2, this.controler.PosPulse[2] + 1);
				}
				else if (this.ManualSpeed == 2)
				{
					this.controler.SPTP(2, 1000.0, CmdType.CMD_PTP_IMM);
				}
				this.buttonXUp.BackgroundImage = Resource.ButtonDown;
				this.ButtonXUp = true;
				this.DealyXUp = (this.CountFor1S + 50) % 100;
				this.DealyXUpDone = false;
			}
		}

		private void buttonXUp_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(2) == CmdResultType.Error)
				{
					return;
				}
				this.buttonXUp.BackgroundImage = Resource.ButtonUp;
				this.ButtonXUp = false;
			}
		}

		private void buttonXDown_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.DI_PrickHome)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				if (this.ManualSpeed == 0)
				{
					this.controler.SPulse(2, this.controler.PosPulse[2] - 1);
				}
				else if (this.ManualSpeed == 2)
				{
					this.controler.SPTP(2, -1000.0, CmdType.CMD_PTP_IMM);
				}
				this.buttonXDown.BackgroundImage = Resource.ButtonDown;
				this.ButtonXDown = true;
				this.DealyXDown = (this.CountFor1S + 50) % 100;
				this.DealyXDownDone = false;
			}
		}

		private void buttonXDown_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(2) == CmdResultType.Error)
				{
					return;
				}
				this.buttonXDown.BackgroundImage = Resource.ButtonUp;
				this.ButtonXDown = false;
			}
		}

		private void buttonYUp_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.DI_PrickHome)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				if (this.ManualSpeed == 0)
				{
					this.controler.SPulse(4, this.controler.PosPulse[4] + 1);
				}
				else if (this.ManualSpeed == 2)
				{
					this.controler.SPTP(4, 1000.0, CmdType.CMD_PTP_IMM);
				}
				this.buttonYUp.BackgroundImage = Resource.ButtonDown;
				this.ButtonYUp = true;
				this.DealyYUp = (this.CountFor1S + 50) % 100;
				this.DealyYUpDone = false;
			}
		}

		private void buttonYUp_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(4) == CmdResultType.Error)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("sendCommandFailed"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				this.buttonYUp.BackgroundImage = Resource.ButtonUp;
				this.ButtonYUp = false;
			}
		}

		private void buttonYDown_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.DI_PrickHome)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				if (this.ManualSpeed == 0)
				{
					this.controler.SPulse(4, this.controler.PosPulse[4] - 1);
				}
				else if (this.ManualSpeed == 2)
				{
					this.controler.SPTP(4, -1000.0, CmdType.CMD_PTP_IMM);
				}
				this.buttonYDown.BackgroundImage = Resource.ButtonDown;
				this.ButtonYDown = true;
				this.DealyYDown = (this.CountFor1S + 50) % 100;
				this.DealyYDownDone = false;
			}
		}

		private void buttonYDown_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(4) == CmdResultType.Error)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("sendCommandFailed"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				this.buttonYDown.BackgroundImage = Resource.ButtonUp;
				this.ButtonYDown = false;
			}
		}

		private void buttonNozzleUp_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM);
				this.buttonNozzle1Up.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonNozzleUp_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(1) == CmdResultType.Error)
				{
					return;
				}
				this.buttonNozzle1Up.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonNozzleDown_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(1, -16.0, CmdType.CMD_PTP_IMM);
				this.buttonNozzle1Down.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonNozzleDown_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(1) == CmdResultType.Error)
				{
					return;
				}
				this.buttonNozzle1Down.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonNozzle2Up_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM);
				this.buttonNozzle2Up.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonNozzle2Up_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(1) == CmdResultType.Error)
				{
					return;
				}
				this.buttonNozzle2Up.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonNozzle2Down_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(1, 16.0, CmdType.CMD_PTP_IMM);
				this.buttonNozzle2Down.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonNozzle2Down_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(1) == CmdResultType.Error)
				{
					return;
				}
				this.buttonNozzle2Down.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonA1Up_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(3, 360.0, CmdType.CMD_PTP_IMM);
				this.buttonA1Up.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonA1Up_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(3) == CmdResultType.Error)
				{
					return;
				}
				this.buttonA1Up.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonA1Down_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(3, 0.0, CmdType.CMD_PTP_IMM);
				this.buttonA1Down.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonA1Down_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(3) == CmdResultType.Error)
				{
					return;
				}
				this.buttonA1Down.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonA2Up_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(5, 360.0, CmdType.CMD_PTP_IMM);
				this.buttonA2Up.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonA2Up_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(5) == CmdResultType.Error)
				{
					return;
				}
				this.buttonA2Up.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonA2Down_MouseDown(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				this.controler.WriteSpeed_IMM(this.ManualSpeed, 100);
				this.controler.SPTP(5, 0.0, CmdType.CMD_PTP_IMM);
				this.buttonA2Down.BackgroundImage = Resource.ButtonDown;
			}
		}

		private void buttonA2Down_MouseUp(object sender, MouseEventArgs e)
		{
			if (this.ManualPanel == 1)
			{
				if (this.controler.SHalt(5) == CmdResultType.Error)
				{
					return;
				}
				this.buttonA2Down.BackgroundImage = Resource.ButtonUp;
			}
		}

		private void buttonVacuum1_Click(object sender, EventArgs e)
		{
			if (!this.controler.DO_Vacuum1)
			{
				this.controler.SetDO_ON_IMM(4u);
				return;
			}
			this.controler.SetDO_OFF_IMM(4u);
		}

		private void buttonVacuum2_Click(object sender, EventArgs e)
		{
			if (!this.controler.DO_Vacuum2)
			{
				this.controler.SetDO_ON_IMM(8192u);
				return;
			}
			this.controler.SetDO_OFF_IMM(8192u);
		}

		private void buttonPrick_Click(object sender, EventArgs e)
		{
			if (!this.controler.DO_Prick)
			{
				this.controler.SetDO_ON_IMM(16384u);
				return;
			}
			this.controler.SetDO_OFF_IMM(16384u);
		}

		private void buttonBlowing1_Click(object sender, EventArgs e)
		{
			if (!this.controler.DO_Blowing1)
			{
				this.controler.SetDO_ON_IMM(2u);
				return;
			}
			this.controler.SetDO_OFF_IMM(2u);
		}

		private void buttonBlowing2_Click(object sender, EventArgs e)
		{
			if (!this.controler.DO_Blowing2)
			{
				this.controler.SetDO_ON_IMM(4096u);
				return;
			}
			this.controler.SetDO_OFF_IMM(4096u);
		}

		private void buttonStrip_Click(object sender, EventArgs e)
		{
			if (this.controler.Status[0] == TcpControler.AxisStateType.AX_READY)
			{
				if (this.controler.PosMM[0] != 0.0)
				{
					double[] array = new double[6];
					array[0] = 0.0;
					this.controler.WritePos(TcpControler.AxisType.FMark, array);
				}
				this.controler.SPTP(0, -2000.0, CmdType.CMD_PTP_IMM);
			}
		}

		private void buttonPump_Click(object sender, EventArgs e)
		{
			if (!this.controler.DO_Pump)
			{
				this.controler.SetDO_ON_IMM(1032u);
				return;
			}
			this.controler.SetDO_OFF_IMM(1032u);
		}

		private void buttonGetNozzleCenter_Click(object sender, EventArgs e)
		{
			if (!this.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			if (this.IsConnect)
			{
				if (this.mVision1.Camera1 == null)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelNotConnectCamera"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).ShowDialog();
				}
				this.RunNozzleSignal = true;
				this.RunSignal = false;
				this.StepSignal = false;
				this.SuspendSignal = false;
				this.IsNozzleOffsetUsed = false;
				this.buttonSetNozzleCenter.Enabled = false;
			}
		}

		private void buttonSetNozzleCenter_Click(object sender, EventArgs e)
		{
			if (this.IsNozzleOffsetUsed)
			{
				this.IsNozzleOffsetUsed = false;
				return;
			}
			this.IsNozzleOffsetUsed = true;
		}

		private void buttonStep_Click(object sender, EventArgs e)
		{
			int i = 0;
			while (i < this.gridViewFile.RowCount && !(bool)this.gridViewFile.Rows[i].Cells[10].Value)
			{
				i++;
			}
			if (!this.RunSignal && i < this.gridViewFile.RowCount)
			{
				this.ResetNeedForm = new MyMessageBoxYesNo(this.controler, this.LocStrings.GetString("labelStartAgain"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				this.ResetNeedForm.buttonYes.Text = this.LocStrings.GetString("labelStartAgainContinue");
				this.ResetNeedForm.buttonNo.Text = this.LocStrings.GetString("labelStartAgainYes");
				this.ResetNeedForm.ShowDialog();
				if (this.ResetNeedForm.DialogResult == DialogResult.No)
				{
					for (i = 0; i < this.gridViewFile.RowCount; i++)
					{
						this.gridViewFile.Rows[i].Cells[10].Value = false;
					}
				}
				else if (this.ResetNeedForm.DialogResult == DialogResult.Cancel)
				{
					this.ResetNeedForm = null;
					return;
				}
				this.ResetNeedForm = null;
			}
			if (!this.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			if (this.IsConnect)
			{
				this.RunNozzleSignal = false;
				this.StepSignal = true;
				this.RunSignal = true;
				this.SuspendSignal = false;
			}
		}

		public void StartRunning()
		{
			if (!this.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			if (this.IsConnect)
			{
				if (this.mVision1.Camera1 == null)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelNotConnectCamera"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).ShowDialog();
				}
				this.RunNozzleSignal = false;
				this.RunSignal = true;
				this.StepSignal = false;
				this.SuspendSignal = false;
			}
		}

		private void StopMotion()
		{
			this.controler.MHalt(TcpControler.AxisType.AxisAll);
		}

		public void StopRunning()
		{
			if (this.IsConnect)
			{
				this.StopMotion();
			}
			this.GoDone(true);
		}

		private void SuspendRunning()
		{
			this.StopMotion();
			this.SuspendSignal = true;
		}

		private void ResumeRunning()
		{
			this.SuspendSignal = false;
		}

		private void buttonRun_Click(object sender, EventArgs e)
		{
			if (!this.RunSignal)
			{
				int i = 0;
				while (i < this.gridViewFile.RowCount && !(bool)this.gridViewFile.Rows[i].Cells[10].Value)
				{
					i++;
				}
				if (i < this.gridViewFile.RowCount)
				{
					this.ResetNeedForm = new MyMessageBoxYesNo(this.controler, this.LocStrings.GetString("labelStartAgain"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
					this.ResetNeedForm.buttonYes.Text = this.LocStrings.GetString("labelStartAgainContinue");
					this.ResetNeedForm.buttonNo.Text = this.LocStrings.GetString("labelStartAgainYes");
					this.ResetNeedForm.ShowDialog();
					if (this.ResetNeedForm.DialogResult == DialogResult.No)
					{
						for (i = 0; i < this.gridViewFile.RowCount; i++)
						{
							this.gridViewFile.Rows[i].Cells[10].Value = false;
						}
					}
					else if (this.ResetNeedForm.DialogResult == DialogResult.Cancel)
					{
						this.ResetNeedForm = null;
						return;
					}
					this.ResetNeedForm = null;
				}
				this.StartRunning();
				return;
			}
			this.StopRunForm = new MyMessageBoxYesNo(this.controler, this.LocStrings.GetString("labelToStop"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
			this.StopRunForm.ShowDialog();
			if (this.StopRunForm.DialogResult == DialogResult.Yes)
			{
				this.StopRunning();
			}
			this.StopRunForm = null;
		}

		private void buttonSuspend_Click(object sender, EventArgs e)
		{
			if (this.SuspendSignal)
			{
				this.ResumeRunning();
				return;
			}
			this.SuspendRunning();
		}

		private bool GoNext()
		{
			this.controler.SendQueueCmd();
			if (!this.StepSignal)
			{
				return this.RunSignal;
			}
			if (this.controler.IsBufRunDone())
			{
				this.StepSignal = false;
				this.SuspendSignal = true;
				return true;
			}
			return false;
		}

		private void GoDone(bool CloseDO)
		{
			this.controler.ControlCmdBuf(ControlBufType.STOP_BUF);
			this.gridViewFile.Enabled = true;
			this.RunState = MainForm.RunStateType.None;
			this.RunTimer = false;
			this.SuspendSignal = false;
			this.RunSignal = false;
			this.StepSignal = false;
			if (CloseDO)
			{
				this.controler.SetDO_OFF_IMM(29742u);
			}
			else
			{
				this.controler.SetDO_OFF_IMM(32u);
			}
			for (int i = 0; i < this.gridViewFile.RowCount; i++)
			{
				this.gridViewFile.Rows[i].DefaultCellStyle.BackColor = SystemColors.Control;
			}
		}

		private bool ChecPrickHome()
		{
			if (this.controler.DI_PrickHome)
			{
				this.ZAxisHomeErrorCounter++;
				if (this.ZAxisHomeErrorCounter > 200)
				{
					this.ZAxisHomeErrorCounter = 0;
					this.SuspendRunning();
					this.controler.SetDO_ON_IMM(2048u);
					MyMessageBox myMessageBox = new MyMessageBox(this.controler, this.LocStrings.GetString("prickHomingError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
					myMessageBox.Show();
				}
				return false;
			}
			this.ZAxisHomeErrorCounter = 0;
			return true;
		}

		private bool ChecPrickDown()
		{
			if (!this.controler.DI_PrickHome)
			{
				this.ZAxisHomeErrorCounter++;
				if (this.ZAxisHomeErrorCounter > 200)
				{
					this.ZAxisHomeErrorCounter = 0;
					this.SuspendRunning();
					this.controler.SetDO_ON_IMM(2048u);
					MyMessageBox myMessageBox = new MyMessageBox(this.controler, this.LocStrings.GetString("prickDowningError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
					myMessageBox.Show();
				}
				return false;
			}
			this.ZAxisHomeErrorCounter = 0;
			return true;
		}

		private bool GetNozzleStack(int num)
		{
			switch (num)
			{
			case 0:
				return this.controler.DI_NozzleStack1;
			case 1:
				return this.controler.DI_NozzleStack2;
			case 2:
				return this.controler.DI_NozzleStack3;
			case 3:
				return this.controler.DI_NozzleStack4;
			default:
				return false;
			}
		}

		private int GetVisionNozzleNum(double Width, double Height)
		{
			double num;
			if (Width < Height)
			{
				num = Width;
			}
			else
			{
				num = Height;
			}
			int result = -1;
			for (int i = 0; i < 4; i++)
			{
				if (this.NozzleSizeMin[i] < num && this.NozzleSizeMax[i] > num)
				{
					result = i;
				}
			}
			return result;
		}

		private bool IsGetTwoComp(MainForm.SMTComponent Comp)
		{
			if (Comp.Nozzle != 2)
			{
				return true;
			}
			int i = Comp.Num + 1;
			if (i >= this.gridViewFile.RowCount)
			{
				return false;
			}
			string a = this.gridViewFile.Rows[i].Cells[2].Value.ToString();
			int num = 1;
			for (i++; i < this.gridViewFile.RowCount; i++)
			{
				string a2 = this.gridViewFile.Rows[i].Cells[1].Value.ToString();
				string b = this.gridViewFile.Rows[i].Cells[2].Value.ToString();
				if (!(a2 == "1/2") || !(a == b))
				{
					break;
				}
				num++;
			}
			return num / 2 == 0;
		}

		private void GetNozzleOffset(int axis, double angle, out double x, out double y)
		{
			if (!this.IsNozzleOffsetUsed)
			{
				x = (y = 0.0);
				return;
			}
			while (angle > 180.0)
			{
				angle -= 360.0;
			}
			while (angle <= -180.0)
			{
				angle += 360.0;
			}
			angle = (angle + 22.5) / 45.0;
			int num = (int)Math.Floor(angle) + 3;
			if (num >= 8)
			{
				num = 0;
			}
			if (num < 0)
			{
				num = 7;
			}
			if (axis == 3)
			{
				x = this.VisionNozzle1CenterX[num];
				y = this.VisionNozzle1CenterY[num];
				return;
			}
			if (axis == 5)
			{
				x = this.VisionNozzle2CenterX[num];
				y = this.VisionNozzle2CenterY[num];
				return;
			}
			x = (y = 0.0);
		}

		private void UpdatelabelFileColor()
		{
			if (this.RunState <= MainForm.RunStateType.None)
			{
				this.labelFile.BackColor = SystemColors.Control;
				return;
			}
			if (this.SuspendSignal)
			{
				this.labelFile.BackColor = Color.FromArgb(255, 255, 128);
				return;
			}
			this.labelFile.BackColor = Color.PaleGreen;
		}

		private void RunStateUpdate()
		{
			this.UpdatelabelFileColor();
			if (this.SuspendSignal)
			{
				return;
			}
			switch (this.RunState)
			{
			case MainForm.RunStateType.None:
				if (this.RunSignal)
				{
					this.SelCells.Clear();
					this.gridViewFile.ClearSelection();
					int num = 0;
					while (num < this.gridViewFile.RowCount && (bool)this.gridViewFile.Rows[num].Cells[10].Value)
					{
						num++;
					}
					if (num >= this.gridViewFile.RowCount)
					{
						new MyMessageBox(this.controler, this.LocStrings.GetString("noComponent"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
						this.RunState = MainForm.RunStateType.Done;
						return;
					}
					this.CurCompNum = 0;
					this.RunDelayCounter = 0;
					this.controler.SetDO_OFF_IMM(16384u);
					this.controler.SetDO_ON_IMM(9228u);
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					this.ZAxisHomeErrorCounter = 0;
					this.ToolStripMenuItemAutoSort_Click(null, null);
					this.RunState = MainForm.RunStateType.Ready;
					return;
				}
				else if (this.RunNozzleSignal)
				{
					this.RunState = MainForm.RunStateType.VisionNozzleCheck1;
					return;
				}
				break;
			case MainForm.RunStateType.Ready:
				if (this.controler.TPTP(1, 3, 5, 0.0, -135.0, -135.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					if (!this.ChecPrickHome())
					{
						return;
					}
					this.gridViewFile.ClearSelection();
					this.gridViewFile.Enabled = false;
					this.progressBarRun.Maximum = this.gridViewFile.RowCount;
					this.RunTimer = true;
					this.DoneNum = 0;
					this.ErrorCounter = 0;
					this.CurPuzzleNum1 = -1;
					this.CurPuzzleNum2 = -1;
					for (int i = 0; i < this.Puzzle.IsMark1Used.Length; i++)
					{
						if (this.Puzzle.IsMark1Used[i])
						{
							this.CurPuzzleNum1 = (this.CurPuzzleNum2 = i);
							break;
						}
					}
					this.controler.SetDO_OFF_IMM(8196u);
					if (this.mVision1.Camera2 != null && this.CurPuzzleNum1 != -1 && this.Mark.Mode != MarkModeType.NoMark)
					{
						if (this.controler.ControlCmdBuf(ControlBufType.STOP_BUF) == CmdResultType.Done)
						{
							this.RunState = MainForm.RunStateType.VisionMark;
							return;
						}
					}
					else if (this.controler.ControlCmdBuf(ControlBufType.RUN_BUF) == CmdResultType.Done)
					{
						this.RunState = MainForm.RunStateType.FindNextComponent;
						return;
					}
				}
				break;
			case MainForm.RunStateType.VisionMark:
				switch (this.SubState)
				{
				case -1:
					this.PreCalcParam();
					this.controler.ControlCmdBuf(ControlBufType.RUN_BUF);
					this.RunState = MainForm.RunStateType.FindNextComponent;
					return;
				case 0:
					if (this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.CurVisionMarkPuzzleNum = 0;
						this.SubState = 1;
						return;
					}
					break;
				case 1:
					while (this.CurVisionMarkPuzzleNum < this.Puzzle.IsMark1Used.Length && !this.Puzzle.IsMark1Used[this.CurVisionMarkPuzzleNum])
					{
						this.CurVisionMarkPuzzleNum++;
					}
					if (this.CurVisionMarkPuzzleNum >= this.Puzzle.IsMark1Used.Length)
					{
						this.SubState = -1;
						return;
					}
					this.SubState = 2;
					return;
				case 2:
					if (this.controler.DPTP(2, 4, this.Puzzle.Mark1MeasureX[this.CurVisionMarkPuzzleNum] - this.VisionOffsetX, this.Puzzle.Mark1MeasureY[this.CurVisionMarkPuzzleNum] - this.VisionOffsetY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						if (this.Mark.Mode == MarkModeType.Auto)
						{
							this.GetMarkCounter = 0;
							this.RunDelayCounter = 5;
							this.SubState = 3;
							return;
						}
						this.MarkOffsetX = this.controler.PosMM[2];
						this.MarkOffsetY = this.controler.PosMM[4];
						this.SubState = 6;
						return;
					}
					break;
				case 3:
					if (this.RunDelayCounter == 0)
					{
						this.RunDelayCounter = 5;
					}
					this.RunDelayCounter--;
					if (this.RunDelayCounter == 0)
					{
						if (this.mVision1.Camera2 != null)
						{
							if (this.Mark.Mode == MarkModeType.Auto)
							{
								switch (this.Mark.Shape)
								{
								case MarkShapeType.Round:
									this.mVision1.ModeShape = 0;
									break;
								case MarkShapeType.Rectangle:
									this.mVision1.ModeShape = 1;
									break;
								case MarkShapeType.Irregular:
									this.mVision1.ModeShape = 2;
									break;
								}
							}
							else
							{
								this.mVision1.ModeShape = -1;
							}
							this.HeartBeatBackup = this.mVision1.HeartBeat;
							this.CheckVision = CheckVisionType.PCBVision;
							this.mVision1.CheckMarkRun();
							this.SubState = 4;
							return;
						}
						this.MarkOffsetX = this.controler.PosMM[2];
						this.MarkOffsetY = this.controler.PosMM[4];
						this.SubState = 6;
						return;
					}
					break;
				case 4:
					if (this.HeartBeatBackup != this.mVision1.HeartBeat)
					{
						if (this.mVision1.CompSum != -1)
						{
							this.MarkOffsetX = this.controler.PosMM[2] + this.mVision1.CompOffsetW2;
							this.MarkOffsetY = this.controler.PosMM[4] + this.mVision1.CompOffsetH2;
							if (Math.Abs(this.mVision1.CompOffsetW2) < 0.03 && Math.Abs(this.mVision1.CompOffsetH2) < 0.03)
							{
								if (this.Mark.IsAutoPause)
								{
									this.SubState = 6;
								}
								else
								{
									this.SuspendPosX = this.controler.PosMM[2];
									this.SuspendPosY = this.controler.PosMM[4];
									this.SubState = 7;
								}
							}
							else
							{
								this.GetMarkCounter++;
								this.SubState = 5;
							}
						}
						else
						{
							this.GetMarkCounter++;
							this.SubState = 3;
						}
						if (this.GetMarkCounter > 50)
						{
							this.controler.SetDO_ON_IMM(2048u);
							new MyMessageBox(this.controler, this.LocStrings.GetString("markIdentiFailure"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
							this.MarkOffsetX = this.controler.PosMM[2];
							this.MarkOffsetY = this.controler.PosMM[4];
							this.SubState = 6;
							return;
						}
					}
					break;
				case 5:
					if (this.controler.DPTP(2, 4, this.MarkOffsetX, this.MarkOffsetY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.RunDelayCounter = 5;
						this.SubState = 3;
						return;
					}
					break;
				case 6:
					this.CheckVision = CheckVisionType.PCBVision;
					this.SuspendPosX = this.controler.PosMM[2];
					this.SuspendPosY = this.controler.PosMM[4];
					this.SuspendRunning();
					this.ManualPanel = 1;
					this.SubState = 7;
					return;
				case 7:
					this.ManualPanel = 0;
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					if (this.SuspendPosX == this.controler.PosMM[2] && this.SuspendPosY == this.controler.PosMM[4])
					{
						this.Puzzle.Mark1MeasureX[this.CurVisionMarkPuzzleNum] = this.MarkOffsetX + this.VisionOffsetX;
						this.Puzzle.Mark1MeasureY[this.CurVisionMarkPuzzleNum] = this.MarkOffsetY + this.VisionOffsetY;
					}
					else
					{
						this.Puzzle.Mark1MeasureX[this.CurVisionMarkPuzzleNum] = this.controler.PosMM[2] + this.VisionOffsetX;
						this.Puzzle.Mark1MeasureY[this.CurVisionMarkPuzzleNum] = this.controler.PosMM[4] + this.VisionOffsetY;
					}
					if (this.Puzzle.IsMark2Used[this.CurVisionMarkPuzzleNum])
					{
						this.SubState = 8;
						return;
					}
					this.SubState = 14;
					return;
				case 8:
					if (this.controler.DPTP(2, 4, this.Puzzle.Mark2MeasureX[this.CurVisionMarkPuzzleNum] - this.VisionOffsetX, this.Puzzle.Mark2MeasureY[this.CurVisionMarkPuzzleNum] - this.VisionOffsetY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						if (this.Mark.Mode == MarkModeType.Auto)
						{
							this.GetMarkCounter = 0;
							this.RunDelayCounter = 5;
							this.SubState = 9;
							return;
						}
						this.MarkOffsetX = this.controler.PosMM[2];
						this.MarkOffsetY = this.controler.PosMM[4];
						this.SubState = 12;
						return;
					}
					break;
				case 9:
					if (this.RunDelayCounter == 0)
					{
						this.RunDelayCounter = 5;
					}
					this.RunDelayCounter--;
					if (this.RunDelayCounter == 0)
					{
						if (this.mVision1.Camera2 != null)
						{
							if (this.Mark.Mode == MarkModeType.Auto)
							{
								switch (this.Mark.Shape)
								{
								case MarkShapeType.Round:
									this.mVision1.ModeShape = 0;
									break;
								case MarkShapeType.Rectangle:
									this.mVision1.ModeShape = 1;
									break;
								case MarkShapeType.Irregular:
									this.mVision1.ModeShape = 2;
									break;
								}
							}
							else
							{
								this.mVision1.ModeShape = -1;
							}
							this.HeartBeatBackup = this.mVision1.HeartBeat;
							this.CheckVision = CheckVisionType.PCBVision;
							this.mVision1.CheckMarkRun();
							this.SubState = 10;
							return;
						}
						this.MarkOffsetX = this.controler.PosMM[2];
						this.MarkOffsetY = this.controler.PosMM[4];
						this.SubState = 12;
						return;
					}
					break;
				case 10:
					if (this.HeartBeatBackup != this.mVision1.HeartBeat)
					{
						if (this.mVision1.CompSum != -1)
						{
							this.MarkOffsetX = this.controler.PosMM[2] + this.mVision1.CompOffsetW2;
							this.MarkOffsetY = this.controler.PosMM[4] + this.mVision1.CompOffsetH2;
							if (Math.Abs(this.mVision1.CompOffsetW2) < 0.03 && Math.Abs(this.mVision1.CompOffsetH2) < 0.03)
							{
								if (this.Mark.IsAutoPause)
								{
									this.SubState = 12;
								}
								else
								{
									this.SuspendPosX = this.controler.PosMM[2];
									this.SuspendPosY = this.controler.PosMM[4];
									this.SubState = 13;
								}
							}
							else
							{
								this.GetMarkCounter++;
								this.SubState = 11;
							}
						}
						else
						{
							this.GetMarkCounter++;
							this.SubState = 9;
						}
						if (this.GetMarkCounter > 50)
						{
							this.controler.SetDO_ON_IMM(2048u);
							new MyMessageBox(this.controler, this.LocStrings.GetString("markIdentiFailure"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
							this.MarkOffsetX = this.controler.PosMM[2];
							this.MarkOffsetY = this.controler.PosMM[4];
							this.SubState = 12;
							return;
						}
					}
					break;
				case 11:
					if (this.controler.DPTP(2, 4, this.MarkOffsetX, this.MarkOffsetY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.RunDelayCounter = 5;
						this.SubState = 9;
						return;
					}
					break;
				case 12:
					this.CheckVision = CheckVisionType.PCBVision;
					this.SuspendPosX = this.controler.PosMM[2];
					this.SuspendPosY = this.controler.PosMM[4];
					this.SuspendRunning();
					this.ManualPanel = 1;
					this.SubState = 13;
					return;
				case 13:
					this.ManualPanel = 0;
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					if (this.SuspendPosX == this.controler.PosMM[2] && this.SuspendPosY == this.controler.PosMM[4])
					{
						this.Puzzle.Mark2MeasureX[this.CurVisionMarkPuzzleNum] = this.MarkOffsetX + this.VisionOffsetX;
						this.Puzzle.Mark2MeasureY[this.CurVisionMarkPuzzleNum] = this.MarkOffsetY + this.VisionOffsetY;
					}
					else
					{
						this.Puzzle.Mark2MeasureX[this.CurVisionMarkPuzzleNum] = this.controler.PosMM[2] + this.VisionOffsetX;
						this.Puzzle.Mark2MeasureY[this.CurVisionMarkPuzzleNum] = this.controler.PosMM[4] + this.VisionOffsetY;
					}
					this.SubState = 14;
					return;
				case 14:
					this.CurVisionMarkPuzzleNum++;
					this.SubState = 1;
					return;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.VisionNozzle:
			case MainForm.RunStateType.ChangeNozzle:
				break;
			case MainForm.RunStateType.VisionNozzleCheck1:
				switch (this.SubState)
				{
				case -1:
					if (this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.RunState = MainForm.RunStateType.VisionNozzleCheck2;
						return;
					}
					break;
				case 0:
					this.controler.SetDO_OFF_IMM(16384u);
					if (this.controler.TPTP(1, 3, 5, 0.0, -135.0, -135.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						if (!this.ChecPrickHome())
						{
							return;
						}
						if (this.controler.PosMM[4] < 0.0)
						{
							this.SubState = 1;
							return;
						}
						this.SubState = 2;
						return;
					}
					break;
				case 1:
					if (this.controler.SPTP(4, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					if (this.controler.SPTP(2, this.VisionX, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = 3;
						return;
					}
					break;
				case 3:
					if (this.controler.SPTP(4, this.VisionY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = 4;
						return;
					}
					break;
				case 4:
					if (this.controler.SPTP(1, this.NozzleVisionZ, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.VisionNozzleTestCounter = 0;
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					if (this.RunDelayCounter == 0)
					{
						this.RunDelayCounter = 5;
					}
					this.RunDelayCounter--;
					if (this.RunDelayCounter == 0)
					{
						if (this.mVision1.Camera1 != null)
						{
							this.CheckVision = CheckVisionType.NozzleVision;
							this.mVision1.CompThreshold = (int)this.VisionValueNozzle;
							this.HeartBeatBackup = this.mVision1.HeartBeat;
							this.mVision1.CheckNozzleRun();
						}
						this.SubState = 6;
						return;
					}
					break;
				case 6:
					if (this.HeartBeatBackup != this.mVision1.HeartBeat)
					{
						if (this.mVision1.CompSum <= 0)
						{
							this.controler.SetDO_ON_IMM(2048u);
							new MyMessageBox(this.controler, this.LocStrings.GetString("cameraError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
							this.controler.SPTP(4, 0.0, CmdType.CMD_PTP_IMM);
							this.SuspendRunning();
							return;
						}
						this.VisionNozzle1CenterX[this.VisionNozzleTestCounter] = this.mVision1.CompOffsetW1;
						this.VisionNozzle1CenterY[this.VisionNozzleTestCounter] = this.mVision1.CompOffsetH1;
						this.VisionNozzleTestCounter++;
						if (this.VisionNozzleTestCounter >= 8)
						{
							this.AvrVisionNozzle1CenterX = this.VisionNozzle1CenterX.Average();
							this.AvrVisionNozzle1CenterY = this.VisionNozzle1CenterY.Average();
							for (int j = 0; j < 8; j++)
							{
								this.VisionNozzle1CenterX[j] -= this.AvrVisionNozzle1CenterX;
								this.VisionNozzle1CenterY[j] -= this.AvrVisionNozzle1CenterY;
							}
							this.SubState = -1;
							return;
						}
						this.SubState = 7;
						return;
					}
					break;
				case 7:
					if (this.controler.SPTP(3, (double)(this.VisionNozzleTestCounter * 45 - 135), CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = 5;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.VisionNozzleCheck2:
				switch (this.SubState)
				{
				case -1:
					if (this.controler.SPTP(4, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.buttonSetNozzleCenter.Enabled = true;
						this.RunNozzleSignal = false;
						this.RunState = MainForm.RunStateType.None;
						return;
					}
					break;
				case 0:
					this.controler.SetDO_OFF_IMM(16384u);
					if (this.controler.TPTP(1, 3, 5, 0.0, 0.0, -135.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						if (!this.ChecPrickHome())
						{
							return;
						}
						this.SubState = 1;
						return;
					}
					break;
				case 1:
					if (this.controler.SPTP(2, this.VisionX + this.NozzleOffsetX, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					if (this.controler.SPTP(4, this.VisionY + this.NozzleOffsetY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = 3;
						return;
					}
					break;
				case 3:
					if (this.controler.SPTP(1, -this.NozzleVisionZ, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.VisionNozzleTestCounter = 0;
						this.SubState = 4;
						return;
					}
					break;
				case 4:
					if (this.RunDelayCounter == 0)
					{
						this.RunDelayCounter = 5;
					}
					this.RunDelayCounter--;
					if (this.RunDelayCounter == 0)
					{
						if (this.mVision1.Camera1 != null)
						{
							this.CheckVision = CheckVisionType.NozzleVision;
							this.mVision1.CompThreshold = (int)this.VisionValueNozzle;
							this.HeartBeatBackup = this.mVision1.HeartBeat;
							this.mVision1.CheckNozzleRun();
						}
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					if (this.HeartBeatBackup != this.mVision1.HeartBeat)
					{
						if (this.mVision1.CompSum <= 0)
						{
							this.controler.SetDO_ON_IMM(2048u);
							new MyMessageBox(this.controler, this.LocStrings.GetString("cameraError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
							this.controler.SPTP(4, 0.0, CmdType.CMD_PTP_IMM);
							this.SuspendRunning();
							return;
						}
						this.VisionNozzle2CenterX[this.VisionNozzleTestCounter] = this.mVision1.CompOffsetW1;
						this.VisionNozzle2CenterY[this.VisionNozzleTestCounter] = this.mVision1.CompOffsetH1;
						this.VisionNozzleTestCounter++;
						if (this.VisionNozzleTestCounter >= 8)
						{
							this.AvrVisionNozzle2CenterX = this.VisionNozzle2CenterX.Average();
							this.AvrVisionNozzle2CenterY = this.VisionNozzle2CenterY.Average();
							for (int k = 0; k < 8; k++)
							{
								this.VisionNozzle2CenterX[k] -= this.AvrVisionNozzle2CenterX;
								this.VisionNozzle2CenterY[k] -= this.AvrVisionNozzle2CenterY;
							}
							this.SubState = 7;
							return;
						}
						this.SubState = 6;
						return;
					}
					break;
				case 6:
					if (this.controler.SPTP(5, (double)(this.VisionNozzleTestCounter * 45 - 135), CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = 4;
						return;
					}
					break;
				case 7:
					if (this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						this.SubState = -1;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.FindNextComponent:
				switch (this.SubState)
				{
				case 0:
					if (this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT) == CmdResultType.Done)
					{
						while (this.CurCompNum < this.gridViewFile.RowCount)
						{
							if ((bool)this.gridViewFile.Rows[this.CurCompNum].Cells[10].Value)
							{
								this.CurCompNum++;
							}
							else
							{
								MainForm.SMTComponent sMTComponent = new MainForm.SMTComponent();
								sMTComponent.Num = this.CurCompNum;
								sMTComponent.State = MainForm.ComponentStateType.None;
								sMTComponent.Nozzle = this.GetNozzleCode(this.gridViewFile.Rows[sMTComponent.Num].Cells[1].Value.ToString());
								sMTComponent.Stack = this.GetStackCode(this.gridViewFile.Rows[sMTComponent.Num].Cells[2].Value.ToString());
								if (sMTComponent.Stack >= 60)
								{
									sMTComponent.IsFrontStack = true;
									sMTComponent.Stack -= 60;
								}
								else
								{
									sMTComponent.IsFrontStack = false;
								}
								sMTComponent.X = double.Parse(this.gridViewFile.Rows[sMTComponent.Num].Cells[3].Value.ToString());
								sMTComponent.Y = double.Parse(this.gridViewFile.Rows[sMTComponent.Num].Cells[4].Value.ToString());
								sMTComponent.Angle = double.Parse(this.gridViewFile.Rows[sMTComponent.Num].Cells[5].Value.ToString());
								if (this.CurPuzzleNum1 != -1)
								{
									sMTComponent.X -= this.Puzzle.Mark1X[this.CurPuzzleNum1];
									sMTComponent.Y -= this.Puzzle.Mark1Y[this.CurPuzzleNum1];
									if (this.Puzzle.Scale[this.CurPuzzleNum1] != 0.0)
									{
										sMTComponent.X *= this.Puzzle.Scale[this.CurPuzzleNum1];
										sMTComponent.Y *= this.Puzzle.Scale[this.CurPuzzleNum1];
									}
									double num2 = this.Puzzle.Angle2[this.CurPuzzleNum1] - this.Puzzle.Angle1[this.CurPuzzleNum1];
									double num3 = Math.Cos(num2);
									double num4 = Math.Sin(num2);
									double num5 = num3 * sMTComponent.X - num4 * sMTComponent.Y;
									double num6 = num4 * sMTComponent.X + num3 * sMTComponent.Y;
									sMTComponent.X = num5 + this.Puzzle.Mark1MeasureX[this.CurPuzzleNum1];
									sMTComponent.Y = num6 + this.Puzzle.Mark1MeasureY[this.CurPuzzleNum1];
									sMTComponent.Angle += num2 * 180.0 / 3.1415926535897931;
								}
								while (sMTComponent.Angle > 180.0)
								{
									sMTComponent.Angle -= 360.0;
								}
								while (sMTComponent.Angle <= -180.0)
								{
									sMTComponent.Angle += 360.0;
								}
								if (sMTComponent.X < 0.0 || sMTComponent.X > this.HomePosX - 1.0 || sMTComponent.Y < 0.0 || sMTComponent.Y > this.HomePosY - 1.0)
								{
									this.controler.SetDO_ON_IMM(2048u);
									new MyMessageBox(this.controler, this.LocStrings.GetString("compPosOverflow"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
									this.SuspendRunning();
									return;
								}
								sMTComponent.Height = double.Parse(this.gridViewFile.Rows[sMTComponent.Num].Cells[6].Value.ToString());
								sMTComponent.Height = ((sMTComponent.Height == 0.0) ? 0.5 : sMTComponent.Height);
								sMTComponent.Speed = int.Parse(this.gridViewFile.Rows[sMTComponent.Num].Cells[7].Value.ToString());
								sMTComponent.VisonType = this.GetVisionType(this.gridViewFile.Rows[sMTComponent.Num].Cells[8].Value.ToString());
								sMTComponent.IsVisionDone = false;
								sMTComponent.IsPressureCheck = (bool)this.gridViewFile.Rows[sMTComponent.Num].Cells[9].Value;
								int num7 = this.CurCompNum + 1;
								if (num7 < this.gridViewFile.RowCount && !(bool)this.gridViewFile.Rows[num7].Cells[10].Value)
								{
									int nozzleCode = this.GetNozzleCode(this.gridViewFile.Rows[num7].Cells[1].Value.ToString());
									int num8 = this.GetStackCode(this.gridViewFile.Rows[num7].Cells[2].Value.ToString());
									bool flag;
									if (num8 >= 60)
									{
										flag = true;
										num8 -= 60;
									}
									else
									{
										flag = false;
									}
									int visionType = this.GetVisionType(this.gridViewFile.Rows[num7].Cells[8].Value.ToString());
									if ((nozzleCode != sMTComponent.Nozzle || nozzleCode == 2) && sMTComponent.IsFrontStack == flag && sMTComponent.VisonType != 0 == (visionType != 0) && (num8 == sMTComponent.Stack || this.IsGetTwoComp(sMTComponent)))
									{
										MainForm.SMTComponent sMTComponent2 = new MainForm.SMTComponent();
										sMTComponent2.Num = num7;
										sMTComponent2.State = MainForm.ComponentStateType.None;
										sMTComponent2.Nozzle = nozzleCode;
										sMTComponent2.Stack = num8;
										sMTComponent2.IsFrontStack = flag;
										sMTComponent2.X = double.Parse(this.gridViewFile.Rows[sMTComponent2.Num].Cells[3].Value.ToString());
										sMTComponent2.Y = double.Parse(this.gridViewFile.Rows[sMTComponent2.Num].Cells[4].Value.ToString());
										sMTComponent2.Angle = double.Parse(this.gridViewFile.Rows[sMTComponent2.Num].Cells[5].Value.ToString());
										if (this.CurPuzzleNum2 != -1)
										{
											sMTComponent2.X -= this.Puzzle.Mark1X[this.CurPuzzleNum2];
											sMTComponent2.Y -= this.Puzzle.Mark1Y[this.CurPuzzleNum2];
											if (this.Puzzle.Scale[this.CurPuzzleNum2] != 0.0)
											{
												sMTComponent2.X *= this.Puzzle.Scale[this.CurPuzzleNum2];
												sMTComponent2.Y *= this.Puzzle.Scale[this.CurPuzzleNum2];
											}
											double num9 = this.Puzzle.Angle2[this.CurPuzzleNum2] - this.Puzzle.Angle1[this.CurPuzzleNum2];
											double num10 = Math.Cos(num9);
											double num11 = Math.Sin(num9);
											double num12 = num10 * sMTComponent2.X - num11 * sMTComponent2.Y;
											double num13 = num11 * sMTComponent2.X + num10 * sMTComponent2.Y;
											sMTComponent2.X = num12 + this.Puzzle.Mark1MeasureX[this.CurPuzzleNum2];
											sMTComponent2.Y = num13 + this.Puzzle.Mark1MeasureY[this.CurPuzzleNum2];
											sMTComponent2.Angle += num9 * 180.0 / 3.1415926535897931;
										}
										while (sMTComponent2.Angle > 180.0)
										{
											sMTComponent2.Angle -= 360.0;
										}
										while (sMTComponent2.Angle <= -180.0)
										{
											sMTComponent2.Angle += 360.0;
										}
										if (sMTComponent2.X < 0.0 || sMTComponent2.X > this.HomePosX - 1.0 || sMTComponent2.Y < 0.0 || sMTComponent2.Y > this.HomePosY - 1.0)
										{
											this.controler.SetDO_ON_IMM(2048u);
											new MyMessageBox(this.controler, this.LocStrings.GetString("compPosOverflow"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
											this.SuspendRunning();
											return;
										}
										sMTComponent2.Height = double.Parse(this.gridViewFile.Rows[sMTComponent2.Num].Cells[6].Value.ToString());
										sMTComponent2.Height = ((sMTComponent2.Height == 0.0) ? 0.5 : sMTComponent2.Height);
										sMTComponent2.Speed = int.Parse(this.gridViewFile.Rows[sMTComponent2.Num].Cells[7].Value.ToString());
										sMTComponent2.VisonType = visionType;
										sMTComponent2.IsVisionDone = false;
										sMTComponent2.IsPressureCheck = (bool)this.gridViewFile.Rows[sMTComponent2.Num].Cells[9].Value;
										if (sMTComponent.Speed >= sMTComponent2.Speed)
										{
											this.CurComp1 = sMTComponent;
											this.CurComp2 = sMTComponent2;
										}
										else
										{
											this.CurComp1 = sMTComponent2;
											this.CurComp2 = sMTComponent;
										}
										switch (this.CurComp1.Nozzle)
										{
										case 0:
											this.CurComp1.AAxis = 3;
											this.CurComp2.AAxis = 5;
											break;
										case 1:
											this.CurComp1.AAxis = 5;
											this.CurComp2.AAxis = 3;
											break;
										case 2:
											switch (this.CurComp2.Nozzle)
											{
											case 0:
												this.CurComp1.AAxis = 5;
												this.CurComp2.AAxis = 3;
												break;
											case 1:
												this.CurComp1.AAxis = 3;
												this.CurComp2.AAxis = 5;
												break;
											case 2:
												this.CurComp1.AAxis = 3;
												this.CurComp2.AAxis = 5;
												break;
											}
											break;
										}
										if (this.CurComp1.AAxis == 5)
										{
											this.CurComp1.OffsetX = this.NozzleOffsetX;
											this.CurComp1.OffsetY = this.NozzleOffsetY;
										}
										else
										{
											this.CurComp1.OffsetX = 0.0;
											this.CurComp1.OffsetY = 0.0;
										}
										if (this.CurComp2.AAxis == 5)
										{
											this.CurComp2.OffsetX = this.NozzleOffsetX;
											this.CurComp2.OffsetY = this.NozzleOffsetY;
										}
										else
										{
											this.CurComp2.OffsetX = 0.0;
											this.CurComp2.OffsetY = 0.0;
										}
										if (this.CurComp1.Stack == this.CurComp2.Stack && !this.CurComp1.IsFrontStack && this.LeftAndBackStack.Feed[this.CurComp1.Stack] <= 4)
										{
											this.RunState = MainForm.RunStateType.FeedComponent1And2;
											break;
										}
										if (sMTComponent.IsFrontStack)
										{
											this.RunState = MainForm.RunStateType.PickComponent1;
											break;
										}
										this.RunState = MainForm.RunStateType.FeedComponent1;
										break;
									}
								}
								this.CurComp1 = sMTComponent;
								this.CurComp2 = null;
								switch (this.CurComp1.Nozzle)
								{
								case 0:
									this.CurComp1.AAxis = 3;
									break;
								case 1:
									this.CurComp1.AAxis = 5;
									break;
								case 2:
									this.CurComp1.AAxis = 3;
									break;
								}
								if (this.CurComp1.AAxis == 5)
								{
									this.CurComp1.OffsetX = this.NozzleOffsetX;
									this.CurComp1.OffsetY = this.NozzleOffsetY;
								}
								if (sMTComponent.IsFrontStack)
								{
									this.RunState = MainForm.RunStateType.PickComponent1;
									break;
								}
								this.RunState = MainForm.RunStateType.FeedComponent1;
								break;
							}
						}
						if (this.CurCompNum >= this.gridViewFile.RowCount)
						{
							this.SubState = 1;
							return;
						}
					}
					break;
				case 1:
					if (this.controler.IsBufRunDone())
					{
						new MyMessageBox(this.controler, this.LocStrings.GetString("placeDone"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
						this.RunState = MainForm.RunStateType.Done;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.FeedComponent1:
				switch (this.SubState)
				{
				case -1:
					if (this.GoNext())
					{
						this.RunState = MainForm.RunStateType.PickComponent1;
						return;
					}
					break;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, 100);
					if (this.LeftAndBackStack.Feed[this.CurComp1.Stack] == 2)
					{
						this.Feed2MM = 2;
					}
					else
					{
						this.Feed2MM = 0;
					}
					if (this.CurComp1.Stack < 30)
					{
						this.CurComp1.PickAngle = 0.0;
						this.controler.TPTP(2, 4, this.CurComp1.AAxis, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp1.Stack] - (double)this.LeftAndBackStack.Feed[this.CurComp1.Stack] - (double)this.Feed2MM - 0.5, this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.LeftPrickOffsetY + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], this.CurComp1.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.CurComp1.PickAngle = -90.0;
						this.controler.TPTP(2, 4, this.CurComp1.AAxis, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.BackPrickOffsetX + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp1.Stack] + (double)this.LeftAndBackStack.Feed[this.CurComp1.Stack] + (double)this.Feed2MM + 0.5, this.CurComp1.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.SubState = 1;
					return;
				case 1:
					if (this.GoNext())
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
				{
					double[] array = new double[6];
					array[0] = 0.0;
					this.controler.WritePos_QUEUE(TcpControler.AxisType.FMark, array);
					this.controler.SetDO_ON_QUEUE(16384u);
					if (this.CurComp2 != null && this.CurComp2.State == MainForm.ComponentStateType.Suck)
					{
						this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp2.Speed);
					}
					else
					{
						this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp1.Speed);
					}
					this.controler.WaitPrickHome_QUEUE(true);
					this.controler.SetDelayMs_QUEUE(10);
					this.SubState = 3;
					return;
				}
				case 3:
					if (this.GoNext())
					{
						this.SubState = 4;
						return;
					}
					break;
				case 4:
					this.controler.SPTP(0, -2000.0, CmdType.CMD_PTP_QUEUE);
					if (this.LeftAndBackStack.Feed[this.CurComp1.Stack] == 2)
					{
						this.Feed2MM = 2;
					}
					else
					{
						this.Feed2MM = 0;
					}
					if (this.CurComp1.Stack < 30)
					{
						this.controler.DPTP(2, 4, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp1.Stack] - (double)this.Feed2MM, this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.LeftPrickOffsetY + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], CmdType.CMD_PTP_QUEUE_WAIT);
						this.controler.SetDelayMs_QUEUE(10);
						this.controler.SPTP(2, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp1.Stack] - (double)this.Feed2MM - this.FeedBack, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.controler.DPTP(2, 4, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.BackPrickOffsetX + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp1.Stack] + (double)this.Feed2MM, CmdType.CMD_PTP_QUEUE_WAIT);
						this.controler.SetDelayMs_QUEUE(10);
						this.controler.SPTP(4, this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp1.Stack] + (double)this.Feed2MM + this.FeedBack, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					if (this.LeftAndBackStack.Feed[this.CurComp1.Stack] == 2)
					{
						if (this.StackMove[this.CurComp1.Stack] == 0)
						{
							this.StackMove[this.CurComp1.Stack] = 2;
						}
						else
						{
							this.StackMove[this.CurComp1.Stack] = 0;
						}
					}
					this.SubState = 5;
					return;
				case 5:
					if (this.GoNext())
					{
						this.SubState = 6;
						return;
					}
					break;
				case 6:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, 100);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.SubState = -1;
					return;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.PickComponent1:
				switch (this.SubState)
				{
				case -1:
					if (this.PressureCheckTotal && this.CurComp1.IsPressureCheck)
					{
						if (this.controler.IsBufRunDone())
						{
							if (!this.controler.GetDiPressure(this.CurComp1.AAxis))
							{
								this.CurComp1.State = MainForm.ComponentStateType.Discard;
								this.RunState = MainForm.RunStateType.DiscardComponent;
								return;
							}
							this.CurComp1.State = MainForm.ComponentStateType.Suck;
							if (this.CurComp2 == null || this.CurComp2.State == MainForm.ComponentStateType.Done)
							{
								if (this.CurComp1.VisonType != 0 && this.mVision1.Camera1 != null)
								{
									this.RunState = MainForm.RunStateType.VisionCenter1;
									return;
								}
								this.RunState = MainForm.RunStateType.PlaceComponent1;
								return;
							}
							else
							{
								if (!this.CurComp2.IsFrontStack)
								{
									this.RunState = MainForm.RunStateType.FeedComponent2;
									return;
								}
								this.RunState = MainForm.RunStateType.PickComponent2;
								return;
							}
						}
					}
					else
					{
						this.CurComp1.State = MainForm.ComponentStateType.Suck;
						if (this.CurComp2 == null || this.CurComp2.State == MainForm.ComponentStateType.Done)
						{
							if (this.CurComp1.VisonType != 0 && this.mVision1.Camera1 != null)
							{
								this.RunState = MainForm.RunStateType.VisionCenter1;
								return;
							}
							this.RunState = MainForm.RunStateType.PlaceComponent1;
							return;
						}
						else
						{
							if (!this.CurComp2.IsFrontStack)
							{
								this.RunState = MainForm.RunStateType.FeedComponent2;
								return;
							}
							this.RunState = MainForm.RunStateType.PickComponent2;
							return;
						}
					}
					break;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.DPTP(1, this.CurComp1.AAxis, 0.0, this.CurComp1.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, 100);
					this.SubState = 1;
					return;
				case 1:
					if (!this.controler.IsBufRunDone())
					{
						this.ZAxisHomeErrorCounter = 0;
						return;
					}
					if (this.ChecPrickHome())
					{
						this.GetNozzleOffset(this.CurComp1.AAxis, this.CurComp1.PickAngle, out this.NozzleOffsetXAtAngle, out this.NozzleOffsetYAtAngle);
						if (!this.CurComp1.IsFrontStack)
						{
							this.controler.TPTP(2, 4, this.CurComp1.AAxis, this.LeftAndBackStack.X[this.CurComp1.Stack] - this.NozzleOffsetXAtAngle + this.CurComp1.OffsetX, this.LeftAndBackStack.Y[this.CurComp1.Stack] - this.NozzleOffsetYAtAngle + this.CurComp1.OffsetY, this.CurComp1.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
						}
						else
						{
							this.GetIcStackXY(this.CurComp1.Stack, out this.IcStackX, out this.IcStackY);
							this.controler.TPTP(2, 4, this.CurComp1.AAxis, this.IcStackX + this.CurComp1.OffsetX, this.IcStackY + this.CurComp1.OffsetY, this.CurComp1.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
						}
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					if (this.GoNext())
					{
						this.SubState = 3;
						return;
					}
					break;
				case 3:
					this.controler.SPTP(1, (double)this.CurComp1.ZAxisDir * (this.CurComp1.IsFrontStack ? (this.ZAxisNozzleDownFrontStackPos + this.CurComp1.Height) : this.ZAxisNozzleDownStackPos), CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, false);
					this.controler.SetDoVacummQueue(this.CurComp1.AAxis, true);
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisStackDealy);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp1.Speed);
					this.SubState = 4;
					return;
				case 4:
					if (this.GoNext())
					{
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.SubState = 6;
					return;
				case 6:
					if (this.GoNext())
					{
						this.SubState = -1;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.FeedComponent2:
				switch (this.SubState)
				{
				case -1:
					if (this.GoNext())
					{
						this.RunState = MainForm.RunStateType.PickComponent2;
						return;
					}
					break;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp1.Speed);
					if (this.LeftAndBackStack.Feed[this.CurComp2.Stack] == 2)
					{
						this.Feed2MM = 2;
					}
					else
					{
						this.Feed2MM = 0;
					}
					if (this.CurComp2.Stack < 30)
					{
						this.CurComp2.PickAngle = 0.0;
						this.controler.FPTP(2, 4, this.CurComp1.AAxis, this.CurComp2.AAxis, this.LeftAndBackStack.X[this.CurComp2.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp2.Stack] - (double)this.LeftAndBackStack.Feed[this.CurComp2.Stack] - (double)this.Feed2MM - 0.5, this.LeftAndBackStack.Y[this.CurComp2.Stack] + this.LeftPrickOffsetY + this.LeftAndBackStack.PrickOffsetY[this.CurComp2.Stack], this.CurComp1.PickAngle, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.CurComp2.PickAngle = -90.0;
						this.controler.FPTP(2, 4, this.CurComp1.AAxis, this.CurComp2.AAxis, this.LeftAndBackStack.X[this.CurComp2.Stack] + this.BackPrickOffsetX + this.LeftAndBackStack.PrickOffsetY[this.CurComp2.Stack], this.LeftAndBackStack.Y[this.CurComp2.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp2.Stack] + (double)this.LeftAndBackStack.Feed[this.CurComp2.Stack] + (double)this.Feed2MM + 0.5, this.CurComp1.PickAngle, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.SubState = 1;
					return;
				case 1:
					if (this.GoNext())
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
				{
					double[] array2 = new double[6];
					array2[0] = 0.0;
					this.controler.WritePos_QUEUE(TcpControler.AxisType.FMark, array2);
					this.controler.SetDO_ON_QUEUE(16384u);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp2.Speed);
					this.controler.WaitPrickHome_QUEUE(true);
					this.controler.SetDelayMs_QUEUE(10);
					this.SubState = 3;
					return;
				}
				case 3:
					if (this.GoNext())
					{
						this.SubState = 4;
						return;
					}
					break;
				case 4:
					this.controler.SPTP(0, -2000.0, CmdType.CMD_PTP_QUEUE);
					if (this.LeftAndBackStack.Feed[this.CurComp2.Stack] == 2)
					{
						this.Feed2MM = 2;
					}
					else
					{
						this.Feed2MM = 0;
					}
					if (this.CurComp2.Stack < 30)
					{
						this.controler.DPTP(2, 4, this.LeftAndBackStack.X[this.CurComp2.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp2.Stack] - (double)this.Feed2MM, this.LeftAndBackStack.Y[this.CurComp2.Stack] + this.LeftPrickOffsetY + this.LeftAndBackStack.PrickOffsetY[this.CurComp2.Stack], CmdType.CMD_PTP_QUEUE_WAIT);
						this.controler.SetDelayMs_QUEUE(10);
						this.controler.SPTP(2, this.LeftAndBackStack.X[this.CurComp2.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp2.Stack] - (double)this.Feed2MM - this.FeedBack, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.controler.DPTP(2, 4, this.LeftAndBackStack.X[this.CurComp2.Stack] + this.BackPrickOffsetX + this.LeftAndBackStack.PrickOffsetY[this.CurComp2.Stack], this.LeftAndBackStack.Y[this.CurComp2.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp2.Stack] + (double)this.Feed2MM, CmdType.CMD_PTP_QUEUE_WAIT);
						this.controler.SetDelayMs_QUEUE(10);
						this.controler.SPTP(4, this.LeftAndBackStack.Y[this.CurComp2.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp2.Stack] + (double)this.Feed2MM + this.FeedBack, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					if (this.LeftAndBackStack.Feed[this.CurComp2.Stack] == 2)
					{
						if (this.StackMove[this.CurComp2.Stack] == 0)
						{
							this.StackMove[this.CurComp2.Stack] = 2;
						}
						else
						{
							this.StackMove[this.CurComp2.Stack] = 0;
						}
					}
					this.SubState = 5;
					return;
				case 5:
					if (this.GoNext())
					{
						this.SubState = 6;
						return;
					}
					break;
				case 6:
					this.controler.SetDO_OFF_QUEUE(16384u);
					if (this.CurComp1.State == MainForm.ComponentStateType.Suck)
					{
						this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp1.Speed);
					}
					else
					{
						this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, 100);
					}
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.SubState = -1;
					return;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.PickComponent2:
				switch (this.SubState)
				{
				case -1:
					this.CurComp2.State = MainForm.ComponentStateType.Suck;
					if (this.PressureCheckTotal && this.CurComp2.IsPressureCheck)
					{
						if (this.controler.IsBufRunDone())
						{
							if (this.CurComp1.State == MainForm.ComponentStateType.Suck && this.CurComp1.IsPressureCheck && !this.controler.GetDiPressure(this.CurComp1.AAxis))
							{
								this.CurComp1.State = MainForm.ComponentStateType.Discard;
								this.RunState = MainForm.RunStateType.DiscardComponent;
							}
							if (this.CurComp2.State == MainForm.ComponentStateType.Suck && this.CurComp2.IsPressureCheck && !this.controler.GetDiPressure(this.CurComp2.AAxis))
							{
								this.CurComp2.State = MainForm.ComponentStateType.Discard;
								this.RunState = MainForm.RunStateType.DiscardComponent;
							}
							if (this.RunState != MainForm.RunStateType.DiscardComponent)
							{
								if (this.mVision1.Camera1 != null && this.CurComp1.VisonType != 0 && this.CurComp1.State == MainForm.ComponentStateType.Suck)
								{
									this.RunState = MainForm.RunStateType.VisionCenter1;
									return;
								}
								if (this.mVision1.Camera1 != null && this.CurComp2.VisonType != 0 && this.CurComp2.State == MainForm.ComponentStateType.Suck)
								{
									this.RunState = MainForm.RunStateType.VisionCenter2;
									return;
								}
								if (this.CurComp1.State == MainForm.ComponentStateType.Suck && this.CurComp2.State == MainForm.ComponentStateType.Suck)
								{
									double num14 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp1.X - this.CurComp1.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp1.Y - this.CurComp1.OffsetY, 2.0));
									double num15 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
									double num16 = Math.Sqrt(Math.Pow(this.CurComp1.X + this.CurComp1.OffsetX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.CurComp1.Y + this.CurComp1.OffsetY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
									double num17 = (num14 + num16) / (double)this.CurComp2.Speed;
									double num18 = num15 / (double)this.CurComp2.Speed + num14 / (double)this.CurComp1.Speed;
									if (num17 < num18)
									{
										this.RunState = MainForm.RunStateType.PlaceComponent1;
										return;
									}
									this.RunState = MainForm.RunStateType.PlaceComponent2;
									return;
								}
								else
								{
									if (this.CurComp1.State == MainForm.ComponentStateType.Suck)
									{
										this.RunState = MainForm.RunStateType.PlaceComponent1;
										return;
									}
									this.RunState = MainForm.RunStateType.PlaceComponent2;
									return;
								}
							}
						}
					}
					else
					{
						if (this.mVision1.Camera1 != null && this.CurComp1.VisonType != 0 && this.CurComp1.State == MainForm.ComponentStateType.Suck)
						{
							this.RunState = MainForm.RunStateType.VisionCenter1;
							return;
						}
						if (this.mVision1.Camera1 != null && this.CurComp2.VisonType != 0 && this.CurComp2.State == MainForm.ComponentStateType.Suck)
						{
							this.RunState = MainForm.RunStateType.VisionCenter2;
							return;
						}
						if (this.CurComp1.State == MainForm.ComponentStateType.Suck && this.CurComp2.State == MainForm.ComponentStateType.Suck)
						{
							double num19 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp1.X - this.CurComp1.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp1.Y - this.CurComp1.OffsetY, 2.0));
							double num20 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
							double num21 = Math.Sqrt(Math.Pow(this.CurComp1.X + this.CurComp1.OffsetX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.CurComp1.Y + this.CurComp1.OffsetY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
							double num22 = (num19 + num21) / (double)this.CurComp2.Speed;
							double num23 = num20 / (double)this.CurComp2.Speed + num19 / (double)this.CurComp1.Speed;
							if (num22 < num23)
							{
								this.RunState = MainForm.RunStateType.PlaceComponent1;
								return;
							}
							this.RunState = MainForm.RunStateType.PlaceComponent2;
							return;
						}
						else
						{
							if (this.CurComp1.State == MainForm.ComponentStateType.Suck)
							{
								this.RunState = MainForm.RunStateType.PlaceComponent1;
								return;
							}
							this.RunState = MainForm.RunStateType.PlaceComponent2;
							return;
						}
					}
					break;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.DPTP(1, this.CurComp2.AAxis, 0.0, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp2.Speed);
					this.SubState = 1;
					return;
				case 1:
					if (!this.controler.IsBufRunDone())
					{
						this.ZAxisHomeErrorCounter = 0;
						return;
					}
					if (this.ChecPrickHome())
					{
						this.GetNozzleOffset(this.CurComp2.AAxis, this.CurComp2.PickAngle, out this.NozzleOffsetXAtAngle, out this.NozzleOffsetYAtAngle);
						if (!this.CurComp2.IsFrontStack)
						{
							this.controler.TPTP(2, 4, this.CurComp2.AAxis, this.AssistPosX = this.LeftAndBackStack.X[this.CurComp2.Stack] - this.NozzleOffsetXAtAngle + this.CurComp2.OffsetX, this.AssistPosY = this.LeftAndBackStack.Y[this.CurComp2.Stack] - this.NozzleOffsetYAtAngle + this.CurComp2.OffsetY, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
						}
						else
						{
							this.GetIcStackXY(this.CurComp2.Stack, out this.IcStackX, out this.IcStackY);
							this.controler.TPTP(2, 4, this.CurComp2.AAxis, this.AssistPosX = this.IcStackX + this.CurComp2.OffsetX, this.AssistPosY = this.IcStackY + this.CurComp2.OffsetY, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
						}
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					if (this.GoNext())
					{
						this.SubState = 3;
						return;
					}
					break;
				case 3:
					this.controler.SPTP(1, (double)this.CurComp2.ZAxisDir * (this.CurComp2.IsFrontStack ? (this.ZAxisNozzleDownFrontStackPos + this.CurComp2.Height) : this.ZAxisNozzleDownStackPos), CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, false);
					this.controler.SetDoVacummQueue(this.CurComp2.AAxis, true);
					this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisStackDealy);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp2.Speed);
					this.SubState = 4;
					return;
				case 4:
					if (this.GoNext())
					{
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.SubState = 6;
					return;
				case 6:
					if (this.GoNext())
					{
						this.SubState = -1;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.FeedComponent1And2:
				switch (this.SubState)
				{
				case -1:
					if (this.GoNext())
					{
						this.RunState = MainForm.RunStateType.PickComponent1And2;
						return;
					}
					break;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, 100);
					if (this.CurComp1.Stack < 30)
					{
						this.CurComp1.PickAngle = (this.CurComp2.PickAngle = 0.0);
						this.controler.FPTP(2, 4, this.CurComp1.AAxis, this.CurComp2.AAxis, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp1.Stack] - (double)(2 * this.LeftAndBackStack.Feed[this.CurComp1.Stack]) - 0.5, this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.LeftPrickOffsetY + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], this.CurComp1.PickAngle, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.CurComp1.PickAngle = (this.CurComp2.PickAngle = -90.0);
						this.controler.FPTP(2, 4, this.CurComp1.AAxis, this.CurComp2.AAxis, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.BackPrickOffsetX + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp1.Stack] + (double)(2 * this.LeftAndBackStack.Feed[this.CurComp1.Stack]) + 0.5, this.CurComp1.PickAngle, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.SubState = 1;
					return;
				case 1:
					if (this.GoNext())
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
				{
					double[] array3 = new double[6];
					array3[0] = 0.0;
					this.controler.WritePos_QUEUE(TcpControler.AxisType.FMark, array3);
					this.controler.SetDO_ON_QUEUE(16384u);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp2.Speed);
					this.controler.WaitPrickHome_QUEUE(true);
					this.controler.SetDelayMs_QUEUE(10);
					this.SubState = 3;
					return;
				}
				case 3:
					if (this.GoNext())
					{
						this.SubState = 4;
						return;
					}
					break;
				case 4:
					this.controler.SPTP(0, -2000.0, CmdType.CMD_PTP_QUEUE);
					if (this.CurComp1.Stack < 30)
					{
						this.controler.DPTP(2, 4, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp1.Stack], this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.LeftPrickOffsetY + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], CmdType.CMD_PTP_QUEUE_WAIT);
						this.controler.SetDelayMs_QUEUE(10);
						this.controler.SPTP(2, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.LeftPrickOffsetX + 26.0 - (double)this.StackMove[this.CurComp1.Stack] - this.FeedBack, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.controler.DPTP(2, 4, this.LeftAndBackStack.X[this.CurComp1.Stack] + this.BackPrickOffsetX + this.LeftAndBackStack.PrickOffsetY[this.CurComp1.Stack], this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp1.Stack], CmdType.CMD_PTP_QUEUE_WAIT);
						this.controler.SetDelayMs_QUEUE(10);
						this.controler.SPTP(4, this.LeftAndBackStack.Y[this.CurComp1.Stack] + this.BackPrickOffsetY - 26.0 + (double)this.StackMove[this.CurComp1.Stack] + this.FeedBack, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.SubState = 5;
					return;
				case 5:
					if (this.GoNext())
					{
						this.SubState = 6;
						return;
					}
					break;
				case 6:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, 100);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.SubState = -1;
					return;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.PickComponent1And2:
				switch (this.SubState)
				{
				case -1:
					this.CurComp1.State = MainForm.ComponentStateType.Suck;
					this.CurComp2.State = MainForm.ComponentStateType.Suck;
					if (this.PressureCheckTotal && (this.CurComp1.IsPressureCheck || this.CurComp2.IsPressureCheck))
					{
						if (this.controler.IsBufRunDone())
						{
							if (this.CurComp1.IsPressureCheck && !this.controler.GetDiPressure(this.CurComp1.AAxis))
							{
								this.CurComp1.State = MainForm.ComponentStateType.Discard;
								this.RunState = MainForm.RunStateType.DiscardComponent;
							}
							if (this.CurComp2.IsPressureCheck && !this.controler.GetDiPressure(this.CurComp2.AAxis))
							{
								this.CurComp2.State = MainForm.ComponentStateType.Discard;
								this.RunState = MainForm.RunStateType.DiscardComponent;
							}
							if (this.RunState != MainForm.RunStateType.DiscardComponent)
							{
								if (this.mVision1.Camera1 != null && this.CurComp1.VisonType != 0)
								{
									this.RunState = MainForm.RunStateType.VisionCenter1;
									return;
								}
								if (this.mVision1.Camera1 != null && this.CurComp2.VisonType != 0)
								{
									this.RunState = MainForm.RunStateType.VisionCenter2;
									return;
								}
								double num24 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp1.X - this.CurComp1.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp1.Y - this.CurComp1.OffsetY, 2.0));
								double num25 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
								double num26 = Math.Sqrt(Math.Pow(this.CurComp1.X + this.CurComp1.OffsetX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.CurComp1.Y + this.CurComp1.OffsetY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
								double num27 = (num24 + num26) / (double)this.CurComp2.Speed;
								double num28 = num25 / (double)this.CurComp2.Speed + num24 / (double)this.CurComp1.Speed;
								if (num27 < num28)
								{
									this.RunState = MainForm.RunStateType.PlaceComponent1;
									return;
								}
								this.RunState = MainForm.RunStateType.PlaceComponent2;
								return;
							}
						}
					}
					else
					{
						if (this.mVision1.Camera1 != null && this.CurComp1.VisonType != 0)
						{
							this.RunState = MainForm.RunStateType.VisionCenter1;
							return;
						}
						if (this.mVision1.Camera1 != null && this.CurComp2.VisonType != 0)
						{
							this.RunState = MainForm.RunStateType.VisionCenter2;
							return;
						}
						double num29 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp1.X - this.CurComp1.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp1.Y - this.CurComp1.OffsetY, 2.0));
						double num30 = Math.Sqrt(Math.Pow(this.AssistPosX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.AssistPosY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
						double num31 = Math.Sqrt(Math.Pow(this.CurComp1.X + this.CurComp1.OffsetX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.CurComp1.Y + this.CurComp1.OffsetY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
						double num32 = (num29 + num31) / (double)this.CurComp2.Speed;
						double num33 = num30 / (double)this.CurComp2.Speed + num29 / (double)this.CurComp1.Speed;
						if (num32 < num33)
						{
							this.RunState = MainForm.RunStateType.PlaceComponent1;
							return;
						}
						this.RunState = MainForm.RunStateType.PlaceComponent2;
						return;
					}
					break;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.DPTP(1, this.CurComp1.AAxis, 0.0, this.CurComp1.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, 100);
					this.SubState = 1;
					return;
				case 1:
					if (!this.controler.IsBufRunDone())
					{
						this.ZAxisHomeErrorCounter = 0;
						return;
					}
					if (this.ChecPrickHome())
					{
						this.GetNozzleOffset(this.CurComp1.AAxis, this.CurComp1.PickAngle, out this.NozzleOffsetXAtAngle, out this.NozzleOffsetYAtAngle);
						this.controler.FPTP(2, 4, this.CurComp1.AAxis, this.CurComp2.AAxis, this.LeftAndBackStack.X[this.CurComp1.Stack] - this.NozzleOffsetXAtAngle + this.CurComp1.OffsetX, this.LeftAndBackStack.Y[this.CurComp1.Stack] - this.NozzleOffsetYAtAngle + this.CurComp1.OffsetY, this.CurComp1.PickAngle, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					if (this.GoNext())
					{
						this.SubState = 3;
						return;
					}
					break;
				case 3:
					this.controler.SPTP(1, (double)this.CurComp1.ZAxisDir * this.ZAxisNozzleDownStackPos, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, false);
					this.controler.SetDoVacummQueue(this.CurComp1.AAxis, true);
					this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisStackDealy);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp1.Speed);
					this.SubState = 4;
					return;
				case 4:
					if (this.GoNext())
					{
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.SubState = 6;
					return;
				case 6:
					if (this.GoNext())
					{
						this.SubState = 7;
						return;
					}
					break;
				case 7:
					this.GetNozzleOffset(this.CurComp2.AAxis, this.CurComp2.PickAngle, out this.NozzleOffsetXAtAngle, out this.NozzleOffsetYAtAngle);
					if (this.CurComp2.Stack < 30)
					{
						this.controler.TPTP(2, 4, this.CurComp2.AAxis, this.AssistPosX = this.LeftAndBackStack.X[this.CurComp2.Stack] - this.NozzleOffsetXAtAngle + this.CurComp2.OffsetX + (double)this.LeftAndBackStack.Feed[this.CurComp2.Stack], this.AssistPosY = this.LeftAndBackStack.Y[this.CurComp2.Stack] - this.NozzleOffsetYAtAngle + this.CurComp2.OffsetY, this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.controler.TPTP(2, 4, this.CurComp2.AAxis, this.AssistPosX = this.LeftAndBackStack.X[this.CurComp2.Stack] - this.NozzleOffsetXAtAngle + this.CurComp2.OffsetX, this.AssistPosY = this.LeftAndBackStack.Y[this.CurComp2.Stack] - this.NozzleOffsetYAtAngle + this.CurComp2.OffsetY - (double)this.LeftAndBackStack.Feed[this.CurComp2.Stack], this.CurComp2.PickAngle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.SubState = 8;
					return;
				case 8:
					if (this.GoNext())
					{
						this.SubState = 9;
						return;
					}
					break;
				case 9:
					this.controler.SPTP(1, (double)this.CurComp2.ZAxisDir * this.ZAxisNozzleDownStackPos, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, false);
					this.controler.SetDoVacummQueue(this.CurComp2.AAxis, true);
					this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisStackDealy);
					this.controler.WriteSpeed_QUEUE(this.controler.SpeedValue, this.CurComp2.Speed);
					this.SubState = 10;
					return;
				case 10:
					if (this.GoNext())
					{
						this.SubState = 11;
						return;
					}
					break;
				case 11:
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.SubState = 12;
					return;
				case 12:
					if (this.GoNext())
					{
						this.SubState = -1;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.DiscardComponent:
				switch (this.SubState)
				{
				case -1:
					this.ErrorCounter++;
					if (this.ErrorCounter > 5)
					{
						this.ErrorCounter = 0;
						this.controler.SetDO_ON_IMM(2048u);
						new MyMessageBox(this.controler, this.LocStrings.GetString("pressureSensingError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
						this.SuspendRunning();
						return;
					}
					if (this.CurComp1.State == MainForm.ComponentStateType.Suck)
					{
						if (this.CurComp1.VisonType != 0 && !this.CurComp1.IsVisionDone)
						{
							this.RunState = MainForm.RunStateType.VisionCenter1;
							return;
						}
						this.RunState = MainForm.RunStateType.PlaceComponent1;
						return;
					}
					else if (this.CurComp2 != null && this.CurComp2.State == MainForm.ComponentStateType.Suck)
					{
						if (this.CurComp2.VisonType != 0 && !this.CurComp2.IsVisionDone)
						{
							this.RunState = MainForm.RunStateType.VisionCenter2;
							return;
						}
						this.RunState = MainForm.RunStateType.PlaceComponent2;
						return;
					}
					else if (this.CurComp1.State == MainForm.ComponentStateType.Discarded && this.CurComp2 != null && this.CurComp2.State == MainForm.ComponentStateType.Discarded)
					{
						if (this.CurComp1.IsFrontStack)
						{
							this.RunState = MainForm.RunStateType.PickComponent1;
							return;
						}
						if (this.CurComp1.Stack == this.CurComp2.Stack && !this.CurComp1.IsFrontStack && this.LeftAndBackStack.Feed[this.CurComp1.Stack] <= 4)
						{
							this.RunState = MainForm.RunStateType.FeedComponent1And2;
							return;
						}
						this.RunState = MainForm.RunStateType.FeedComponent1;
						return;
					}
					else if (this.CurComp1.State == MainForm.ComponentStateType.Discarded)
					{
						if (this.CurComp1.IsFrontStack)
						{
							this.RunState = MainForm.RunStateType.PickComponent1;
							return;
						}
						this.RunState = MainForm.RunStateType.FeedComponent1;
						return;
					}
					else
					{
						if (this.CurComp2 != null && this.CurComp2.IsFrontStack)
						{
							this.RunState = MainForm.RunStateType.PickComponent2;
							return;
						}
						this.RunState = MainForm.RunStateType.FeedComponent2;
						return;
					}
					break;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					if (this.CurComp1.State == MainForm.ComponentStateType.Discard)
					{
						this.SubState = 1;
						return;
					}
					if (this.CurComp2 != null && this.CurComp2.State == MainForm.ComponentStateType.Discard)
					{
						this.SubState = 6;
						return;
					}
					new MyMessageBox(this.controler, this.LocStrings.GetString("pressureSensingError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					this.SuspendRunning();
					return;
				case 1:
					this.controler.DPTP(2, 4, this.DiscardCompPosX + this.CurComp1.OffsetX, this.DiscardCompPosY + this.CurComp1.OffsetY, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SPTP(1, (double)this.CurComp1.ZAxisDir * this.ZAxisNozzleDownDiscardPos, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDoVacummQueue(this.CurComp1.AAxis, false);
					this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, true);
					this.controler.SetDelayMs_QUEUE(10);
					this.SubState = 2;
					return;
				case 2:
					if (this.GoNext())
					{
						this.SubState = 3;
						return;
					}
					break;
				case 3:
					this.controler.SetDoVacummQueue(this.CurComp1.AAxis, false);
					this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, false);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.CurComp1.State = MainForm.ComponentStateType.Discarded;
					this.SubState = 4;
					return;
				case 4:
					if (this.GoNext())
					{
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					if (this.controler.IsBufRunDone())
					{
						if (this.CurComp2 != null && (this.CurComp2.State == MainForm.ComponentStateType.Discard || (this.CurComp2.State == MainForm.ComponentStateType.Suck && this.CurComp2.IsPressureCheck && !this.controler.GetDiPressure(this.CurComp2.AAxis))))
						{
							this.CurComp2.State = MainForm.ComponentStateType.Discard;
							this.SubState = 6;
							return;
						}
						this.SubState = -1;
						return;
					}
					break;
				case 6:
					this.controler.DPTP(2, 4, this.DiscardCompPosX + this.CurComp2.OffsetX, this.DiscardCompPosY + this.CurComp2.OffsetY, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SPTP(1, (double)this.CurComp2.ZAxisDir * this.ZAxisNozzleDownDiscardPos, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, true);
					this.controler.SetDoVacummQueue(this.CurComp2.AAxis, false);
					this.controler.SetDelayMs_QUEUE(10);
					this.SubState = 7;
					return;
				case 7:
					if (this.GoNext())
					{
						this.SubState = 8;
						return;
					}
					break;
				case 8:
					this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, false);
					this.controler.SetDoVacummQueue(this.CurComp2.AAxis, false);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.CurComp2.State = MainForm.ComponentStateType.Discarded;
					this.SubState = 9;
					return;
				case 9:
					if (this.GoNext())
					{
						this.SubState = 10;
						return;
					}
					break;
				case 10:
					if (this.controler.IsBufRunDone())
					{
						if (this.CurComp1.State == MainForm.ComponentStateType.Discard || (this.CurComp1.State == MainForm.ComponentStateType.Suck && this.CurComp1.IsPressureCheck && !this.controler.GetDiPressure(this.CurComp1.AAxis)))
						{
							this.CurComp1.State = MainForm.ComponentStateType.Discard;
							this.SubState = 1;
							return;
						}
						this.SubState = -1;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.VisionCenter1:
				switch (this.SubState)
				{
				case -1:
				{
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.CurComp1.IsVisionDone = true;
					if (this.CurComp2 != null && this.CurComp2.State == MainForm.ComponentStateType.Suck && this.CurComp2.VisonType != 0 && !this.CurComp2.IsVisionDone)
					{
						this.RunState = MainForm.RunStateType.VisionCenter2;
						return;
					}
					if (this.CurComp2 == null || this.CurComp2.State != MainForm.ComponentStateType.Suck)
					{
						this.RunState = MainForm.RunStateType.PlaceComponent1;
						return;
					}
					double num34 = Math.Sqrt(Math.Pow(this.controler.PosMM[2] - this.CurComp1.X - this.CurComp1.OffsetX, 2.0) + Math.Pow(this.controler.PosMM[4] - this.CurComp1.Y - this.CurComp1.OffsetY, 2.0));
					double num35 = Math.Sqrt(Math.Pow(this.controler.PosMM[2] - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.controler.PosMM[4] - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
					double num36 = Math.Sqrt(Math.Pow(this.CurComp1.X + this.CurComp1.OffsetX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.CurComp1.Y + this.CurComp1.OffsetY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
					double num37 = (num34 + num36) / (double)this.CurComp2.Speed;
					double num38 = num35 / (double)this.CurComp2.Speed + num34 / (double)this.CurComp1.Speed;
					if (num37 < num38)
					{
						this.RunState = MainForm.RunStateType.PlaceComponent1;
						return;
					}
					this.RunState = MainForm.RunStateType.PlaceComponent2;
					return;
				}
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					if (this.CurComp2 != null)
					{
						this.controler.TPTP(1, this.CurComp1.AAxis, this.CurComp2.AAxis, 0.0, this.CurComp1.Angle, this.CurComp2.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.controler.DPTP(1, this.CurComp1.AAxis, 0.0, this.CurComp1.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.VisionRightCounter = 0;
					this.VisionCounter = 0;
					this.VisionOffsetTempX = 0.0;
					this.VisionOffsetTempY = 0.0;
					this.VisionOffsetTempAngle = 0.0;
					this.VisionCenterAngles = this.CurComp1.Angle;
					while (this.VisionCenterAngles >= 90.0)
					{
						this.VisionCenterAngles -= 90.0;
					}
					while (this.VisionCenterAngles < 0.0)
					{
						this.VisionCenterAngles += 90.0;
					}
					this.SubState = 1;
					return;
				case 1:
					if (this.GoNext())
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					this.controler.DPTP(2, 4, this.VisionX + this.CurComp1.OffsetX + this.VisionOffsetTempX, this.VisionY + this.CurComp1.OffsetY + this.VisionOffsetTempY, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.DPTP(1, this.CurComp1.AAxis, (double)this.CurComp1.ZAxisDir * this.VisionZ, this.CurComp1.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDelayMs_QUEUE(100);
					this.SubState = 3;
					return;
				case 3:
					if (this.GoNext())
					{
						this.SubState = 4;
						return;
					}
					break;
				case 4:
					if (this.controler.IsBufRunDone())
					{
						if (this.mVision1.Camera1 != null)
						{
							this.CheckVision = CheckVisionType.ComponentVision;
							this.mVision1.CompThreshold = (int)this.VisionValueComp;
							this.HeartBeatBackup = this.mVision1.HeartBeat;
							this.mVision1.CheckCompRun();
						}
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					if (this.HeartBeatBackup != this.mVision1.HeartBeat)
					{
						if (this.mVision1.CompSum <= 0)
						{
							this.controler.SetDO_ON_IMM(2048u);
							new MyMessageBox(this.controler, this.LocStrings.GetString("cameraError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
							this.controler.SPTP(4, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
							this.SuspendRunning();
							return;
						}
						this.VisionCounter++;
						double num39 = this.mVision1.CompOffsetW1 - this.Nozzle1VisionX;
						double num40 = this.mVision1.CompOffsetH1 - this.Nozzle1VisionY;
						double num41 = this.VisionCenterAngles - this.mVision1.CompOffsetA;
						if (num41 > 45.0)
						{
							num41 -= 90.0;
						}
						else if (num41 <= -45.0)
						{
							num41 += 90.0;
						}
						double num42 = num41 / 180.0 * 3.1415926535897931;
						double num43 = Math.Cos(num42) * num39 - Math.Sin(num42) * num40;
						double num44 = Math.Cos(num42) * num40 + Math.Sin(num42) * num39;
						if (this.CurComp1.VisonType == 1)
						{
							this.CurComp1.Angle += num41;
							this.CurComp1.X -= num43;
							this.CurComp1.Y -= num44;
							this.SubState = -1;
							return;
						}
						if (Math.Abs(num41) > 1.0)
						{
							this.CurComp1.Angle += num41;
						}
						else if (Math.Abs(num41) > 0.225)
						{
							if (num41 > 0.0)
							{
								this.CurComp1.Angle += 0.225;
							}
							else
							{
								this.CurComp1.Angle -= 0.225;
							}
						}
						this.CurComp1.X -= num43;
						this.CurComp1.Y -= num44;
						this.VisionOffsetTempX -= num43;
						this.VisionOffsetTempY -= num44;
						if (Math.Abs(num41) >= 0.25 + (double)this.VisionCounter / 200.0 || Math.Abs(num43) >= 0.2 || Math.Abs(num44) >= 0.2)
						{
							this.VisionRightCounter = 0;
							this.SubState = 2;
							return;
						}
						this.VisionRightCounter++;
						if (this.VisionRightCounter >= 3)
						{
							this.SubState = -1;
							return;
						}
						this.SubState = 2;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.VisionCenter2:
				switch (this.SubState)
				{
				case -1:
				{
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					this.CurComp2.IsVisionDone = true;
					if (this.CurComp1.VisonType != 0 && this.CurComp1.State == MainForm.ComponentStateType.Suck && !this.CurComp1.IsVisionDone)
					{
						this.RunState = MainForm.RunStateType.VisionCenter1;
						return;
					}
					if (this.CurComp1 == null || this.CurComp1.State != MainForm.ComponentStateType.Suck)
					{
						this.RunState = MainForm.RunStateType.PlaceComponent2;
						return;
					}
					double num45 = Math.Sqrt(Math.Pow(this.controler.PosMM[2] - this.CurComp1.X - this.CurComp1.OffsetX, 2.0) + Math.Pow(this.controler.PosMM[4] - this.CurComp1.Y - this.CurComp1.OffsetY, 2.0));
					double num46 = Math.Sqrt(Math.Pow(this.controler.PosMM[2] - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.controler.PosMM[4] - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
					double num47 = Math.Sqrt(Math.Pow(this.CurComp1.X + this.CurComp1.OffsetX - this.CurComp2.X - this.CurComp2.OffsetX, 2.0) + Math.Pow(this.CurComp1.Y + this.CurComp1.OffsetY - this.CurComp2.Y - this.CurComp2.OffsetY, 2.0));
					double num48 = (num45 + num47) / (double)this.CurComp2.Speed;
					double num49 = num46 / (double)this.CurComp2.Speed + num45 / (double)this.CurComp1.Speed;
					if (num48 < num49)
					{
						this.RunState = MainForm.RunStateType.PlaceComponent1;
						return;
					}
					this.RunState = MainForm.RunStateType.PlaceComponent2;
					return;
				}
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.DPTP(1, this.CurComp2.AAxis, 0.0, this.CurComp2.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					this.VisionRightCounter = 0;
					this.VisionOffsetTempX = 0.0;
					this.VisionOffsetTempY = 0.0;
					this.VisionOffsetTempAngle = 0.0;
					this.VisionCenterAngles = this.CurComp2.Angle;
					while (this.VisionCenterAngles >= 90.0)
					{
						this.VisionCenterAngles -= 90.0;
					}
					while (this.VisionCenterAngles < 0.0)
					{
						this.VisionCenterAngles += 90.0;
					}
					this.SubState = 1;
					return;
				case 1:
					if (this.GoNext())
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					this.controler.DPTP(2, 4, this.VisionX + this.CurComp2.OffsetX + this.VisionOffsetTempX, this.VisionY + this.CurComp2.OffsetY + this.VisionOffsetTempY, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.DPTP(1, this.CurComp2.AAxis, (double)this.CurComp2.ZAxisDir * this.VisionZ, this.CurComp2.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					this.controler.SetDelayMs_QUEUE(100);
					this.SubState = 3;
					return;
				case 3:
					if (this.GoNext())
					{
						this.SubState = 4;
						return;
					}
					break;
				case 4:
					if (this.controler.IsBufRunDone())
					{
						if (this.mVision1.Camera1 != null)
						{
							this.CheckVision = CheckVisionType.ComponentVision;
							this.mVision1.CompThreshold = (int)this.VisionValueComp;
							this.HeartBeatBackup = this.mVision1.HeartBeat;
							this.mVision1.CheckCompRun();
						}
						this.SubState = 5;
						return;
					}
					break;
				case 5:
					if (this.HeartBeatBackup != this.mVision1.HeartBeat)
					{
						if (this.mVision1.CompSum <= 0)
						{
							this.controler.SetDO_ON_IMM(2048u);
							new MyMessageBox(this.controler, this.LocStrings.GetString("cameraError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
							this.controler.SPTP(4, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
							this.SuspendRunning();
							return;
						}
						double num50 = this.mVision1.CompOffsetW1 - this.Nozzle2VisionX;
						double num51 = this.mVision1.CompOffsetH1 - this.Nozzle2VisionY;
						double num52 = this.VisionCenterAngles - this.mVision1.CompOffsetA;
						if (num52 > 45.0)
						{
							num52 -= 90.0;
						}
						else if (num52 <= -45.0)
						{
							num52 += 90.0;
						}
						double num53 = num52 / 180.0 * 3.1415926535897931;
						double num54 = Math.Cos(num53) * num50 - Math.Sin(num53) * num51;
						double num55 = Math.Cos(num53) * num51 + Math.Sin(num53) * num50;
						if (this.CurComp2.VisonType == 1)
						{
							this.CurComp2.Angle += num52;
							this.CurComp2.X -= num54;
							this.CurComp2.Y -= num55;
							this.SubState = -1;
							return;
						}
						if (Math.Abs(num52) > 1.0)
						{
							this.CurComp2.Angle += num52;
						}
						else if (Math.Abs(num52) > 0.225)
						{
							if (num52 > 0.0)
							{
								this.CurComp2.Angle += 0.225;
							}
							else
							{
								this.CurComp2.Angle -= 0.225;
							}
						}
						this.CurComp2.X -= num54;
						this.CurComp2.Y -= num55;
						this.VisionOffsetTempX -= num54;
						this.VisionOffsetTempY -= num55;
						if (Math.Abs(num52) >= 0.25 || Math.Abs(num54) >= 0.2 || Math.Abs(num55) >= 0.2)
						{
							this.VisionRightCounter = 0;
							this.SubState = 2;
							return;
						}
						this.VisionRightCounter++;
						if (this.VisionRightCounter >= 3)
						{
							this.SubState = -1;
							return;
						}
						this.SubState = 2;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.PlaceComponent1:
				switch (this.SubState)
				{
				case -1:
					this.ErrorCounter = 0;
					this.CurComp1.State = MainForm.ComponentStateType.Done;
					do
					{
						this.CurPuzzleNum1++;
					}
					while (this.CurPuzzleNum1 < this.Puzzle.IsMark1Used.Length && !this.Puzzle.IsMark1Used[this.CurPuzzleNum1]);
					if (this.CurPuzzleNum1 >= this.Puzzle.IsMark1Used.Length)
					{
						this.CurPuzzleNum1 = -1;
						for (int l = 0; l < this.Puzzle.IsMark1Used.Length; l++)
						{
							if (this.Puzzle.IsMark1Used[l])
							{
								this.CurPuzzleNum1 = l;
								break;
							}
						}
						this.gridViewFile.Rows[this.CurComp1.Num].Cells[10].Value = true;
						this.gridViewFile.Rows[this.CurComp1.Num].DefaultCellStyle.BackColor = Color.Gray;
						if (this.CurComp1.Num < 10)
						{
							this.gridViewFile.FirstDisplayedScrollingRowIndex = 0;
						}
						else
						{
							this.gridViewFile.FirstDisplayedScrollingRowIndex = this.CurComp1.Num - 10;
						}
						this.DoneNum++;
					}
					if (this.CurComp2 != null && this.CurComp2.State == MainForm.ComponentStateType.Suck)
					{
						this.RunState = MainForm.RunStateType.PlaceComponent2;
						return;
					}
					if (this.CurComp2 == null || this.CurComp2.State != MainForm.ComponentStateType.Discarded)
					{
						this.RunState = MainForm.RunStateType.FindNextComponent;
						return;
					}
					if (this.CurComp2.IsFrontStack)
					{
						this.RunState = MainForm.RunStateType.PickComponent2;
						return;
					}
					this.RunState = MainForm.RunStateType.FeedComponent2;
					return;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					if (this.CurComp1.VisonType == 0)
					{
						this.GetNozzleOffset(this.CurComp1.AAxis, this.CurComp1.Angle, out this.NozzleOffsetXAtAngle, out this.NozzleOffsetYAtAngle);
					}
					if (this.CurComp2 != null && this.CurComp2.State == MainForm.ComponentStateType.Suck)
					{
						this.controler.FPTP(2, 4, this.CurComp1.AAxis, this.CurComp2.AAxis, this.CurComp1.X - this.NozzleOffsetXAtAngle + this.CurComp1.OffsetX, this.CurComp1.Y - this.NozzleOffsetYAtAngle + this.CurComp1.OffsetY, this.CurComp1.Angle, this.CurComp2.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.controler.TPTP(2, 4, this.CurComp1.AAxis, this.CurComp1.X - this.NozzleOffsetXAtAngle + this.CurComp1.OffsetX, this.CurComp1.Y - this.NozzleOffsetYAtAngle + this.CurComp1.OffsetY, this.CurComp1.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.SubState = 1;
					return;
				case 1:
					if (this.GoNext())
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					if (!this.CurComp1.IsPressureCheck || !this.PressureCheckTotal || (this.controler.InPos(2, this.CurComp1.X - this.NozzleOffsetXAtAngle + this.CurComp1.OffsetX) && this.controler.InPos(4, this.CurComp1.Y - this.NozzleOffsetYAtAngle + this.CurComp1.OffsetY)))
					{
						if (this.CurComp1.IsPressureCheck && this.PressureCheckTotal && !this.controler.GetDiPressure(this.CurComp1.AAxis))
						{
							this.CurComp1.State = MainForm.ComponentStateType.Discard;
							this.RunState = MainForm.RunStateType.DiscardComponent;
							return;
						}
						this.controler.SPTP(1, (double)this.CurComp1.ZAxisDir * (this.ZAxisNozzleDownPCBPos + this.CurComp1.Height + this.Other.PCBThickness), CmdType.CMD_PTP_QUEUE_WAIT);
						int num56 = 0;
						switch (this.CurComp1.AAxis)
						{
						case 3:
							num56 = this.BlowingDealy1;
							break;
						case 5:
							num56 = this.BlowingDealy2;
							break;
						}
						switch (num56)
						{
						case 0:
							this.RunDelayCounter = 0;
							break;
						case 1:
							this.RunDelayCounter = 10;
							break;
						case 2:
							this.RunDelayCounter = 30;
							break;
						}
						if (this.RunDelayCounter <= 0)
						{
							this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, true);
							this.controler.SetDelayMs_QUEUE(5);
							this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, false);
							this.controler.SetDoVacummQueue(this.CurComp1.AAxis, false);
						}
						else
						{
							this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, true);
							this.controler.SetDoVacummQueue(this.CurComp1.AAxis, false);
							this.controler.SetDelayMs_QUEUE((ushort)this.RunDelayCounter);
							this.controler.SetDoBlowingQueue(this.CurComp1.AAxis, false);
						}
						switch (this.CurComp1.AAxis)
						{
						case 3:
							this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisPcbDealy1);
							break;
						case 5:
							this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisPcbDealy2);
							break;
						}
						this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
						this.SubState = -1;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.PlaceComponent2:
				switch (this.SubState)
				{
				case -1:
					this.ErrorCounter = 0;
					this.CurComp2.State = MainForm.ComponentStateType.Done;
					do
					{
						this.CurPuzzleNum2++;
					}
					while (this.CurPuzzleNum2 < this.Puzzle.IsMark1Used.Length && !this.Puzzle.IsMark1Used[this.CurPuzzleNum2]);
					if (this.CurPuzzleNum2 >= this.Puzzle.IsMark1Used.Length)
					{
						this.CurPuzzleNum2 = -1;
						for (int m = 0; m < this.Puzzle.IsMark1Used.Length; m++)
						{
							if (this.Puzzle.IsMark1Used[m])
							{
								this.CurPuzzleNum2 = m;
								break;
							}
						}
						this.gridViewFile.Rows[this.CurComp2.Num].Cells[10].Value = true;
						this.gridViewFile.Rows[this.CurComp2.Num].DefaultCellStyle.BackColor = Color.Gray;
						if (this.CurComp2.Num < 10)
						{
							this.gridViewFile.FirstDisplayedScrollingRowIndex = 0;
						}
						else
						{
							this.gridViewFile.FirstDisplayedScrollingRowIndex = this.CurComp2.Num - 10;
						}
						this.DoneNum++;
					}
					if (this.CurComp1 != null && this.CurComp1.State == MainForm.ComponentStateType.Suck)
					{
						this.RunState = MainForm.RunStateType.PlaceComponent1;
						return;
					}
					if (this.CurComp1 == null || this.CurComp1.State != MainForm.ComponentStateType.Discarded)
					{
						this.RunState = MainForm.RunStateType.FindNextComponent;
						return;
					}
					if (this.CurComp1.IsFrontStack)
					{
						this.RunState = MainForm.RunStateType.PickComponent1;
						return;
					}
					this.RunState = MainForm.RunStateType.FeedComponent1;
					return;
				case 0:
					this.controler.SetDO_OFF_QUEUE(16384u);
					this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
					if (this.CurComp2.VisonType == 0)
					{
						this.GetNozzleOffset(this.CurComp2.AAxis, this.CurComp2.Angle, out this.NozzleOffsetXAtAngle, out this.NozzleOffsetYAtAngle);
					}
					if (this.CurComp1 != null && this.CurComp1.State == MainForm.ComponentStateType.Suck)
					{
						this.controler.FPTP(2, 4, this.CurComp1.AAxis, this.CurComp2.AAxis, this.CurComp2.X - this.NozzleOffsetXAtAngle + this.CurComp2.OffsetX, this.CurComp2.Y - this.NozzleOffsetYAtAngle + this.CurComp2.OffsetY, this.CurComp1.Angle, this.CurComp2.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					else
					{
						this.controler.TPTP(2, 4, this.CurComp2.AAxis, this.CurComp2.X - this.NozzleOffsetXAtAngle + this.CurComp2.OffsetX, this.CurComp2.Y - this.NozzleOffsetYAtAngle + this.CurComp2.OffsetY, this.CurComp2.Angle, CmdType.CMD_PTP_QUEUE_WAIT);
					}
					this.SubState = 1;
					return;
				case 1:
					if (this.GoNext())
					{
						this.SubState = 2;
						return;
					}
					break;
				case 2:
					if (!this.CurComp2.IsPressureCheck || !this.PressureCheckTotal || (this.controler.InPos(2, this.CurComp2.X - this.NozzleOffsetXAtAngle + this.CurComp2.OffsetX) && this.controler.InPos(4, this.CurComp2.Y - this.NozzleOffsetYAtAngle + this.CurComp2.OffsetY)))
					{
						if (this.CurComp2.IsPressureCheck && this.PressureCheckTotal && !this.controler.GetDiPressure(this.CurComp2.AAxis))
						{
							this.CurComp2.State = MainForm.ComponentStateType.Discard;
							this.RunState = MainForm.RunStateType.DiscardComponent;
							return;
						}
						this.controler.SPTP(1, (double)this.CurComp2.ZAxisDir * (this.ZAxisNozzleDownPCBPos + this.CurComp2.Height + this.Other.PCBThickness), CmdType.CMD_PTP_QUEUE_WAIT);
						int num57 = 0;
						switch (this.CurComp2.AAxis)
						{
						case 3:
							num57 = this.BlowingDealy1;
							break;
						case 5:
							num57 = this.BlowingDealy2;
							break;
						}
						switch (num57)
						{
						case 0:
							this.RunDelayCounter = 0;
							break;
						case 1:
							this.RunDelayCounter = 10;
							break;
						case 2:
							this.RunDelayCounter = 30;
							break;
						}
						if (this.RunDelayCounter <= 0)
						{
							this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, true);
							this.controler.SetDelayMs_QUEUE(5);
							this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, false);
							this.controler.SetDoVacummQueue(this.CurComp2.AAxis, false);
						}
						else
						{
							this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, true);
							this.controler.SetDoVacummQueue(this.CurComp2.AAxis, false);
							this.controler.SetDelayMs_QUEUE((ushort)this.RunDelayCounter);
							this.controler.SetDoBlowingQueue(this.CurComp2.AAxis, false);
						}
						switch (this.CurComp2.AAxis)
						{
						case 3:
							this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisPcbDealy1);
							break;
						case 5:
							this.controler.SetDelayMs_QUEUE((ushort)this.ZAxisPcbDealy2);
							break;
						}
						this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_QUEUE_WAIT);
						this.SubState = -1;
						return;
					}
					break;
				default:
					return;
				}
				break;
			case MainForm.RunStateType.Done:
				switch (this.SubState)
				{
				case -1:
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					if (this.controler.SPTP(4, this.HomePosY - 5.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
					{
						for (int n = 0; n < this.gridViewFile.RowCount; n++)
						{
							this.gridViewFile.Rows[n].Cells[10].Value = false;
						}
						this.GoDone(true);
					}
					break;
				case 0:
					if (this.controler.ControlCmdBuf(ControlBufType.STOP_BUF) == CmdResultType.Done)
					{
						this.controler.SetDO_OFF_IMM(16384u);
						if (this.controler.SPTP(1, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
						{
							if (!this.ChecPrickHome())
							{
								return;
							}
							this.SubState = -1;
							return;
						}
					}
					break;
				default:
					return;
				}
				break;
			default:
				return;
			}
		}

		private void UpdatePosUI(int axis, Label label)
		{
			if (this.PosBack[axis] != this.controler.PosMM[axis])
			{
				label.Text = this.controler.PosMM[axis].ToString("F2");
				this.PosBack[axis] = this.controler.PosMM[axis];
			}
		}

		private void UpdateGreenLedUI(ref bool back, bool value, Label led)
		{
			if (back != value)
			{
				back = value;
				if (back)
				{
					led.Image = Resource.GreenLED;
					return;
				}
				led.Image = Resource.TransparentLED;
			}
		}

		private void timerUpdate_Tick(object sender, EventArgs e)
		{
			this.IsEnter = true;
			this.CountFor100MS++;
			if (this.CountFor100MS >= 10)
			{
				if (this.RunState != MainForm.RunStateType.None)
				{
					this.labelTime.Text = (this.RunTime / 60L).ToString("D2") + ":" + (this.RunTime % 60L).ToString("D2");
				}
				this.CountFor100MS = 0;
			}
			this.CountFor1S++;
			if (this.CountFor1S >= 100)
			{
				this.CountFor1S = 0;
			}
			if (this.IsConnect)
			{
				if (!this.controler.IsConnected || this.controler.Update() == CmdResultType.Error)
				{
					this.controler.CloseConnect();
					this.IsConnect = false;
					if (this.RunSignal)
					{
						this.SuspendSignal = true;
					}
				}
				this.RunStateUpdate();
				this.controler.SendQueueCmd();
			}
			else if (this.CountFor1S == 0 && this.controler.ConnectTo())
			{
				this.IsConnect = true;
				if (!this.controler.IsHomed)
				{
					this.StopRunning();
					this.SuspendSignal = false;
				}
			}
			if (this.controler.XLMTN != this.XLMTNBack)
			{
				this.XLMTNBack = this.controler.XLMTN;
				if (this.XLMTNBack && this.IsConnect)
				{
					this.controler.SetDO_ON_IMM(2048u);
					new MyMessageBox(this.controler, this.LocStrings.GetString("xNegLimit"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				}
			}
			if (this.controler.XLMTP != this.XLMTPBack)
			{
				this.XLMTPBack = this.controler.XLMTP;
				if (this.XLMTPBack && this.IsConnect && !this.IsHoming)
				{
					this.controler.SetDO_ON_IMM(2048u);
					new MyMessageBox(this.controler, this.LocStrings.GetString("xPosLimit"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				}
			}
			if (this.controler.YLMTN != this.YLMTNBack)
			{
				this.YLMTNBack = this.controler.YLMTN;
				if (this.YLMTNBack && this.IsConnect)
				{
					this.controler.SetDO_ON_IMM(2048u);
					new MyMessageBox(this.controler, this.LocStrings.GetString("yNegLimit"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				}
			}
			if (this.controler.YLMTP != this.YLMTPBack)
			{
				this.YLMTPBack = this.controler.YLMTP;
				if (this.YLMTPBack && this.IsConnect && !this.IsHoming)
				{
					this.controler.SetDO_ON_IMM(2048u);
					new MyMessageBox(this.controler, this.LocStrings.GetString("yPosLimit"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				}
			}
			if (this.controler.DI_Button_X_Up != this.buttonXUpBack)
			{
				this.buttonXUpBack = this.controler.DI_Button_X_Up;
				if (this.buttonXUpBack)
				{
					this.buttonXUp_MouseDown(null, null);
				}
				else
				{
					this.buttonXUp_MouseUp(null, null);
				}
			}
			else if (this.ManualSpeed == 0)
			{
				if (this.ButtonXUp && this.DealyXUpDone)
				{
					this.buttonXUp_MouseDown(null, null);
					this.DealyXUpDone = true;
				}
				else if (this.ButtonXUp && this.DealyXUp == this.CountFor1S)
				{
					this.DealyXUpDone = true;
				}
			}
			if (this.controler.DI_Button_X_Down != this.buttonXDownBack)
			{
				this.buttonXDownBack = this.controler.DI_Button_X_Down;
				if (this.buttonXDownBack)
				{
					this.buttonXDown_MouseDown(null, null);
				}
				else
				{
					this.buttonXDown_MouseUp(null, null);
				}
			}
			else if (this.ManualSpeed == 0)
			{
				if (this.ButtonXDown && this.DealyXDownDone)
				{
					this.buttonXDown_MouseDown(null, null);
					this.DealyXDownDone = true;
				}
				else if (this.ButtonXDown && this.DealyXDown == this.CountFor1S)
				{
					this.DealyXDownDone = true;
				}
			}
			if (this.controler.DI_Button_Y_Up != this.buttonYUpBack)
			{
				this.buttonYUpBack = this.controler.DI_Button_Y_Up;
				if (this.buttonYUpBack)
				{
					this.buttonYUp_MouseDown(null, null);
				}
				else
				{
					this.buttonYUp_MouseUp(null, null);
				}
			}
			else if (this.ManualSpeed == 0)
			{
				if (this.ButtonYUp && this.DealyYUpDone)
				{
					this.buttonYUp_MouseDown(null, null);
					this.DealyYUpDone = true;
				}
				else if (this.ButtonYUp && this.DealyYUp == this.CountFor1S)
				{
					this.DealyYUpDone = true;
				}
			}
			if (this.controler.DI_Button_Y_Down != this.buttonYDownBack)
			{
				this.buttonYDownBack = this.controler.DI_Button_Y_Down;
				if (this.buttonYDownBack)
				{
					this.buttonYDown_MouseDown(null, null);
				}
				else
				{
					this.buttonYDown_MouseUp(null, null);
				}
			}
			else if (this.ManualSpeed == 0)
			{
				if (this.ButtonYDown && this.DealyYDownDone)
				{
					this.buttonYDown_MouseDown(null, null);
					this.DealyYDownDone = true;
				}
				else if (this.ButtonYDown && this.DealyYDown == this.CountFor1S)
				{
					this.DealyYDownDone = true;
				}
			}
			if (this.controler.DI_Button_Speed_Grade != this.buttonSpeedGradeBack)
			{
				this.buttonSpeedGradeBack = this.controler.DI_Button_Speed_Grade;
				if (!this.buttonSpeedGradeBack)
				{
					if (this.ManualSpeed >= 1)
					{
						this.ManualSpeed = 0;
					}
					else
					{
						this.ManualSpeed = 2;
					}
				}
			}
			if (this.controler.DI_Button_Start != this.buttonStartBack)
			{
				this.buttonStartBack = this.controler.DI_Button_Start;
				if (!this.buttonStartBack)
				{
					if (this.ResetNeedForm != null)
					{
						this.ResetNeedForm.DialogResult = DialogResult.Yes;
						this.ResetNeedForm.Close();
					}
					else if (this.StopRunForm != null)
					{
						this.StopRunForm.DialogResult = DialogResult.Yes;
						this.StopRunForm.Close();
					}
					else
					{
						this.buttonRun_Click(null, null);
					}
				}
			}
			if (this.controler.DI_Button_Step != this.buttonStepBack)
			{
				this.buttonStepBack = this.controler.DI_Button_Step;
				if (!this.buttonStepBack)
				{
					if (this.ResetNeedForm != null)
					{
						this.ResetNeedForm.DialogResult = DialogResult.Yes;
						this.ResetNeedForm.Close();
					}
					else
					{
						this.buttonStep_Click(null, null);
					}
				}
			}
			if (this.controler.DI_Button_Pause != this.buttonPauseBack)
			{
				this.buttonPauseBack = this.controler.DI_Button_Pause;
				if (!this.buttonPauseBack)
				{
					if (this.ResetNeedForm != null)
					{
						this.ResetNeedForm.DialogResult = DialogResult.No;
						this.ResetNeedForm.Close();
					}
					else if (this.StopRunForm != null)
					{
						this.StopRunForm.DialogResult = DialogResult.No;
						this.StopRunForm.Close();
					}
					else
					{
						this.buttonSuspend_Click(null, null);
					}
				}
			}
			switch (this.CountFor100MS)
			{
			case 0:
				this.UpdatePosUI(3, this.labelA1Pos);
				this.UpdatePosUI(5, this.labelA2Pos);
				this.UpdatePosUI(2, this.labelXPos);
				this.UpdatePosUI(4, this.labelYPos);
				if (this.PosBack[1] != this.controler.PosMM[1])
				{
					double num = this.controler.PosMM[1];
					if (num < 0.0)
					{
						this.labelZ1Pos.Text = num.ToString("F2");
						this.labelZ2Pos.Text = 0.ToString("F2");
					}
					else
					{
						this.labelZ1Pos.Text = 0.ToString("F2");
						this.labelZ2Pos.Text = (-num).ToString("F2");
					}
					this.PosBack[1] = this.controler.PosMM[1];
				}
				if (this.IsConnect && this.controler.IsHomed)
				{
					if (this.labelX.ForeColor != Color.Black)
					{
						this.labelX.ForeColor = Color.Black;
						this.labelY.ForeColor = Color.Black;
						this.labelZ1.ForeColor = Color.Black;
						this.labelZ2.ForeColor = Color.Black;
						this.labelA1.ForeColor = Color.Black;
						this.labelA2.ForeColor = Color.Black;
						this.labelXPos.ForeColor = Color.Black;
						this.labelYPos.ForeColor = Color.Black;
						this.labelZ1Pos.ForeColor = Color.Black;
						this.labelZ2Pos.ForeColor = Color.Black;
						this.labelA1Pos.ForeColor = Color.Black;
						this.labelA2Pos.ForeColor = Color.Black;
					}
				}
				else if (this.labelX.ForeColor != Color.Red)
				{
					this.labelX.ForeColor = Color.Red;
					this.labelY.ForeColor = Color.Red;
					this.labelZ1.ForeColor = Color.Red;
					this.labelZ2.ForeColor = Color.Red;
					this.labelA1.ForeColor = Color.Red;
					this.labelA2.ForeColor = Color.Red;
					this.labelXPos.ForeColor = Color.Red;
					this.labelYPos.ForeColor = Color.Red;
					this.labelZ1Pos.ForeColor = Color.Red;
					this.labelZ2Pos.ForeColor = Color.Red;
					this.labelA1Pos.ForeColor = Color.Red;
					this.labelA2Pos.ForeColor = Color.Red;
				}
				break;
			case 1:
				this.UpdateGreenLedUI(ref this.labelVacuumBack, this.controler.DO_Vacuum1, this.labelVacuum1);
				this.UpdateGreenLedUI(ref this.labelVacuum2Back, this.controler.DO_Vacuum2, this.labelVacuum2);
				this.UpdateGreenLedUI(ref this.labelBlowingBack, this.controler.DO_Blowing1, this.labelBlowing1);
				this.UpdateGreenLedUI(ref this.labelBlowing2Back, this.controler.DO_Blowing2, this.labelBlowing2);
				break;
			case 2:
				this.UpdateGreenLedUI(ref this.labelPressureBack, this.controler.DI_Pressure, this.labelPressure1);
				this.UpdateGreenLedUI(ref this.labelPressure2Back, this.controler.DI_Pressure2, this.labelPressure2);
				this.UpdateGreenLedUI(ref this.labelPrickHomeBack, this.controler.DI_PrickHome, this.labelPrickHome);
				this.UpdateGreenLedUI(ref this.labelStripBack, this.controler.Status[0] == TcpControler.AxisStateType.AX_PTP_MOTION, this.labelStrip);
				break;
			case 3:
				if (this.CheckVision != CheckVisionType.PCBVision)
				{
					this.labelWidth.Text = this.mVision1.CompWidth1.ToString("F2");
					this.labelHeight.Text = this.mVision1.CompHeight1.ToString("F2");
					this.labelOffetW.Text = this.mVision1.CompOffsetW1.ToString("F2");
					this.labelOffsetH.Text = this.mVision1.CompOffsetH1.ToString("F2");
					this.labelAngle.Text = this.mVision1.CompOffsetA.ToString("F2");
				}
				else
				{
					this.labelWidth.Text = this.mVision1.CompWidth2.ToString("F2");
					this.labelHeight.Text = this.mVision1.CompHeight2.ToString("F2");
					this.labelOffetW.Text = this.mVision1.CompOffsetW2.ToString("F2");
					this.labelOffsetH.Text = this.mVision1.CompOffsetH2.ToString("F2");
					this.labelAngle.Text = this.mVision1.CompOffsetA.ToString("F2");
				}
				break;
			case 4:
				if (this.isPumpOn != this.controler.DO_Pump)
				{
					if (this.controler.DO_Pump)
					{
						this.buttonPump.BackgroundImage = Resource.ButtonDown;
					}
					else
					{
						this.buttonPump.BackgroundImage = Resource.ButtonUp;
					}
					this.isPumpOn = this.controler.DO_Pump;
				}
				if (this.isVacuum1On != this.controler.DO_Vacuum1)
				{
					if (this.controler.DO_Vacuum1)
					{
						this.buttonVacuum1.BackgroundImage = Resource.ButtonDown;
					}
					else
					{
						this.buttonVacuum1.BackgroundImage = Resource.ButtonUp;
					}
					this.isVacuum1On = this.controler.DO_Vacuum1;
				}
				if (this.isVacuum2On != this.controler.DO_Vacuum2)
				{
					if (this.controler.DO_Vacuum2)
					{
						this.buttonVacuum2.BackgroundImage = Resource.ButtonDown;
					}
					else
					{
						this.buttonVacuum2.BackgroundImage = Resource.ButtonUp;
					}
					this.isVacuum2On = this.controler.DO_Vacuum2;
				}
				if (this.isPrickOn != this.controler.DO_Prick)
				{
					if (this.controler.DO_Prick)
					{
						this.buttonPrick.BackgroundImage = Resource.ButtonDown;
					}
					else
					{
						this.buttonPrick.BackgroundImage = Resource.ButtonUp;
					}
					this.isPrickOn = this.controler.DO_Prick;
				}
				if (this.isBlowing1On != this.controler.DO_Blowing1)
				{
					if (this.controler.DO_Blowing1)
					{
						this.buttonBlowing1.BackgroundImage = Resource.ButtonDown;
					}
					else
					{
						this.buttonBlowing1.BackgroundImage = Resource.ButtonUp;
					}
					this.isBlowing1On = this.controler.DO_Blowing1;
				}
				if (this.isBlowing2On != this.controler.DO_Blowing2)
				{
					if (this.controler.DO_Blowing2)
					{
						this.buttonBlowing2.BackgroundImage = Resource.ButtonDown;
					}
					else
					{
						this.buttonBlowing2.BackgroundImage = Resource.ButtonUp;
					}
					this.isBlowing2On = this.controler.DO_Blowing2;
				}
				break;
			}
			if ((this.CountFor1S == 0 || this.CountFor1S == 50) && ((this.RunState == MainForm.RunStateType.None && this.MainVisionTest) || this.SuspendSignal))
			{
				switch (this.CheckVision)
				{
				case CheckVisionType.NozzleVision:
					this.mVision1.CompThreshold = (int)this.VisionValueNozzle;
					this.mVision1.CheckNozzleRun();
					break;
				case CheckVisionType.ComponentVision:
					this.mVision1.CompThreshold = (int)this.VisionValueComp;
					this.mVision1.CheckCompRun();
					break;
				case CheckVisionType.PCBVision:
					if (this.Mark.Mode == MarkModeType.Auto)
					{
						switch (this.Mark.Shape)
						{
						case MarkShapeType.Round:
							this.mVision1.ModeShape = 0;
							break;
						case MarkShapeType.Rectangle:
							this.mVision1.ModeShape = 1;
							break;
						case MarkShapeType.Irregular:
							this.mVision1.ModeShape = 2;
							break;
						}
					}
					else
					{
						this.mVision1.ModeShape = -1;
					}
					this.mVision1.CheckMarkRun();
					break;
				}
			}
			this.IsEnter = false;
		}

		private void trackBarSpeed_ValueChanged(object sender, EventArgs e)
		{
			this.controler.SpeedValue = this.trackBarSpeed.Value;
			this.trackBarSpeed.Value = this.controler.SpeedValue;
			this.labelSpeed.Text = this.LocStrings.GetString("labelSpeed.Text") + (this.controler.SpeedValue + 1).ToString() + "0%";
		}

		private void buttonPTPX_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				if (!this.controler.IsHomed)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.controler.DI_PrickHome || this.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double goal;
				if (double.TryParse(this.textBoxX.Text, out goal))
				{
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					this.controler.SPTP(2, goal, CmdType.CMD_PTP_IMM);
					return;
				}
				new MyMessageBox(this.controler, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
			}
		}

		private void buttonPTPY_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				if (!this.controler.IsHomed)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.controler.DI_PrickHome || this.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double goal;
				if (double.TryParse(this.textBoxY.Text, out goal))
				{
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					this.controler.SPTP(4, goal, CmdType.CMD_PTP_IMM);
					return;
				}
				new MyMessageBox(this.controler, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
			}
		}

		private void buttonPTPZ1_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				double num;
				if (double.TryParse(this.textBoxZ1.Text, out num))
				{
					if (num > 0.0)
					{
						num = 0.0;
					}
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					this.controler.SPTP(1, num, CmdType.CMD_PTP_IMM);
					return;
				}
				new MyMessageBox(this.controler, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
			}
		}

		private void buttonPTPZ2_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				double num;
				if (double.TryParse(this.textBoxZ1.Text, out num))
				{
					if (num > 0.0)
					{
						num = 0.0;
					}
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					this.controler.SPTP(1, -num, CmdType.CMD_PTP_IMM);
					return;
				}
				new MyMessageBox(this.controler, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
			}
		}

		private void buttonPTPA1_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				double goal;
				if (double.TryParse(this.textBoxA1.Text, out goal))
				{
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					this.controler.SPTP(3, goal, CmdType.CMD_PTP_IMM);
					return;
				}
				new MyMessageBox(this.controler, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
			}
		}

		private void buttonPTPA2_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				double goal;
				if (double.TryParse(this.textBoxA2.Text, out goal))
				{
					this.controler.WriteSpeed_IMM(this.controler.SpeedValue, 100);
					this.controler.SPTP(5, goal, CmdType.CMD_PTP_IMM);
					return;
				}
				new MyMessageBox(this.controler, this.LocStrings.GetString("formatError"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
			}
		}

		private void buttonStopPTPX_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				this.controler.SHalt(2);
			}
		}

		private void buttonStopPTPY_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				this.controler.SHalt(4);
			}
		}

		private void buttonStopPTPZ1_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				this.controler.SHalt(1);
			}
		}

		private void buttonStopPTPZ2_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				this.controler.SHalt(1);
			}
		}

		private void buttonStopPTPA1_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				this.controler.SHalt(3);
			}
		}

		private void buttonStopPTPA2_Click(object sender, EventArgs e)
		{
			if (this.IsConnect)
			{
				this.controler.SHalt(5);
			}
		}

		private void mVision1_DoubleClick(object sender, EventArgs e)
		{
			switch (this.CheckVision)
			{
			case CheckVisionType.NozzleVision:
				this.CheckVision = CheckVisionType.PCBVision;
				return;
			case CheckVisionType.ComponentVision:
				this.CheckVision = CheckVisionType.NozzleVision;
				return;
			case CheckVisionType.PCBVision:
				this.CheckVision = CheckVisionType.ComponentVision;
				return;
			default:
				return;
			}
		}
	}
}
