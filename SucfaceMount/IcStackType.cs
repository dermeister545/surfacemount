using System;

namespace SucfaceMount
{
	public struct IcStackType
	{
		public double[] StartX;

		public double[] StartY;

		public double[] EndX;

		public double[] EndY;

		public int[] ArrayX;

		public int[] ArrayY;
	}
}
