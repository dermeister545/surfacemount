using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SucfaceMount
{
	[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"), DebuggerNonUserCode, CompilerGenerated]
	internal class MyStrings
	{
		private static ResourceManager resourceMan;

		private static CultureInfo resourceCulture;

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(MyStrings.resourceMan, null))
				{
					ResourceManager resourceManager = new ResourceManager("SucfaceMount.MyStrings", typeof(MyStrings).Assembly);
					MyStrings.resourceMan = resourceManager;
				}
				return MyStrings.resourceMan;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Advanced)]
		internal static CultureInfo Culture
		{
			get
			{
				return MyStrings.resourceCulture;
			}
			set
			{
				MyStrings.resourceCulture = value;
			}
		}

		internal static string AboutUs
		{
			get
			{
				return MyStrings.ResourceManager.GetString("AboutUs", MyStrings.resourceCulture);
			}
		}

		internal static string buttonRunStop
		{
			get
			{
				return MyStrings.ResourceManager.GetString("buttonRunStop", MyStrings.resourceCulture);
			}
		}

		internal static string buttonRunText
		{
			get
			{
				return MyStrings.ResourceManager.GetString("buttonRunText", MyStrings.resourceCulture);
			}
		}

		internal static string buttonStopText
		{
			get
			{
				return MyStrings.ResourceManager.GetString("buttonStopText", MyStrings.resourceCulture);
			}
		}

		internal static string cameraError
		{
			get
			{
				return MyStrings.ResourceManager.GetString("cameraError", MyStrings.resourceCulture);
			}
		}

		internal static string compPosOverflow
		{
			get
			{
				return MyStrings.ResourceManager.GetString("compPosOverflow", MyStrings.resourceCulture);
			}
		}

		internal static string csvErrorDesignator
		{
			get
			{
				return MyStrings.ResourceManager.GetString("csvErrorDesignator", MyStrings.resourceCulture);
			}
		}

		internal static string csvErrorFormat
		{
			get
			{
				return MyStrings.ResourceManager.GetString("csvErrorFormat", MyStrings.resourceCulture);
			}
		}

		internal static string csvErrorMidX
		{
			get
			{
				return MyStrings.ResourceManager.GetString("csvErrorMidX", MyStrings.resourceCulture);
			}
		}

		internal static string csvErrorMidY
		{
			get
			{
				return MyStrings.ResourceManager.GetString("csvErrorMidY", MyStrings.resourceCulture);
			}
		}

		internal static string csvErrorRotation
		{
			get
			{
				return MyStrings.ResourceManager.GetString("csvErrorRotation", MyStrings.resourceCulture);
			}
		}

		internal static string displaygridViewFileVisionsAccurate
		{
			get
			{
				return MyStrings.ResourceManager.GetString("displaygridViewFileVisionsAccurate", MyStrings.resourceCulture);
			}
		}

		internal static string displaygridViewFileVisionsNone
		{
			get
			{
				return MyStrings.ResourceManager.GetString("displaygridViewFileVisionsNone", MyStrings.resourceCulture);
			}
		}

		internal static string displaygridViewFileVisionsQuick
		{
			get
			{
				return MyStrings.ResourceManager.GetString("displaygridViewFileVisionsQuick", MyStrings.resourceCulture);
			}
		}

		internal static string Done
		{
			get
			{
				return MyStrings.ResourceManager.GetString("Done", MyStrings.resourceCulture);
			}
		}

		internal static string formatError
		{
			get
			{
				return MyStrings.ResourceManager.GetString("formatError", MyStrings.resourceCulture);
			}
		}

		internal static string GoHoming
		{
			get
			{
				return MyStrings.ResourceManager.GetString("GoHoming", MyStrings.resourceCulture);
			}
		}

		internal static string labelAlreadyRunning
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelAlreadyRunning", MyStrings.resourceCulture);
			}
		}

		internal static string labelBack
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelBack", MyStrings.resourceCulture);
			}
		}

		internal static string labelCancel
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelCancel", MyStrings.resourceCulture);
			}
		}

		internal static string labelCompNew
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelCompNew", MyStrings.resourceCulture);
			}
		}

		internal static string labelConnectionFailed
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelConnectionFailed", MyStrings.resourceCulture);
			}
		}

		internal static string labelContinue
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelContinue", MyStrings.resourceCulture);
			}
		}

		internal static string labelFornt
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelFornt", MyStrings.resourceCulture);
			}
		}

		internal static string labelGoHomeFirst
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelGoHomeFirst", MyStrings.resourceCulture);
			}
		}

		internal static string labelIcStack
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelIcStack", MyStrings.resourceCulture);
			}
		}

		internal static string labelIpAddInvalid
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelIpAddInvalid", MyStrings.resourceCulture);
			}
		}

		internal static string labelLeft
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelLeft", MyStrings.resourceCulture);
			}
		}

		internal static string labelNotConnectCamera
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelNotConnectCamera", MyStrings.resourceCulture);
			}
		}

		internal static string labelPCB
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelPCB", MyStrings.resourceCulture);
			}
		}

		internal static string labelPortInvalid
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelPortInvalid", MyStrings.resourceCulture);
			}
		}

		internal static string labelSameMark
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelSameMark", MyStrings.resourceCulture);
			}
		}

		internal static string labelSpeedHigh
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelSpeedHigh", MyStrings.resourceCulture);
			}
		}

		internal static string labelSpeedLow
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelSpeedLow", MyStrings.resourceCulture);
			}
		}

		internal static string labelStartAgain
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelStartAgain", MyStrings.resourceCulture);
			}
		}

		internal static string labelStartAgainContinue
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelStartAgainContinue", MyStrings.resourceCulture);
			}
		}

		internal static string labelStartAgainYes
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelStartAgainYes", MyStrings.resourceCulture);
			}
		}

		internal static string labelToStop
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelToStop", MyStrings.resourceCulture);
			}
		}

		internal static string labelWarning
		{
			get
			{
				return MyStrings.ResourceManager.GetString("labelWarning", MyStrings.resourceCulture);
			}
		}

		internal static string lableSelectLines1
		{
			get
			{
				return MyStrings.ResourceManager.GetString("lableSelectLines1", MyStrings.resourceCulture);
			}
		}

		internal static string lableSelectLines2
		{
			get
			{
				return MyStrings.ResourceManager.GetString("lableSelectLines2", MyStrings.resourceCulture);
			}
		}

		internal static string markAndReadDeviationIsTooLarge
		{
			get
			{
				return MyStrings.ResourceManager.GetString("markAndReadDeviationIsTooLarge", MyStrings.resourceCulture);
			}
		}

		internal static string markIdentiFailure
		{
			get
			{
				return MyStrings.ResourceManager.GetString("markIdentiFailure", MyStrings.resourceCulture);
			}
		}

		internal static string markIsNotSet
		{
			get
			{
				return MyStrings.ResourceManager.GetString("markIsNotSet", MyStrings.resourceCulture);
			}
		}

		internal static string mustAtHomePostion
		{
			get
			{
				return MyStrings.ResourceManager.GetString("mustAtHomePostion", MyStrings.resourceCulture);
			}
		}

		internal static string mustSelectSMDOrTotal
		{
			get
			{
				return MyStrings.ResourceManager.GetString("mustSelectSMDOrTotal", MyStrings.resourceCulture);
			}
		}

		internal static string mustSelectTopOrBottom
		{
			get
			{
				return MyStrings.ResourceManager.GetString("mustSelectTopOrBottom", MyStrings.resourceCulture);
			}
		}

		internal static string noComponent
		{
			get
			{
				return MyStrings.ResourceManager.GetString("noComponent", MyStrings.resourceCulture);
			}
		}

		internal static string placeDone
		{
			get
			{
				return MyStrings.ResourceManager.GetString("placeDone", MyStrings.resourceCulture);
			}
		}

		internal static string pressureSensingError
		{
			get
			{
				return MyStrings.ResourceManager.GetString("pressureSensingError", MyStrings.resourceCulture);
			}
		}

		internal static string prickDowningError
		{
			get
			{
				return MyStrings.ResourceManager.GetString("prickDowningError", MyStrings.resourceCulture);
			}
		}

		internal static string prickHomingError
		{
			get
			{
				return MyStrings.ResourceManager.GetString("prickHomingError", MyStrings.resourceCulture);
			}
		}

		internal static string sendCommandFailed
		{
			get
			{
				return MyStrings.ResourceManager.GetString("sendCommandFailed", MyStrings.resourceCulture);
			}
		}

		internal static string SysParam
		{
			get
			{
				return MyStrings.ResourceManager.GetString("SysParam", MyStrings.resourceCulture);
			}
		}

		internal static string xNegLimit
		{
			get
			{
				return MyStrings.ResourceManager.GetString("xNegLimit", MyStrings.resourceCulture);
			}
		}

		internal static string xPosLimit
		{
			get
			{
				return MyStrings.ResourceManager.GetString("xPosLimit", MyStrings.resourceCulture);
			}
		}

		internal static string yNegLimit
		{
			get
			{
				return MyStrings.ResourceManager.GetString("yNegLimit", MyStrings.resourceCulture);
			}
		}

		internal static string yPosLimit
		{
			get
			{
				return MyStrings.ResourceManager.GetString("yPosLimit", MyStrings.resourceCulture);
			}
		}

		internal MyStrings()
		{
		}
	}
}
