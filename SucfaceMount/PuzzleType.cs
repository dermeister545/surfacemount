using System;

namespace SucfaceMount
{
	public struct PuzzleType
	{
		public bool[] IsMark1Used;

		public bool[] IsMark2Used;

		public double[] Mark1X;

		public double[] Mark1Y;

		public double[] Mark1MeasureX;

		public double[] Mark1MeasureY;

		public double[] Mark2X;

		public double[] Mark2Y;

		public double[] Mark2MeasureX;

		public double[] Mark2MeasureY;

		public double[] Angle1;

		public double[] Angle2;

		public double[] Scale;
	}
}
