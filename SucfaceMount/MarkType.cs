using System;

namespace SucfaceMount
{
	public struct MarkType
	{
		public MarkModeType Mode;

		public MarkShapeType Shape;

		public bool IsAutoPause;

		public int Exposure;
	}
}
