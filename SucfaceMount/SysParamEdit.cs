using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class SysParamEdit : Form
	{
		private enum VisionTestStateType
		{
			None,
			Stage1,
			Stage2,
			Stage3,
			Stage4,
			Stage5,
			Stage6,
			Stage7,
			OK,
			Done
		}

		private const int VisionTestNum = 8;

		private const int FAxis = 0;

		private const int ZAxis = 1;

		private const int XAxis = 2;

		private const int A1Axis = 3;

		private const int YAxis = 4;

		private const int A2Axis = 5;

		private IContainer components;

		private TextBox textBoxMac;

		private Label label9;

		private TextBox textBoxHomeX;

		private Label label1;

		private TextBox textBoxHomeY;

		private TextBox textBoxVisionY;

		private TextBox textBoxVisionX;

		private Label label2;

		private TextBox textBoxDiscardCompPosY;

		private TextBox textBoxDiscardCompPosX;

		private Label label5;

		private Button buttonCancel;

		private Button buttonSave;

		private TextBox textBoxVisionZ;

		private Button buttonReadFile;

		private Button buttonSaveFile;

		private SaveFileDialog saveFileDialog;

		private OpenFileDialog openFileDialog;

		private Label labelVisionValue;

		private TextBox textBoxVisionValueLow;

		private TextBox textBoxVisionValueHigh;

		private TextBox textBoxNozzleVisionZ;

		private Label label6;

		private Button buttonVisionTest;

		private Timer timer;

		private ComboBox comboBoxFeedCamera1;

		private ComboBox comboBoxFeedCamera2;

		private Label label3;

		private Label label4;

		private Label label7;

		private Label label8;

		private TextBox textBoxFeedBack;

		private TextBox textBoxMarkVisionOffsetY;

		private TextBox textBoxMarkVisionOffsetX;

		private Label label10;

		private MainForm owner;

		private TcpControler controler;

		private SysParamEdit.VisionTestStateType VisionTestState;

		private int RunVisionDelayCounter;

		private double[] VisionCenterX = new double[8];

		private double[] VisionCenterY = new double[8];

		private int VisionTestCounter;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			this.textBoxMac = new TextBox();
			this.label9 = new Label();
			this.textBoxHomeX = new TextBox();
			this.label1 = new Label();
			this.textBoxHomeY = new TextBox();
			this.textBoxVisionY = new TextBox();
			this.textBoxVisionX = new TextBox();
			this.label2 = new Label();
			this.textBoxDiscardCompPosY = new TextBox();
			this.textBoxDiscardCompPosX = new TextBox();
			this.label5 = new Label();
			this.textBoxVisionZ = new TextBox();
			this.saveFileDialog = new SaveFileDialog();
			this.openFileDialog = new OpenFileDialog();
			this.labelVisionValue = new Label();
			this.textBoxVisionValueLow = new TextBox();
			this.textBoxVisionValueHigh = new TextBox();
			this.textBoxNozzleVisionZ = new TextBox();
			this.label6 = new Label();
			this.timer = new Timer(this.components);
			this.buttonVisionTest = new Button();
			this.buttonSaveFile = new Button();
			this.buttonReadFile = new Button();
			this.buttonCancel = new Button();
			this.buttonSave = new Button();
			this.comboBoxFeedCamera1 = new ComboBox();
			this.comboBoxFeedCamera2 = new ComboBox();
			this.label3 = new Label();
			this.label4 = new Label();
			this.label7 = new Label();
			this.label8 = new Label();
			this.textBoxFeedBack = new TextBox();
			this.textBoxMarkVisionOffsetY = new TextBox();
			this.textBoxMarkVisionOffsetX = new TextBox();
			this.label10 = new Label();
			base.SuspendLayout();
			this.textBoxMac.Font = new Font("宋体", 12f);
			this.textBoxMac.Location = new Point(131, 25);
			this.textBoxMac.Name = "textBoxMac";
			this.textBoxMac.ReadOnly = true;
			this.textBoxMac.Size = new Size(238, 26);
			this.textBoxMac.TabIndex = 1;
			this.textBoxMac.Text = "8";
			this.textBoxMac.TextAlign = HorizontalAlignment.Center;
			this.label9.AutoSize = true;
			this.label9.Font = new Font("宋体", 12f);
			this.label9.Location = new Point(51, 28);
			this.label9.Name = "label9";
			this.label9.Size = new Size(80, 16);
			this.label9.TabIndex = 23;
			this.label9.Text = "MAC地址：";
			this.textBoxHomeX.Font = new Font("宋体", 12f);
			this.textBoxHomeX.Location = new Point(131, 57);
			this.textBoxHomeX.Name = "textBoxHomeX";
			this.textBoxHomeX.Size = new Size(110, 26);
			this.textBoxHomeX.TabIndex = 2;
			this.textBoxHomeX.Text = "8";
			this.textBoxHomeX.TextAlign = HorizontalAlignment.Center;
			this.label1.AutoSize = true;
			this.label1.Font = new Font("宋体", 12f);
			this.label1.Location = new Point(43, 60);
			this.label1.Name = "label1";
			this.label1.Size = new Size(88, 16);
			this.label1.TabIndex = 30;
			this.label1.Text = "原点坐标：";
			this.textBoxHomeY.Font = new Font("宋体", 12f);
			this.textBoxHomeY.Location = new Point(259, 57);
			this.textBoxHomeY.Name = "textBoxHomeY";
			this.textBoxHomeY.Size = new Size(110, 26);
			this.textBoxHomeY.TabIndex = 3;
			this.textBoxHomeY.Text = "8";
			this.textBoxHomeY.TextAlign = HorizontalAlignment.Center;
			this.textBoxVisionY.Font = new Font("宋体", 12f);
			this.textBoxVisionY.Location = new Point(212, 89);
			this.textBoxVisionY.Name = "textBoxVisionY";
			this.textBoxVisionY.Size = new Size(75, 26);
			this.textBoxVisionY.TabIndex = 6;
			this.textBoxVisionY.Text = "8";
			this.textBoxVisionY.TextAlign = HorizontalAlignment.Center;
			this.textBoxVisionX.Font = new Font("宋体", 12f);
			this.textBoxVisionX.Location = new Point(131, 89);
			this.textBoxVisionX.Name = "textBoxVisionX";
			this.textBoxVisionX.Size = new Size(75, 26);
			this.textBoxVisionX.TabIndex = 5;
			this.textBoxVisionX.Text = "8";
			this.textBoxVisionX.TextAlign = HorizontalAlignment.Center;
			this.label2.AutoSize = true;
			this.label2.Font = new Font("宋体", 12f);
			this.label2.Location = new Point(43, 92);
			this.label2.Name = "label2";
			this.label2.Size = new Size(88, 16);
			this.label2.TabIndex = 33;
			this.label2.Text = "相机坐标：";
			this.textBoxDiscardCompPosY.Font = new Font("宋体", 12f);
			this.textBoxDiscardCompPosY.Location = new Point(259, 153);
			this.textBoxDiscardCompPosY.Name = "textBoxDiscardCompPosY";
			this.textBoxDiscardCompPosY.Size = new Size(110, 26);
			this.textBoxDiscardCompPosY.TabIndex = 12;
			this.textBoxDiscardCompPosY.Text = "8";
			this.textBoxDiscardCompPosY.TextAlign = HorizontalAlignment.Center;
			this.textBoxDiscardCompPosX.Font = new Font("宋体", 12f);
			this.textBoxDiscardCompPosX.Location = new Point(131, 153);
			this.textBoxDiscardCompPosX.Name = "textBoxDiscardCompPosX";
			this.textBoxDiscardCompPosX.Size = new Size(110, 26);
			this.textBoxDiscardCompPosX.TabIndex = 11;
			this.textBoxDiscardCompPosX.Text = "8";
			this.textBoxDiscardCompPosX.TextAlign = HorizontalAlignment.Center;
			this.label5.AutoSize = true;
			this.label5.Font = new Font("宋体", 12f);
			this.label5.Location = new Point(43, 156);
			this.label5.Name = "label5";
			this.label5.Size = new Size(88, 16);
			this.label5.TabIndex = 42;
			this.label5.Text = "弃料坐标：";
			this.textBoxVisionZ.Font = new Font("宋体", 12f);
			this.textBoxVisionZ.Location = new Point(293, 89);
			this.textBoxVisionZ.Name = "textBoxVisionZ";
			this.textBoxVisionZ.Size = new Size(75, 26);
			this.textBoxVisionZ.TabIndex = 43;
			this.textBoxVisionZ.Text = "8";
			this.textBoxVisionZ.TextAlign = HorizontalAlignment.Center;
			this.saveFileDialog.DefaultExt = "dat";
			this.saveFileDialog.Filter = "DAT files(*.dat)|*.dat";
			this.openFileDialog.DefaultExt = "dat";
			this.openFileDialog.Filter = "DAT files(*.dat)|*.dat";
			this.labelVisionValue.AutoSize = true;
			this.labelVisionValue.Font = new Font("宋体", 12f);
			this.labelVisionValue.Location = new Point(43, 188);
			this.labelVisionValue.Name = "labelVisionValue";
			this.labelVisionValue.Size = new Size(88, 16);
			this.labelVisionValue.TabIndex = 46;
			this.labelVisionValue.Text = "吸嘴阈值：";
			this.textBoxVisionValueLow.Font = new Font("宋体", 12f);
			this.textBoxVisionValueLow.Location = new Point(131, 185);
			this.textBoxVisionValueLow.Name = "textBoxVisionValueLow";
			this.textBoxVisionValueLow.Size = new Size(110, 26);
			this.textBoxVisionValueLow.TabIndex = 47;
			this.textBoxVisionValueLow.Text = "8";
			this.textBoxVisionValueLow.TextAlign = HorizontalAlignment.Center;
			this.textBoxVisionValueHigh.Font = new Font("宋体", 12f);
			this.textBoxVisionValueHigh.Location = new Point(341, 185);
			this.textBoxVisionValueHigh.Name = "textBoxVisionValueHigh";
			this.textBoxVisionValueHigh.Size = new Size(110, 26);
			this.textBoxVisionValueHigh.TabIndex = 48;
			this.textBoxVisionValueHigh.Text = "8";
			this.textBoxVisionValueHigh.TextAlign = HorizontalAlignment.Center;
			this.textBoxNozzleVisionZ.Font = new Font("宋体", 12f);
			this.textBoxNozzleVisionZ.Location = new Point(293, 121);
			this.textBoxNozzleVisionZ.Name = "textBoxNozzleVisionZ";
			this.textBoxNozzleVisionZ.Size = new Size(75, 26);
			this.textBoxNozzleVisionZ.TabIndex = 49;
			this.textBoxNozzleVisionZ.Text = "8";
			this.textBoxNozzleVisionZ.TextAlign = HorizontalAlignment.Center;
			this.label6.AutoSize = true;
			this.label6.Font = new Font("宋体", 12f);
			this.label6.Location = new Point(43, 124);
			this.label6.Name = "label6";
			this.label6.Size = new Size(120, 16);
			this.label6.TabIndex = 50;
			this.label6.Text = "相机吸嘴坐标：";
			this.timer.Interval = 10;
			this.timer.Tick += new EventHandler(this.timer_Tick);
			this.buttonVisionTest.BackgroundImage = Resource.ButtonUp;
			this.buttonVisionTest.BackgroundImageLayout = ImageLayout.Stretch;
			this.buttonVisionTest.Font = new Font("宋体", 14f);
			this.buttonVisionTest.ImageAlign = ContentAlignment.MiddleLeft;
			this.buttonVisionTest.Location = new Point(380, 89);
			this.buttonVisionTest.Name = "buttonVisionTest";
			this.buttonVisionTest.Size = new Size(57, 58);
			this.buttonVisionTest.TabIndex = 51;
			this.buttonVisionTest.Text = "测量";
			this.buttonVisionTest.UseVisualStyleBackColor = true;
			this.buttonVisionTest.Click += new EventHandler(this.buttonVisionTest_Click);
			this.buttonSaveFile.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.buttonSaveFile.BackgroundImage = Resource.ButtonUp;
			this.buttonSaveFile.BackgroundImageLayout = ImageLayout.Stretch;
			this.buttonSaveFile.Font = new Font("宋体", 14f);
			this.buttonSaveFile.ImageAlign = ContentAlignment.MiddleLeft;
			this.buttonSaveFile.Location = new Point(53, 319);
			this.buttonSaveFile.Name = "buttonSaveFile";
			this.buttonSaveFile.Size = new Size(85, 49);
			this.buttonSaveFile.TabIndex = 45;
			this.buttonSaveFile.Text = "保存";
			this.buttonSaveFile.UseVisualStyleBackColor = true;
			this.buttonSaveFile.Click += new EventHandler(this.buttonSaveFile_Click);
			this.buttonReadFile.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.buttonReadFile.BackgroundImage = Resource.ButtonUp;
			this.buttonReadFile.BackgroundImageLayout = ImageLayout.Stretch;
			this.buttonReadFile.Font = new Font("宋体", 14f);
			this.buttonReadFile.ImageAlign = ContentAlignment.MiddleLeft;
			this.buttonReadFile.Location = new Point(152, 319);
			this.buttonReadFile.Name = "buttonReadFile";
			this.buttonReadFile.Size = new Size(85, 49);
			this.buttonReadFile.TabIndex = 44;
			this.buttonReadFile.Text = "读取";
			this.buttonReadFile.UseVisualStyleBackColor = true;
			this.buttonReadFile.Click += new EventHandler(this.buttonReadFile_Click);
			this.buttonCancel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
			this.buttonCancel.BackgroundImageLayout = ImageLayout.Stretch;
			this.buttonCancel.Font = new Font("宋体", 14f);
			this.buttonCancel.ImageAlign = ContentAlignment.MiddleLeft;
			this.buttonCancel.Location = new Point(350, 319);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new Size(85, 49);
			this.buttonCancel.TabIndex = 0;
			this.buttonCancel.Text = "取消";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			this.buttonSave.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left);
			this.buttonSave.BackgroundImage = Resource.ButtonUp;
			this.buttonSave.BackgroundImageLayout = ImageLayout.Stretch;
			this.buttonSave.Font = new Font("宋体", 14f);
			this.buttonSave.ImageAlign = ContentAlignment.MiddleLeft;
			this.buttonSave.Location = new Point(251, 319);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new Size(85, 49);
			this.buttonSave.TabIndex = 13;
			this.buttonSave.Text = "确定";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new EventHandler(this.buttonSave_Click);
			this.comboBoxFeedCamera1.DropDownStyle = ComboBoxStyle.DropDownList;
			this.comboBoxFeedCamera1.Font = new Font("宋体", 13f);
			this.comboBoxFeedCamera1.Location = new Point(131, 217);
			this.comboBoxFeedCamera1.Name = "comboBoxFeedCamera1";
			this.comboBoxFeedCamera1.Size = new Size(110, 25);
			this.comboBoxFeedCamera1.TabIndex = 52;
			this.comboBoxFeedCamera2.DropDownStyle = ComboBoxStyle.DropDownList;
			this.comboBoxFeedCamera2.Font = new Font("宋体", 13f);
			this.comboBoxFeedCamera2.Location = new Point(341, 217);
			this.comboBoxFeedCamera2.Name = "comboBoxFeedCamera2";
			this.comboBoxFeedCamera2.Size = new Size(110, 25);
			this.comboBoxFeedCamera2.TabIndex = 53;
			this.label3.AutoSize = true;
			this.label3.Font = new Font("宋体", 12f);
			this.label3.Location = new Point(43, 221);
			this.label3.Name = "label3";
			this.label3.Size = new Size(88, 16);
			this.label3.TabIndex = 54;
			this.label3.Text = "上视相机：";
			this.label4.AutoSize = true;
			this.label4.Font = new Font("宋体", 12f);
			this.label4.Location = new Point(253, 188);
			this.label4.Name = "label4";
			this.label4.Size = new Size(88, 16);
			this.label4.TabIndex = 55;
			this.label4.Text = "元件阈值：";
			this.label7.AutoSize = true;
			this.label7.Font = new Font("宋体", 12f);
			this.label7.Location = new Point(253, 221);
			this.label7.Name = "label7";
			this.label7.Size = new Size(88, 16);
			this.label7.TabIndex = 56;
			this.label7.Text = "下视相机：";
			this.label8.AutoSize = true;
			this.label8.Font = new Font("宋体", 12f);
			this.label8.Location = new Point(43, 254);
			this.label8.Name = "label8";
			this.label8.Size = new Size(88, 16);
			this.label8.TabIndex = 57;
			this.label8.Text = "拨针回退：";
			this.textBoxFeedBack.Font = new Font("宋体", 12f);
			this.textBoxFeedBack.Location = new Point(131, 249);
			this.textBoxFeedBack.Name = "textBoxFeedBack";
			this.textBoxFeedBack.Size = new Size(110, 26);
			this.textBoxFeedBack.TabIndex = 58;
			this.textBoxFeedBack.Text = "8";
			this.textBoxFeedBack.TextAlign = HorizontalAlignment.Center;
			this.textBoxMarkVisionOffsetY.Font = new Font("宋体", 12f);
			this.textBoxMarkVisionOffsetY.Location = new Point(259, 281);
			this.textBoxMarkVisionOffsetY.Name = "textBoxMarkVisionOffsetY";
			this.textBoxMarkVisionOffsetY.Size = new Size(110, 26);
			this.textBoxMarkVisionOffsetY.TabIndex = 60;
			this.textBoxMarkVisionOffsetY.Text = "8";
			this.textBoxMarkVisionOffsetY.TextAlign = HorizontalAlignment.Center;
			this.textBoxMarkVisionOffsetX.Font = new Font("宋体", 12f);
			this.textBoxMarkVisionOffsetX.Location = new Point(131, 281);
			this.textBoxMarkVisionOffsetX.Name = "textBoxMarkVisionOffsetX";
			this.textBoxMarkVisionOffsetX.Size = new Size(110, 26);
			this.textBoxMarkVisionOffsetX.TabIndex = 59;
			this.textBoxMarkVisionOffsetX.Text = "8";
			this.textBoxMarkVisionOffsetX.TextAlign = HorizontalAlignment.Center;
			this.label10.AutoSize = true;
			this.label10.Font = new Font("宋体", 12f);
			this.label10.Location = new Point(43, 286);
			this.label10.Name = "label10";
			this.label10.Size = new Size(88, 16);
			this.label10.TabIndex = 61;
			this.label10.Text = "下视中心：";
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new Size(488, 382);
			base.Controls.Add(this.textBoxMarkVisionOffsetY);
			base.Controls.Add(this.textBoxMarkVisionOffsetX);
			base.Controls.Add(this.label10);
			base.Controls.Add(this.textBoxFeedBack);
			base.Controls.Add(this.label8);
			base.Controls.Add(this.label7);
			base.Controls.Add(this.label4);
			base.Controls.Add(this.label3);
			base.Controls.Add(this.comboBoxFeedCamera2);
			base.Controls.Add(this.comboBoxFeedCamera1);
			base.Controls.Add(this.buttonVisionTest);
			base.Controls.Add(this.label6);
			base.Controls.Add(this.textBoxNozzleVisionZ);
			base.Controls.Add(this.textBoxVisionValueHigh);
			base.Controls.Add(this.textBoxVisionValueLow);
			base.Controls.Add(this.labelVisionValue);
			base.Controls.Add(this.buttonSaveFile);
			base.Controls.Add(this.buttonReadFile);
			base.Controls.Add(this.textBoxVisionZ);
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonSave);
			base.Controls.Add(this.textBoxDiscardCompPosY);
			base.Controls.Add(this.textBoxDiscardCompPosX);
			base.Controls.Add(this.label5);
			base.Controls.Add(this.textBoxVisionY);
			base.Controls.Add(this.textBoxVisionX);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.textBoxHomeY);
			base.Controls.Add(this.textBoxHomeX);
			base.Controls.Add(this.label1);
			base.Controls.Add(this.textBoxMac);
			base.Controls.Add(this.label9);
			base.Name = "SysParamEdit";
			base.ShowIcon = false;
			base.StartPosition = FormStartPosition.Manual;
			this.Text = "厂家参数";
			base.TopMost = true;
			base.FormClosing += new FormClosingEventHandler(this.SysParamEdit_FormClosing);
			base.Load += new EventHandler(this.SysParamEdit_Load);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public SysParamEdit(MainForm ow)
		{
			this.InitializeComponent();
			this.owner = ow;
		}

		private void SysParamEdit_Load(object sender, EventArgs e)
		{
			this.textBoxMac.Text = this.owner.MacStr;
			this.textBoxHomeX.Text = this.owner.HomePosX.ToString("F2");
			this.textBoxHomeY.Text = this.owner.HomePosY.ToString("F2");
			this.textBoxVisionX.Text = this.owner.VisionX.ToString("F2");
			this.textBoxVisionY.Text = this.owner.VisionY.ToString("F2");
			this.textBoxVisionZ.Text = this.owner.VisionZ.ToString("F2");
			this.textBoxNozzleVisionZ.Text = this.owner.NozzleVisionZ.ToString("F2");
			this.textBoxDiscardCompPosX.Text = this.owner.DiscardCompPosX.ToString("F2");
			this.textBoxDiscardCompPosY.Text = this.owner.DiscardCompPosY.ToString("F2");
			this.textBoxVisionValueLow.Text = this.owner.VisionValueNozzle.ToString("F2");
			this.textBoxVisionValueHigh.Text = this.owner.VisionValueComp.ToString("F2");
			this.textBoxFeedBack.Text = this.owner.FeedBack.ToString("F2");
			this.textBoxMarkVisionOffsetX.Text = this.owner.MarkVisionOffsetX.ToString("F2");
			this.textBoxMarkVisionOffsetY.Text = this.owner.MarkVisionOffsetY.ToString("F2");
			this.LoadCameraKey();
		}

		private void LoadCameraKey()
		{
			this.comboBoxFeedCamera1.Items.Add(this.owner.CameraKey1.ToString("D9"));
			this.comboBoxFeedCamera1.SelectedIndex = 0;
			this.comboBoxFeedCamera2.Items.Add(this.owner.CameraKey2.ToString("D9"));
			this.comboBoxFeedCamera2.SelectedIndex = 0;
			RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\services\\usbccgp\\Enum");
			if (registryKey != null)
			{
				string[] valueNames = registryKey.GetValueNames();
				string[] array = valueNames;
				for (int i = 0; i < array.Length; i++)
				{
					string name = array[i];
					if (registryKey.GetValueKind(name) == RegistryValueKind.String)
					{
						string text = (string)registryKey.GetValue(name);
						int num = text.Length;
						uint num2 = 0u;
						for (int j = 0; j < 8; j++)
						{
							if (--num >= 0)
							{
								num2 += (uint)((double)(text[num] - '0') * Math.Pow(10.0, (double)j));
							}
						}
						text = num2.ToString("D9");
						if (num2 != this.owner.CameraKey1)
						{
							this.comboBoxFeedCamera1.Items.Add(text);
						}
						if (num2 != this.owner.CameraKey2)
						{
							this.comboBoxFeedCamera2.Items.Add(text);
						}
					}
				}
			}
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			double num;
			if (double.TryParse(this.textBoxHomeX.Text, out num))
			{
				this.owner.HomePosX = num;
			}
			if (double.TryParse(this.textBoxHomeY.Text, out num))
			{
				this.owner.HomePosY = num;
			}
			if (double.TryParse(this.textBoxVisionX.Text, out num))
			{
				this.owner.VisionX = num;
			}
			if (double.TryParse(this.textBoxVisionY.Text, out num))
			{
				this.owner.VisionY = num;
			}
			if (double.TryParse(this.textBoxVisionZ.Text, out num))
			{
				this.owner.VisionZ = num;
			}
			if (double.TryParse(this.textBoxNozzleVisionZ.Text, out num))
			{
				this.owner.NozzleVisionZ = num;
			}
			if (double.TryParse(this.textBoxDiscardCompPosX.Text, out num))
			{
				this.owner.DiscardCompPosX = num;
			}
			if (double.TryParse(this.textBoxDiscardCompPosY.Text, out num))
			{
				this.owner.DiscardCompPosY = num;
			}
			if (double.TryParse(this.textBoxVisionValueLow.Text, out num))
			{
				if (num > 500.0)
				{
					num = 500.0;
				}
				if (num < 0.0)
				{
					num = 0.0;
				}
				this.owner.VisionValueNozzle = num;
			}
			if (double.TryParse(this.textBoxVisionValueHigh.Text, out num))
			{
				if (num > 500.0)
				{
					num = 500.0;
				}
				if (num < 0.0)
				{
					num = 0.0;
				}
				this.owner.VisionValueComp = num;
			}
			uint num2;
			if (uint.TryParse(this.comboBoxFeedCamera1.Text, out num2))
			{
				this.owner.CameraKey1 = num2;
			}
			if (uint.TryParse(this.comboBoxFeedCamera2.Text, out num2))
			{
				this.owner.CameraKey2 = num2;
			}
			if (double.TryParse(this.textBoxFeedBack.Text, out num))
			{
				this.owner.FeedBack = num;
			}
			if (double.TryParse(this.textBoxMarkVisionOffsetX.Text, out num))
			{
				this.owner.MarkVisionOffsetX = num;
			}
			if (double.TryParse(this.textBoxMarkVisionOffsetY.Text, out num))
			{
				this.owner.MarkVisionOffsetY = num;
			}
			this.owner.SaveSysParam();
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void buttonSaveFile_Click(object sender, EventArgs e)
		{
			double num;
			if (double.TryParse(this.textBoxHomeX.Text, out num))
			{
				this.owner.HomePosX = num;
			}
			if (double.TryParse(this.textBoxHomeY.Text, out num))
			{
				this.owner.HomePosY = num;
			}
			if (double.TryParse(this.textBoxVisionX.Text, out num))
			{
				this.owner.VisionX = num;
			}
			if (double.TryParse(this.textBoxVisionY.Text, out num))
			{
				this.owner.VisionY = num;
			}
			if (double.TryParse(this.textBoxVisionZ.Text, out num))
			{
				this.owner.VisionZ = num;
			}
			if (double.TryParse(this.textBoxNozzleVisionZ.Text, out num))
			{
				this.owner.NozzleVisionZ = num;
			}
			if (double.TryParse(this.textBoxDiscardCompPosX.Text, out num))
			{
				this.owner.DiscardCompPosX = num;
			}
			if (double.TryParse(this.textBoxDiscardCompPosY.Text, out num))
			{
				this.owner.DiscardCompPosY = num;
			}
			if (double.TryParse(this.textBoxVisionValueLow.Text, out num))
			{
				if (num > 500.0)
				{
					num = 500.0;
				}
				if (num < 0.0)
				{
					num = 0.0;
				}
				this.owner.VisionValueNozzle = num;
			}
			if (double.TryParse(this.textBoxVisionValueHigh.Text, out num))
			{
				if (num > 500.0)
				{
					num = 500.0;
				}
				if (num < 0.0)
				{
					num = 0.0;
				}
				this.owner.VisionValueComp = num;
			}
			uint num2;
			if (uint.TryParse(this.comboBoxFeedCamera1.Text, out num2))
			{
				this.owner.CameraKey1 = num2;
			}
			if (uint.TryParse(this.comboBoxFeedCamera2.Text, out num2))
			{
				this.owner.CameraKey2 = num2;
			}
			if (double.TryParse(this.textBoxFeedBack.Text, out num))
			{
				this.owner.FeedBack = num;
			}
			if (double.TryParse(this.textBoxMarkVisionOffsetX.Text, out num))
			{
				this.owner.MarkVisionOffsetX = num;
			}
			if (double.TryParse(this.textBoxMarkVisionOffsetY.Text, out num))
			{
				this.owner.MarkVisionOffsetY = num;
			}
			if (this.saveFileDialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}
			string fileName = this.saveFileDialog.FileName;
			FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
			StreamWriter streamWriter = new StreamWriter(stream, Encoding.Default);
			streamWriter.WriteLine(this.owner.macAdd1.ToString());
			streamWriter.WriteLine(this.owner.macAdd2.ToString());
			streamWriter.WriteLine(this.owner.ipAdd.ToString());
			streamWriter.WriteLine(this.owner.ipPort.ToString());
			streamWriter.WriteLine(this.owner.HomePosX.ToString());
			streamWriter.WriteLine(this.owner.HomePosY.ToString());
			streamWriter.WriteLine(this.owner.VisionX.ToString());
			streamWriter.WriteLine(this.owner.VisionY.ToString());
			streamWriter.WriteLine(this.owner.VisionOffsetX.ToString());
			streamWriter.WriteLine(this.owner.VisionOffsetY.ToString());
			streamWriter.WriteLine(this.owner.LeftPrickOffsetX.ToString());
			streamWriter.WriteLine(this.owner.LeftPrickOffsetY.ToString());
			streamWriter.WriteLine(this.owner.DiscardCompPosX.ToString());
			streamWriter.WriteLine(this.owner.DiscardCompPosY.ToString());
			streamWriter.WriteLine(this.owner.ZAxisNozzleDownPCBPos.ToString());
			streamWriter.WriteLine(this.owner.ZAxisNozzleDownStackPos.ToString());
			streamWriter.WriteLine(this.owner.ZAxisNozzleDownFrontStackPos.ToString());
			streamWriter.WriteLine(this.owner.ZAxisNozzleDownDiscardPos.ToString());
			streamWriter.WriteLine(this.owner.ZAxisPrickDownPos.ToString());
			streamWriter.WriteLine(this.owner.DoubleVisionCheck.ToString());
			streamWriter.WriteLine(this.owner.PixelScaleX1.ToString());
			streamWriter.WriteLine(this.owner.PixelScaleY1.ToString());
			streamWriter.WriteLine(this.owner.VisionZ.ToString());
			streamWriter.WriteLine(this.owner.PressureCheckTotal.ToString());
			streamWriter.WriteLine(this.owner.VisionValueNozzle.ToString());
			streamWriter.WriteLine(this.owner.VisionValueComp.ToString());
			streamWriter.WriteLine(this.owner.NozzleVisionZ.ToString());
			streamWriter.WriteLine(this.owner.NozzleOffsetX.ToString());
			streamWriter.WriteLine(this.owner.NozzleOffsetY.ToString());
			streamWriter.WriteLine(this.owner.ZAxisStackDealy.ToString());
			streamWriter.WriteLine(this.owner.ZAxisPcbDealy1.ToString());
			streamWriter.WriteLine(this.owner.ZAxisPcbDealy2.ToString());
			streamWriter.WriteLine(this.owner.CameraKey1.ToString());
			streamWriter.WriteLine(this.owner.CameraKey2.ToString());
			streamWriter.WriteLine(this.owner.PixelScaleX2.ToString());
			streamWriter.WriteLine(this.owner.PixelScaleY2.ToString());
			streamWriter.WriteLine(this.owner.CameraAngle1.ToString());
			streamWriter.WriteLine(this.owner.FeedBack.ToString());
			streamWriter.WriteLine(this.owner.Nozzle1VisionX.ToString());
			streamWriter.WriteLine(this.owner.Nozzle1VisionY.ToString());
			streamWriter.WriteLine(this.owner.Nozzle2VisionX.ToString());
			streamWriter.WriteLine(this.owner.Nozzle2VisionY.ToString());
			streamWriter.WriteLine(this.owner.MarkVisionOffsetX.ToString());
			streamWriter.WriteLine(this.owner.MarkVisionOffsetY.ToString());
			streamWriter.WriteLine(this.owner.BlowingDealy1.ToString());
			streamWriter.WriteLine(this.owner.BlowingDealy2.ToString());
			streamWriter.WriteLine(this.owner.BackPrickOffsetX.ToString());
			streamWriter.WriteLine(this.owner.BackPrickOffsetY.ToString());
			for (uint num3 = 0u; num3 < 4u; num3 += 1u)
			{
				streamWriter.WriteLine(this.owner.Nozzle.X[(int)((UIntPtr)num3)].ToString());
				streamWriter.WriteLine(this.owner.Nozzle.Y[(int)((UIntPtr)num3)].ToString());
				streamWriter.WriteLine(this.owner.Nozzle.Z[(int)((UIntPtr)num3)].ToString());
			}
			for (uint num4 = 0u; num4 < 60u; num4 += 1u)
			{
				streamWriter.WriteLine(this.owner.LeftAndBackStack.X[(int)((UIntPtr)num4)].ToString());
				streamWriter.WriteLine(this.owner.LeftAndBackStack.Y[(int)((UIntPtr)num4)].ToString());
				streamWriter.WriteLine(this.owner.LeftAndBackStack.Feed[(int)((UIntPtr)num4)].ToString());
				streamWriter.WriteLine(this.owner.LeftAndBackStack.Width[(int)((UIntPtr)num4)].ToString());
			}
			for (uint num5 = 0u; num5 < 30u; num5 += 1u)
			{
				streamWriter.WriteLine(this.owner.IcStack.StartX[(int)((UIntPtr)num5)].ToString());
				streamWriter.WriteLine(this.owner.IcStack.StartY[(int)((UIntPtr)num5)].ToString());
				streamWriter.WriteLine(this.owner.IcStack.EndX[(int)((UIntPtr)num5)].ToString());
				streamWriter.WriteLine(this.owner.IcStack.EndY[(int)((UIntPtr)num5)].ToString());
				streamWriter.WriteLine(this.owner.IcStack.ArrayX[(int)((UIntPtr)num5)].ToString());
				streamWriter.WriteLine(this.owner.IcStack.ArrayY[(int)((UIntPtr)num5)].ToString());
			}
			streamWriter.Close();
			stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			StreamReader streamReader = new StreamReader(stream, Encoding.Default);
			if (this.owner.macAdd1.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "MAC 地址保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.macAdd2.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "MAC 地址保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ipAdd.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "IP地址保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ipPort.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "IP端口保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.HomePosX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "X原点坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.HomePosY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "Y原点坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.VisionX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "X轴视觉保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.VisionY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "Y轴视觉保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.VisionOffsetX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "X轴相机偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.VisionOffsetY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "Y轴相机偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.LeftPrickOffsetX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "X轴拨针偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.LeftPrickOffsetY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "Y轴拨针偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.DiscardCompPosX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "弃料位置X坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.DiscardCompPosY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "弃料位置Y坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisNozzleDownPCBPos.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴下降PCB保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisNozzleDownStackPos.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴下降料栈保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisNozzleDownFrontStackPos.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴下降前置料栈保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisNozzleDownDiscardPos.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴下降弃料保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisPrickDownPos.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "拨针下降坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.DoubleVisionCheck.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "参数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.PixelScaleX1.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "视觉X系数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.PixelScaleY1.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "视觉Y系数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.VisionZ.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "视觉Z坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.PressureCheckTotal.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "压力检测保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.VisionValueNozzle.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "视觉阀值1保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.VisionValueComp.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "视觉阀值2保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.NozzleVisionZ.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴视觉保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.NozzleOffsetX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴偏移X保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.NozzleOffsetY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴偏移Y保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisStackDealy.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴下降料栈等待保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisPcbDealy1.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴下降PCB等待保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.ZAxisPcbDealy2.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴下降PCB等待保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.CameraKey1.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "相机序号保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.CameraKey2.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "相机序号保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.PixelScaleX2.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "视觉X系数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.PixelScaleY2.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "视觉Y系数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.CameraAngle1.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "上视角度保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.FeedBack.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "拨针回退保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.Nozzle1VisionX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.Nozzle1VisionY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.Nozzle2VisionX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.Nozzle2VisionY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "吸嘴坐标保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.MarkVisionOffsetX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "下视偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.MarkVisionOffsetY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "下视偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.BlowingDealy1.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "贴料吹气保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.BlowingDealy2.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "贴料吹气保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.BackPrickOffsetX.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "X轴拨针偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (this.owner.BackPrickOffsetY.ToString() != streamReader.ReadLine())
			{
				streamReader.Close();
				new MyMessageBox(null, "Y轴拨针偏移保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			for (uint num6 = 0u; num6 < 4u; num6 += 1u)
			{
				if (this.owner.Nozzle.X[(int)((UIntPtr)num6)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "吸嘴X保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.Nozzle.Y[(int)((UIntPtr)num6)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "吸嘴Y保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.Nozzle.Z[(int)((UIntPtr)num6)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "吸嘴Z保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
			}
			for (uint num7 = 0u; num7 < 60u; num7 += 1u)
			{
				if (this.owner.LeftAndBackStack.X[(int)((UIntPtr)num7)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "料栈X保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.LeftAndBackStack.Y[(int)((UIntPtr)num7)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "料栈Y保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.LeftAndBackStack.Feed[(int)((UIntPtr)num7)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "料栈进给保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.LeftAndBackStack.Width[(int)((UIntPtr)num7)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "料栈宽度保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
			}
			for (uint num8 = 0u; num8 < 30u; num8 += 1u)
			{
				if (this.owner.IcStack.StartX[(int)((UIntPtr)num8)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "IC托盘参数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.IcStack.StartY[(int)((UIntPtr)num8)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "IC托盘参数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.IcStack.EndX[(int)((UIntPtr)num8)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "IC托盘参数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.IcStack.EndY[(int)((UIntPtr)num8)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "IC托盘参数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.IcStack.ArrayX[(int)((UIntPtr)num8)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "IC托盘参数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.IcStack.ArrayY[(int)((UIntPtr)num8)].ToString() != streamReader.ReadLine())
				{
					streamReader.Close();
					new MyMessageBox(null, "IC托盘参数保存错误！", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
			}
			streamReader.Close();
			new MyMessageBox(null, "参数保存成功。", base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
		}

		private void buttonReadFile_Click(object sender, EventArgs e)
		{
			if (this.openFileDialog.ShowDialog() != DialogResult.OK)
			{
				return;
			}
			string fileName = this.openFileDialog.FileName;
			FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			StreamReader streamReader = new StreamReader(stream, Encoding.Default);
			this.owner.macAdd1 = uint.Parse(streamReader.ReadLine());
			this.owner.macAdd2 = uint.Parse(streamReader.ReadLine());
			this.owner.ipAdd = uint.Parse(streamReader.ReadLine());
			this.owner.ipPort = uint.Parse(streamReader.ReadLine());
			this.owner.HomePosX = double.Parse(streamReader.ReadLine());
			this.owner.HomePosY = double.Parse(streamReader.ReadLine());
			this.owner.VisionX = double.Parse(streamReader.ReadLine());
			this.owner.VisionY = double.Parse(streamReader.ReadLine());
			this.owner.VisionOffsetX = double.Parse(streamReader.ReadLine());
			this.owner.VisionOffsetY = double.Parse(streamReader.ReadLine());
			this.owner.LeftPrickOffsetX = double.Parse(streamReader.ReadLine());
			this.owner.LeftPrickOffsetY = double.Parse(streamReader.ReadLine());
			this.owner.DiscardCompPosX = double.Parse(streamReader.ReadLine());
			this.owner.DiscardCompPosY = double.Parse(streamReader.ReadLine());
			this.owner.ZAxisNozzleDownPCBPos = double.Parse(streamReader.ReadLine());
			this.owner.ZAxisNozzleDownStackPos = double.Parse(streamReader.ReadLine());
			this.owner.ZAxisNozzleDownFrontStackPos = double.Parse(streamReader.ReadLine());
			this.owner.ZAxisNozzleDownDiscardPos = double.Parse(streamReader.ReadLine());
			this.owner.ZAxisPrickDownPos = double.Parse(streamReader.ReadLine());
			this.owner.DoubleVisionCheck = bool.Parse(streamReader.ReadLine());
			this.owner.PixelScaleX1 = double.Parse(streamReader.ReadLine());
			this.owner.PixelScaleY1 = double.Parse(streamReader.ReadLine());
			this.owner.VisionZ = double.Parse(streamReader.ReadLine());
			this.owner.PressureCheckTotal = bool.Parse(streamReader.ReadLine());
			this.owner.VisionValueNozzle = double.Parse(streamReader.ReadLine());
			this.owner.VisionValueComp = double.Parse(streamReader.ReadLine());
			this.owner.NozzleVisionZ = double.Parse(streamReader.ReadLine());
			this.owner.NozzleOffsetX = double.Parse(streamReader.ReadLine());
			this.owner.NozzleOffsetY = double.Parse(streamReader.ReadLine());
			this.owner.ZAxisStackDealy = uint.Parse(streamReader.ReadLine());
			this.owner.ZAxisPcbDealy1 = uint.Parse(streamReader.ReadLine());
			this.owner.ZAxisPcbDealy2 = uint.Parse(streamReader.ReadLine());
			this.owner.CameraKey1 = uint.Parse(streamReader.ReadLine());
			this.owner.CameraKey2 = uint.Parse(streamReader.ReadLine());
			this.owner.PixelScaleX2 = double.Parse(streamReader.ReadLine());
			this.owner.PixelScaleY2 = double.Parse(streamReader.ReadLine());
			this.owner.CameraAngle1 = double.Parse(streamReader.ReadLine());
			this.owner.FeedBack = double.Parse(streamReader.ReadLine());
			this.owner.Nozzle1VisionX = double.Parse(streamReader.ReadLine());
			this.owner.Nozzle1VisionY = double.Parse(streamReader.ReadLine());
			this.owner.Nozzle2VisionX = double.Parse(streamReader.ReadLine());
			this.owner.Nozzle2VisionY = double.Parse(streamReader.ReadLine());
			this.owner.MarkVisionOffsetX = double.Parse(streamReader.ReadLine());
			this.owner.MarkVisionOffsetY = double.Parse(streamReader.ReadLine());
			this.owner.BlowingDealy1 = int.Parse(streamReader.ReadLine());
			this.owner.BlowingDealy2 = int.Parse(streamReader.ReadLine());
			this.owner.BackPrickOffsetX = double.Parse(streamReader.ReadLine());
			this.owner.BackPrickOffsetY = double.Parse(streamReader.ReadLine());
			for (uint num = 0u; num < 4u; num += 1u)
			{
				this.owner.Nozzle.X[(int)((UIntPtr)num)] = double.Parse(streamReader.ReadLine());
				this.owner.Nozzle.Y[(int)((UIntPtr)num)] = double.Parse(streamReader.ReadLine());
				this.owner.Nozzle.Z[(int)((UIntPtr)num)] = double.Parse(streamReader.ReadLine());
			}
			for (uint num2 = 0u; num2 < 60u; num2 += 1u)
			{
				this.owner.LeftAndBackStack.X[(int)((UIntPtr)num2)] = double.Parse(streamReader.ReadLine());
				this.owner.LeftAndBackStack.Y[(int)((UIntPtr)num2)] = double.Parse(streamReader.ReadLine());
				this.owner.LeftAndBackStack.Feed[(int)((UIntPtr)num2)] = int.Parse(streamReader.ReadLine());
				this.owner.LeftAndBackStack.Width[(int)((UIntPtr)num2)] = int.Parse(streamReader.ReadLine());
			}
			for (uint num3 = 0u; num3 < 30u; num3 += 1u)
			{
				this.owner.IcStack.StartX[(int)((UIntPtr)num3)] = double.Parse(streamReader.ReadLine());
				this.owner.IcStack.StartY[(int)((UIntPtr)num3)] = double.Parse(streamReader.ReadLine());
				this.owner.IcStack.EndX[(int)((UIntPtr)num3)] = double.Parse(streamReader.ReadLine());
				this.owner.IcStack.EndY[(int)((UIntPtr)num3)] = double.Parse(streamReader.ReadLine());
				this.owner.IcStack.ArrayX[(int)((UIntPtr)num3)] = int.Parse(streamReader.ReadLine());
				this.owner.IcStack.ArrayY[(int)((UIntPtr)num3)] = int.Parse(streamReader.ReadLine());
			}
			streamReader.Close();
			this.textBoxHomeX.Text = this.owner.HomePosX.ToString("F2");
			this.textBoxHomeY.Text = this.owner.HomePosY.ToString("F2");
			this.textBoxVisionX.Text = this.owner.VisionX.ToString("F2");
			this.textBoxVisionY.Text = this.owner.VisionY.ToString("F2");
			this.textBoxVisionZ.Text = this.owner.VisionZ.ToString("F2");
			this.textBoxNozzleVisionZ.Text = this.owner.NozzleVisionZ.ToString("F2");
			this.textBoxDiscardCompPosX.Text = this.owner.DiscardCompPosX.ToString("F2");
			this.textBoxDiscardCompPosY.Text = this.owner.DiscardCompPosY.ToString("F2");
			this.textBoxVisionValueLow.Text = this.owner.VisionValueNozzle.ToString("F2");
			this.textBoxVisionValueHigh.Text = this.owner.VisionValueComp.ToString("F2");
			this.textBoxFeedBack.Text = this.owner.FeedBack.ToString("F2");
			this.textBoxMarkVisionOffsetX.Text = this.owner.MarkVisionOffsetX.ToString("F2");
			this.textBoxMarkVisionOffsetY.Text = this.owner.MarkVisionOffsetY.ToString("F2");
			this.LoadCameraKey();
		}

		private void buttonVisionTest_Click(object sender, EventArgs e)
		{
			if (this.controler == null)
			{
				this.controler = this.owner.controler;
			}
			if (this.VisionTestState == SysParamEdit.VisionTestStateType.None)
			{
				this.VisionTestState = SysParamEdit.VisionTestStateType.Stage1;
				this.timer.Enabled = true;
			}
		}

		private void timer_Tick(object sender, EventArgs e)
		{
			switch (this.VisionTestState)
			{
			case SysParamEdit.VisionTestStateType.None:
				this.timer.Enabled = false;
				return;
			case SysParamEdit.VisionTestStateType.Stage1:
				if (this.controler.DPTP(1, 3, 0.0, 0.0, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionTestState = SysParamEdit.VisionTestStateType.Stage2;
					return;
				}
				break;
			case SysParamEdit.VisionTestStateType.Stage2:
				if (this.controler.SPTP(2, this.owner.VisionX, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionTestState = SysParamEdit.VisionTestStateType.Stage3;
					return;
				}
				break;
			case SysParamEdit.VisionTestStateType.Stage3:
				if (this.controler.SPTP(4, this.owner.VisionY, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionTestState = SysParamEdit.VisionTestStateType.Stage4;
					return;
				}
				break;
			case SysParamEdit.VisionTestStateType.Stage4:
				if (this.controler.SPTP(1, this.owner.NozzleVisionZ, CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionTestCounter = 0;
					this.VisionTestState = SysParamEdit.VisionTestStateType.Stage5;
					return;
				}
				break;
			case SysParamEdit.VisionTestStateType.Stage5:
				if (this.RunVisionDelayCounter == 0)
				{
					this.RunVisionDelayCounter = 5;
				}
				this.RunVisionDelayCounter--;
				if (this.RunVisionDelayCounter == 0)
				{
					if (this.owner.mVision1.Camera1 != null)
					{
						this.owner.mVision1.CompThreshold = (int)this.owner.VisionValueNozzle;
						this.owner.HeartBeatBackup = this.owner.mVision1.HeartBeat;
						this.owner.mVision1.CheckNozzleRun();
					}
					this.VisionTestState = SysParamEdit.VisionTestStateType.Stage6;
					return;
				}
				break;
			case SysParamEdit.VisionTestStateType.Stage6:
				if (this.owner.HeartBeatBackup != this.owner.mVision1.HeartBeat)
				{
					this.VisionCenterX[this.VisionTestCounter] = this.owner.mVision1.CompOffsetW1;
					this.VisionCenterY[this.VisionTestCounter] = this.owner.mVision1.CompOffsetH1;
					this.VisionTestCounter++;
					if (this.VisionTestCounter >= 8)
					{
						this.VisionTestState = SysParamEdit.VisionTestStateType.OK;
						return;
					}
					this.VisionTestState = SysParamEdit.VisionTestStateType.Stage7;
					return;
				}
				break;
			case SysParamEdit.VisionTestStateType.Stage7:
				if (this.controler.SPTP(3, (double)(this.VisionTestCounter * 45), CmdType.CMD_PTP_IMM) == CmdResultType.Done)
				{
					this.VisionTestState = SysParamEdit.VisionTestStateType.Stage5;
					return;
				}
				break;
			case SysParamEdit.VisionTestStateType.OK:
			{
				double num = this.VisionCenterX.Average();
				double num2 = this.VisionCenterY.Average();
				new MyMessageBox(null, string.Concat(new string[]
				{
					"心中点坐标:(",
					num.ToString("F2"),
					",",
					num2.ToString("F2"),
					")"
				}), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				this.VisionTestState = SysParamEdit.VisionTestStateType.None;
				break;
			}
			default:
				return;
			}
		}

		private void SysParamEdit_FormClosing(object sender, FormClosingEventArgs e)
		{
			this.timer.Enabled = false;
			this.VisionTestState = SysParamEdit.VisionTestStateType.None;
		}
	}
}
