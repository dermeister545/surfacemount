using System;
using System.ComponentModel;
using System.Net;
using System.Resources;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class IpConfig : Form
	{
		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		private IPAddress address;

		private string ipStr;

		private uint port;

		private IContainer components;

		private Label label1;

		private TextBox textBoxIp;

		private Label label2;

		private TextBox textBoxPort;

		private Button buttonCancel;

		private Button buttonNext;

		public uint IpAdd
		{
			get
			{
				return (uint)IPAddress.Parse(this.ipStr).Address;
			}
		}

		public uint Port
		{
			get
			{
				return this.port;
			}
			set
			{
				this.port = value;
			}
		}

		public IpConfig(uint ip, uint port)
		{
			this.InitializeComponent();
			this.textBoxIp.Text = new IPAddress((long)((ulong)ip)).ToString();
			this.textBoxPort.Text = port.ToString();
		}

		private void buttonNext_Click(object sender, EventArgs e)
		{
			if (!IPAddress.TryParse(this.textBoxIp.Text, out this.address))
			{
				new MyMessageBox(null, this.LocStrings.GetString("labelIpAddInvalid"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (!uint.TryParse(this.textBoxPort.Text, out this.port))
			{
				new MyMessageBox(null, this.LocStrings.GetString("labelPortInvalid"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			this.ipStr = this.textBoxIp.Text;
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            ComponentResourceManager resources = new ComponentResourceManager(typeof(IpConfig));
			this.label1 = new Label();
			this.textBoxIp = new TextBox();
			this.label2 = new Label();
			this.textBoxPort = new TextBox();
			this.buttonCancel = new Button();
			this.buttonNext = new Button();
			base.SuspendLayout();
            resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
            resources.ApplyResources(this.textBoxIp, "textBoxIp");
			this.textBoxIp.Name = "textBoxIp";
            resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
            resources.ApplyResources(this.textBoxPort, "textBoxPort");
			this.textBoxPort.Name = "textBoxPort";
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			this.buttonNext.BackgroundImage = Resource.ButtonUp;
            resources.ApplyResources(this.buttonNext, "buttonNext");
			this.buttonNext.Name = "buttonNext";
			this.buttonNext.UseVisualStyleBackColor = true;
			this.buttonNext.Click += new EventHandler(this.buttonNext_Click);
            resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonNext);
			base.Controls.Add(this.textBoxPort);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.textBoxIp);
			base.Controls.Add(this.label1);
			base.Name = "IpConfig";
			base.ShowIcon = false;
			base.TopMost = true;
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
