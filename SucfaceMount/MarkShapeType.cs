using System;

namespace SucfaceMount
{
	public enum MarkShapeType
	{
		Round,
		Rectangle,
		Irregular
	}
}
