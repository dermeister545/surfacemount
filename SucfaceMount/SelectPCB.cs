using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class SelectPCB : Form
	{
		private IContainer components;

		private Panel panel1;

		private Label labelText;

		private Panel panel2;

		private Button buttonCancel;

		private Button buttonOK;

		public ComboBox comboBoxWidthSelPCB;

		public string SelPCB;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager resources = new ComponentResourceManager(typeof(SelectPCB));
			this.panel1 = new Panel();
			this.buttonCancel = new Button();
			this.buttonOK = new Button();
			this.comboBoxWidthSelPCB = new ComboBox();
			this.labelText = new Label();
			this.panel2 = new Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			base.SuspendLayout();
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.BackColor = SystemColors.Control;
			this.panel1.BorderStyle = BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.buttonCancel);
			this.panel1.Controls.Add(this.buttonOK);
			this.panel1.Controls.Add(this.comboBoxWidthSelPCB);
			this.panel1.Controls.Add(this.labelText);
			this.panel1.Name = "panel1";
			resources.ApplyResources(this.buttonCancel, "buttonCancel");
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			resources.ApplyResources(this.buttonOK, "buttonOK");
			this.buttonOK.BackgroundImage = Resource.ButtonUp;
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.UseVisualStyleBackColor = true;
			this.buttonOK.Click += new EventHandler(this.buttonOK_Click);
			resources.ApplyResources(this.comboBoxWidthSelPCB, "comboBoxWidthSelPCB");
			this.comboBoxWidthSelPCB.DropDownStyle = ComboBoxStyle.DropDownList;
			this.comboBoxWidthSelPCB.Name = "comboBoxWidthSelPCB";
			resources.ApplyResources(this.labelText, "labelText");
			this.labelText.Name = "labelText";
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.BorderStyle = BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.panel1);
			this.panel2.Name = "panel2";
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.panel2);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "SelectPCB";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.TopMost = true;
			base.Load += new EventHandler(this.SelectPCB_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		public SelectPCB()
		{
			this.InitializeComponent();
		}

		public SelectPCB(int x, int y) : this()
		{
			base.Location = new Point(x - base.Size.Width / 2, y - base.Size.Height / 2);
		}

		private void SelectPCB_Load(object sender, EventArgs e)
		{
			this.comboBoxWidthSelPCB.SelectedIndex = 0;
			this.buttonOK.Focus();
		}

		private void buttonOK_Click(object sender, EventArgs e)
		{
			this.SelPCB = this.comboBoxWidthSelPCB.SelectedText;
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}
	}
}
