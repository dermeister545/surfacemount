using System;
using System.ComponentModel;
using System.Drawing;
using System.Resources;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class PCBEdit : Form
	{
		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		private MainForm owner;

		public bool[] IsMark1Used;

		public bool[] IsMark2Used;

		public double[] PuzzleMark1X;

		public double[] PuzzleMark1Y;

		public double[] PuzzleMark1MeasureX;

		public double[] PuzzleMark1MeasureY;

		public double[] PuzzleMark2X;

		public double[] PuzzleMark2Y;

		public double[] PuzzleMark2MeasureX;

		public double[] PuzzleMark2MeasureY;

		private double PCBThickness;

		private int puzzlePageNum;

		private IContainer components;

		private Button buttonUp;

		private Button buttonDown;

		private Button buttonSave;

		private Button buttonCancel;

		private Button buttonReset;

		private Button buttonGetCur1;

		private TextBox textBoxMark1X1;

		private TextBox textBoxMark1Y1;

		private TextBox textBoxPCBThickness;

		private GroupBox groupBox1;

		private GroupBox groupBox2;

		private Label label11;

		private TextBox textBoxMark1MeasureX1;

		private TextBox textBoxMark1MeasureY1;

		private TextBox textBoxMark2MeasureX1;

		private TextBox textBoxMark2MeasureY1;

		private TextBox textBoxMark2X1;

		private TextBox textBoxMark2Y1;

		private GroupBox groupBoxPuzzle1;

		private Label label4;

		private CheckBox checkBoxMark2PCB1;

		private CheckBox checkBoxMark1PCB1;

		private Label label7;

		private Label label9;

		private Label label6;

		private Label label5;

		private GroupBox groupBoxPuzzle5;

		private CheckBox checkBoxMark2PCB5;

		private CheckBox checkBoxMark1PCB5;

		private Label label22;

		private Label label23;

		private Label label24;

		private Label label25;

		private Label label26;

		private TextBox textBoxMark1Y5;

		private TextBox textBoxMark1X5;

		private TextBox textBoxMark1MeasureY5;

		private TextBox textBoxMark1MeasureX5;

		private TextBox textBoxMark2Y5;

		private TextBox textBoxMark2X5;

		private TextBox textBoxMark2MeasureY5;

		private TextBox textBoxMark2MeasureX5;

		private GroupBox groupBoxPuzzle4;

		private CheckBox checkBoxMark2PCB4;

		private CheckBox checkBoxMark1PCB4;

		private Label label17;

		private Label label18;

		private Label label19;

		private Label label20;

		private Label label21;

		private TextBox textBoxMark1Y4;

		private TextBox textBoxMark1X4;

		private TextBox textBoxMark1MeasureY4;

		private TextBox textBoxMark1MeasureX4;

		private TextBox textBoxMark2Y4;

		private TextBox textBoxMark2X4;

		private TextBox textBoxMark2MeasureY4;

		private TextBox textBoxMark2MeasureX4;

		private GroupBox groupBoxPuzzle3;

		private CheckBox checkBoxMark2PCB3;

		private CheckBox checkBoxMark1PCB3;

		private Label label12;

		private Label label13;

		private Label label14;

		private Label label15;

		private Label label16;

		private TextBox textBoxMark1Y3;

		private TextBox textBoxMark1X3;

		private TextBox textBoxMark1MeasureY3;

		private TextBox textBoxMark1MeasureX3;

		private TextBox textBoxMark2Y3;

		private TextBox textBoxMark2X3;

		private TextBox textBoxMark2MeasureY3;

		private TextBox textBoxMark2MeasureX3;

		private GroupBox groupBoxPuzzle2;

		private CheckBox checkBoxMark2PCB2;

		private CheckBox checkBoxMark1PCB2;

		private Label label1;

		private Label label2;

		private Label label3;

		private Label label8;

		private Label label10;

		private TextBox textBoxMark1Y2;

		private TextBox textBoxMark1X2;

		private TextBox textBoxMark1MeasureY2;

		private TextBox textBoxMark1MeasureX2;

		private TextBox textBoxMark2Y2;

		private TextBox textBoxMark2X2;

		private TextBox textBoxMark2MeasureY2;

		private TextBox textBoxMark2MeasureX2;

		private Button buttonGetCur2;

		private Button buttonMoveTo2;

		private Button buttonMoveTo1;

		private Button buttonMoveTo10;

		private Button buttonMoveTo9;

		private Button buttonGetCur10;

		private Button buttonGetCur9;

		private Button buttonMoveTo8;

		private Button buttonMoveTo7;

		private Button buttonGetCur8;

		private Button buttonGetCur7;

		private Button buttonMoveTo6;

		private Button buttonMoveTo5;

		private Button buttonGetCur6;

		private Button buttonGetCur5;

		private Button buttonMoveTo4;

		private Button buttonMoveTo3;

		private Button buttonGetCur4;

		private Button buttonGetCur3;

		private Button buttonLaser;

		private GroupBox groupBox3;

		private CheckBox checkBoxAutoPause;

		private Label label28;

		private RadioButton radioButtonIrregularMark;

		private RadioButton radioButtonRectangleMark;

		private Label label27;

		private RadioButton radioButtonRoundMark;

		private RadioButton radioButtonNoMark;

		private RadioButton radioButtonManualMark;

		private RadioButton radioButtonAutoMark;

		private Label label29;

		private ComboBox comboBoxExposure;

		public int PuzzlePageNum
		{
			get
			{
				return this.puzzlePageNum;
			}
			set
			{
				if (value >= 0 && value <= 9)
				{
					this.puzzlePageNum = value;
					this.UpdatePuzzleUI();
				}
			}
		}

		public PCBEdit(MainForm ow)
		{
			this.InitializeComponent();
			this.owner = ow;
			this.IsMark1Used = (bool[])this.owner.Puzzle.IsMark1Used.Clone();
			this.IsMark2Used = (bool[])this.owner.Puzzle.IsMark2Used.Clone();
			this.PuzzleMark1X = (double[])this.owner.Puzzle.Mark1X.Clone();
			this.PuzzleMark1Y = (double[])this.owner.Puzzle.Mark1Y.Clone();
			this.PuzzleMark1MeasureX = (double[])this.owner.Puzzle.Mark1MeasureX.Clone();
			this.PuzzleMark1MeasureY = (double[])this.owner.Puzzle.Mark1MeasureY.Clone();
			this.PuzzleMark2X = (double[])this.owner.Puzzle.Mark2X.Clone();
			this.PuzzleMark2Y = (double[])this.owner.Puzzle.Mark2Y.Clone();
			this.PuzzleMark2MeasureX = (double[])this.owner.Puzzle.Mark2MeasureX.Clone();
			this.PuzzleMark2MeasureY = (double[])this.owner.Puzzle.Mark2MeasureY.Clone();
			this.PCBThickness = this.owner.Other.PCBThickness;
			switch (this.owner.Mark.Mode)
			{
			case MarkModeType.NoMark:
				this.radioButtonAutoMark.Checked = false;
				this.radioButtonManualMark.Checked = false;
				this.radioButtonNoMark.Checked = true;
				this.radioButtonRoundMark.Enabled = false;
				this.radioButtonRectangleMark.Enabled = false;
				this.radioButtonIrregularMark.Enabled = false;
				this.checkBoxAutoPause.Enabled = false;
				break;
			case MarkModeType.Manual:
				this.radioButtonAutoMark.Checked = false;
				this.radioButtonManualMark.Checked = true;
				this.radioButtonNoMark.Checked = false;
				this.radioButtonRoundMark.Enabled = false;
				this.radioButtonRectangleMark.Enabled = false;
				this.radioButtonIrregularMark.Enabled = false;
				this.checkBoxAutoPause.Enabled = false;
				break;
			case MarkModeType.Auto:
				this.radioButtonAutoMark.Checked = true;
				this.radioButtonManualMark.Checked = false;
				this.radioButtonNoMark.Checked = false;
				this.radioButtonRoundMark.Enabled = true;
				this.radioButtonRectangleMark.Enabled = true;
				this.radioButtonIrregularMark.Enabled = true;
				this.checkBoxAutoPause.Enabled = true;
				break;
			}
			switch (this.owner.Mark.Shape)
			{
			case MarkShapeType.Round:
				this.radioButtonRoundMark.Checked = true;
				this.radioButtonRectangleMark.Checked = false;
				this.radioButtonIrregularMark.Checked = false;
				break;
			case MarkShapeType.Rectangle:
				this.radioButtonRoundMark.Checked = false;
				this.radioButtonRectangleMark.Checked = true;
				this.radioButtonIrregularMark.Checked = false;
				break;
			case MarkShapeType.Irregular:
				this.radioButtonRoundMark.Checked = false;
				this.radioButtonRectangleMark.Checked = false;
				this.radioButtonIrregularMark.Checked = true;
				break;
			}
			this.checkBoxAutoPause.Checked = this.owner.Mark.IsAutoPause;
			this.comboBoxExposure.Text = this.owner.Mark.Exposure.ToString();
			this.UpdatePuzzleUI();
			this.UpdateOtherUI();
		}

		private void UpdatePuzzleUI()
		{
			int num = this.puzzlePageNum * 5;
			this.groupBoxPuzzle1.Text = this.LocStrings.GetString("labelPCB") + (num + 1).ToString();
			this.checkBoxMark1PCB1.Checked = this.IsMark1Used[num];
			this.checkBoxMark2PCB1.Checked = this.IsMark2Used[num];
			this.textBoxMark1X1.Text = this.PuzzleMark1X[num].ToString("F2");
			this.textBoxMark1Y1.Text = this.PuzzleMark1Y[num].ToString("F2");
			this.textBoxMark1MeasureX1.Text = this.PuzzleMark1MeasureX[num].ToString("F2");
			this.textBoxMark1MeasureY1.Text = this.PuzzleMark1MeasureY[num].ToString("F2");
			this.textBoxMark2X1.Text = this.PuzzleMark2X[num].ToString("F2");
			this.textBoxMark2Y1.Text = this.PuzzleMark2Y[num].ToString("F2");
			this.textBoxMark2MeasureX1.Text = this.PuzzleMark2MeasureX[num].ToString("F2");
			this.textBoxMark2MeasureY1.Text = this.PuzzleMark2MeasureY[num].ToString("F2");
			num++;
			this.groupBoxPuzzle2.Text = this.LocStrings.GetString("labelPCB") + (num + 1).ToString();
			this.checkBoxMark1PCB2.Checked = this.IsMark1Used[num];
			this.checkBoxMark2PCB2.Checked = this.IsMark2Used[num];
			this.textBoxMark1X2.Text = this.PuzzleMark1X[num].ToString("F2");
			this.textBoxMark1Y2.Text = this.PuzzleMark1Y[num].ToString("F2");
			this.textBoxMark1MeasureX2.Text = this.PuzzleMark1MeasureX[num].ToString("F2");
			this.textBoxMark1MeasureY2.Text = this.PuzzleMark1MeasureY[num].ToString("F2");
			this.textBoxMark2X2.Text = this.PuzzleMark2X[num].ToString("F2");
			this.textBoxMark2Y2.Text = this.PuzzleMark2Y[num].ToString("F2");
			this.textBoxMark2MeasureX2.Text = this.PuzzleMark2MeasureX[num].ToString("F2");
			this.textBoxMark2MeasureY2.Text = this.PuzzleMark2MeasureY[num].ToString("F2");
			num++;
			this.groupBoxPuzzle3.Text = this.LocStrings.GetString("labelPCB") + (num + 1).ToString();
			this.checkBoxMark1PCB3.Checked = this.IsMark1Used[num];
			this.checkBoxMark2PCB3.Checked = this.IsMark2Used[num];
			this.textBoxMark1X3.Text = this.PuzzleMark1X[num].ToString("F2");
			this.textBoxMark1Y3.Text = this.PuzzleMark1Y[num].ToString("F2");
			this.textBoxMark1MeasureX3.Text = this.PuzzleMark1MeasureX[num].ToString("F2");
			this.textBoxMark1MeasureY3.Text = this.PuzzleMark1MeasureY[num].ToString("F2");
			this.textBoxMark2X3.Text = this.PuzzleMark2X[num].ToString("F2");
			this.textBoxMark2Y3.Text = this.PuzzleMark2Y[num].ToString("F2");
			this.textBoxMark2MeasureX3.Text = this.PuzzleMark2MeasureX[num].ToString("F2");
			this.textBoxMark2MeasureY3.Text = this.PuzzleMark2MeasureY[num].ToString("F2");
			num++;
			this.groupBoxPuzzle4.Text = this.LocStrings.GetString("labelPCB") + (num + 1).ToString();
			this.checkBoxMark1PCB4.Checked = this.IsMark1Used[num];
			this.checkBoxMark2PCB4.Checked = this.IsMark2Used[num];
			this.textBoxMark1X4.Text = this.PuzzleMark1X[num].ToString("F2");
			this.textBoxMark1Y4.Text = this.PuzzleMark1Y[num].ToString("F2");
			this.textBoxMark1MeasureX4.Text = this.PuzzleMark1MeasureX[num].ToString("F2");
			this.textBoxMark1MeasureY4.Text = this.PuzzleMark1MeasureY[num].ToString("F2");
			this.textBoxMark2X4.Text = this.PuzzleMark2X[num].ToString("F2");
			this.textBoxMark2Y4.Text = this.PuzzleMark2Y[num].ToString("F2");
			this.textBoxMark2MeasureX4.Text = this.PuzzleMark2MeasureX[num].ToString("F2");
			this.textBoxMark2MeasureY4.Text = this.PuzzleMark2MeasureY[num].ToString("F2");
			num++;
			this.groupBoxPuzzle5.Text = this.LocStrings.GetString("labelPCB") + (num + 1).ToString();
			this.checkBoxMark1PCB5.Checked = this.IsMark1Used[num];
			this.checkBoxMark2PCB5.Checked = this.IsMark2Used[num];
			this.textBoxMark1X5.Text = this.PuzzleMark1X[num].ToString("F2");
			this.textBoxMark1Y5.Text = this.PuzzleMark1Y[num].ToString("F2");
			this.textBoxMark1MeasureX5.Text = this.PuzzleMark1MeasureX[num].ToString("F2");
			this.textBoxMark1MeasureY5.Text = this.PuzzleMark1MeasureY[num].ToString("F2");
			this.textBoxMark2X5.Text = this.PuzzleMark2X[num].ToString("F2");
			this.textBoxMark2Y5.Text = this.PuzzleMark2Y[num].ToString("F2");
			this.textBoxMark2MeasureX5.Text = this.PuzzleMark2MeasureX[num].ToString("F2");
			this.textBoxMark2MeasureY5.Text = this.PuzzleMark2MeasureY[num].ToString("F2");
		}

		private void UpdateOtherUI()
		{
			this.textBoxPCBThickness.Text = this.PCBThickness.ToString("F1");
		}

		private void buttonUp_Click(object sender, EventArgs e)
		{
			this.PuzzlePageNum--;
		}

		private void buttonDown_Click(object sender, EventArgs e)
		{
			this.PuzzlePageNum++;
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < 50; i++)
			{
				if (this.IsMark1Used[i] && this.IsMark2Used[i])
				{
					double num = Math.Pow(this.PuzzleMark2X[i] - this.PuzzleMark1X[i], 2.0) + Math.Pow(this.PuzzleMark2Y[i] - this.PuzzleMark1Y[i], 2.0);
					if (num == 0.0)
					{
						MyMessageBox myMessageBox = new MyMessageBox(null, this.LocStrings.GetString("labelPCB") + (i + 1).ToString() + this.LocStrings.GetString("labelSameMark"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
						myMessageBox.ShowDialog();
						return;
					}
					double num2 = Math.Sqrt((Math.Pow(this.PuzzleMark2MeasureX[i] - this.PuzzleMark1MeasureX[i], 2.0) + Math.Pow(this.PuzzleMark2MeasureY[i] - this.PuzzleMark1MeasureY[i], 2.0)) / num);
					if (Math.Abs(num2 - 1.0) > 0.1)
					{
						MyMessageBoxYesNo myMessageBoxYesNo = new MyMessageBoxYesNo(null, this.LocStrings.GetString("labelPCB") + (i + 1).ToString() + ":" + this.LocStrings.GetString("markAndReadDeviationIsTooLarge"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
						myMessageBoxYesNo.buttonYes.Text = this.LocStrings.GetString("labelContinue");
						myMessageBoxYesNo.buttonNo.Text = this.LocStrings.GetString("labelCancel");
						myMessageBoxYesNo.ShowDialog();
						if (myMessageBoxYesNo.DialogResult != DialogResult.Yes)
						{
							return;
						}
					}
				}
			}
			this.IsMark1Used.CopyTo(this.owner.Puzzle.IsMark1Used, 0);
			this.IsMark2Used.CopyTo(this.owner.Puzzle.IsMark2Used, 0);
			this.PuzzleMark1X.CopyTo(this.owner.Puzzle.Mark1X, 0);
			this.PuzzleMark1Y.CopyTo(this.owner.Puzzle.Mark1Y, 0);
			this.PuzzleMark1MeasureX.CopyTo(this.owner.Puzzle.Mark1MeasureX, 0);
			this.PuzzleMark1MeasureY.CopyTo(this.owner.Puzzle.Mark1MeasureY, 0);
			this.PuzzleMark2X.CopyTo(this.owner.Puzzle.Mark2X, 0);
			this.PuzzleMark2Y.CopyTo(this.owner.Puzzle.Mark2Y, 0);
			this.PuzzleMark2MeasureX.CopyTo(this.owner.Puzzle.Mark2MeasureX, 0);
			this.PuzzleMark2MeasureY.CopyTo(this.owner.Puzzle.Mark2MeasureY, 0);
			this.owner.Other.PCBThickness = this.PCBThickness;
			if (this.radioButtonAutoMark.Checked)
			{
				this.owner.Mark.Mode = MarkModeType.Auto;
			}
			else if (this.radioButtonManualMark.Checked)
			{
				this.owner.Mark.Mode = MarkModeType.Manual;
			}
			else
			{
				this.owner.Mark.Mode = MarkModeType.NoMark;
			}
			if (this.radioButtonRoundMark.Checked)
			{
				this.owner.Mark.Shape = MarkShapeType.Round;
			}
			else if (this.radioButtonRectangleMark.Checked)
			{
				this.owner.Mark.Shape = MarkShapeType.Rectangle;
			}
			else
			{
				this.owner.Mark.Shape = MarkShapeType.Irregular;
			}
			this.owner.Mark.IsAutoPause = this.checkBoxAutoPause.Checked;
			this.owner.Mark.Exposure = int.Parse(this.comboBoxExposure.Text);
			this.owner.PreCalcParam();
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void buttonReset_Click(object sender, EventArgs e)
		{
			this.IsMark1Used = new bool[50];
			this.IsMark2Used = new bool[50];
			this.PuzzleMark1X = new double[50];
			this.PuzzleMark1Y = new double[50];
			this.PuzzleMark1MeasureX = new double[50];
			this.PuzzleMark1MeasureY = new double[50];
			this.PuzzleMark2X = new double[50];
			this.PuzzleMark2Y = new double[50];
			this.PuzzleMark2MeasureX = new double[50];
			this.PuzzleMark2MeasureY = new double[50];
			this.PCBThickness = 1.6;
			this.UpdatePuzzleUI();
			this.UpdateOtherUI();
			this.radioButtonAutoMark.Checked = false;
			this.radioButtonManualMark.Checked = false;
			this.radioButtonNoMark.Checked = true;
			this.radioButtonRoundMark.Checked = true;
			this.radioButtonRoundMark.Enabled = false;
			this.radioButtonRectangleMark.Checked = false;
			this.radioButtonRectangleMark.Enabled = false;
			this.radioButtonIrregularMark.Checked = false;
			this.radioButtonIrregularMark.Enabled = false;
			this.checkBoxAutoPause.Checked = true;
			this.checkBoxAutoPause.Enabled = false;
			this.comboBoxExposure.Text = 6.ToString();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			if (this.owner.mVision1.Camera2 != null)
			{
				this.owner.mVision1.Camera2.Exposure = -this.owner.Mark.Exposure;
			}
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void textBoxMark1X_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark1X[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark1X[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxMark1Y_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark1Y[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark1Y[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxMark1MeasureX_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark1MeasureX[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark1MeasureX[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxMark1MeasureY_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark1MeasureY[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark1Y[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxMark2X_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark2X[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark2X[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxMark2Y_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark2Y[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark2Y[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxMark2MeasureX_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark2MeasureX[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark2MeasureX[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxMark2MeasureY_Leave(object sender, EventArgs e)
		{
			int num = int.Parse(((TextBox)sender).Name.Remove(0, ((TextBox)sender).Name.Length - 1)) - 1;
			double num2;
			if (double.TryParse(((TextBox)sender).Text, out num2))
			{
				this.PuzzleMark2MeasureY[this.PuzzlePageNum * 5 + num] = num2;
				return;
			}
			((TextBox)sender).Text = this.PuzzleMark2Y[this.PuzzlePageNum * 5 + num].ToString("F2");
		}

		private void textBoxPCBThickness_Leave(object sender, EventArgs e)
		{
			double pCBThickness;
			if (double.TryParse(((TextBox)sender).Text, out pCBThickness))
			{
				this.PCBThickness = pCBThickness;
				return;
			}
			((TextBox)sender).Text = this.PCBThickness.ToString("F1");
		}

		private void PCBEdit_FormClosed(object sender, FormClosedEventArgs e)
		{
			this.owner.IsPCBEdit = false;
			this.owner.buttonPCB.BackgroundImage = Resource.ButtonUp;
		}

		private void buttonMoveTo1_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark1MeasureX1.Text, out num) && double.TryParse(this.textBoxMark1MeasureY1.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo2_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark2MeasureX1.Text, out num) && double.TryParse(this.textBoxMark2MeasureY1.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo3_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark1MeasureX2.Text, out num) && double.TryParse(this.textBoxMark1MeasureY2.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo4_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark2MeasureX2.Text, out num) && double.TryParse(this.textBoxMark2MeasureY2.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo5_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark1MeasureX3.Text, out num) && double.TryParse(this.textBoxMark1MeasureY3.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo6_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark2MeasureX3.Text, out num) && double.TryParse(this.textBoxMark2MeasureY3.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo7_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark1MeasureX4.Text, out num) && double.TryParse(this.textBoxMark1MeasureY4.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo8_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark2MeasureX4.Text, out num) && double.TryParse(this.textBoxMark2MeasureY4.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo9_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark1MeasureX5.Text, out num) && double.TryParse(this.textBoxMark1MeasureY5.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonMoveTo10_Click(object sender, EventArgs e)
		{
			if (this.owner.IsConnect)
			{
				if (!this.owner.controler.IsHomed)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				if (this.owner.controler.DI_PrickHome || this.owner.controler.PosMM[1] != 0.0)
				{
					new MyMessageBox(this.owner.controler, this.LocStrings.GetString("mustAtHomePostion"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
					return;
				}
				double num;
				double num2;
				if (double.TryParse(this.textBoxMark2MeasureX5.Text, out num) && double.TryParse(this.textBoxMark2MeasureY5.Text, out num2))
				{
					this.owner.controler.WriteSpeed_IMM(5, 100);
					this.owner.controler.DPTP(2, 4, num - this.owner.VisionOffsetX, num2 - this.owner.VisionOffsetY, CmdType.CMD_PTP_IMM);
				}
			}
		}

		private void buttonGetCur1_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark1MeasureX1.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark1MeasureX[this.PuzzlePageNum * 5] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark1MeasureY1.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark1MeasureY[this.PuzzlePageNum * 5] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur2_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark2MeasureX1.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark2MeasureX[this.PuzzlePageNum * 5] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark2MeasureY1.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark2MeasureY[this.PuzzlePageNum * 5] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur3_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark1MeasureX2.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark1MeasureX[this.PuzzlePageNum * 5 + 1] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark1MeasureY2.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark1MeasureY[this.PuzzlePageNum * 5 + 1] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur4_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark2MeasureX2.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark2MeasureX[this.PuzzlePageNum * 5 + 1] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark2MeasureY2.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark2MeasureY[this.PuzzlePageNum * 5 + 1] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur5_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark1MeasureX3.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark1MeasureX[this.PuzzlePageNum * 5 + 2] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark1MeasureY3.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark1MeasureY[this.PuzzlePageNum * 5 + 2] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur6_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark2MeasureX3.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark2MeasureX[this.PuzzlePageNum * 5 + 2] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark2MeasureY3.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark2MeasureY[this.PuzzlePageNum * 5 + 2] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur7_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark1MeasureX4.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark1MeasureX[this.PuzzlePageNum * 5 + 3] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark1MeasureY4.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark1MeasureY[this.PuzzlePageNum * 5 + 3] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur8_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark2MeasureX4.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark2MeasureX[this.PuzzlePageNum * 5 + 3] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark2MeasureY4.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark2MeasureY[this.PuzzlePageNum * 5 + 3] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur9_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark1MeasureX5.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark1MeasureX[this.PuzzlePageNum * 5 + 4] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark1MeasureY5.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark1MeasureY[this.PuzzlePageNum * 5 + 4] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonGetCur10_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.IsHomed)
			{
				MyMessageBox myMessageBox = new MyMessageBox(this.owner.controler, this.LocStrings.GetString("labelGoHomeFirst"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2);
				myMessageBox.Show();
				return;
			}
			this.textBoxMark2MeasureX5.Text = (this.owner.controler.PosMM[2] + this.owner.VisionOffsetX).ToString("F2");
			this.PuzzleMark2MeasureX[this.PuzzlePageNum * 5 + 4] = this.owner.controler.PosMM[2] + this.owner.VisionOffsetX;
			this.textBoxMark2MeasureY5.Text = (this.owner.controler.PosMM[4] + this.owner.VisionOffsetY).ToString("F2");
			this.PuzzleMark2MeasureY[this.PuzzlePageNum * 5 + 4] = this.owner.controler.PosMM[4] + this.owner.VisionOffsetY;
		}

		private void buttonLaser_Click(object sender, EventArgs e)
		{
			if (!this.owner.controler.DO_Laser)
			{
				this.owner.controler.SetDO_ON_IMM(1u);
				return;
			}
			this.owner.controler.SetDO_OFF_IMM(1u);
		}

		private void checkBoxMark1PCB1_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark1Used[this.PuzzlePageNum * 5] = @checked;
			this.textBoxMark1X1.Enabled = @checked;
			this.textBoxMark1Y1.Enabled = @checked;
			this.textBoxMark1MeasureX1.Enabled = @checked;
			this.textBoxMark1MeasureY1.Enabled = @checked;
		}

		private void checkBoxMark2PCB1_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark2Used[this.PuzzlePageNum * 5] = @checked;
			this.textBoxMark2X1.Enabled = @checked;
			this.textBoxMark2Y1.Enabled = @checked;
			this.textBoxMark2MeasureX1.Enabled = @checked;
			this.textBoxMark2MeasureY1.Enabled = @checked;
		}

		private void checkBoxMark1PCB2_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark1Used[this.PuzzlePageNum * 5 + 1] = @checked;
			this.textBoxMark1X2.Enabled = @checked;
			this.textBoxMark1Y2.Enabled = @checked;
			this.textBoxMark1MeasureX2.Enabled = @checked;
			this.textBoxMark1MeasureY2.Enabled = @checked;
		}

		private void checkBoxMark2PCB2_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark2Used[this.PuzzlePageNum * 5 + 1] = @checked;
			this.textBoxMark2X2.Enabled = @checked;
			this.textBoxMark2Y2.Enabled = @checked;
			this.textBoxMark2MeasureX2.Enabled = @checked;
			this.textBoxMark2MeasureY2.Enabled = @checked;
		}

		private void checkBoxMark1PCB3_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark1Used[this.PuzzlePageNum * 5 + 2] = @checked;
			this.textBoxMark1X3.Enabled = @checked;
			this.textBoxMark1Y3.Enabled = @checked;
			this.textBoxMark1MeasureX3.Enabled = @checked;
			this.textBoxMark1MeasureY3.Enabled = @checked;
		}

		private void checkBoxMark2PCB3_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark2Used[this.PuzzlePageNum * 5 + 2] = @checked;
			this.textBoxMark2X3.Enabled = @checked;
			this.textBoxMark2Y3.Enabled = @checked;
			this.textBoxMark2MeasureX3.Enabled = @checked;
			this.textBoxMark2MeasureY3.Enabled = @checked;
		}

		private void checkBoxMark1PCB4_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark1Used[this.PuzzlePageNum * 5 + 3] = @checked;
			this.textBoxMark1X4.Enabled = @checked;
			this.textBoxMark1Y4.Enabled = @checked;
			this.textBoxMark1MeasureX4.Enabled = @checked;
			this.textBoxMark1MeasureY4.Enabled = @checked;
		}

		private void checkBoxMark2PCB4_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark2Used[this.PuzzlePageNum * 5 + 3] = @checked;
			this.textBoxMark2X4.Enabled = @checked;
			this.textBoxMark2Y4.Enabled = @checked;
			this.textBoxMark2MeasureX4.Enabled = @checked;
			this.textBoxMark2MeasureY4.Enabled = @checked;
		}

		private void checkBoxMark1PCB5_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark1Used[this.PuzzlePageNum * 5 + 4] = @checked;
			this.textBoxMark1X5.Enabled = @checked;
			this.textBoxMark1Y5.Enabled = @checked;
			this.textBoxMark1MeasureX5.Enabled = @checked;
			this.textBoxMark1MeasureY5.Enabled = @checked;
		}

		private void checkBoxMark2PCB5_CheckedChanged(object sender, EventArgs e)
		{
			bool @checked = ((CheckBox)sender).Checked;
			this.IsMark2Used[this.PuzzlePageNum * 5 + 4] = @checked;
			this.textBoxMark2X5.Enabled = @checked;
			this.textBoxMark2Y5.Enabled = @checked;
			this.textBoxMark2MeasureX5.Enabled = @checked;
			this.textBoxMark2MeasureY5.Enabled = @checked;
		}

		private void radioButtonAutoMark_Click(object sender, EventArgs e)
		{
			if (!this.radioButtonAutoMark.Checked)
			{
				this.radioButtonAutoMark.Checked = true;
				this.radioButtonManualMark.Checked = false;
				this.radioButtonNoMark.Checked = false;
				this.radioButtonRoundMark.Enabled = true;
				this.radioButtonRectangleMark.Enabled = true;
				this.radioButtonIrregularMark.Enabled = true;
				this.checkBoxAutoPause.Enabled = true;
			}
		}

		private void radioButtonManualMark_Click(object sender, EventArgs e)
		{
			if (!this.radioButtonManualMark.Checked)
			{
				this.radioButtonAutoMark.Checked = false;
				this.radioButtonManualMark.Checked = true;
				this.radioButtonNoMark.Checked = false;
				this.radioButtonRoundMark.Enabled = false;
				this.radioButtonRectangleMark.Enabled = false;
				this.radioButtonIrregularMark.Enabled = false;
				this.checkBoxAutoPause.Enabled = false;
			}
		}

		private void radioButtonNoMark_Click(object sender, EventArgs e)
		{
			if (!this.radioButtonNoMark.Checked)
			{
				this.radioButtonAutoMark.Checked = false;
				this.radioButtonManualMark.Checked = false;
				this.radioButtonNoMark.Checked = true;
				this.radioButtonRoundMark.Enabled = false;
				this.radioButtonRectangleMark.Enabled = false;
				this.radioButtonIrregularMark.Enabled = false;
				this.checkBoxAutoPause.Enabled = false;
			}
		}

		private void radioButtonRoundMark_Click(object sender, EventArgs e)
		{
			if (!this.radioButtonRoundMark.Checked)
			{
				this.radioButtonRoundMark.Checked = true;
				this.radioButtonRectangleMark.Checked = false;
				this.radioButtonIrregularMark.Checked = false;
			}
		}

		private void radioButtonRectangleMark_Click(object sender, EventArgs e)
		{
			if (!this.radioButtonRectangleMark.Checked)
			{
				this.radioButtonRoundMark.Checked = false;
				this.radioButtonRectangleMark.Checked = true;
				this.radioButtonIrregularMark.Checked = false;
			}
		}

		private void radioButtonIrregularMark_Click(object sender, EventArgs e)
		{
			if (!this.radioButtonIrregularMark.Checked)
			{
				this.radioButtonRoundMark.Checked = false;
				this.radioButtonRectangleMark.Checked = false;
				this.radioButtonIrregularMark.Checked = true;
			}
		}

		private void comboBoxExposure_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.owner.mVision1.Camera2 != null)
			{
				this.owner.mVision1.Camera2.Exposure = -int.Parse(this.comboBoxExposure.Text);
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager resources = new ComponentResourceManager(typeof(PCBEdit));
			this.textBoxMark1X1 = new TextBox();
			this.textBoxMark1Y1 = new TextBox();
			this.textBoxPCBThickness = new TextBox();
			this.groupBox1 = new GroupBox();
			this.buttonLaser = new Button();
			this.groupBoxPuzzle5 = new GroupBox();
			this.buttonMoveTo10 = new Button();
			this.buttonMoveTo9 = new Button();
			this.buttonGetCur10 = new Button();
			this.buttonGetCur9 = new Button();
			this.checkBoxMark2PCB5 = new CheckBox();
			this.checkBoxMark1PCB5 = new CheckBox();
			this.label22 = new Label();
			this.label23 = new Label();
			this.label24 = new Label();
			this.label25 = new Label();
			this.label26 = new Label();
			this.textBoxMark1Y5 = new TextBox();
			this.textBoxMark1X5 = new TextBox();
			this.textBoxMark1MeasureY5 = new TextBox();
			this.textBoxMark1MeasureX5 = new TextBox();
			this.textBoxMark2Y5 = new TextBox();
			this.textBoxMark2X5 = new TextBox();
			this.textBoxMark2MeasureY5 = new TextBox();
			this.textBoxMark2MeasureX5 = new TextBox();
			this.groupBoxPuzzle4 = new GroupBox();
			this.buttonMoveTo8 = new Button();
			this.buttonMoveTo7 = new Button();
			this.buttonGetCur8 = new Button();
			this.buttonGetCur7 = new Button();
			this.checkBoxMark2PCB4 = new CheckBox();
			this.checkBoxMark1PCB4 = new CheckBox();
			this.label17 = new Label();
			this.label18 = new Label();
			this.label19 = new Label();
			this.label20 = new Label();
			this.label21 = new Label();
			this.textBoxMark1Y4 = new TextBox();
			this.textBoxMark1X4 = new TextBox();
			this.textBoxMark1MeasureY4 = new TextBox();
			this.textBoxMark1MeasureX4 = new TextBox();
			this.textBoxMark2Y4 = new TextBox();
			this.textBoxMark2X4 = new TextBox();
			this.textBoxMark2MeasureY4 = new TextBox();
			this.textBoxMark2MeasureX4 = new TextBox();
			this.groupBoxPuzzle3 = new GroupBox();
			this.buttonMoveTo6 = new Button();
			this.buttonMoveTo5 = new Button();
			this.buttonGetCur6 = new Button();
			this.buttonGetCur5 = new Button();
			this.checkBoxMark2PCB3 = new CheckBox();
			this.checkBoxMark1PCB3 = new CheckBox();
			this.label12 = new Label();
			this.label13 = new Label();
			this.label14 = new Label();
			this.label15 = new Label();
			this.label16 = new Label();
			this.textBoxMark1Y3 = new TextBox();
			this.textBoxMark1X3 = new TextBox();
			this.textBoxMark1MeasureY3 = new TextBox();
			this.textBoxMark1MeasureX3 = new TextBox();
			this.textBoxMark2Y3 = new TextBox();
			this.textBoxMark2X3 = new TextBox();
			this.textBoxMark2MeasureY3 = new TextBox();
			this.textBoxMark2MeasureX3 = new TextBox();
			this.groupBoxPuzzle2 = new GroupBox();
			this.buttonMoveTo4 = new Button();
			this.checkBoxMark2PCB2 = new CheckBox();
			this.buttonMoveTo3 = new Button();
			this.buttonGetCur4 = new Button();
			this.checkBoxMark1PCB2 = new CheckBox();
			this.buttonGetCur3 = new Button();
			this.label1 = new Label();
			this.label2 = new Label();
			this.label3 = new Label();
			this.label8 = new Label();
			this.label10 = new Label();
			this.textBoxMark1Y2 = new TextBox();
			this.textBoxMark1X2 = new TextBox();
			this.textBoxMark1MeasureY2 = new TextBox();
			this.textBoxMark1MeasureX2 = new TextBox();
			this.textBoxMark2Y2 = new TextBox();
			this.textBoxMark2X2 = new TextBox();
			this.textBoxMark2MeasureY2 = new TextBox();
			this.textBoxMark2MeasureX2 = new TextBox();
			this.groupBoxPuzzle1 = new GroupBox();
			this.buttonMoveTo2 = new Button();
			this.buttonMoveTo1 = new Button();
			this.buttonGetCur2 = new Button();
			this.checkBoxMark2PCB1 = new CheckBox();
			this.checkBoxMark1PCB1 = new CheckBox();
			this.label7 = new Label();
			this.label9 = new Label();
			this.label6 = new Label();
			this.label5 = new Label();
			this.label4 = new Label();
			this.textBoxMark1MeasureY1 = new TextBox();
			this.textBoxMark1MeasureX1 = new TextBox();
			this.textBoxMark2Y1 = new TextBox();
			this.textBoxMark2X1 = new TextBox();
			this.textBoxMark2MeasureY1 = new TextBox();
			this.textBoxMark2MeasureX1 = new TextBox();
			this.buttonGetCur1 = new Button();
			this.buttonUp = new Button();
			this.buttonDown = new Button();
			this.groupBox2 = new GroupBox();
			this.label11 = new Label();
			this.buttonReset = new Button();
			this.buttonCancel = new Button();
			this.buttonSave = new Button();
			this.groupBox3 = new GroupBox();
			this.comboBoxExposure = new ComboBox();
			this.label29 = new Label();
			this.checkBoxAutoPause = new CheckBox();
			this.label28 = new Label();
			this.radioButtonIrregularMark = new RadioButton();
			this.radioButtonRectangleMark = new RadioButton();
			this.label27 = new Label();
			this.radioButtonRoundMark = new RadioButton();
			this.radioButtonNoMark = new RadioButton();
			this.radioButtonManualMark = new RadioButton();
			this.radioButtonAutoMark = new RadioButton();
			this.groupBox1.SuspendLayout();
			this.groupBoxPuzzle5.SuspendLayout();
			this.groupBoxPuzzle4.SuspendLayout();
			this.groupBoxPuzzle3.SuspendLayout();
			this.groupBoxPuzzle2.SuspendLayout();
			this.groupBoxPuzzle1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			base.SuspendLayout();
			resources.ApplyResources(this.textBoxMark1X1, "textBoxMark1X1");
			this.textBoxMark1X1.Name = "textBoxMark1X1";
			this.textBoxMark1X1.Leave += new EventHandler(this.textBoxMark1X_Leave);
			resources.ApplyResources(this.textBoxMark1Y1, "textBoxMark1Y1");
			this.textBoxMark1Y1.Name = "textBoxMark1Y1";
			this.textBoxMark1Y1.Leave += new EventHandler(this.textBoxMark1Y_Leave);
			resources.ApplyResources(this.textBoxPCBThickness, "textBoxPCBThickness");
			this.textBoxPCBThickness.Name = "textBoxPCBThickness";
			this.textBoxPCBThickness.Leave += new EventHandler(this.textBoxPCBThickness_Leave);
			resources.ApplyResources(this.groupBox1, "groupBox1");
			this.groupBox1.Controls.Add(this.buttonLaser);
			this.groupBox1.Controls.Add(this.groupBoxPuzzle5);
			this.groupBox1.Controls.Add(this.groupBoxPuzzle4);
			this.groupBox1.Controls.Add(this.groupBoxPuzzle3);
			this.groupBox1.Controls.Add(this.groupBoxPuzzle2);
			this.groupBox1.Controls.Add(this.groupBoxPuzzle1);
			this.groupBox1.Controls.Add(this.buttonUp);
			this.groupBox1.Controls.Add(this.buttonDown);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.TabStop = false;
			resources.ApplyResources(this.buttonLaser, "buttonLaser");
			this.buttonLaser.BackgroundImage = Resource.ButtonUp;
			this.buttonLaser.Name = "buttonLaser";
			this.buttonLaser.UseVisualStyleBackColor = true;
			this.buttonLaser.Click += new EventHandler(this.buttonLaser_Click);
			resources.ApplyResources(this.groupBoxPuzzle5, "groupBoxPuzzle5");
			this.groupBoxPuzzle5.Controls.Add(this.buttonMoveTo10);
			this.groupBoxPuzzle5.Controls.Add(this.buttonMoveTo9);
			this.groupBoxPuzzle5.Controls.Add(this.buttonGetCur10);
			this.groupBoxPuzzle5.Controls.Add(this.buttonGetCur9);
			this.groupBoxPuzzle5.Controls.Add(this.checkBoxMark2PCB5);
			this.groupBoxPuzzle5.Controls.Add(this.checkBoxMark1PCB5);
			this.groupBoxPuzzle5.Controls.Add(this.label22);
			this.groupBoxPuzzle5.Controls.Add(this.label23);
			this.groupBoxPuzzle5.Controls.Add(this.label24);
			this.groupBoxPuzzle5.Controls.Add(this.label25);
			this.groupBoxPuzzle5.Controls.Add(this.label26);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark1Y5);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark1X5);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark1MeasureY5);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark1MeasureX5);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark2Y5);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark2X5);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark2MeasureY5);
			this.groupBoxPuzzle5.Controls.Add(this.textBoxMark2MeasureX5);
			this.groupBoxPuzzle5.Name = "groupBoxPuzzle5";
			this.groupBoxPuzzle5.TabStop = false;
			resources.ApplyResources(this.buttonMoveTo10, "buttonMoveTo10");
			this.buttonMoveTo10.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo10.Name = "buttonMoveTo10";
			this.buttonMoveTo10.UseVisualStyleBackColor = true;
			this.buttonMoveTo10.Click += new EventHandler(this.buttonMoveTo10_Click);
			resources.ApplyResources(this.buttonMoveTo9, "buttonMoveTo9");
			this.buttonMoveTo9.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo9.Name = "buttonMoveTo9";
			this.buttonMoveTo9.UseVisualStyleBackColor = true;
			this.buttonMoveTo9.Click += new EventHandler(this.buttonMoveTo9_Click);
			resources.ApplyResources(this.buttonGetCur10, "buttonGetCur10");
			this.buttonGetCur10.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur10.Name = "buttonGetCur10";
			this.buttonGetCur10.UseVisualStyleBackColor = true;
			this.buttonGetCur10.Click += new EventHandler(this.buttonGetCur10_Click);
			resources.ApplyResources(this.buttonGetCur9, "buttonGetCur9");
			this.buttonGetCur9.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur9.Name = "buttonGetCur9";
			this.buttonGetCur9.UseVisualStyleBackColor = true;
			this.buttonGetCur9.Click += new EventHandler(this.buttonGetCur9_Click);
			resources.ApplyResources(this.checkBoxMark2PCB5, "checkBoxMark2PCB5");
			this.checkBoxMark2PCB5.Checked = true;
			this.checkBoxMark2PCB5.CheckState = CheckState.Checked;
			this.checkBoxMark2PCB5.Name = "checkBoxMark2PCB5";
			this.checkBoxMark2PCB5.UseVisualStyleBackColor = true;
			this.checkBoxMark2PCB5.CheckedChanged += new EventHandler(this.checkBoxMark2PCB5_CheckedChanged);
			resources.ApplyResources(this.checkBoxMark1PCB5, "checkBoxMark1PCB5");
			this.checkBoxMark1PCB5.Checked = true;
			this.checkBoxMark1PCB5.CheckState = CheckState.Checked;
			this.checkBoxMark1PCB5.Name = "checkBoxMark1PCB5";
			this.checkBoxMark1PCB5.UseVisualStyleBackColor = true;
			this.checkBoxMark1PCB5.CheckedChanged += new EventHandler(this.checkBoxMark1PCB5_CheckedChanged);
			resources.ApplyResources(this.label22, "label22");
			this.label22.Name = "label22";
			resources.ApplyResources(this.label23, "label23");
			this.label23.Name = "label23";
			resources.ApplyResources(this.label24, "label24");
			this.label24.Name = "label24";
			resources.ApplyResources(this.label25, "label25");
			this.label25.Name = "label25";
			resources.ApplyResources(this.label26, "label26");
			this.label26.Name = "label26";
			resources.ApplyResources(this.textBoxMark1Y5, "textBoxMark1Y5");
			this.textBoxMark1Y5.Name = "textBoxMark1Y5";
			this.textBoxMark1Y5.Leave += new EventHandler(this.textBoxMark1Y_Leave);
			resources.ApplyResources(this.textBoxMark1X5, "textBoxMark1X5");
			this.textBoxMark1X5.Name = "textBoxMark1X5";
			this.textBoxMark1X5.Leave += new EventHandler(this.textBoxMark1X_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureY5, "textBoxMark1MeasureY5");
			this.textBoxMark1MeasureY5.Name = "textBoxMark1MeasureY5";
			this.textBoxMark1MeasureY5.Leave += new EventHandler(this.textBoxMark1MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureX5, "textBoxMark1MeasureX5");
			this.textBoxMark1MeasureX5.Name = "textBoxMark1MeasureX5";
			this.textBoxMark1MeasureX5.Leave += new EventHandler(this.textBoxMark1MeasureX_Leave);
			resources.ApplyResources(this.textBoxMark2Y5, "textBoxMark2Y5");
			this.textBoxMark2Y5.Name = "textBoxMark2Y5";
			this.textBoxMark2Y5.Leave += new EventHandler(this.textBoxMark2Y_Leave);
			resources.ApplyResources(this.textBoxMark2X5, "textBoxMark2X5");
			this.textBoxMark2X5.Name = "textBoxMark2X5";
			this.textBoxMark2X5.Leave += new EventHandler(this.textBoxMark2X_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureY5, "textBoxMark2MeasureY5");
			this.textBoxMark2MeasureY5.Name = "textBoxMark2MeasureY5";
			this.textBoxMark2MeasureY5.Leave += new EventHandler(this.textBoxMark2MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureX5, "textBoxMark2MeasureX5");
			this.textBoxMark2MeasureX5.Name = "textBoxMark2MeasureX5";
			this.textBoxMark2MeasureX5.Leave += new EventHandler(this.textBoxMark2MeasureX_Leave);
			resources.ApplyResources(this.groupBoxPuzzle4, "groupBoxPuzzle4");
			this.groupBoxPuzzle4.Controls.Add(this.buttonMoveTo8);
			this.groupBoxPuzzle4.Controls.Add(this.buttonMoveTo7);
			this.groupBoxPuzzle4.Controls.Add(this.buttonGetCur8);
			this.groupBoxPuzzle4.Controls.Add(this.buttonGetCur7);
			this.groupBoxPuzzle4.Controls.Add(this.checkBoxMark2PCB4);
			this.groupBoxPuzzle4.Controls.Add(this.checkBoxMark1PCB4);
			this.groupBoxPuzzle4.Controls.Add(this.label17);
			this.groupBoxPuzzle4.Controls.Add(this.label18);
			this.groupBoxPuzzle4.Controls.Add(this.label19);
			this.groupBoxPuzzle4.Controls.Add(this.label20);
			this.groupBoxPuzzle4.Controls.Add(this.label21);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark1Y4);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark1X4);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark1MeasureY4);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark1MeasureX4);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark2Y4);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark2X4);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark2MeasureY4);
			this.groupBoxPuzzle4.Controls.Add(this.textBoxMark2MeasureX4);
			this.groupBoxPuzzle4.Name = "groupBoxPuzzle4";
			this.groupBoxPuzzle4.TabStop = false;
			resources.ApplyResources(this.buttonMoveTo8, "buttonMoveTo8");
			this.buttonMoveTo8.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo8.Name = "buttonMoveTo8";
			this.buttonMoveTo8.UseVisualStyleBackColor = true;
			this.buttonMoveTo8.Click += new EventHandler(this.buttonMoveTo8_Click);
			resources.ApplyResources(this.buttonMoveTo7, "buttonMoveTo7");
			this.buttonMoveTo7.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo7.Name = "buttonMoveTo7";
			this.buttonMoveTo7.UseVisualStyleBackColor = true;
			this.buttonMoveTo7.Click += new EventHandler(this.buttonMoveTo7_Click);
			resources.ApplyResources(this.buttonGetCur8, "buttonGetCur8");
			this.buttonGetCur8.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur8.Name = "buttonGetCur8";
			this.buttonGetCur8.UseVisualStyleBackColor = true;
			this.buttonGetCur8.Click += new EventHandler(this.buttonGetCur8_Click);
			resources.ApplyResources(this.buttonGetCur7, "buttonGetCur7");
			this.buttonGetCur7.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur7.Name = "buttonGetCur7";
			this.buttonGetCur7.UseVisualStyleBackColor = true;
			this.buttonGetCur7.Click += new EventHandler(this.buttonGetCur7_Click);
			resources.ApplyResources(this.checkBoxMark2PCB4, "checkBoxMark2PCB4");
			this.checkBoxMark2PCB4.Checked = true;
			this.checkBoxMark2PCB4.CheckState = CheckState.Checked;
			this.checkBoxMark2PCB4.Name = "checkBoxMark2PCB4";
			this.checkBoxMark2PCB4.UseVisualStyleBackColor = true;
			this.checkBoxMark2PCB4.CheckedChanged += new EventHandler(this.checkBoxMark2PCB4_CheckedChanged);
			resources.ApplyResources(this.checkBoxMark1PCB4, "checkBoxMark1PCB4");
			this.checkBoxMark1PCB4.Checked = true;
			this.checkBoxMark1PCB4.CheckState = CheckState.Checked;
			this.checkBoxMark1PCB4.Name = "checkBoxMark1PCB4";
			this.checkBoxMark1PCB4.UseVisualStyleBackColor = true;
			this.checkBoxMark1PCB4.CheckedChanged += new EventHandler(this.checkBoxMark1PCB4_CheckedChanged);
			resources.ApplyResources(this.label17, "label17");
			this.label17.Name = "label17";
			resources.ApplyResources(this.label18, "label18");
			this.label18.Name = "label18";
			resources.ApplyResources(this.label19, "label19");
			this.label19.Name = "label19";
			resources.ApplyResources(this.label20, "label20");
			this.label20.Name = "label20";
			resources.ApplyResources(this.label21, "label21");
			this.label21.Name = "label21";
			resources.ApplyResources(this.textBoxMark1Y4, "textBoxMark1Y4");
			this.textBoxMark1Y4.Name = "textBoxMark1Y4";
			this.textBoxMark1Y4.Leave += new EventHandler(this.textBoxMark1Y_Leave);
			resources.ApplyResources(this.textBoxMark1X4, "textBoxMark1X4");
			this.textBoxMark1X4.Name = "textBoxMark1X4";
			this.textBoxMark1X4.Leave += new EventHandler(this.textBoxMark1X_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureY4, "textBoxMark1MeasureY4");
			this.textBoxMark1MeasureY4.Name = "textBoxMark1MeasureY4";
			this.textBoxMark1MeasureY4.Leave += new EventHandler(this.textBoxMark1MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureX4, "textBoxMark1MeasureX4");
			this.textBoxMark1MeasureX4.Name = "textBoxMark1MeasureX4";
			this.textBoxMark1MeasureX4.Leave += new EventHandler(this.textBoxMark1MeasureX_Leave);
			resources.ApplyResources(this.textBoxMark2Y4, "textBoxMark2Y4");
			this.textBoxMark2Y4.Name = "textBoxMark2Y4";
			this.textBoxMark2Y4.Leave += new EventHandler(this.textBoxMark2Y_Leave);
			resources.ApplyResources(this.textBoxMark2X4, "textBoxMark2X4");
			this.textBoxMark2X4.Name = "textBoxMark2X4";
			this.textBoxMark2X4.Leave += new EventHandler(this.textBoxMark2X_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureY4, "textBoxMark2MeasureY4");
			this.textBoxMark2MeasureY4.Name = "textBoxMark2MeasureY4";
			this.textBoxMark2MeasureY4.Leave += new EventHandler(this.textBoxMark2MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureX4, "textBoxMark2MeasureX4");
			this.textBoxMark2MeasureX4.Name = "textBoxMark2MeasureX4";
			this.textBoxMark2MeasureX4.Leave += new EventHandler(this.textBoxMark2MeasureX_Leave);
			resources.ApplyResources(this.groupBoxPuzzle3, "groupBoxPuzzle3");
			this.groupBoxPuzzle3.Controls.Add(this.buttonMoveTo6);
			this.groupBoxPuzzle3.Controls.Add(this.buttonMoveTo5);
			this.groupBoxPuzzle3.Controls.Add(this.buttonGetCur6);
			this.groupBoxPuzzle3.Controls.Add(this.buttonGetCur5);
			this.groupBoxPuzzle3.Controls.Add(this.checkBoxMark2PCB3);
			this.groupBoxPuzzle3.Controls.Add(this.checkBoxMark1PCB3);
			this.groupBoxPuzzle3.Controls.Add(this.label12);
			this.groupBoxPuzzle3.Controls.Add(this.label13);
			this.groupBoxPuzzle3.Controls.Add(this.label14);
			this.groupBoxPuzzle3.Controls.Add(this.label15);
			this.groupBoxPuzzle3.Controls.Add(this.label16);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark1Y3);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark1X3);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark1MeasureY3);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark1MeasureX3);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark2Y3);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark2X3);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark2MeasureY3);
			this.groupBoxPuzzle3.Controls.Add(this.textBoxMark2MeasureX3);
			this.groupBoxPuzzle3.Name = "groupBoxPuzzle3";
			this.groupBoxPuzzle3.TabStop = false;
			resources.ApplyResources(this.buttonMoveTo6, "buttonMoveTo6");
			this.buttonMoveTo6.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo6.Name = "buttonMoveTo6";
			this.buttonMoveTo6.UseVisualStyleBackColor = true;
			this.buttonMoveTo6.Click += new EventHandler(this.buttonMoveTo6_Click);
			resources.ApplyResources(this.buttonMoveTo5, "buttonMoveTo5");
			this.buttonMoveTo5.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo5.Name = "buttonMoveTo5";
			this.buttonMoveTo5.UseVisualStyleBackColor = true;
			this.buttonMoveTo5.Click += new EventHandler(this.buttonMoveTo5_Click);
			resources.ApplyResources(this.buttonGetCur6, "buttonGetCur6");
			this.buttonGetCur6.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur6.Name = "buttonGetCur6";
			this.buttonGetCur6.UseVisualStyleBackColor = true;
			this.buttonGetCur6.Click += new EventHandler(this.buttonGetCur6_Click);
			resources.ApplyResources(this.buttonGetCur5, "buttonGetCur5");
			this.buttonGetCur5.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur5.Name = "buttonGetCur5";
			this.buttonGetCur5.UseVisualStyleBackColor = true;
			this.buttonGetCur5.Click += new EventHandler(this.buttonGetCur5_Click);
			resources.ApplyResources(this.checkBoxMark2PCB3, "checkBoxMark2PCB3");
			this.checkBoxMark2PCB3.Checked = true;
			this.checkBoxMark2PCB3.CheckState = CheckState.Checked;
			this.checkBoxMark2PCB3.Name = "checkBoxMark2PCB3";
			this.checkBoxMark2PCB3.UseVisualStyleBackColor = true;
			this.checkBoxMark2PCB3.CheckedChanged += new EventHandler(this.checkBoxMark2PCB3_CheckedChanged);
			resources.ApplyResources(this.checkBoxMark1PCB3, "checkBoxMark1PCB3");
			this.checkBoxMark1PCB3.Checked = true;
			this.checkBoxMark1PCB3.CheckState = CheckState.Checked;
			this.checkBoxMark1PCB3.Name = "checkBoxMark1PCB3";
			this.checkBoxMark1PCB3.UseVisualStyleBackColor = true;
			this.checkBoxMark1PCB3.CheckedChanged += new EventHandler(this.checkBoxMark1PCB3_CheckedChanged);
			resources.ApplyResources(this.label12, "label12");
			this.label12.Name = "label12";
			resources.ApplyResources(this.label13, "label13");
			this.label13.Name = "label13";
			resources.ApplyResources(this.label14, "label14");
			this.label14.Name = "label14";
			resources.ApplyResources(this.label15, "label15");
			this.label15.Name = "label15";
			resources.ApplyResources(this.label16, "label16");
			this.label16.Name = "label16";
			resources.ApplyResources(this.textBoxMark1Y3, "textBoxMark1Y3");
			this.textBoxMark1Y3.Name = "textBoxMark1Y3";
			this.textBoxMark1Y3.Leave += new EventHandler(this.textBoxMark1Y_Leave);
			resources.ApplyResources(this.textBoxMark1X3, "textBoxMark1X3");
			this.textBoxMark1X3.Name = "textBoxMark1X3";
			this.textBoxMark1X3.Leave += new EventHandler(this.textBoxMark1X_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureY3, "textBoxMark1MeasureY3");
			this.textBoxMark1MeasureY3.Name = "textBoxMark1MeasureY3";
			this.textBoxMark1MeasureY3.Leave += new EventHandler(this.textBoxMark1MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureX3, "textBoxMark1MeasureX3");
			this.textBoxMark1MeasureX3.Name = "textBoxMark1MeasureX3";
			this.textBoxMark1MeasureX3.Leave += new EventHandler(this.textBoxMark1MeasureX_Leave);
			resources.ApplyResources(this.textBoxMark2Y3, "textBoxMark2Y3");
			this.textBoxMark2Y3.Name = "textBoxMark2Y3";
			this.textBoxMark2Y3.Leave += new EventHandler(this.textBoxMark2Y_Leave);
			resources.ApplyResources(this.textBoxMark2X3, "textBoxMark2X3");
			this.textBoxMark2X3.Name = "textBoxMark2X3";
			this.textBoxMark2X3.Leave += new EventHandler(this.textBoxMark2X_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureY3, "textBoxMark2MeasureY3");
			this.textBoxMark2MeasureY3.Name = "textBoxMark2MeasureY3";
			this.textBoxMark2MeasureY3.Leave += new EventHandler(this.textBoxMark2MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureX3, "textBoxMark2MeasureX3");
			this.textBoxMark2MeasureX3.Name = "textBoxMark2MeasureX3";
			this.textBoxMark2MeasureX3.Leave += new EventHandler(this.textBoxMark2MeasureX_Leave);
			resources.ApplyResources(this.groupBoxPuzzle2, "groupBoxPuzzle2");
			this.groupBoxPuzzle2.Controls.Add(this.buttonMoveTo4);
			this.groupBoxPuzzle2.Controls.Add(this.checkBoxMark2PCB2);
			this.groupBoxPuzzle2.Controls.Add(this.buttonMoveTo3);
			this.groupBoxPuzzle2.Controls.Add(this.buttonGetCur4);
			this.groupBoxPuzzle2.Controls.Add(this.checkBoxMark1PCB2);
			this.groupBoxPuzzle2.Controls.Add(this.buttonGetCur3);
			this.groupBoxPuzzle2.Controls.Add(this.label1);
			this.groupBoxPuzzle2.Controls.Add(this.label2);
			this.groupBoxPuzzle2.Controls.Add(this.label3);
			this.groupBoxPuzzle2.Controls.Add(this.label8);
			this.groupBoxPuzzle2.Controls.Add(this.label10);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark1Y2);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark1X2);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark1MeasureY2);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark1MeasureX2);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark2Y2);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark2X2);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark2MeasureY2);
			this.groupBoxPuzzle2.Controls.Add(this.textBoxMark2MeasureX2);
			this.groupBoxPuzzle2.Name = "groupBoxPuzzle2";
			this.groupBoxPuzzle2.TabStop = false;
			resources.ApplyResources(this.buttonMoveTo4, "buttonMoveTo4");
			this.buttonMoveTo4.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo4.Name = "buttonMoveTo4";
			this.buttonMoveTo4.UseVisualStyleBackColor = true;
			this.buttonMoveTo4.Click += new EventHandler(this.buttonMoveTo4_Click);
			resources.ApplyResources(this.checkBoxMark2PCB2, "checkBoxMark2PCB2");
			this.checkBoxMark2PCB2.Checked = true;
			this.checkBoxMark2PCB2.CheckState = CheckState.Checked;
			this.checkBoxMark2PCB2.Name = "checkBoxMark2PCB2";
			this.checkBoxMark2PCB2.UseVisualStyleBackColor = true;
			this.checkBoxMark2PCB2.CheckedChanged += new EventHandler(this.checkBoxMark2PCB2_CheckedChanged);
			resources.ApplyResources(this.buttonMoveTo3, "buttonMoveTo3");
			this.buttonMoveTo3.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo3.Name = "buttonMoveTo3";
			this.buttonMoveTo3.UseVisualStyleBackColor = true;
			this.buttonMoveTo3.Click += new EventHandler(this.buttonMoveTo3_Click);
			resources.ApplyResources(this.buttonGetCur4, "buttonGetCur4");
			this.buttonGetCur4.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur4.Name = "buttonGetCur4";
			this.buttonGetCur4.UseVisualStyleBackColor = true;
			this.buttonGetCur4.Click += new EventHandler(this.buttonGetCur4_Click);
			resources.ApplyResources(this.checkBoxMark1PCB2, "checkBoxMark1PCB2");
			this.checkBoxMark1PCB2.Checked = true;
			this.checkBoxMark1PCB2.CheckState = CheckState.Checked;
			this.checkBoxMark1PCB2.Name = "checkBoxMark1PCB2";
			this.checkBoxMark1PCB2.UseVisualStyleBackColor = true;
			this.checkBoxMark1PCB2.CheckedChanged += new EventHandler(this.checkBoxMark1PCB2_CheckedChanged);
			resources.ApplyResources(this.buttonGetCur3, "buttonGetCur3");
			this.buttonGetCur3.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur3.Name = "buttonGetCur3";
			this.buttonGetCur3.UseVisualStyleBackColor = true;
			this.buttonGetCur3.Click += new EventHandler(this.buttonGetCur3_Click);
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			resources.ApplyResources(this.label3, "label3");
			this.label3.Name = "label3";
			resources.ApplyResources(this.label8, "label8");
			this.label8.Name = "label8";
			resources.ApplyResources(this.label10, "label10");
			this.label10.Name = "label10";
			resources.ApplyResources(this.textBoxMark1Y2, "textBoxMark1Y2");
			this.textBoxMark1Y2.Name = "textBoxMark1Y2";
			this.textBoxMark1Y2.Leave += new EventHandler(this.textBoxMark1Y_Leave);
			resources.ApplyResources(this.textBoxMark1X2, "textBoxMark1X2");
			this.textBoxMark1X2.Name = "textBoxMark1X2";
			this.textBoxMark1X2.Leave += new EventHandler(this.textBoxMark1X_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureY2, "textBoxMark1MeasureY2");
			this.textBoxMark1MeasureY2.Name = "textBoxMark1MeasureY2";
			this.textBoxMark1MeasureY2.Leave += new EventHandler(this.textBoxMark1MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureX2, "textBoxMark1MeasureX2");
			this.textBoxMark1MeasureX2.Name = "textBoxMark1MeasureX2";
			this.textBoxMark1MeasureX2.Leave += new EventHandler(this.textBoxMark1MeasureX_Leave);
			resources.ApplyResources(this.textBoxMark2Y2, "textBoxMark2Y2");
			this.textBoxMark2Y2.Name = "textBoxMark2Y2";
			this.textBoxMark2Y2.Leave += new EventHandler(this.textBoxMark2Y_Leave);
			resources.ApplyResources(this.textBoxMark2X2, "textBoxMark2X2");
			this.textBoxMark2X2.Name = "textBoxMark2X2";
			this.textBoxMark2X2.Leave += new EventHandler(this.textBoxMark2X_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureY2, "textBoxMark2MeasureY2");
			this.textBoxMark2MeasureY2.Name = "textBoxMark2MeasureY2";
			this.textBoxMark2MeasureY2.Leave += new EventHandler(this.textBoxMark2MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureX2, "textBoxMark2MeasureX2");
			this.textBoxMark2MeasureX2.Name = "textBoxMark2MeasureX2";
			this.textBoxMark2MeasureX2.Leave += new EventHandler(this.textBoxMark2MeasureX_Leave);
			resources.ApplyResources(this.groupBoxPuzzle1, "groupBoxPuzzle1");
			this.groupBoxPuzzle1.Controls.Add(this.buttonMoveTo2);
			this.groupBoxPuzzle1.Controls.Add(this.buttonMoveTo1);
			this.groupBoxPuzzle1.Controls.Add(this.buttonGetCur2);
			this.groupBoxPuzzle1.Controls.Add(this.checkBoxMark2PCB1);
			this.groupBoxPuzzle1.Controls.Add(this.checkBoxMark1PCB1);
			this.groupBoxPuzzle1.Controls.Add(this.label7);
			this.groupBoxPuzzle1.Controls.Add(this.label9);
			this.groupBoxPuzzle1.Controls.Add(this.label6);
			this.groupBoxPuzzle1.Controls.Add(this.label5);
			this.groupBoxPuzzle1.Controls.Add(this.label4);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark1Y1);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark1X1);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark1MeasureY1);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark1MeasureX1);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark2Y1);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark2X1);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark2MeasureY1);
			this.groupBoxPuzzle1.Controls.Add(this.textBoxMark2MeasureX1);
			this.groupBoxPuzzle1.Controls.Add(this.buttonGetCur1);
			this.groupBoxPuzzle1.Name = "groupBoxPuzzle1";
			this.groupBoxPuzzle1.TabStop = false;
			resources.ApplyResources(this.buttonMoveTo2, "buttonMoveTo2");
			this.buttonMoveTo2.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo2.Name = "buttonMoveTo2";
			this.buttonMoveTo2.UseVisualStyleBackColor = true;
			this.buttonMoveTo2.Click += new EventHandler(this.buttonMoveTo2_Click);
			resources.ApplyResources(this.buttonMoveTo1, "buttonMoveTo1");
			this.buttonMoveTo1.BackgroundImage = Resource.ButtonUp;
			this.buttonMoveTo1.Name = "buttonMoveTo1";
			this.buttonMoveTo1.UseVisualStyleBackColor = true;
			this.buttonMoveTo1.Click += new EventHandler(this.buttonMoveTo1_Click);
			resources.ApplyResources(this.buttonGetCur2, "buttonGetCur2");
			this.buttonGetCur2.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur2.Name = "buttonGetCur2";
			this.buttonGetCur2.UseVisualStyleBackColor = true;
			this.buttonGetCur2.Click += new EventHandler(this.buttonGetCur2_Click);
			resources.ApplyResources(this.checkBoxMark2PCB1, "checkBoxMark2PCB1");
			this.checkBoxMark2PCB1.Checked = true;
			this.checkBoxMark2PCB1.CheckState = CheckState.Checked;
			this.checkBoxMark2PCB1.Name = "checkBoxMark2PCB1";
			this.checkBoxMark2PCB1.UseVisualStyleBackColor = true;
			this.checkBoxMark2PCB1.CheckedChanged += new EventHandler(this.checkBoxMark2PCB1_CheckedChanged);
			resources.ApplyResources(this.checkBoxMark1PCB1, "checkBoxMark1PCB1");
			this.checkBoxMark1PCB1.Checked = true;
			this.checkBoxMark1PCB1.CheckState = CheckState.Checked;
			this.checkBoxMark1PCB1.Name = "checkBoxMark1PCB1";
			this.checkBoxMark1PCB1.UseVisualStyleBackColor = true;
			this.checkBoxMark1PCB1.CheckedChanged += new EventHandler(this.checkBoxMark1PCB1_CheckedChanged);
			resources.ApplyResources(this.label7, "label7");
			this.label7.Name = "label7";
			resources.ApplyResources(this.label9, "label9");
			this.label9.Name = "label9";
			resources.ApplyResources(this.label6, "label6");
			this.label6.Name = "label6";
			resources.ApplyResources(this.label5, "label5");
			this.label5.Name = "label5";
			resources.ApplyResources(this.label4, "label4");
			this.label4.Name = "label4";
			resources.ApplyResources(this.textBoxMark1MeasureY1, "textBoxMark1MeasureY1");
			this.textBoxMark1MeasureY1.Name = "textBoxMark1MeasureY1";
			this.textBoxMark1MeasureY1.Leave += new EventHandler(this.textBoxMark1MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark1MeasureX1, "textBoxMark1MeasureX1");
			this.textBoxMark1MeasureX1.Name = "textBoxMark1MeasureX1";
			this.textBoxMark1MeasureX1.Leave += new EventHandler(this.textBoxMark1MeasureX_Leave);
			resources.ApplyResources(this.textBoxMark2Y1, "textBoxMark2Y1");
			this.textBoxMark2Y1.Name = "textBoxMark2Y1";
			this.textBoxMark2Y1.Leave += new EventHandler(this.textBoxMark2Y_Leave);
			resources.ApplyResources(this.textBoxMark2X1, "textBoxMark2X1");
			this.textBoxMark2X1.Name = "textBoxMark2X1";
			this.textBoxMark2X1.Leave += new EventHandler(this.textBoxMark2X_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureY1, "textBoxMark2MeasureY1");
			this.textBoxMark2MeasureY1.Name = "textBoxMark2MeasureY1";
			this.textBoxMark2MeasureY1.Leave += new EventHandler(this.textBoxMark2MeasureY_Leave);
			resources.ApplyResources(this.textBoxMark2MeasureX1, "textBoxMark2MeasureX1");
			this.textBoxMark2MeasureX1.Name = "textBoxMark2MeasureX1";
			this.textBoxMark2MeasureX1.Leave += new EventHandler(this.textBoxMark2MeasureX_Leave);
			resources.ApplyResources(this.buttonGetCur1, "buttonGetCur1");
			this.buttonGetCur1.BackgroundImage = Resource.ButtonUp;
			this.buttonGetCur1.Name = "buttonGetCur1";
			this.buttonGetCur1.UseVisualStyleBackColor = true;
			this.buttonGetCur1.Click += new EventHandler(this.buttonGetCur1_Click);
			resources.ApplyResources(this.buttonUp, "buttonUp");
			this.buttonUp.BackgroundImage = Resource.ButtonUp;
			this.buttonUp.Name = "buttonUp";
			this.buttonUp.UseVisualStyleBackColor = true;
			this.buttonUp.Click += new EventHandler(this.buttonUp_Click);
			resources.ApplyResources(this.buttonDown, "buttonDown");
			this.buttonDown.BackgroundImage = Resource.ButtonUp;
			this.buttonDown.Name = "buttonDown";
			this.buttonDown.UseVisualStyleBackColor = true;
			this.buttonDown.Click += new EventHandler(this.buttonDown_Click);
			resources.ApplyResources(this.groupBox2, "groupBox2");
			this.groupBox2.Controls.Add(this.textBoxPCBThickness);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.TabStop = false;
			resources.ApplyResources(this.label11, "label11");
			this.label11.Name = "label11";
			resources.ApplyResources(this.buttonReset, "buttonReset");
			this.buttonReset.BackgroundImage = Resource.ButtonUp;
			this.buttonReset.Name = "buttonReset";
			this.buttonReset.UseVisualStyleBackColor = true;
			this.buttonReset.Click += new EventHandler(this.buttonReset_Click);
			resources.ApplyResources(this.buttonCancel, "buttonCancel");
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			resources.ApplyResources(this.buttonSave, "buttonSave");
			this.buttonSave.BackgroundImage = Resource.ButtonUp;
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new EventHandler(this.buttonSave_Click);
			resources.ApplyResources(this.groupBox3, "groupBox3");
			this.groupBox3.Controls.Add(this.comboBoxExposure);
			this.groupBox3.Controls.Add(this.label29);
			this.groupBox3.Controls.Add(this.checkBoxAutoPause);
			this.groupBox3.Controls.Add(this.label28);
			this.groupBox3.Controls.Add(this.radioButtonIrregularMark);
			this.groupBox3.Controls.Add(this.radioButtonRectangleMark);
			this.groupBox3.Controls.Add(this.label27);
			this.groupBox3.Controls.Add(this.radioButtonRoundMark);
			this.groupBox3.Controls.Add(this.radioButtonNoMark);
			this.groupBox3.Controls.Add(this.radioButtonManualMark);
			this.groupBox3.Controls.Add(this.radioButtonAutoMark);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.TabStop = false;
			resources.ApplyResources(this.comboBoxExposure, "comboBoxExposure");
			this.comboBoxExposure.DropDownStyle = ComboBoxStyle.DropDownList;
			this.comboBoxExposure.Items.AddRange(new object[]
			{
				resources.GetString("comboBoxExposure.Items"),
				resources.GetString("comboBoxExposure.Items1"),
				resources.GetString("comboBoxExposure.Items2"),
				resources.GetString("comboBoxExposure.Items3"),
				resources.GetString("comboBoxExposure.Items4"),
				resources.GetString("comboBoxExposure.Items5"),
				resources.GetString("comboBoxExposure.Items6"),
				resources.GetString("comboBoxExposure.Items7"),
				resources.GetString("comboBoxExposure.Items8")
			});
			this.comboBoxExposure.Name = "comboBoxExposure";
			this.comboBoxExposure.SelectedIndexChanged += new EventHandler(this.comboBoxExposure_SelectedIndexChanged);
			resources.ApplyResources(this.label29, "label29");
			this.label29.Name = "label29";
			resources.ApplyResources(this.checkBoxAutoPause, "checkBoxAutoPause");
			this.checkBoxAutoPause.Name = "checkBoxAutoPause";
			this.checkBoxAutoPause.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.label28, "label28");
			this.label28.Name = "label28";
			resources.ApplyResources(this.radioButtonIrregularMark, "radioButtonIrregularMark");
			this.radioButtonIrregularMark.AutoCheck = false;
			this.radioButtonIrregularMark.Name = "radioButtonIrregularMark";
			this.radioButtonIrregularMark.TabStop = true;
			this.radioButtonIrregularMark.UseVisualStyleBackColor = true;
			this.radioButtonIrregularMark.Click += new EventHandler(this.radioButtonIrregularMark_Click);
			resources.ApplyResources(this.radioButtonRectangleMark, "radioButtonRectangleMark");
			this.radioButtonRectangleMark.AutoCheck = false;
			this.radioButtonRectangleMark.Name = "radioButtonRectangleMark";
			this.radioButtonRectangleMark.TabStop = true;
			this.radioButtonRectangleMark.UseVisualStyleBackColor = true;
			this.radioButtonRectangleMark.Click += new EventHandler(this.radioButtonRectangleMark_Click);
			resources.ApplyResources(this.label27, "label27");
			this.label27.Name = "label27";
			resources.ApplyResources(this.radioButtonRoundMark, "radioButtonRoundMark");
			this.radioButtonRoundMark.AutoCheck = false;
			this.radioButtonRoundMark.Name = "radioButtonRoundMark";
			this.radioButtonRoundMark.TabStop = true;
			this.radioButtonRoundMark.UseVisualStyleBackColor = true;
			this.radioButtonRoundMark.Click += new EventHandler(this.radioButtonRoundMark_Click);
			resources.ApplyResources(this.radioButtonNoMark, "radioButtonNoMark");
			this.radioButtonNoMark.AutoCheck = false;
			this.radioButtonNoMark.Name = "radioButtonNoMark";
			this.radioButtonNoMark.TabStop = true;
			this.radioButtonNoMark.UseVisualStyleBackColor = true;
			this.radioButtonNoMark.Click += new EventHandler(this.radioButtonNoMark_Click);
			resources.ApplyResources(this.radioButtonManualMark, "radioButtonManualMark");
			this.radioButtonManualMark.AutoCheck = false;
			this.radioButtonManualMark.Name = "radioButtonManualMark";
			this.radioButtonManualMark.TabStop = true;
			this.radioButtonManualMark.UseVisualStyleBackColor = true;
			this.radioButtonManualMark.Click += new EventHandler(this.radioButtonManualMark_Click);
			resources.ApplyResources(this.radioButtonAutoMark, "radioButtonAutoMark");
			this.radioButtonAutoMark.AutoCheck = false;
			this.radioButtonAutoMark.Name = "radioButtonAutoMark";
			this.radioButtonAutoMark.TabStop = true;
			this.radioButtonAutoMark.UseVisualStyleBackColor = true;
			this.radioButtonAutoMark.Click += new EventHandler(this.radioButtonAutoMark_Click);
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = SystemColors.Control;
			base.Controls.Add(this.groupBox3);
			base.Controls.Add(this.groupBox2);
			base.Controls.Add(this.groupBox1);
			base.Controls.Add(this.buttonReset);
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonSave);
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "PCBEdit";
			base.ShowIcon = false;
			base.FormClosed += new FormClosedEventHandler(this.PCBEdit_FormClosed);
			this.groupBox1.ResumeLayout(false);
			this.groupBoxPuzzle5.ResumeLayout(false);
			this.groupBoxPuzzle5.PerformLayout();
			this.groupBoxPuzzle4.ResumeLayout(false);
			this.groupBoxPuzzle4.PerformLayout();
			this.groupBoxPuzzle3.ResumeLayout(false);
			this.groupBoxPuzzle3.PerformLayout();
			this.groupBoxPuzzle2.ResumeLayout(false);
			this.groupBoxPuzzle2.PerformLayout();
			this.groupBoxPuzzle1.ResumeLayout(false);
			this.groupBoxPuzzle1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			base.ResumeLayout(false);
		}
	}
}
