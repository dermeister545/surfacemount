using System;
using System.ComponentModel;
using System.Resources;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class SelectStackDialog : Form
	{
		private IContainer components;

		private Button buttonCancel;

		private Button buttonOK;

		private Label label2;

		private RadioButton radioButtonTop;

		private RadioButton radioButtonBottom;

		private RadioButton radioButtonUseAll;

		private RadioButton radioButtonUseSMD;

		private Label label1;

		private Panel panel1;

		private Panel panel2;

		private Panel panel3;

		private CheckBox checkBoxXMirror;

		private ResourceManager LocStrings = new ResourceManager(typeof(MyStrings));

		public bool IsWithTopLayer;

		public bool IsWithBottomLayer;

		public bool IsWithSMD;

		public bool UseTopLayer;

		public bool UseBottomLayer;

		public bool UseXMirror;

		public bool UseSMD;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager resources = new ComponentResourceManager(typeof(SelectStackDialog));
			this.buttonCancel = new Button();
			this.buttonOK = new Button();
			this.label2 = new Label();
			this.radioButtonTop = new RadioButton();
			this.radioButtonBottom = new RadioButton();
			this.radioButtonUseAll = new RadioButton();
			this.radioButtonUseSMD = new RadioButton();
			this.label1 = new Label();
			this.panel1 = new Panel();
			this.panel2 = new Panel();
			this.panel3 = new Panel();
			this.checkBoxXMirror = new CheckBox();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			base.SuspendLayout();
			resources.ApplyResources(this.buttonCancel, "buttonCancel");
			this.buttonCancel.BackgroundImage = Resource.ButtonUp;
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new EventHandler(this.buttonCancel_Click);
			resources.ApplyResources(this.buttonOK, "buttonOK");
			this.buttonOK.BackgroundImage = Resource.ButtonUp;
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.UseVisualStyleBackColor = true;
			this.buttonOK.Click += new EventHandler(this.buttonOK_Click);
			resources.ApplyResources(this.label2, "label2");
			this.label2.Name = "label2";
			resources.ApplyResources(this.radioButtonTop, "radioButtonTop");
			this.radioButtonTop.Name = "radioButtonTop";
			this.radioButtonTop.TabStop = true;
			this.radioButtonTop.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.radioButtonBottom, "radioButtonBottom");
			this.radioButtonBottom.Name = "radioButtonBottom";
			this.radioButtonBottom.TabStop = true;
			this.radioButtonBottom.UseVisualStyleBackColor = true;
			this.radioButtonBottom.CheckedChanged += new EventHandler(this.radioButtonBottom_CheckedChanged);
			resources.ApplyResources(this.radioButtonUseAll, "radioButtonUseAll");
			this.radioButtonUseAll.Name = "radioButtonUseAll";
			this.radioButtonUseAll.TabStop = true;
			this.radioButtonUseAll.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.radioButtonUseSMD, "radioButtonUseSMD");
			this.radioButtonUseSMD.Name = "radioButtonUseSMD";
			this.radioButtonUseSMD.TabStop = true;
			this.radioButtonUseSMD.UseVisualStyleBackColor = true;
			resources.ApplyResources(this.label1, "label1");
			this.label1.Name = "label1";
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.Controls.Add(this.radioButtonTop);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.radioButtonBottom);
			this.panel1.Name = "panel1";
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.Controls.Add(this.label1);
			this.panel2.Controls.Add(this.radioButtonUseSMD);
			this.panel2.Controls.Add(this.radioButtonUseAll);
			this.panel2.Name = "panel2";
			resources.ApplyResources(this.panel3, "panel3");
			this.panel3.Controls.Add(this.checkBoxXMirror);
			this.panel3.Name = "panel3";
			resources.ApplyResources(this.checkBoxXMirror, "checkBoxXMirror");
			this.checkBoxXMirror.Name = "checkBoxXMirror";
			this.checkBoxXMirror.UseVisualStyleBackColor = true;
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.panel3);
			base.Controls.Add(this.panel2);
			base.Controls.Add(this.panel1);
			base.Controls.Add(this.buttonCancel);
			base.Controls.Add(this.buttonOK);
			base.Name = "SelectStackDialog";
			base.ShowIcon = false;
			base.Load += new EventHandler(this.SelectStackDialog_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			base.ResumeLayout(false);
		}

		public SelectStackDialog()
		{
			this.InitializeComponent();
		}

		private void SelectStackDialog_Load(object sender, EventArgs e)
		{
			this.radioButtonTop.Enabled = this.IsWithTopLayer;
			this.radioButtonBottom.Enabled = this.IsWithBottomLayer;
			if (this.IsWithTopLayer)
			{
				this.radioButtonTop.Checked = true;
				this.radioButtonBottom.Checked = false;
				this.checkBoxXMirror.Enabled = false;
			}
			else if (this.IsWithBottomLayer)
			{
				this.radioButtonTop.Checked = false;
				this.radioButtonBottom.Checked = true;
				this.checkBoxXMirror.Enabled = true;
			}
			else
			{
				this.radioButtonTop.Checked = false;
				this.radioButtonBottom.Checked = false;
				this.checkBoxXMirror.Enabled = false;
			}
			if (this.IsWithSMD)
			{
				this.radioButtonUseSMD.Enabled = true;
				this.radioButtonUseAll.Enabled = true;
				this.radioButtonUseSMD.Checked = true;
				this.radioButtonUseAll.Checked = false;
				return;
			}
			this.radioButtonUseSMD.Enabled = false;
			this.radioButtonUseAll.Enabled = true;
			this.radioButtonUseSMD.Checked = false;
			this.radioButtonUseAll.Checked = true;
		}

		private void buttonOK_Click(object sender, EventArgs e)
		{
			if ((this.radioButtonTop.Enabled || this.radioButtonBottom.Enabled) && !this.radioButtonTop.Checked && !this.radioButtonBottom.Checked)
			{
				new MyMessageBox(null, this.LocStrings.GetString("mustSelectTopOrBottom"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			if (!this.radioButtonUseSMD.Checked && !this.radioButtonUseAll.Checked)
			{
				new MyMessageBox(null, this.LocStrings.GetString("mustSelectSMDOrTotal"), base.Location.X + base.Size.Width / 2, base.Location.Y + base.Size.Height / 2).Show();
				return;
			}
			this.UseTopLayer = this.radioButtonTop.Checked;
			this.UseBottomLayer = this.radioButtonBottom.Checked;
			this.UseXMirror = this.checkBoxXMirror.Checked;
			this.UseSMD = this.radioButtonUseSMD.Checked;
			base.DialogResult = DialogResult.OK;
			base.Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
			base.Close();
		}

		private void radioButtonBottom_CheckedChanged(object sender, EventArgs e)
		{
			if (this.radioButtonBottom.Checked)
			{
				this.checkBoxXMirror.Enabled = true;
				return;
			}
			this.checkBoxXMirror.Enabled = false;
			this.checkBoxXMirror.Checked = false;
		}
	}
}
