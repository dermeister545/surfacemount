using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SucfaceMount
{
	public class MyMessageBoxYesNo : Form
	{
		private IContainer components;

		private Panel panel1;

		private Label labelText;

		private Label labelMessage;

		private Panel panel2;

		public Button buttonYes;

		public Button buttonNo;

		private TcpControler controler;

		public string MessageText
		{
			get
			{
				return this.labelText.Text;
			}
			set
			{
				this.labelText.Text = value;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager resources = new ComponentResourceManager(typeof(MyMessageBoxYesNo));
			this.buttonYes = new Button();
			this.panel1 = new Panel();
			this.buttonNo = new Button();
			this.labelText = new Label();
			this.labelMessage = new Label();
			this.panel2 = new Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			base.SuspendLayout();
			resources.ApplyResources(this.buttonYes, "buttonYes");
			this.buttonYes.Name = "buttonYes";
			this.buttonYes.UseVisualStyleBackColor = true;
			this.buttonYes.Click += new EventHandler(this.buttonYes_Click);
			resources.ApplyResources(this.panel1, "panel1");
			this.panel1.BackColor = SystemColors.Control;
			this.panel1.BorderStyle = BorderStyle.FixedSingle;
			this.panel1.Controls.Add(this.buttonNo);
			this.panel1.Controls.Add(this.buttonYes);
			this.panel1.Controls.Add(this.labelText);
			this.panel1.Controls.Add(this.labelMessage);
			this.panel1.Name = "panel1";
			resources.ApplyResources(this.buttonNo, "buttonNo");
			this.buttonNo.Name = "buttonNo";
			this.buttonNo.UseVisualStyleBackColor = true;
			this.buttonNo.Click += new EventHandler(this.buttonNo_Click);
			resources.ApplyResources(this.labelText, "labelText");
			this.labelText.Name = "labelText";
			resources.ApplyResources(this.labelMessage, "labelMessage");
			this.labelMessage.Name = "labelMessage";
			resources.ApplyResources(this.panel2, "panel2");
			this.panel2.BorderStyle = BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.panel1);
			this.panel2.Name = "panel2";
			resources.ApplyResources(this, "$this");
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.Controls.Add(this.panel2);
			base.Name = "MyMessageBoxYesNo";
			base.ShowIcon = false;
			base.ShowInTaskbar = false;
			base.TopMost = true;
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		public MyMessageBoxYesNo()
		{
			this.InitializeComponent();
		}

		public MyMessageBoxYesNo(TcpControler c, string text, int x, int y) : this()
		{
			this.controler = c;
			this.MessageText = text;
			base.Location = new Point(x - base.Size.Width / 2, y - base.Size.Height / 2);
			base.DialogResult = DialogResult.No;
		}

		private void buttonYes_Click(object sender, EventArgs e)
		{
			if (this.controler != null && this.controler.DO_Buzzer)
			{
				this.controler.SetDO_OFF_IMM(2048u);
			}
			base.DialogResult = DialogResult.Yes;
			base.Close();
		}

		private void buttonNo_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.No;
			base.Close();
		}

		private void MyMessageBoxYesNo_Load(object sender, EventArgs e)
		{
			this.buttonYes.Focus();
		}
	}
}
