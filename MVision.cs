using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace MVisionControl
{
	public class MVision : UserControl
	{
		private const int MaxTempSize = 100;

		private IContainer components;

		private int camNum;

		private int answer;

		private int tempDia;

		private int matchCount;

		private double[] cirArray = new double[100];

		private volatile bool paraPosChanged;

		private volatile bool paraColChanged;

		private volatile bool paraWhtChanged;

		private double thresLevel;

		private double distLevel;

		private bool filterEn;

		private bool showPosEn;

		private int[,] colorRange = new int[5, 6];

		private double[] whiteRate = new double[4];

		private double lengthTolerance;

		private double angleTolerance;

		private int colorSelect;

		public double PixelScaleX1 = 1.0;

		public double PixelScaleY1 = 1.0;

		public double PixelScaleX2 = 1.0;

		public double PixelScaleY2 = 1.0;

		private int compSum;

		private double compWidth;

		private double compHeight;

		private double compOffsetW;

		private double compOffsetH;

		private double compOoffsetA;

		private int compThreshold;

		private double compAngle1;

		private double markVisionOffsetX;

		private double markVisionOffsetY;

		private int modeShape = -1;

		public bool HeartBeat;

		private int handle;

		private CheckVisionType CheckVision = CheckVisionType.ComponentVision;

		public Camera Camera1;

		public Camera Camera2;

		private string cKey1;

		private string cKey2;

		private Thread CheckCompThread;

		private volatile bool _CheckCompStop;

		private static Semaphore sem = new Semaphore(0, 1);

		private bool goDone = true;

		private IntPtr m_ip;

		public int CamNum
		{
			get
			{
				return this.camNum;
			}
			set
			{
				this.camNum = value;
			}
		}

		public int Answer
		{
			get
			{
				return this.answer;
			}
			set
			{
				this.answer = value;
			}
		}

		public int MatchCount
		{
			get
			{
				return this.matchCount;
			}
		}

		public int TempDia
		{
			get
			{
				return this.tempDia;
			}
			set
			{
				this.paraPosChanged = true;
				this.tempDia = value;
			}
		}

		public double[] CirArray
		{
			get
			{
				this.paraPosChanged = true;
				return this.cirArray;
			}
		}

		public double ThresLevel
		{
			get
			{
				return this.thresLevel;
			}
			set
			{
				this.thresLevel = value;
				MVision.SetThresLevel(this.thresLevel);
			}
		}

		public double DistLevel
		{
			get
			{
				return this.distLevel;
			}
			set
			{
				this.distLevel = value;
				MVision.SetDistLevel(this.distLevel);
			}
		}

		public bool FilterEn
		{
			get
			{
				return this.filterEn;
			}
			set
			{
				this.filterEn = value;
				MVision.SetFilterEn(this.filterEn);
			}
		}

		public bool ShowPosEn
		{
			get
			{
				return this.showPosEn;
			}
			set
			{
				this.showPosEn = value;
				MVision.SetShowPosEn(this.showPosEn);
			}
		}

		public int[,] ColorRange
		{
			get
			{
				this.paraColChanged = true;
				return this.colorRange;
			}
		}

		public double[] WhiteRate
		{
			get
			{
				this.paraWhtChanged = true;
				return this.whiteRate;
			}
		}

		public double LengthTolerance
		{
			get
			{
				return this.lengthTolerance;
			}
			set
			{
				this.lengthTolerance = value;
				MVision.SetLengthTolerance(this.lengthTolerance);
			}
		}

		public double AngleTolerance
		{
			get
			{
				return this.angleTolerance;
			}
			set
			{
				this.angleTolerance = value;
				MVision.SetAngleTolerance(this.angleTolerance);
			}
		}

		public int ColorSelect
		{
			get
			{
				return this.colorSelect;
			}
			set
			{
				this.colorSelect = value;
				MVision.SetColorSelect(this.colorSelect);
			}
		}

		public int CompSum
		{
			get
			{
				return this.compSum;
			}
			set
			{
				this.compSum = value;
			}
		}

		public double CompWidth1
		{
			get
			{
				return this.compWidth * this.PixelScaleX1;
			}
		}

		public double CompWidth2
		{
			get
			{
				return this.compWidth * this.PixelScaleX2;
			}
		}

		public double CompHeight1
		{
			get
			{
				return this.compHeight * this.PixelScaleY1;
			}
		}

		public double CompHeight2
		{
			get
			{
				return this.compHeight * this.PixelScaleY2;
			}
		}

		public double CompOffsetW1
		{
			get
			{
				return this.compOffsetW * this.PixelScaleX1;
			}
		}

		public double CompOffsetW2
		{
			get
			{
				return this.compOffsetW * this.PixelScaleX2;
			}
		}

		public double CompOffsetH1
		{
			get
			{
				return this.compOffsetH * this.PixelScaleY1;
			}
		}

		public double CompOffsetH2
		{
			get
			{
				return this.compOffsetH * this.PixelScaleY2;
			}
		}

		public double CompOffsetA
		{
			get
			{
				return this.compOoffsetA;
			}
		}

		public int CompThreshold
		{
			get
			{
				return this.compThreshold;
			}
			set
			{
				this.compThreshold = value;
			}
		}

		public double CompAngle1
		{
			get
			{
				return this.compAngle1;
			}
			set
			{
				this.compAngle1 = value;
				MVision.MVSetCompAngle(this.compAngle1);
			}
		}

		public double MarkVisionOffsetX
		{
			get
			{
				return this.markVisionOffsetX;
			}
			set
			{
				this.markVisionOffsetX = value;
				MVision.MVSetMarkVisionOffsetXY(this.markVisionOffsetX, this.markVisionOffsetY);
			}
		}

		public double MarkVisionOffsetY
		{
			get
			{
				return this.markVisionOffsetY;
			}
			set
			{
				this.markVisionOffsetY = value;
				MVision.MVSetMarkVisionOffsetXY(this.markVisionOffsetX, this.markVisionOffsetY);
			}
		}

		public int ModeShape
		{
			get
			{
				return this.modeShape;
			}
			set
			{
				this.modeShape = value;
			}
		}

		public bool GoDone
		{
			get
			{
				return this.goDone;
			}
			set
			{
				this.goDone = value;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			base.SuspendLayout();
			base.AutoScaleDimensions = new SizeF(6f, 12f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            base.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			base.Name = "MVisionControl";
			base.Size = new Size(600, 400);
			base.Load += new EventHandler(this.MVisionControl_Load);
			base.Disposed += new EventHandler(this.MVisionControl_Disposed);
			base.ResumeLayout(false);
		}

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "Initialize")]
		public static extern int MVInitialize(int CamNum, int TempSize, double[] TempVal);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "CheckComp")]
		public static extern int MVCheckComp(int Data, int hWnd, int DstWidth, int DstHeight);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "CheckNozzle")]
		public static extern int MVCheckNozzle(int Data, int hWnd, int DstWidth, int DstHeight);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "CheckMark")]
		public static extern int MVCheckMark(int Data, int hWnd, int DstWidth, int DstHeight, int MarkShape);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "GetOffset")]
		public static extern void MVGetOffset(out double w, out double h, out double ow, out double oh, out double a);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "SetThreshold")]
		public static extern void MVSetThreshold(int v);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "SetCompAngle")]
		public static extern void MVSetCompAngle(double a);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "SetMarkVisionOffsetXY")]
		public static extern void MVSetMarkVisionOffsetXY(double x, double y);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "CheckAvPos")]
		public static extern int MVCheckAvPos(int hWnd, int CamNum, int Width, int Height);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "CheckAvColor")]
		public static extern int MVCheckAvColor(int hWnd, int CamNum, int Width, int Height);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetImgTemplateSize(int size);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetImgTemplateSize(out int size);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetImgTemplate(int size, double[] c);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetImgTemplate(double[] c);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetThresLevel(double t);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetThresLevel(out double t);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetDistLevel(double d);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetDistLevel(out double d);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetFilterEn(bool f);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetFilterEn(out bool f);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetShowPosEn(bool sp);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetShowPosEn(out bool sp);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetColorRange(int c, int row, int col);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetColorRange(out int c, int row, int col);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetWhiteRate(double w, int i);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetWhiteRate(out int w, int i);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetLengthTolerance(double l);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetLengthTolerance(out double l);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetAngleTolerance(double a);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void GetAngleTolerance(out double a);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern void SetColorSelect(int c);

		[DllImport("MVision.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "Release")]
		public static extern void MVRelease();

		public MVision()
		{
			this.handle = base.Handle.ToInt32();
			this.InitializeComponent();
		}

		private void MVisionControl_Load(object sender, EventArgs e)
		{
		}

		public bool Initialize(string key1, string key2)
		{
			if (this.Camera1 != null)
			{
				this.Camera1.Dispose();
				this.Camera1 = null;
			}
			if (this.Camera2 != null)
			{
				this.Camera2.Dispose();
				this.Camera2 = null;
			}
			this.cKey1 = key1;
			this.Camera1 = new Camera();
			if (!this.Camera1.Initialize(this.cKey1, 960, 720, -8, 4770))
			{
				this.Camera1 = null;
			}
			this.cKey2 = key2;
			this.Camera2 = new Camera();
			if (!this.Camera2.Initialize(this.cKey2, 960, 720, -6, 2770))
			{
				this.Camera2 = null;
			}
			if (this.CheckCompThread == null)
			{
				this.CheckCompThread = new Thread(new ThreadStart(this.Check));
				this.CheckCompThread.Start();
				while (!this.CheckCompThread.IsAlive)
				{
				}
			}
			else if (!this.CheckCompThread.IsAlive)
			{
				this.CheckCompThread.Start();
				while (!this.CheckCompThread.IsAlive)
				{
				}
			}
			return MVision.MVInitialize(this.camNum, this.tempDia, this.cirArray) == 0;
		}

		public void CheckCompRun()
		{
			this.CheckVision = CheckVisionType.ComponentVision;
			if (this.goDone)
			{
				this.goDone = false;
				MVision.sem.Release();
			}
		}

		public void CheckNozzleRun()
		{
			this.CheckVision = CheckVisionType.NozzleVision;
			if (this.goDone)
			{
				this.goDone = false;
				MVision.sem.Release();
			}
		}

		public void CheckMarkRun()
		{
			this.CheckVision = CheckVisionType.PCBVision;
			if (this.goDone)
			{
				this.goDone = false;
				MVision.sem.Release();
			}
		}

		private void Check()
		{
			while (!this._CheckCompStop)
			{
				MVision.sem.WaitOne();
				if (this._CheckCompStop)
				{
					break;
				}
				TimeSpan timeSpan = new TimeSpan(DateTime.Now.Ticks);
				Marshal.FreeCoTaskMem(this.m_ip);
				this.m_ip = IntPtr.Zero;
				switch (this.CheckVision)
				{
				case CheckVisionType.NozzleVision:
					if (this.Camera1 != null)
					{
						this.m_ip = this.Camera1.QueryFrame();
						if (this.m_ip == IntPtr.Zero)
						{
							this.Camera1 = null;
						}
					}
					MVision.MVSetThreshold(this.compThreshold);
					this.compSum = MVision.MVCheckNozzle((int)this.m_ip, this.handle, base.Width, base.Height);
					MVision.MVGetOffset(out this.compWidth, out this.compHeight, out this.compOffsetW, out this.compOffsetH, out this.compOoffsetA);
					break;
				case CheckVisionType.ComponentVision:
					if (this.Camera1 != null)
					{
						this.m_ip = this.Camera1.QueryFrame();
						if (this.m_ip == IntPtr.Zero)
						{
							this.Camera1 = null;
						}
					}
					MVision.MVSetThreshold(this.compThreshold);
					this.compSum = MVision.MVCheckComp((int)this.m_ip, this.handle, base.Width, base.Height);
					MVision.MVGetOffset(out this.compWidth, out this.compHeight, out this.compOffsetW, out this.compOffsetH, out this.compOoffsetA);
					break;
				case CheckVisionType.PCBVision:
					if (this.Camera2 != null)
					{
						this.m_ip = this.Camera2.QueryFrame();
						if (this.m_ip == IntPtr.Zero)
						{
							this.Camera1 = null;
						}
					}
					MVision.MVSetThreshold(this.compThreshold);
					this.compSum = MVision.MVCheckMark((int)this.m_ip, this.handle, base.Width, base.Height, this.ModeShape);
					MVision.MVGetOffset(out this.compWidth, out this.compHeight, out this.compOffsetW, out this.compOffsetH, out this.compOoffsetA);
					break;
				}
				TimeSpan ts = new TimeSpan(DateTime.Now.Ticks);
				timeSpan.Subtract(ts).Duration();
				this.HeartBeat = !this.HeartBeat;
				this.goDone = true;
			}
			this._CheckCompStop = false;
		}

		public void CheckCompStop()
		{
			if (this.CheckCompThread != null && this.CheckCompThread.IsAlive)
			{
				this._CheckCompStop = true;
				MVision.sem.Release();
				this.CheckCompThread.Join();
				this.CheckCompThread.Abort();
				this.CheckCompThread = null;
			}
		}

		private void MVisionControl_Disposed(object sender, EventArgs e)
		{
			this.CheckCompStop();
		}

		public void Release()
		{
			this.CheckCompStop();
			MVision.MVRelease();
			if (this.Camera1 != null)
			{
				this.Camera1.Dispose();
				this.Camera1 = null;
			}
			if (this.Camera2 != null)
			{
				this.Camera2.Dispose();
				this.Camera2 = null;
			}
		}
	}
}
