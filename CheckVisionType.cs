using System;

namespace MVisionControl
{
	public enum CheckVisionType
	{
		NozzleVision,
		ComponentVision,
		PCBVision
	}
}
