using DirectShowLib;
using Microsoft.Win32;
using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace MVisionControl
{
	public class Camera : ISampleGrabberCB, IDisposable
	{
		private IFilterGraph2 m_FilterGraph;

		private IAMVideoControl m_VidControl;

		private IPin m_pinStill;

		private ManualResetEvent m_PictureReady;

		private bool m_WantOne;

		private int m_videoWidth;

		private int m_videoHeight;

		private int m_stride;

		private IntPtr m_ipBuffer = IntPtr.Zero;

		private int exposure;

		private IBaseFilter capFilter;

		public int Width
		{
			get
			{
				return this.m_videoWidth;
			}
		}

		public int Height
		{
			get
			{
				return this.m_videoHeight;
			}
		}

		public int Stride
		{
			get
			{
				return this.m_stride;
			}
		}

		public int Exposure
		{
			get
			{
				return this.exposure;
			}
			set
			{
				if (this.exposure != value)
				{
					this.exposure = value;
					((IAMCameraControl)this.capFilter).Set(CameraControlProperty.Exposure, this.exposure, CameraControlFlags.Manual);
				}
			}
		}

		[DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(IntPtr Destination, IntPtr Source, [MarshalAs(UnmanagedType.U4)] int Length);

		public void Dispose()
		{
			this.CloseInterfaces();
			if (this.m_PictureReady != null)
			{
				this.m_PictureReady.Close();
			}
		}

		~Camera()
		{
			this.Dispose();
		}

		public bool Initialize(string strDeviceNum, int iWidth, int iHeight, short iExposure, int iWhiteBalance)
		{
			int num = -1;
			string text = "";
			RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum\\USB\\VID_045E&PID_0294");
			if (registryKey == null)
			{
				return false;
			}
			string[] subKeyNames = registryKey.GetSubKeyNames();
			string[] array = subKeyNames;
			for (int i = 0; i < array.Length; i++)
			{
				string text2 = array[i];
				int num2 = text2.Length;
				uint num3 = 0u;
				for (int j = 0; j < 8; j++)
				{
					if (--num2 >= 0)
					{
						num3 += (uint)((double)(text2[num2] - '0') * Math.Pow(10.0, (double)j));
					}
				}
				if (strDeviceNum == num3.ToString("D9"))
				{
					text = (string)registryKey.OpenSubKey(text2).GetValue("ParentIdPrefix");
					break;
				}
			}
			if (text == "" || text == null)
			{
				return false;
			}
			DsDevice[] devicesOfCat = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
			for (int k = 0; k < devicesOfCat.Length; k++)
			{
				if (devicesOfCat[k].DevicePath != null && devicesOfCat[k].DevicePath.Contains(text))
				{
					num = k;
					break;
				}
			}
			if (num == -1)
			{
				return false;
			}
			try
			{
				this.m_PictureReady = new ManualResetEvent(false);
				this.SetupGraph(devicesOfCat[num], iWidth, iHeight, iExposure, iWhiteBalance);
			}
			catch
			{
				return false;
			}
			return true;
		}

		public IntPtr QueryFrame()
		{
			this.m_PictureReady.Reset();
			this.m_ipBuffer = Marshal.AllocCoTaskMem(Math.Abs(this.m_stride) * this.m_videoHeight);
			try
			{
				this.m_WantOne = true;
				TimeSpan timeSpan = new TimeSpan(DateTime.Now.Ticks);
				if (this.m_VidControl != null)
				{
					int hr = this.m_VidControl.SetMode(this.m_pinStill, VideoControlFlags.Trigger);
					DsError.ThrowExceptionForHR(hr);
				}
				if (!this.m_PictureReady.WaitOne(2000, true))
				{
					throw new Exception("Timeout waiting to get picture");
				}
				TimeSpan ts = new TimeSpan(DateTime.Now.Ticks);
				timeSpan.Subtract(ts).Duration();
			}
			catch
			{
				Marshal.FreeCoTaskMem(this.m_ipBuffer);
				this.m_ipBuffer = IntPtr.Zero;
			}
			return this.m_ipBuffer;
		}

		private void SetupGraph(DsDevice dev, int iWidth, int iHeight, short iExposure, int iWhiteBalance)
		{
			ISampleGrabber sampleGrabber = null;
			IPin pin = null;
			IPin pin2 = null;
			IPin pin3 = null;
			this.m_FilterGraph = (new FilterGraph() as IFilterGraph2);
			try
			{
				int hr = this.m_FilterGraph.AddSourceFilterForMoniker(dev.Mon, null, dev.Name, out this.capFilter);
				DsError.ThrowExceptionForHR(hr);
				((IAMCameraControl)this.capFilter).Set(CameraControlProperty.Exposure, (int)iExposure, CameraControlFlags.Manual);
				this.exposure = (int)iExposure;
				((IAMVideoProcAmp)this.capFilter).Set(VideoProcAmpProperty.WhiteBalance, iWhiteBalance, VideoProcAmpFlags.Manual);
				if (this.m_pinStill == null)
				{
					IPin pin4 = null;
					IPin pin5 = null;
					this.m_VidControl = null;
					IBaseFilter baseFilter = (IBaseFilter)new SmartTee();
					try
					{
						hr = this.m_FilterGraph.AddFilter(baseFilter, "SmartTee");
						DsError.ThrowExceptionForHR(hr);
						pin4 = DsFindPin.ByCategory(this.capFilter, PinCategory.Capture, 0);
						pin5 = DsFindPin.ByDirection(baseFilter, PinDirection.Input, 0);
						hr = this.m_FilterGraph.Connect(pin4, pin5);
						DsError.ThrowExceptionForHR(hr);
						this.m_pinStill = DsFindPin.ByName(baseFilter, "Preview");
						if (iHeight + iWidth > 0)
						{
							this.SetConfigParms(pin4, iWidth, iHeight);
						}
						goto IL_165;
					}
					finally
					{
						if (pin4 != null)
						{
							Marshal.ReleaseComObject(pin4);
						}
						if (pin4 != pin5)
						{
							Marshal.ReleaseComObject(pin5);
						}
						if (pin4 != baseFilter)
						{
							Marshal.ReleaseComObject(baseFilter);
						}
					}
				}
				this.m_VidControl = (this.capFilter as IAMVideoControl);
				pin = DsFindPin.ByCategory(this.capFilter, PinCategory.Capture, 0);
				if (iHeight + iWidth > 0)
				{
					this.SetConfigParms(this.m_pinStill, iWidth, iHeight);
				}
				IL_165:
				sampleGrabber = (new SampleGrabber() as ISampleGrabber);
				IBaseFilter baseFilter2 = sampleGrabber as IBaseFilter;
				this.ConfigureSampleGrabber(sampleGrabber);
				pin2 = DsFindPin.ByDirection(baseFilter2, PinDirection.Input, 0);
				IBaseFilter baseFilter3 = new NullRenderer() as IBaseFilter;
				hr = this.m_FilterGraph.AddFilter(baseFilter3, "Renderer");
				DsError.ThrowExceptionForHR(hr);
				pin3 = DsFindPin.ByDirection(baseFilter3, PinDirection.Input, 0);
				hr = this.m_FilterGraph.AddFilter(baseFilter2, "Ds.NET Grabber");
				DsError.ThrowExceptionForHR(hr);
				if (this.m_VidControl == null)
				{
					hr = this.m_FilterGraph.Connect(this.m_pinStill, pin2);
					DsError.ThrowExceptionForHR(hr);
				}
				else
				{
					hr = this.m_FilterGraph.Connect(pin, pin3);
					DsError.ThrowExceptionForHR(hr);
					hr = this.m_FilterGraph.Connect(this.m_pinStill, pin2);
					DsError.ThrowExceptionForHR(hr);
				}
				this.SaveSizeInfo(sampleGrabber);
				IMediaControl mediaControl = this.m_FilterGraph as IMediaControl;
				hr = mediaControl.Run();
				DsError.ThrowExceptionForHR(hr);
			}
			finally
			{
				if (sampleGrabber != null)
				{
					Marshal.ReleaseComObject(sampleGrabber);
					sampleGrabber = null;
				}
				if (pin != null)
				{
					Marshal.ReleaseComObject(pin);
					pin = null;
				}
				if (pin3 != null)
				{
					Marshal.ReleaseComObject(pin3);
					pin3 = null;
				}
				if (pin2 != null)
				{
					Marshal.ReleaseComObject(pin2);
					pin2 = null;
				}
			}
		}

		private void SaveSizeInfo(ISampleGrabber sampGrabber)
		{
			AMMediaType aMMediaType = new AMMediaType();
			int connectedMediaType = sampGrabber.GetConnectedMediaType(aMMediaType);
			DsError.ThrowExceptionForHR(connectedMediaType);
			if (aMMediaType.formatType != FormatType.VideoInfo || aMMediaType.formatPtr == IntPtr.Zero)
			{
				throw new NotSupportedException("Unknown Grabber Media Format");
			}
			VideoInfoHeader videoInfoHeader = (VideoInfoHeader)Marshal.PtrToStructure(aMMediaType.formatPtr, typeof(VideoInfoHeader));
			this.m_videoWidth = videoInfoHeader.BmiHeader.Width;
			this.m_videoHeight = videoInfoHeader.BmiHeader.Height;
			this.m_stride = this.m_videoWidth * (int)(videoInfoHeader.BmiHeader.BitCount / 8);
			DsUtils.FreeAMMediaType(aMMediaType);
		}

		private void ConfigureSampleGrabber(ISampleGrabber sampGrabber)
		{
			AMMediaType aMMediaType = new AMMediaType();
			aMMediaType.majorType = MediaType.Video;
			aMMediaType.subType = MediaSubType.RGB24;
			aMMediaType.formatType = FormatType.VideoInfo;
			int hr = sampGrabber.SetMediaType(aMMediaType);
			DsError.ThrowExceptionForHR(hr);
			DsUtils.FreeAMMediaType(aMMediaType);
			hr = sampGrabber.SetCallback(this, 1);
			DsError.ThrowExceptionForHR(hr);
		}

		private void SetConfigParms(IPin pStill, int iWidth, int iHeight)
		{
			IAMStreamConfig iAMStreamConfig = pStill as IAMStreamConfig;
			AMMediaType aMMediaType;
			int hr = iAMStreamConfig.GetFormat(out aMMediaType);
			DsError.ThrowExceptionForHR(hr);
			try
			{
				VideoInfoHeader videoInfoHeader = new VideoInfoHeader();
				Marshal.PtrToStructure(aMMediaType.formatPtr, videoInfoHeader);
				if (iWidth > 0)
				{
					videoInfoHeader.BmiHeader.Width = iWidth;
				}
				if (iHeight > 0)
				{
					videoInfoHeader.BmiHeader.Height = iHeight;
				}
				Marshal.StructureToPtr(videoInfoHeader, aMMediaType.formatPtr, false);
				hr = iAMStreamConfig.SetFormat(aMMediaType);
				DsError.ThrowExceptionForHR(hr);
			}
			finally
			{
				DsUtils.FreeAMMediaType(aMMediaType);
				aMMediaType = null;
			}
		}

		private void CloseInterfaces()
		{
			try
			{
				if (this.m_FilterGraph != null)
				{
					IMediaControl mediaControl = this.m_FilterGraph as IMediaControl;
					mediaControl.Stop();
				}
			}
			catch (Exception)
			{
			}
			if (this.m_FilterGraph != null)
			{
				Marshal.ReleaseComObject(this.m_FilterGraph);
				this.m_FilterGraph = null;
			}
			if (this.m_VidControl != null)
			{
				Marshal.ReleaseComObject(this.m_VidControl);
				this.m_VidControl = null;
			}
			if (this.m_pinStill != null)
			{
				Marshal.ReleaseComObject(this.m_pinStill);
				this.m_pinStill = null;
			}
		}

		int ISampleGrabberCB.SampleCB(double SampleTime, IMediaSample pSample)
		{
			Marshal.ReleaseComObject(pSample);
			return 0;
		}

		int ISampleGrabberCB.BufferCB(double SampleTime, IntPtr pBuffer, int BufferLen)
		{
			if (this.m_WantOne)
			{
				this.m_WantOne = false;
				if (this.m_ipBuffer != IntPtr.Zero)
				{
					Camera.CopyMemory(this.m_ipBuffer, pBuffer, BufferLen);
				}
				this.m_PictureReady.Set();
			}
			return 0;
		}
	}
}
